package org.genfork.auth.config;

import org.genfork.tools.config.abstraction.ConfigPropertiesLoader;
import org.genfork.tools.config.annotation.ConfigClass;
import org.genfork.tools.config.annotation.ConfigField;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@ConfigClass(fileName = "Server")
public class ServerConfig extends ConfigPropertiesLoader {
	@ConfigField(name = "PlayerServerHost", value = "127.0.0.1")
	public static String PLAYER_SERVER_HOST;

	@ConfigField(name = "PlayerServerPort", value = "2106")
	public static int PLAYER_SERVER_PORT;

	@ConfigField(name = "AuthServerHost", value = "127.0.0.1")
	public static String AUTH_SERVER_HOST;

	@ConfigField(name = "AuthServerPort", value = "9014")
	public static int AUTH_SERVER_PORT;

	@ConfigField(name = "DiscoveryServerHost", value = "127.0.0.1")
	public static String DISCOVERY_SERVER_HOST;

	@ConfigField(name = "DiscoveryServerPort", value = "5000")
	public static int DISCOVERY_SERVER_PORT;

	@ConfigField(name = "DiscoveryRootClientPort", value = "9014")
	public static int DISCOVERY_ROOT_CLIENT_PORT;

	@ConfigField(name = "ServerKey", value = "{bcrypt}$2a$10$QLekRpQ7ib/LoQzaok7wBuNbLAklR5mQWMop8oH8k4PBmB8yCXHIO")
	public static String SERVER_PRIVATE_KEY;
}
