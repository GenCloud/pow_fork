package org.genfork.auth.config;

import org.genfork.tools.config.generator.AbstractConfigGenerator;
import org.genfork.tools.loader.AppLoader;
import org.genfork.tools.loader.AppLoader.ApplicationMode;

public final class ConfigGenerator extends AbstractConfigGenerator {
	public static void main(String[] args) {
		AppLoader.MODE = ApplicationMode.DATA;
		new ConfigGenerator();
	}

	@Override
	protected String getPackageName() {
		return ConfigMarker.class.getPackage().getName();
	}
}
