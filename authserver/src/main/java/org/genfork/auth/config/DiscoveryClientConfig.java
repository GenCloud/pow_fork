package org.genfork.auth.config;

import org.genfork.tools.network.DiscoveryClients;
import org.genfork.tools.registry.config.IRegistryConfig;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class DiscoveryClientConfig implements IRegistryConfig {
	@Override
	public String getClientName() {
		return DiscoveryClients.AUTH_NODE;
	}

	@Override
	public String getClientSecretKey() {
		return ServerConfig.SERVER_PRIVATE_KEY;
	}

	@Override
	public String getClientIp() {
		return ServerConfig.AUTH_SERVER_HOST;
	}

	@Override
	public int getClientPort() {
		return ServerConfig.AUTH_SERVER_PORT;
	}

	@Override
	public int getRootClientPort() {
		return ServerConfig.DISCOVERY_ROOT_CLIENT_PORT;
	}
}
