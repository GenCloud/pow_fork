package org.genfork.auth.config;

import org.genfork.tools.config.abstraction.ConfigPropertiesLoader;
import org.genfork.tools.config.annotation.ConfigClass;
import org.genfork.tools.config.annotation.ConfigField;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@ConfigClass(fileName = "Auth")
public class AuthConfig extends ConfigPropertiesLoader {
	@ConfigField(name = "EnableAutoCreationAccounts", value = "true")
	public static boolean ENABLE_AUTO_CREATION_ACCOUNTS;
}
