package org.genfork.auth.network.clients.recv.store;

import org.apache.commons.lang3.StringUtils;
import org.genfork.auth.network.clients.send.player.AuthFailPacket;
import org.genfork.auth.network.clients.send.player.AuthSuccessPacket;
import org.genfork.auth.network.clients.send.player.BlockedAccountPacket;
import org.genfork.auth.network.discovery.DiscoveryServerHandler;
import org.genfork.auth.network.enums.ConnectionState;
import org.genfork.auth.network.server.PlayerClientNetworkController;
import org.genfork.auth.network.server.PlayerHandler;
import org.genfork.tools.common.Rnd;
import org.genfork.tools.network.IncomingPacket;
import org.genfork.tools.network.SessionInfo;

import java.util.UUID;

import static org.genfork.auth.data.store.controller.AuthController.*;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class RequestAccountResultPacket extends IncomingPacket<DiscoveryServerHandler> {
	private String playerClientId;
	private short code;
	private String accountName;
	private String email;
	private String ipAddress;
	private int lastServerId;

	@Override
	public boolean read(DiscoveryServerHandler client) {
		playerClientId = readS();
		code = readC();

		if (code == 0) {
			accountName = readS();
			email = readS();
			ipAddress = readS();
			lastServerId = readD();
		}

		return true;
	}

	@Override
	public void run(DiscoveryServerHandler client) {
		if (StringUtils.isEmpty(playerClientId)) {
			return;
		}

		final PlayerHandler playerHandler = PlayerClientNetworkController.getInstance().getClient(UUID.fromString(playerClientId));

		switch (code) {
			case SERVER_ERROR: {
				playerHandler.close(AuthFailPacket.SYSTEM_ERROR);
				break;
			}
			case ACCOUNT_VALID_CODE: {
				playerHandler.setConnectionState(ConnectionState.AUTHED_LICENCE);
				playerHandler.setAccountName(accountName);
				playerHandler.setLastServerId(lastServerId);
				playerHandler.setLastIpAddress(ipAddress);

				final SessionInfo info = new SessionInfo(Rnd.nextInt(), Rnd.nextInt(), Rnd.nextInt(), Rnd.nextInt());

				playerHandler.setSessionInfo(info);
				playerHandler.sendPacket(new AuthSuccessPacket(info.getAuthKeyFirst(), info.getAuthKeySecond(), info.getPlayKeyFirst(), info.getPlayKeySecond()));
				break;
			}
			case ACCOUNT_NOT_VALID_CODE: {
				playerHandler.close(AuthFailPacket.ACCESS_FAILED);
				break;
			}
			case ACCOUNT_NOT_MATCHES_PASSWORD_CODE:
			case ACCOUNT_DOES_NOT_EXISTS: {
				playerHandler.close(AuthFailPacket.THE_USERNAME_AND_PASSWORD_DO_NOT_MATCH_PLEASE_CHECK_YOUR_ACCOUNT_INFORMATION_AND_TRY_LOGGING_IN_AGAIN);
				break;
			}
			case ACCOUNT_BANNED: {
				playerHandler.close(BlockedAccountPacket.YOUR_ACCOUNT_HAS_BEEN_RESTRICTED_IN_ACCORDANCE_WITH_OUR_TERMS_OF_SERVICE_DUE_TO_YOUR_CONFIRMED_ABUSE_OF_IN_GAME_SYSTEMS_RESULTING_IN_ABNORMAL_GAMEPLAY_FOR_MORE_DETAILS_PLEASE_VISIT_THE_LINEAGE_II_SUPPORT_WEBSITE_HTTPS_SUPPORT_LINEAGE2_COM);
				break;
			}
		}
	}
}
