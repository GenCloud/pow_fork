package org.genfork.auth.network.clients.send.player;

import lombok.RequiredArgsConstructor;
import org.genfork.tools.network.OutgoingPacket;

import static org.genfork.tools.network.opcodes.AuthPlayerBranchOpcodes.AUTH_GAME_GUARD_PACKET;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@RequiredArgsConstructor
public class AuthGameGuardPacket extends OutgoingPacket {
	private final int connectionId;

	@Override
	public boolean write() {
		AUTH_GAME_GUARD_PACKET.writeId(this);

		writeD(connectionId);
		writeD(0x29DD954E); // UNKNOWN GAMEGUARD
		writeD(0x77C39CFC); // UNKNOWN GAMEGUARD
		writeD(0x97ADB620); // UNKNOWN GAMEGUARD
		writeD(0x07BDE0F7); // UNKNOWN GAMEGUARD

		return true;
	}
}
