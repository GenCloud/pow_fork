package org.genfork.auth.network.clients.send.player;

import lombok.RequiredArgsConstructor;
import org.genfork.auth.data.game.GameServer;
import org.genfork.auth.network.server.PlayerHandler;
import org.genfork.tools.network.OutgoingPacket;

import java.util.Collection;

import static org.genfork.tools.network.opcodes.AuthPlayerBranchOpcodes.GAME_SERVER_LIST_PACKET;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@RequiredArgsConstructor
public class GameServerListPacket extends OutgoingPacket {
	private final PlayerHandler handler;
	private final Collection<GameServer> gameServers;

	@Override
	public boolean write() {
		GAME_SERVER_LIST_PACKET.writeId(this);

		writeC(gameServers.size());
		writeC(handler.getLastServerId());

		gameServers.forEach(gameServer -> {
			writeC(gameServer.getId());
			final int[] ipParts = gameServer.getIpParts();
			writeC(ipParts[0]); // IP
			writeC(ipParts[1]); // IP
			writeC(ipParts[2]); // IP
			writeC(ipParts[3]); // IP
			writeD(ipParts[4]); // Port
			writeC(gameServer.getAgeLimit().getAge());
			writeC(0);
			writeH(1); // Now Player Count
			writeH(gameServer.getPlayerLimit());
			writeC(gameServer.getServerState().ordinal());
			writeD(gameServer.getServerTypesMask());
			writeC(0);
		});

		writeH(0xA4);

		writeC(gameServers.size());
		for (GameServer gameServer : gameServers) {
			writeC(gameServer.getId());
			writeC(2); // Character Count
		}

		return true;
	}
}
