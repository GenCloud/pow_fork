package org.genfork.auth.network.clients.send.player;

import lombok.RequiredArgsConstructor;
import org.genfork.tools.network.OutgoingPacket;

import static org.genfork.tools.network.opcodes.AuthPlayerBranchOpcodes.GAME_SERVER_PLAYER_SUCCESS_PACKET;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@RequiredArgsConstructor
public class GameServerPlaySuccessPacket extends OutgoingPacket {
	private final int sessionId;

	@Override
	public boolean write() {
		GAME_SERVER_PLAYER_SUCCESS_PACKET.writeId(this);

		writeD(sessionId);
		writeD(sessionId);
		return true;
	}
}
