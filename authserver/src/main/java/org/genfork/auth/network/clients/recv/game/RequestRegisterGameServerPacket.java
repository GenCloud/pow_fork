package org.genfork.auth.network.clients.recv.game;

import lombok.extern.slf4j.Slf4j;
import org.genfork.auth.data.game.GameServer;
import org.genfork.auth.data.game.GameServerController;
import org.genfork.auth.network.discovery.DiscoveryServerHandler;
import org.genfork.tools.network.IncomingPacket;
import org.genfork.tools.servers.enums.AgeLimit;
import org.genfork.tools.servers.enums.ServerState;
import org.genfork.tools.servers.enums.ServerType;

import java.util.Collections;
import java.util.stream.IntStream;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Slf4j
public class RequestRegisterGameServerPacket extends IncomingPacket<DiscoveryServerHandler> {
	private short serverId;
	private int[] ipParts;
	private AgeLimit ageLimit;
	private int playerLimit;

	private ServerState serverState;
	private ServerType serverType;

	@Override
	public boolean read(DiscoveryServerHandler client) {
		serverId = readC();

		final int size = readD();
		ipParts = new int[size];

		IntStream.range(0, size).forEach(i -> ipParts[i] = readD());

		serverState = ServerState.values()[readC()];
		playerLimit = readD();
		ageLimit = AgeLimit.byAge(readD());
		serverType = ServerType.byMask(readD());

		return true;
	}

	@Override
	public void run(DiscoveryServerHandler client) throws Exception {
		if (GameServerController.getInstance().findClientBy(p -> p.getId() == serverId) == null) {
			final GameServer gameServer = new GameServer(serverId, ipParts, playerLimit, serverState, ageLimit, Collections.singleton(serverType));
			GameServerController.getInstance().addClient(client.getUuid(), gameServer);
			log.info("Registering game server : {}", gameServer);
		}
	}
}
