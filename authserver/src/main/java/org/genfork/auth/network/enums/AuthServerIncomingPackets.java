package org.genfork.auth.network.enums;

import org.genfork.auth.network.clients.recv.SystemPackets;
import org.genfork.auth.network.clients.recv.game.RequestRegisterGameServerPacket;
import org.genfork.auth.network.clients.recv.store.RequestAccountResultPacket;
import org.genfork.auth.network.discovery.DiscoveryServerHandler;
import org.genfork.tools.network.IConnectionState;
import org.genfork.tools.network.IIncomingPackets;
import org.genfork.tools.network.IncomingPacket;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Supplier;

import static org.genfork.tools.network.opcodes.ClientsBranchOpcodes.ACCOUNT_SELECT_RESULT_PACKET;
import static org.genfork.tools.network.opcodes.ClientsBranchOpcodes.REGISTER_GAME_SERVER_PACKET;

public enum AuthServerIncomingPackets implements IIncomingPackets<DiscoveryServerHandler> {
	REQUEST_ACCOUNT_RESULT_PACKET(ACCOUNT_SELECT_RESULT_PACKET.getId1(), RequestAccountResultPacket::new, ConnectionState.CONNECTED),

	REQUEST_REGISTER_GAME_SERVER_PACKET(REGISTER_GAME_SERVER_PACKET.getId1(), RequestRegisterGameServerPacket::new, ConnectionState.REGISTERED),

	DISCOVERY_PACKETS(0x0E, SystemPackets::new, ConnectionState.REGISTERED),
	;
	public static final AuthServerIncomingPackets[] PACKET_ARRAY;

	static {
		final short maxPacketId = (short) Arrays.stream(values()).mapToInt(IIncomingPackets::getPacketId).max().orElse(0);
		PACKET_ARRAY = new AuthServerIncomingPackets[maxPacketId + 1];
		Arrays.stream(values()).forEach(incomingPacket -> PACKET_ARRAY[incomingPacket.getPacketId()] = incomingPacket);
	}

	private short packetId;
	private Supplier<IncomingPacket<DiscoveryServerHandler>> incomingPacketFactory;
	private Set<IConnectionState> connectionStates;

	AuthServerIncomingPackets(int packetId, Supplier<IncomingPacket<DiscoveryServerHandler>> incomingPacketFactory, IConnectionState... connectionStates) {
		// packetId is an unsigned byte
		if (packetId > 0xFF) {
			throw new IllegalArgumentException("packetId must not be bigger than 0xFF");
		}

		this.packetId = (short) packetId;
		this.incomingPacketFactory = incomingPacketFactory != null ? incomingPacketFactory : () -> null;
		this.connectionStates = new HashSet<>(Arrays.asList(connectionStates));
	}

	@Override
	public int getPacketId() {
		return packetId;
	}

	@Override
	public IncomingPacket<DiscoveryServerHandler> newIncomingPacket() {
		return incomingPacketFactory.get();
	}

	@Override
	public Set<IConnectionState> getConnectionStates() {
		return connectionStates;
	}

	@Override
	public IIncomingPackets<DiscoveryServerHandler>[] getArray() {
		return PACKET_ARRAY;
	}
}
