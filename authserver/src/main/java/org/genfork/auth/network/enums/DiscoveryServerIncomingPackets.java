package org.genfork.auth.network.enums;

import org.genfork.auth.network.discovery.DiscoveryServerHandler;
import org.genfork.tools.network.*;
import org.genfork.tools.network.opcodes.ClientsBranchOpcodes;
import org.genfork.tools.registry.network.recv.ReceiveClientDataPacket;
import org.genfork.tools.registry.network.recv.RequestDiscoveryServerHealthCheckPacket;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Supplier;

import static org.genfork.auth.network.enums.ConnectionState.CONNECTED;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public enum DiscoveryServerIncomingPackets implements IIncomingPackets<DiscoveryServerHandler> {
	REQUEST_DISCOVERY_CLIENT_HEALTH_CHECK(ClientsBranchOpcodes.DISCOVERY_CLIENT_HEALTH_CHECK.getId1(), () -> new RequestDiscoveryServerHealthCheckPacket<>(DiscoveryClients.AUTH_NODE), CONNECTED),

	REQUEST_RECEIVE_CLIENT_DATA_PACKET(ClientsBranchOpcodes.DISCOVERY_RETRANSMIT_DATA.getId1(), new ReceiveClientDataPacket(AuthServerIncomingPackets.PACKET_ARRAY), CONNECTED),

	;
	public static final DiscoveryServerIncomingPackets[] PACKET_ARRAY;

	static {
		final short maxPacketId = (short) Arrays.stream(values()).mapToInt(IIncomingPackets::getPacketId).max().orElse(0);
		PACKET_ARRAY = new DiscoveryServerIncomingPackets[maxPacketId + 1];
		for (DiscoveryServerIncomingPackets incomingPacket : values()) {
			PACKET_ARRAY[incomingPacket.getPacketId()] = incomingPacket;
		}
	}

	private short packetId;
	private Supplier<IncomingPacket<? extends ChannelInboundHandler<?>>> incomingPacketFactory;
	private Set<IConnectionState> connectionStates;

	DiscoveryServerIncomingPackets(int packetId, IncomingPacket<? extends ChannelInboundHandler<?>> incomingPacketFactory, IConnectionState... connectionStates) {
		this(packetId, () -> incomingPacketFactory, connectionStates);
	}

	DiscoveryServerIncomingPackets(int packetId, Supplier<IncomingPacket<? extends ChannelInboundHandler<?>>> incomingPacketFactory, IConnectionState... connectionStates) {
		// packetId is an unsigned byte
		if (packetId > 0xFF) {
			throw new IllegalArgumentException("packetId must not be bigger than 0xFF");
		}

		this.packetId = (short) packetId;
		this.incomingPacketFactory = incomingPacketFactory != null ? incomingPacketFactory : () -> null;
		this.connectionStates = new HashSet<>(Arrays.asList(connectionStates));
	}

	@Override
	public int getPacketId() {
		return packetId;
	}

	@Override
	public IncomingPacket<DiscoveryServerHandler> newIncomingPacket() {
		return (IncomingPacket<DiscoveryServerHandler>) incomingPacketFactory.get();
	}

	@Override
	public Set<IConnectionState> getConnectionStates() {
		return connectionStates;
	}

	@Override
	public IIncomingPackets<DiscoveryServerHandler>[] getArray() {
		return PACKET_ARRAY;
	}
}
