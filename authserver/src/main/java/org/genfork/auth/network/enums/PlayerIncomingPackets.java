package org.genfork.auth.network.enums;

import org.genfork.auth.network.clients.recv.player.RequestAuthGameGuardPacket;
import org.genfork.auth.network.clients.recv.player.RequestAuthPacket;
import org.genfork.auth.network.clients.recv.player.RequestGameServerListPacket;
import org.genfork.auth.network.clients.recv.player.RequestGameServerLoginPacket;
import org.genfork.auth.network.server.PlayerHandler;
import org.genfork.tools.network.IConnectionState;
import org.genfork.tools.network.IIncomingPackets;
import org.genfork.tools.network.IncomingPacket;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Supplier;

public enum PlayerIncomingPackets implements IIncomingPackets<PlayerHandler> {
	REQUEST_AUTH_LOGIN(0x00, RequestAuthPacket::new, ConnectionState.AUTHED_GG),
	REQUEST_SERVER_LOGIN(0x02, RequestGameServerLoginPacket::new, ConnectionState.AUTHED_SERVER_LIST),
	REQUEST_SERVER_LIST(0x05, RequestGameServerListPacket::new, ConnectionState.AUTHED_LICENCE),
	RESPONSE_AUTH_GAMEGUARD(0x07, RequestAuthGameGuardPacket::new, ConnectionState.CONNECTED);

	public static final PlayerIncomingPackets[] PACKET_ARRAY;

	static {
		final short maxPacketId = (short) Arrays.stream(values()).mapToInt(IIncomingPackets::getPacketId).max().orElse(0);
		PACKET_ARRAY = new PlayerIncomingPackets[maxPacketId + 1];
		for (PlayerIncomingPackets incomingPacket : values()) {
			PACKET_ARRAY[incomingPacket.getPacketId()] = incomingPacket;
		}
	}

	private short packetId;
	private Supplier<IncomingPacket<PlayerHandler>> incomingPacketFactory;
	private Set<IConnectionState> connectionStates;

	PlayerIncomingPackets(int packetId, Supplier<IncomingPacket<PlayerHandler>> incomingPacketFactory, IConnectionState... connectionStates) {
		// packetId is an unsigned byte
		if (packetId > 0xFF) {
			throw new IllegalArgumentException("packetId must not be bigger than 0xFF");
		}

		this.packetId = (short) packetId;
		this.incomingPacketFactory = incomingPacketFactory != null ? incomingPacketFactory : () -> null;
		this.connectionStates = new HashSet<>(Arrays.asList(connectionStates));
	}

	@Override
	public int getPacketId() {
		return packetId;
	}

	@Override
	public IncomingPacket<PlayerHandler> newIncomingPacket() {
		return incomingPacketFactory.get();
	}

	@Override
	public Set<IConnectionState> getConnectionStates() {
		return connectionStates;
	}

	@Override
	public IIncomingPackets<PlayerHandler>[] getArray() {
		return PACKET_ARRAY;
	}
}
