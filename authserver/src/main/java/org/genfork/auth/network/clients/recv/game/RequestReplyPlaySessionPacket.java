package org.genfork.auth.network.clients.recv.game;

import org.genfork.auth.network.clients.send.player.AuthFailPacket;
import org.genfork.auth.network.clients.send.player.GameServerPlaySuccessPacket;
import org.genfork.auth.network.discovery.DiscoveryServerHandler;
import org.genfork.auth.network.server.PlayerClientNetworkController;
import org.genfork.auth.network.server.PlayerHandler;
import org.genfork.tools.network.IncomingPacket;

import java.util.UUID;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class RequestReplyPlaySessionPacket extends IncomingPacket<DiscoveryServerHandler> {
	private static final short SUCCESS = 1;
	private String playerClientId;
	private int sessionId;
	private short code;

	@Override
	public boolean read(DiscoveryServerHandler client) {
		playerClientId = readS();
		sessionId = readD();
		code = readC();
		return true;
	}

	@Override
	public void run(DiscoveryServerHandler client) throws Exception {
		final PlayerHandler playerHandler
				= PlayerClientNetworkController.getInstance().getClient(UUID.fromString(playerClientId));
		if (code == SUCCESS) {
			playerHandler.close(new GameServerPlaySuccessPacket(sessionId));
		} else {
			playerHandler.sendPacket(new AuthFailPacket(0x07));
			playerHandler.closeNow();
		}
	}
}
