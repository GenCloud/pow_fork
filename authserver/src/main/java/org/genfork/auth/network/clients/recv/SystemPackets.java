package org.genfork.auth.network.clients.recv;

import lombok.extern.slf4j.Slf4j;
import org.genfork.auth.network.discovery.DiscoveryServerHandler;
import org.genfork.auth.network.enums.SystemIncomingPackets;
import org.genfork.tools.network.IncomingPacket;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Slf4j
public class SystemPackets extends IncomingPacket<DiscoveryServerHandler> {
	@Override
	public boolean read(DiscoveryServerHandler client) {
		final int opcode = readH() & 0xFFFF;

		final SystemIncomingPackets[] packets = SystemIncomingPackets.PACKET_ARRAY;

		if (opcode >= packets.length) {
			log.warn("Unknown packet: {}", Integer.toHexString(opcode));
			return false;
		}

		final SystemIncomingPackets incomingPacket = packets[opcode];
		if (incomingPacket == null) {
			log.warn("Unknown packet: {}", Integer.toHexString(opcode));
			return false;
		}

		final IncomingPacket<DiscoveryServerHandler> packet = incomingPacket.newIncomingPacket();

		if (packet != null) {
			packet.setByteBuf(getByteBuf());

			packet.setFromClientName(getFromClientName());
			packet.setToClientName(getToClientName());
			packet.setSessionInfo(getSessionInfo());

			if (!packet.read(client)) {
				log.error("Error read data packet [{}]", packet);
			} else {
				try {
					packet.run(client);
				} catch (Exception e) {
					log.error("Error run data packet [{}]: {}", packet, e.getLocalizedMessage());
				}
			}
		}
		return true;
	}

	@Override
	public void run(DiscoveryServerHandler client) throws Exception {

	}
}
