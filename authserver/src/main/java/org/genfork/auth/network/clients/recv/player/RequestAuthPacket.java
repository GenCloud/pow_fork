package org.genfork.auth.network.clients.recv.player;

import org.genfork.auth.data.store.controller.AuthController;
import org.genfork.auth.network.clients.send.player.AuthFailPacket;
import org.genfork.auth.network.enums.ConnectionState;
import org.genfork.auth.network.server.PlayerHandler;

import javax.crypto.Cipher;
import java.security.GeneralSecurityException;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class RequestAuthPacket extends AbstractPlayerIncomingPacket {
	private static final int OLD_AUTH_PACKET_LENGTH = 148;
	private static final int NEW_AUTH_PACKET_LENGTH = 276;

	private byte[] raw;
	private byte[] raw2;
	private int connectionId;
	private byte[] gameGuard;

	private boolean newAuth;

	@Override
	public boolean read(PlayerHandler client) {
		final int readableBytes = getReadableBytes();
		if (readableBytes >= NEW_AUTH_PACKET_LENGTH) {
			raw = readB(128);
			raw2 = readB(128);
			connectionId = readD();
			gameGuard = readB(16);
			newAuth = true;
			return true;
		} else if (readableBytes >= OLD_AUTH_PACKET_LENGTH) {
			raw = readB(128);
			connectionId = readD();
			gameGuard = readB(16);
			newAuth = false;
			return true;
		}
		return false;
	}

	@Override
	public void run(PlayerHandler client) throws Exception {
		client.setConnectionState(ConnectionState.AUTHING);

		if (connectionId != client.getConnectionId()) {
			client.close(AuthFailPacket.ACCESS_FAILED_PLEASE_TRY_AGAIN_LATER);
			return;
		}

		if (!client.checkGameGuard(gameGuard)) {
			client.close(AuthFailPacket.ACCESS_FAILED_PLEASE_TRY_AGAIN_LATER);
			return;
		}

		final byte[] decrypted = new byte[newAuth ? 256 : 128];
		try {
			final Cipher rsaCipher = Cipher.getInstance("RSA/ECB/NoPadding");
			rsaCipher.init(Cipher.DECRYPT_MODE, client.getScrambledRSAKeyPair().getPrivateKey());
			rsaCipher.doFinal(raw, 0, 128, decrypted, 0);
			if (newAuth) {
				rsaCipher.doFinal(raw2, 0, 128, decrypted, 128);
			}
		} catch (GeneralSecurityException e) {
			client.close(AuthFailPacket.ACCESS_FAILED_PLEASE_TRY_AGAIN_LATER);
			return;
		}

		final String accountName;
		final String password;
		final int ncotp;

		if (newAuth) {
			accountName = new String(decrypted, 0x4E, 50).trim() + new String(decrypted, 0xCE, 14).trim();
			password = new String(decrypted, 0xDC, 16).trim();
			ncotp = (decrypted[0xFC] & 0xFF) | ((decrypted[0xFD] & 0xFF) << 8) | ((decrypted[0xFE] & 0xFF) << 16) | ((decrypted[0xFF] & 0xFF) << 24);
		} else {
			accountName = new String(decrypted, 0x5E, 14).trim();
			password = new String(decrypted, 0x6C, 16).trim();
			ncotp = (decrypted[0x7C] & 0xFF) | ((decrypted[0x7D] & 0xFF) << 8) | ((decrypted[0x7E] & 0xFF) << 16) | ((decrypted[0x7F] & 0xFF) << 24);
		}

//		TODO: ncotp
		AuthController.getInstance().tryPlayerAuth(client, accountName, password);
	}
}
