package org.genfork.auth.network.clients.send.player;

import lombok.RequiredArgsConstructor;
import org.genfork.tools.network.OutgoingPacket;
import org.genfork.tools.servers.enums.PlayFailReason;

import static org.genfork.tools.network.opcodes.AuthPlayerBranchOpcodes.GAME_SERVER_PLAYER_FAIL_PACKET;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@RequiredArgsConstructor
public class GameServerPlayFailPacket extends OutgoingPacket {
	private final PlayFailReason playFailReason;

	@Override
	public boolean write() {
		GAME_SERVER_PLAYER_FAIL_PACKET.writeId(this);

		writeC(playFailReason.getCode());
		return true;
	}
}
