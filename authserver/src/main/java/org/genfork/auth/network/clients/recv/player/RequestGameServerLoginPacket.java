package org.genfork.auth.network.clients.recv.player;

import org.genfork.auth.data.store.controller.AuthController;
import org.genfork.auth.network.server.PlayerHandler;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class RequestGameServerLoginPacket extends AbstractPlayerIncomingPacket {
	private int authKeyFirst, authKeySecond;
	private int serverId;

	@Override
	public boolean read(PlayerHandler client) {
		authKeyFirst = readD();
		authKeySecond = readD(); // second play key
		serverId = readD();
		return true;
	}

	@Override
	public void run(PlayerHandler client) throws Exception {
		AuthController.getInstance().tryPlayerPlayGame(client, serverId, authKeyFirst, authKeySecond);
	}
}
