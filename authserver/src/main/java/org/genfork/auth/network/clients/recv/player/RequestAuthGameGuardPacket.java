package org.genfork.auth.network.clients.recv.player;

import org.genfork.auth.network.clients.send.player.AuthFailPacket;
import org.genfork.auth.network.clients.send.player.AuthGameGuardPacket;
import org.genfork.auth.network.enums.ConnectionState;
import org.genfork.auth.network.server.PlayerHandler;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class RequestAuthGameGuardPacket extends AbstractPlayerIncomingPacket {
	private int connectionId;
	private byte[] data;

	@Override
	public boolean read(PlayerHandler client) {
		connectionId = readD();
		data = readB(16);
		return true;
	}

	@Override
	public void run(PlayerHandler client) throws Exception {
		if (connectionId == client.getConnectionId()) {
			client.setGameGuard(data);
			client.setConnectionState(ConnectionState.AUTHED_GG);
			client.sendPacket(new AuthGameGuardPacket(connectionId));
		} else {
			client.close(AuthFailPacket.ACCESS_FAILED);
		}
	}
}
