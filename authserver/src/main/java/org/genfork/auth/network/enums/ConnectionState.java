package org.genfork.auth.network.enums;

import org.genfork.tools.network.IConnectionState;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public enum ConnectionState implements IConnectionState {
	CONNECTED,
	REGISTERED,
	CLOSED,
	AUTHED_GG,
	AUTHED_LICENCE,
	AUTHED_SERVER_LIST,
	AUTHING
}
