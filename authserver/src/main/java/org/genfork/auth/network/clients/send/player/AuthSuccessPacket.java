package org.genfork.auth.network.clients.send.player;

import lombok.RequiredArgsConstructor;
import org.genfork.tools.network.OutgoingPacket;

import static org.genfork.tools.network.opcodes.AuthPlayerBranchOpcodes.AUTH_SUCCESS_PACKET;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@RequiredArgsConstructor
public class AuthSuccessPacket extends OutgoingPacket {
	private final int authKeyFirst;
	private final int authKeySecond;
	private final int playKeyFirst;
	private final int playKeySecond;

	@Override
	public boolean write() {
		AUTH_SUCCESS_PACKET.writeId(this);

		writeD(authKeyFirst);
		writeD(authKeySecond);
		writeD(playKeyFirst);
		writeD(playKeySecond);
		writeD(0x000003ea);
		writeD(0x00);
		writeD(0x00);
		writeD(0x00);
		writeB(new byte[16]);

		return true;
	}
}
