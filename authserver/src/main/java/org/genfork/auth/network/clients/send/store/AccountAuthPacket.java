package org.genfork.auth.network.clients.send.store;

import org.genfork.tools.network.OutgoingPacket;

import java.util.UUID;

import static org.genfork.auth.config.AuthConfig.ENABLE_AUTO_CREATION_ACCOUNTS;
import static org.genfork.tools.network.opcodes.ClientsBranchOpcodes.ACCOUNT_AUTH_PACKET;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class AccountAuthPacket extends OutgoingPacket {
	private final UUID playerClientId;
	private final String ipAddress;
	private final String accountName;
	private final String password;

	public AccountAuthPacket(UUID playerClientId, String ipAddress, String accountName, String password) {
		this.playerClientId = playerClientId;
		this.ipAddress = ipAddress;
		this.accountName = accountName;
		this.password = password;
	}

	@Override
	public boolean write() {
		ACCOUNT_AUTH_PACKET.writeId(this);

		writeS(playerClientId.toString());
		writeC(ENABLE_AUTO_CREATION_ACCOUNTS ? 1 : 0);
		writeS(accountName);
		writeS(password);
		writeS(ipAddress);

		return true;
	}
}
