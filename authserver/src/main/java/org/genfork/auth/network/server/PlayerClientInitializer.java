package org.genfork.auth.network.server;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.MessageToMessageEncoder;
import org.genfork.auth.data.player.controller.SecretKeyController;
import org.genfork.auth.data.player.crypt.PlayerCrypt;
import org.genfork.auth.network.enums.PlayerIncomingPackets;
import org.genfork.tools.network.codecs.CryptCodec;
import org.genfork.tools.network.codecs.PacketDecoder;
import org.genfork.tools.network.codecs.PacketEncoder;

import javax.crypto.SecretKey;
import java.nio.ByteOrder;
import java.util.List;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class PlayerClientInitializer extends ChannelInitializer<SocketChannel> {
	private static final PacketEncoder PACKET_ENCODER = new PacketEncoder(32768 - 2);

	@Override
	protected void initChannel(SocketChannel ch) {
		final SecretKey secretKey = SecretKeyController.getInstance().getRandomSecretKey();
		final PlayerHandler playerHandler = new PlayerHandler(secretKey);
		ch.pipeline().addLast("length-decoder", new LengthFieldBasedFrameDecoder(ByteOrder.LITTLE_ENDIAN, 32768 - 2, 0, 2, -2, 2, false));
		ch.pipeline().addLast("length-encoder", new MessageToMessageEncoder<ByteBuf>() {
			@Override
			protected void encode(ChannelHandlerContext ctx, ByteBuf msg, List<Object> out) throws Exception {
				final ByteBuf buf = ctx.alloc().buffer(2);
				final short length = (short) (msg.readableBytes() + 2);
				buf.writeShortLE(length);
				out.add(buf);
				out.add(msg.retain());
			}
		});

		ch.pipeline().addLast("crypt-codec", new CryptCodec(new PlayerCrypt(secretKey)));
//		ch.pipeline().addLast(new LoggingHandler(LogLevel.INFO));
		ch.pipeline().addLast("packet-decoder", new PacketDecoder<>(PlayerIncomingPackets.PACKET_ARRAY, playerHandler));
		ch.pipeline().addLast("packet-encoder", PACKET_ENCODER);
		ch.pipeline().addLast(playerHandler);
	}
}
