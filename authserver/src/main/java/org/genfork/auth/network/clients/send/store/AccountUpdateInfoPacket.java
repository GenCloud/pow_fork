package org.genfork.auth.network.clients.send.store;

import org.genfork.tools.network.OutgoingPacket;

import static org.genfork.tools.network.opcodes.ClientsBranchOpcodes.ACCOUNT_UPDATE_INFO_PACKET;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class AccountUpdateInfoPacket extends OutgoingPacket {
	private final String accountName;
	private final String ipAddr;
	private final int lastServerId;

	public AccountUpdateInfoPacket(String accountName, String ipAddr, int lastServerId) {
		this.accountName = accountName;
		this.ipAddr = ipAddr;
		this.lastServerId = lastServerId;
	}

	@Override
	public boolean write() {
		ACCOUNT_UPDATE_INFO_PACKET.writeId(this);

		writeS(accountName);
		writeS(ipAddr);
		writeD(lastServerId);
		return true;
	}
}
