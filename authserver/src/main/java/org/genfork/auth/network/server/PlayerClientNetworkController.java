package org.genfork.auth.network.server;

import lombok.Getter;
import org.genfork.auth.async.EventLoopGroupData;
import org.genfork.auth.config.ServerConfig;
import org.genfork.tools.loader.annotations.InstanceGetter;
import org.genfork.tools.loader.annotations.Load;
import org.genfork.tools.loader.interfaces.ClientAccessLoadGroup;
import org.genfork.tools.network.ServerNetworkController;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class PlayerClientNetworkController extends ServerNetworkController<PlayerHandler> {
	@Getter(lazy = true, onMethod = @__(@InstanceGetter))
	private static final PlayerClientNetworkController instance = new PlayerClientNetworkController();

	public PlayerClientNetworkController() {
		super(EventLoopGroupData.getInstance().getBossGroup(),
				EventLoopGroupData.getInstance().getWorkerGroup(),
				new PlayerClientInitializer(),
				ServerConfig.PLAYER_SERVER_HOST,
				ServerConfig.PLAYER_SERVER_PORT);
	}

	@Load(group = ClientAccessLoadGroup.class)
	private void load() throws InterruptedException {
		start();
	}
}
