package org.genfork.auth.network.discovery;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import org.genfork.auth.network.enums.DiscoveryServerIncomingPackets;
import org.genfork.tools.network.codecs.LengthFieldBasedFrameEncoder;
import org.genfork.tools.network.codecs.PacketDecoder;
import org.genfork.tools.network.codecs.PacketEncoder;

import java.nio.ByteOrder;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class DiscoveryServerInitializer extends ChannelInitializer<NioSocketChannel> {
	private static final LengthFieldBasedFrameEncoder LENGTH_ENCODER = new LengthFieldBasedFrameEncoder();
	private static final PacketEncoder PACKET_ENCODER = new PacketEncoder(1024000 - 4);

	@Override
	protected void initChannel(NioSocketChannel ch) throws Exception {
		final DiscoveryServerHandler dataHandler = new DiscoveryServerHandler();
		ch.pipeline().addLast("length-decoder", new LengthFieldBasedFrameDecoder(ByteOrder.LITTLE_ENDIAN, 1024000 - 4, 0, 4, -4, 4, false));
		ch.pipeline().addLast("length-encoder", LENGTH_ENCODER);
//		ch.pipeline().addLast(new LoggingHandler(LogLevel.INFO));
		ch.pipeline().addLast("packet-decoder", new PacketDecoder<>(DiscoveryServerIncomingPackets.PACKET_ARRAY, dataHandler));
		ch.pipeline().addLast("packet-encoder", PACKET_ENCODER);
		ch.pipeline().addLast(dataHandler);
	}
}
