package org.genfork.auth.network.clients.recv.player;

import org.genfork.auth.network.server.PlayerHandler;
import org.genfork.tools.network.IncomingPacket;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public abstract class AbstractPlayerIncomingPacket extends IncomingPacket<PlayerHandler> {
}
