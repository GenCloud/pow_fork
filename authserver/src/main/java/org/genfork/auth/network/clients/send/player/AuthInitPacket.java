package org.genfork.auth.network.clients.send.player;

import lombok.RequiredArgsConstructor;
import org.genfork.tools.crypto.ScrambledRSAKeyPair;
import org.genfork.tools.network.OutgoingPacket;
import org.genfork.tools.network.opcodes.AuthPlayerBranchOpcodes;

import javax.crypto.SecretKey;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@RequiredArgsConstructor
public class AuthInitPacket extends OutgoingPacket {
	private final int connectionId;
	private final ScrambledRSAKeyPair scrambledRSAKeyPair;
	private final SecretKey secretKey;

	@Override
	public boolean write() {
		AuthPlayerBranchOpcodes.INIT_PACKET.writeId(this);

		writeD(connectionId); // connection id
		writeD(0x0000C621); // protocol revision
		writeB(scrambledRSAKeyPair.getScrambledModulus()); // RSA Public Key
		writeD(0x29DD954E); // UNKNOWN GAMEGUARD
		writeD(0x77C39CFC); // UNKNOWN GAMEGUARD
		writeD(0x97ADB620); // UNKNOWN GAMEGUARD
		writeD(0x07BDE0F7); // UNKNOWN GAMEGUARD
		writeB(secretKey.getEncoded()); // String (BlowFishkey)
		writeC(0x00); // String (NULL termination)

		return true;
	}
}
