package org.genfork.auth.network.server;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.genfork.auth.data.player.controller.SecretKeyController;
import org.genfork.auth.data.store.controller.AuthController;
import org.genfork.auth.network.clients.send.player.AuthInitPacket;
import org.genfork.auth.network.enums.ConnectionState;
import org.genfork.tools.crypto.ScrambledRSAKeyPair;
import org.genfork.tools.network.ChannelInboundHandler;
import org.genfork.tools.network.IncomingPacket;
import org.genfork.tools.network.OutgoingPacket;
import org.genfork.tools.network.SessionInfo;

import javax.crypto.SecretKey;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Arrays;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Slf4j
public class PlayerHandler extends ChannelInboundHandler<PlayerHandler> {
	@Getter
	private final SecretKey secretKey;
	@Getter
	private final ScrambledRSAKeyPair scrambledRSAKeyPair;

	@Getter
	private InetAddress inetAddress;

	@Getter
	@Setter
	private int currentServerId;
	@Getter
	@Setter
	private int connectionId;

	@Getter
	@Setter
	private SessionInfo sessionInfo;
	@Getter
	@Setter
	private byte[] gameGuard;

	@Getter
	@Setter
	private String accountName;
	@Getter
	@Setter
	private String accountEmail;
	@Getter
	@Setter
	private String lastIpAddress;
	@Getter
	@Setter
	private int accessLevel;
	@Getter
	@Setter
	private int lastServerId;

	private Channel channel;

	public PlayerHandler(SecretKey secretKey) {
		super();
		this.secretKey = secretKey;
		scrambledRSAKeyPair = SecretKeyController.getInstance().getRandomScrambledRSAKeyPair();
	}

	@Override
	public void channelActive(ChannelHandlerContext ctx) {
		super.channelActive(ctx);

		setConnectionState(ConnectionState.CONNECTED);

		final InetSocketAddress address = (InetSocketAddress) ctx.channel().remoteAddress();
		inetAddress = address.getAddress();
		channel = ctx.channel();
		connectionId = AuthController.getInstance().getNextConnectionId();

		if (log.isDebugEnabled()) {
			log.debug("Client Connected: {}", ctx.channel());
		}

		sendPacket(new AuthInitPacket(connectionId, scrambledRSAKeyPair, secretKey));

		PlayerClientNetworkController.getInstance().addClient(getUuid(), this);
	}

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, IncomingPacket<PlayerHandler> packet) {
		try {
			packet.run(this);
		} catch (Exception ex) {
			log.warn("Exception for: {} on packet.run: {}", toString(), packet.getClass().getSimpleName(), ex);
		}
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
		log.warn("Network exception caught for: {} : {}", toString(), cause.getLocalizedMessage());
	}

	public void closeNow() {
		if (channel != null) {
			channel.close();
		}
	}

	public void close(OutgoingPacket packet) {
		sendPacket(packet);
		closeNow();
	}

	public void sendPacket(OutgoingPacket packet) {
		if (packet == null) {
			return;
		}

		channel.writeAndFlush(packet);
	}

	public boolean checkGameGuard(byte[] data) {
		return Arrays.equals(gameGuard, data);
	}

	@Override
	public String toString() {
		try {
			final InetAddress address = inetAddress;
			return "[IP: " + (address == null ? "disconnected" : address.getHostAddress()) + "]";
		} catch (NullPointerException e) {
			return "Read failed due to disconnect]";
		}
	}
}
