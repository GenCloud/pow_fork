package org.genfork.auth.network.clients.send.game;

import lombok.RequiredArgsConstructor;
import org.genfork.tools.network.OutgoingPacket;

import static org.genfork.tools.network.opcodes.ClientsBranchOpcodes.PLAYER_TRY_PLAYER_GAME_PACKET;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@RequiredArgsConstructor
public class PlayerTryPlayerGamePacket extends OutgoingPacket {
	private final String playerClientId;
	private final String accountName;
	private final int sessionId;

	@Override
	public boolean write() {
		PLAYER_TRY_PLAYER_GAME_PACKET.writeId(this);

		writeS(playerClientId);
		writeS(accountName);
		writeD(sessionId);

		return true;
	}
}
