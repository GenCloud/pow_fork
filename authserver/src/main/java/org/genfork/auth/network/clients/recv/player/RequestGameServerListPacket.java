package org.genfork.auth.network.clients.recv.player;

import org.genfork.auth.data.game.GameServer;
import org.genfork.auth.data.game.GameServerController;
import org.genfork.auth.network.clients.send.player.AuthFailPacket;
import org.genfork.auth.network.clients.send.player.GameServerListPacket;
import org.genfork.auth.network.enums.ConnectionState;
import org.genfork.auth.network.server.PlayerHandler;

import java.util.Collection;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class RequestGameServerListPacket extends AbstractPlayerIncomingPacket {
	private long sessionId;

	@Override
	public boolean read(PlayerHandler client) {
		sessionId = readD();
		return true;
	}

	@Override
	public void run(PlayerHandler client) throws Exception {
		if (client.getSessionInfo().getAuthKeyFirst() == sessionId) {
			client.setConnectionState(ConnectionState.AUTHED_SERVER_LIST);

			final Collection<GameServer> collect = GameServerController.getInstance().getAllClients();

			client.sendPacket(new GameServerListPacket(client, collect));
		} else {
			client.close(AuthFailPacket.ACCESS_FAILED_PLEASE_TRY_AGAIN_LATER);
		}
	}
}
