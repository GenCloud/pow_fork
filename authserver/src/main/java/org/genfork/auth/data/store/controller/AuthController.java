package org.genfork.auth.data.store.controller;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.genfork.auth.data.game.GameServer;
import org.genfork.auth.data.game.GameServerController;
import org.genfork.auth.network.clients.send.game.PlayerTryPlayerGamePacket;
import org.genfork.auth.network.clients.send.player.GameServerPlayFailPacket;
import org.genfork.auth.network.clients.send.store.AccountAuthPacket;
import org.genfork.auth.network.clients.send.store.AccountUpdateInfoPacket;
import org.genfork.auth.network.server.PlayerHandler;
import org.genfork.tools.network.ChannelInboundHandler;
import org.genfork.tools.network.DiscoveryClients;
import org.genfork.tools.network.NetworkConnectionPool;
import org.genfork.tools.network.SessionInfo;

import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

import static org.genfork.tools.servers.enums.PlayFailReason.REASON_ACCESS_FAILED_TRY_AGAIN_LATER;
import static org.genfork.tools.servers.enums.PlayFailReason.REASON_MASTER_ACCOUNT_RESTRICTED;
import static org.genfork.tools.servers.enums.ServerState.ONLINE;
import static org.genfork.tools.servers.enums.ServerType.TEST;
import static org.genfork.tools.servers.enums.ServerType.WORLD_RAID;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Slf4j
public class AuthController {
	public static final int SERVER_ERROR = 1;
	public static final int ACCOUNT_VALID_CODE = 0;
	public static final int ACCOUNT_NOT_VALID_CODE = 2;
	public static final int ACCOUNT_DOES_NOT_EXISTS = 3;
	public static final int ACCOUNT_NOT_MATCHES_PASSWORD_CODE = 4;
	public static final int ACCOUNT_BANNED = 5;

	@Getter(lazy = true)
	private static final AuthController instance = new AuthController();

	private final AtomicInteger connectionId = new AtomicInteger();

	public int getNextConnectionId() {
		return connectionId.getAndIncrement();
	}

	public void tryPlayerAuth(PlayerHandler playerHandler, String accountName, String password) {
		final ChannelInboundHandler<?> serverHandler = NetworkConnectionPool.getInstance().getNextChannel(DiscoveryClients.AUTH_NODE);
		if (serverHandler != null) {
			final String hostAddress = playerHandler.getInetAddress().getHostAddress();
			final UUID playerClientUuid = playerHandler.getUuid();

			if (log.isDebugEnabled()) {
				log.debug("Try auth account by name - {}, clientId - {}", accountName, playerClientUuid);
			}

			final AccountAuthPacket accountAuthPacket = new AccountAuthPacket(playerClientUuid, hostAddress, accountName, password);

			serverHandler.sendPacket(DiscoveryClients.DATA_STORE_NODE,
					DiscoveryClients.AUTH_NODE,
					accountAuthPacket);
		}
	}

	public void updateAccountInfo(PlayerHandler playerHandler) {
		final ChannelInboundHandler<?> serverHandler = NetworkConnectionPool.getInstance().getNextChannel(DiscoveryClients.AUTH_NODE);
		if (serverHandler != null) {
			final String hostAddress = playerHandler.getInetAddress().getHostAddress();
			final String accountName = playerHandler.getAccountName();
			final int lastServerId = playerHandler.getLastServerId();

			if (log.isDebugEnabled()) {
				log.debug("Try update account info by name - {}", accountName);
			}

			final AccountUpdateInfoPacket result = new AccountUpdateInfoPacket(accountName, hostAddress, lastServerId);

			serverHandler.sendPacket(DiscoveryClients.DATA_STORE_NODE,
					DiscoveryClients.AUTH_NODE,
					result);
		}
	}

	public void tryPlayerPlayGame(PlayerHandler handler, int serverId, int authKeyFirst, int authKeySecond) {
		final SessionInfo sessionInfo = handler.getSessionInfo();
		if (sessionInfo.getAuthKeyFirst() == authKeyFirst && sessionInfo.getAuthKeySecond() == authKeySecond) {
			final GameServer gameServer = GameServerController.getInstance().getAllClients()
					.stream()
					.filter(gs ->
							gs.getId() == serverId)
					.findFirst()
					.orElse(null);
			if (gameServer != null
					&& gameServer.getServerState() == ONLINE) {
				if (gameServer.getServerTypes().stream().anyMatch(s -> s == TEST || s == WORLD_RAID)) {
					if (handler.getAccessLevel() > 0) {
						sendSuccessResult(handler, serverId, authKeyFirst);
						return;
					}

					handler.close(new GameServerPlayFailPacket(REASON_MASTER_ACCOUNT_RESTRICTED));
				} else {
					sendSuccessResult(handler, serverId, authKeyFirst);
				}
			}
		} else {
			handler.close(new GameServerPlayFailPacket(REASON_ACCESS_FAILED_TRY_AGAIN_LATER));
		}
	}

	private void sendSuccessResult(PlayerHandler playerHandler, int serverId, int sessionId) {
		playerHandler.setCurrentServerId(serverId);
		playerHandler.setLastServerId(serverId);

		final GameServer gameServer = GameServerController.getInstance()
				.findClientBy(c -> c.getId() == serverId);
		if (gameServer != null) {
			final String uuid = playerHandler.getUuid().toString();
			final String accountName = playerHandler.getAccountName();
			final SessionInfo sessionInfo = playerHandler.getSessionInfo();

			final PlayerTryPlayerGamePacket result = new PlayerTryPlayerGamePacket(uuid, accountName, sessionId);

			final ChannelInboundHandler<?> handler = NetworkConnectionPool.getInstance().getNextChannel(DiscoveryClients.AUTH_NODE);
			handler.sendPacket(DiscoveryClients.GAME_SLAVE_NODE,
					DiscoveryClients.AUTH_NODE,
					sessionInfo,
					result);

			updateAccountInfo(playerHandler);
		}
	}
}
