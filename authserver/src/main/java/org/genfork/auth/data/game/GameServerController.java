package org.genfork.auth.data.game;

import lombok.Getter;

import java.util.Collection;
import java.util.Collections;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Predicate;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class GameServerController {
	@Getter(lazy = true)
	private static final GameServerController instance = new GameServerController();

	private final ConcurrentMap<String, GameServer> gameServers = new ConcurrentHashMap<>();

	public void addClient(UUID uuid, GameServer client) {
		if (!gameServers.containsKey(uuid.toString())) {
			gameServers.put(uuid.toString(), client);
		}
	}

	public void removeClient(UUID uuid) {
		gameServers.remove(uuid.toString());
	}

	public GameServer getClient(UUID uuid) {
		return gameServers.get(uuid.toString());
	}

	public Collection<GameServer> getAllClients() {
		return Collections.unmodifiableCollection(gameServers.values());
	}

	public GameServer findClientBy(Predicate<GameServer> p) {
		return getAllClients().stream().filter(p).findFirst().orElse(null);
	}
}
