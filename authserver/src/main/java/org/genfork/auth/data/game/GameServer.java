package org.genfork.auth.data.game;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.genfork.tools.servers.enums.AgeLimit;
import org.genfork.tools.servers.enums.ServerState;
import org.genfork.tools.servers.enums.ServerType;

import java.util.Set;

@RequiredArgsConstructor
@Data
public class GameServer {
	private final short id;
	private final int[] ipParts;
	private final int playerLimit;
	private final ServerState serverState;
	private final AgeLimit ageLimit;
	private final Set<ServerType> serverTypes;

	public int getServerTypesMask() {
		return serverTypes.stream().mapToInt(ServerType::getMask).reduce((r, e) -> r | e).orElse(0);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		final GameServer that = (GameServer) o;
		return id == that.id;
	}

	@Override
	public int hashCode() {
		return id;
	}
}
