package org.genfork.auth.data.player.crypt;

import io.netty.buffer.ByteBuf;
import org.genfork.tools.common.Rnd;
import org.genfork.tools.crypto.controller.BlowfishController;
import org.genfork.tools.network.ICrypt;

import javax.crypto.SecretKey;
import java.nio.ByteOrder;

public class PlayerCrypt implements ICrypt {
	private static final byte[] STATIC_BLOWFISH_KEY =
			{
					(byte) 0x6b,
					(byte) 0x60,
					(byte) 0xcb,
					(byte) 0x5b,
					(byte) 0x82,
					(byte) 0xce,
					(byte) 0x90,
					(byte) 0xb1,
					(byte) 0xcc,
					(byte) 0x2b,
					(byte) 0x6c,
					(byte) 0x55,
					(byte) 0x6c,
					(byte) 0x6c,
					(byte) 0x6c,
					(byte) 0x6c
			};

	private static final BlowfishController STATIC_BLOWFISH_ENGINE = new BlowfishController();

	static {
		STATIC_BLOWFISH_ENGINE.init(STATIC_BLOWFISH_KEY);
	}

	private final BlowfishController blowfishController = new BlowfishController();
	private boolean staticKey = true;

	public PlayerCrypt(SecretKey blowfishKey) {
		blowfishController.init(blowfishKey.getEncoded());
	}

	@Override
	@SuppressWarnings("deprecation")
	public void encrypt(ByteBuf buf) {
		if (buf.order() != ByteOrder.LITTLE_ENDIAN) {
			buf = buf.order(ByteOrder.LITTLE_ENDIAN);
		}

		// Checksum & XOR Key or Checksum only
		buf.writeZero(staticKey ? 8 : 4);

		// Padding
		buf.writeZero(8 - (buf.readableBytes() % 8));

		if (staticKey) {
			staticKey = false;

			int key = Rnd.nextInt();
			buf.skipBytes(4); // The first 4 bytes are ignored
			while (buf.readerIndex() < (buf.writerIndex() - 8)) {
				int data = buf.readInt();
				key += data;
				data ^= key;
				buf.setInt(buf.readerIndex() - 4, data);
			}
			buf.setInt(buf.readerIndex(), key);

			buf.resetReaderIndex();

			final byte[] block = new byte[8];
			while (buf.isReadable(8)) {
				buf.readBytes(block);
				STATIC_BLOWFISH_ENGINE.encryptBlock(block, 0);
				buf.setBytes(buf.readerIndex() - block.length, block);
			}
		} else {
			int checksum = 0;
			while (buf.isReadable(8)) {
				checksum ^= buf.readInt();
			}

			buf.setInt(buf.readerIndex(), checksum);

			buf.resetReaderIndex();

			final byte[] block = new byte[8];
			while (buf.isReadable(8)) {
				buf.readBytes(block);
				blowfishController.encryptBlock(block, 0);
				buf.setBytes(buf.readerIndex() - block.length, block);
			}
		}
	}

	@Override
	public void decrypt(ByteBuf buf) {
		if ((buf.readableBytes() % 8) != 0) {
			buf.clear();
			return;
		}

		final byte[] block = new byte[8];
		while (buf.isReadable(8)) {
			buf.readBytes(block);
			blowfishController.decryptBlock(block, 0);
			buf.setBytes(buf.readerIndex() - block.length, block);
		}
	}
}
