package org.genfork.auth.data.player.controller;

import lombok.extern.slf4j.Slf4j;
import org.genfork.tools.common.Rnd;
import org.genfork.tools.crypto.ScrambledRSAKeyPair;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.security.GeneralSecurityException;
import java.security.KeyPairGenerator;
import java.security.spec.RSAKeyGenParameterSpec;
import java.util.Arrays;

@Slf4j
public class SecretKeyController {
	private final KeyGenerator blowfishKeyGenerator = KeyGenerator.getInstance("Blowfish");
	private final ScrambledRSAKeyPair[] scrambledRSAKeyPairs = new ScrambledRSAKeyPair[50];

	protected SecretKeyController() throws GeneralSecurityException {
		final KeyPairGenerator rsaKeyPairGenerator = KeyPairGenerator.getInstance("RSA");
		final RSAKeyGenParameterSpec spec = new RSAKeyGenParameterSpec(1024, RSAKeyGenParameterSpec.F4);
		rsaKeyPairGenerator.initialize(spec);

		Arrays.setAll(scrambledRSAKeyPairs, i -> new ScrambledRSAKeyPair(rsaKeyPairGenerator.generateKeyPair()));

		log.info("Cached {} RSA key pairs.", scrambledRSAKeyPairs.length);
	}

	/**
	 * Gets the single instance of {@code KeyGen}.
	 *
	 * @return the instance
	 */
	public static SecretKeyController getInstance() {
		return SingletonHolder._instance;
	}

	/**
	 * Generates a Blowfish key.
	 *
	 * @return the blowfish {@code SecretKey}
	 */
	public SecretKey getRandomSecretKey() {
		return blowfishKeyGenerator.generateKey();
	}

	/**
	 * Gets a random pre-cached {@code ScrambledRSAKeyPair}.
	 *
	 * @return the {@code ScrambledRSAKeyPair}
	 */
	public ScrambledRSAKeyPair getRandomScrambledRSAKeyPair() {
		return scrambledRSAKeyPairs[Rnd.nextInt(scrambledRSAKeyPairs.length)];
	}

	private static class SingletonHolder {
		protected static final SecretKeyController _instance;

		static {
			SecretKeyController instance = null;
			try {
				instance = new SecretKeyController();
			} catch (GeneralSecurityException e) {
				e.printStackTrace();
			} finally {
				_instance = instance;
			}
		}
	}
}
