package org.genfork.gameserver.master.network.client.packets.recv.system;

import io.netty.buffer.ByteBuf;
import org.genfork.gameserver.master.network.client.ClientHandler;
import org.genfork.gameserver.master.network.client.packets.send.SendFromPlayerData;
import org.genfork.tools.network.ChannelInboundHandler;
import org.genfork.tools.network.DiscoveryClients;
import org.genfork.tools.network.IncomingPacket;
import org.genfork.tools.network.NetworkConnectionPool;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class RequestRetransmitDataToSlavePacket extends IncomingPacket<ClientHandler> {
	private ByteBuf data;

	@Override
	public boolean read(ClientHandler client) {
		data = readB();
		return true;
	}

	@Override
	public void run(ClientHandler client) throws Exception {
		final ChannelInboundHandler<?> handler = NetworkConnectionPool.getInstance().getNextChannel(DiscoveryClients.GAME_MASTER_NODE);
		if (handler != null && handler.getChannel().isActive()) {
			handler.sendPacket(DiscoveryClients.GAME_SLAVE_NODE,
					DiscoveryClients.GAME_MASTER_NODE,
					client.getSessionInfo(),
					new SendFromPlayerData(data));
		} else {
			client.closeNow();
		}
	}
}
