package org.genfork.gameserver.master.network.client.packets.recv.player;

import org.apache.commons.lang3.ArrayUtils;
import org.genfork.gameserver.master.config.ServerConfig;
import org.genfork.gameserver.master.network.client.ClientHandler;
import org.genfork.gameserver.master.network.client.packets.send.player.VersionCheckPacket;
import org.genfork.tools.network.IncomingPacket;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class ProtocolVersion extends IncomingPacket<ClientHandler> {
	private int version;

	@Override
	public boolean read(ClientHandler client) {
		version = readD();
		return true;
	}

	@Override
	public void run(ClientHandler client) throws Exception {
		if (version == -2) {
			client.closeNow();
			return;
		}

		if (!ArrayUtils.contains(ServerConfig.ALLOWED_PROTOCOL_VERSION, version)) {
			client.close(new VersionCheckPacket(client.enableCrypt(), 0));
		} else {
			client.sendPacket(new VersionCheckPacket(client.enableCrypt(), 1));
		}
	}
}
