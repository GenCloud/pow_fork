package org.genfork.gameserver.master.network.client;

import io.netty.channel.ChannelHandlerContext;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.genfork.gameserver.master.network.enums.ConnectionState;
import org.genfork.tools.async.ThreadPool;
import org.genfork.tools.crypto.BlowFishKeygen;
import org.genfork.tools.crypto.ClientCrypt;
import org.genfork.tools.network.ChannelInboundHandler;
import org.genfork.tools.network.IncomingPacket;
import org.genfork.tools.network.OutgoingPacket;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ScheduledFuture;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Slf4j
public class ClientHandler extends ChannelInboundHandler<ClientHandler> {
	@Getter
	private final ClientCrypt clientCrypt = new ClientCrypt();

	private final Queue<IncomingPacket<ClientHandler>> packetQueue = new ConcurrentLinkedQueue<>();

	@Getter
	private InetAddress inetAddress;

	@Getter
	private byte[] cryptKey;

	private ScheduledFuture<?> answerTask;

	public ClientHandler() {
		answerTask = ThreadPool.scheduleAtFixedRate(() -> {
			IncomingPacket<ClientHandler> packet;
			while ((packet = packetQueue.poll()) != null) {
				try {
					packet.run(this);
				} catch (Exception ex) {
					log.warn("Exception for: {} on packet.run: {}", toString(), packet.getClass().getSimpleName(), ex);
				}
			}
		}, 0, 10, MILLISECONDS);
	}

	@Override
	public void channelActive(ChannelHandlerContext ctx) {
		super.channelActive(ctx);

		setConnectionState(ConnectionState.CONNECTED);

		final InetSocketAddress address = (InetSocketAddress) ctx.channel().remoteAddress();
		inetAddress = address.getAddress();

		if (log.isDebugEnabled()) {
			log.debug("Client Connected: {}", ctx.channel());
		}
	}

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, IncomingPacket<ClientHandler> packet) throws Exception {
		packetQueue.add(packet);
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
		log.warn("Network exception caught for: {} : {}", toString(), cause.getLocalizedMessage());
	}

	@Override
	public void channelInactive(ChannelHandlerContext ctx) throws Exception {
		super.channelInactive(ctx);
		answerTask.cancel(true);
		answerTask = null;
	}

	public byte[] enableCrypt() {
		final byte[] cryptKey = BlowFishKeygen.getRandomKey();
		clientCrypt.setKey(cryptKey);
		this.cryptKey = cryptKey;
		return cryptKey;
	}

	public void close(OutgoingPacket packet) {
		sendPacket(packet);
		closeNow();
	}
}
