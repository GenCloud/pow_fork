package org.genfork.gameserver.master.network.client;

import lombok.Getter;
import org.genfork.gameserver.master.async.EventLoopGroupData;
import org.genfork.gameserver.master.config.ServerConfig;
import org.genfork.tools.loader.annotations.InstanceGetter;
import org.genfork.tools.loader.annotations.Load;
import org.genfork.tools.loader.interfaces.ClientAccessLoadGroup;
import org.genfork.tools.network.ServerNetworkController;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class ClientNetworkController extends ServerNetworkController<ClientHandler> {
	@Getter(lazy = true, onMethod = @__(@InstanceGetter))
	private static final ClientNetworkController instance = new ClientNetworkController();

	public ClientNetworkController() {
		super(EventLoopGroupData.getInstance().getBossGroup(),
				EventLoopGroupData.getInstance().getWorkerGroup(),
				new ClientInitializer(),
				ServerConfig.PLAYER_SERVER_HOST,
				ServerConfig.PLAYER_SERVER_PORT);
	}

	@Load(group = ClientAccessLoadGroup.class)
	private void load() throws InterruptedException {
		start();
	}
}
