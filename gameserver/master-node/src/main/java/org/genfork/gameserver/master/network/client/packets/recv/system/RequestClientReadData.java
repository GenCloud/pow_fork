package org.genfork.gameserver.master.network.client.packets.recv.system;

import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;
import org.genfork.gameserver.master.manager.SessionManager;
import org.genfork.gameserver.master.network.client.packets.send.SendFromPlayerData;
import org.genfork.gameserver.master.network.discovery.DiscoveryServerHandler;
import org.genfork.tools.network.ChannelInboundHandler;
import org.genfork.tools.network.IncomingPacket;

import java.util.Arrays;
import java.util.stream.IntStream;

import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.genfork.tools.network.opcodes.GameBranchOpcodes.SERVER_CLOSE;
import static org.genfork.tools.registry.NetworkConstants.*;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Slf4j
public class RequestClientReadData extends IncomingPacket<DiscoveryServerHandler> {
	private int authKeyFirst, authKeySecond, playKeyFirst, playKeySecond;

	private ByteBuf byteBuf;

	@Override
	public boolean read(DiscoveryServerHandler client) {
		@SuppressWarnings("unused") final String clientName = readS();
		@SuppressWarnings("unused") final String originClientName = readS();

		final boolean fromPlayer = readC() == 1;

		if (fromPlayer) {
			authKeyFirst = readD();
			authKeySecond = readD();
			playKeyFirst = readD();
			playKeySecond = readD();
		}

		if (log.isDebugEnabled()) {
			log.debug("Retransmit slave data to player by session: {}:{}:{}:{}", authKeyFirst, authKeySecond, playKeyFirst, playKeySecond);
		}

		byteBuf = readB();
		return true;
	}

	@Override
	public void run(DiscoveryServerHandler client) throws Exception {
		final ChannelInboundHandler<?> handler = SessionManager.getInstance().getClient(playKeyFirst, playKeySecond, authKeyFirst, authKeySecond);
		if (handler != null) {
			try {
				final String string = readS(byteBuf);
				if (string.contains(PACKET_PARTS_PARAM)) {
					final String[] params = string.split(PACKET_PARAMS_DELIMITER);
					final int parts = Integer.parseInt(params[0].replace(PACKET_PARTS_PARAM, EMPTY).trim());
					final int[] partsLength = Arrays.stream(params[1].replace(PACKET_PARTS_LENGTH_PARAM, EMPTY).trim().split(PACKET_PARTS_DELIMITER))
							.mapToInt(Integer::valueOf)
							.toArray();

					IntStream.range(0, parts)
							.mapToObj(part ->
									byteBuf.readBytes(partsLength[part]))
							.map(SendFromPlayerData::new)
							.forEach(handler::sendPacket);
					return;
				} else {
					readSimpleData(handler);
				}
			} catch (Exception e) {
				readSimpleData(handler);
			}
		}

		byteBuf.retain();
	}

	private void readSimpleData(ChannelInboundHandler<?> handler) {
		byteBuf.resetReaderIndex();

		if (byteBuf.readUnsignedByte() == SERVER_CLOSE.getId1()) {
			handler.closeNow();
		} else {
			byteBuf.resetReaderIndex();
			boolean invPacket = byteBuf.readUnsignedByte() == 0x11;
			if (invPacket) {
				short sendType = byteBuf.readUnsignedByte();
				log.info("inv sendType: {}", sendType);
			}

			byteBuf.resetReaderIndex();
			handler.sendPacket(new SendFromPlayerData(byteBuf));
		}
	}
}
