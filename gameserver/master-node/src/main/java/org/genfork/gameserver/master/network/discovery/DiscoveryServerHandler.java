package org.genfork.gameserver.master.network.discovery;

import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;
import org.genfork.gameserver.master.config.DiscoveryClientConfig;
import org.genfork.tools.network.*;
import org.genfork.tools.registry.annotation.DiscoveryClient;

import java.net.InetAddress;

import static org.genfork.gameserver.master.network.enums.ConnectionState.CONNECTED;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Slf4j
@DiscoveryClient(config = DiscoveryClientConfig.class)
public class DiscoveryServerHandler extends ChannelInboundHandler<DiscoveryServerHandler> {
	private InetAddress inetAddress;

	@Override
	public void channelActive(ChannelHandlerContext ctx) {
		super.channelActive(ctx);

		setConnectionState(CONNECTED);

		NetworkConnectionPool.getInstance().addChannel(DiscoveryClients.GAME_MASTER_NODE, this);
	}

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, IncomingPacket<DiscoveryServerHandler> packet) throws Exception {
		try {
			packet.run(this);
		} catch (Exception ex) {
			log.warn("Exception for: {} on packet.run: {}", toString(), packet.getClass().getSimpleName(), ex);
		}
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
		log.warn("Network exception caught for: {} : {}", toString(), cause.getLocalizedMessage());
	}

	public void closeNow() {
		if (channel != null) {
			channel.close();
		}
	}

	public void sendPacket(OutgoingPacket packet) {
		if (packet == null) {
			return;
		}

		channel.writeAndFlush(packet);
	}
}
