package org.genfork.gameserver.master.network.client.packets.recv.player;

import org.apache.commons.lang3.StringUtils;
import org.genfork.gameserver.master.manager.SessionManager;
import org.genfork.gameserver.master.network.client.ClientHandler;
import org.genfork.gameserver.master.network.client.packets.send.slave.TryAuthPacket;
import org.genfork.tools.network.*;

import static org.genfork.gameserver.master.network.enums.ConnectionState.REGISTERED;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class RequestGameAuthPacket extends IncomingPacket<ClientHandler> {
	private String accountName;
	private int playKeySecond;
	private int playKeyFirst;
	private int authKeyFirst;
	private int authKeySecond;

	@Override
	public boolean read(ClientHandler client) {
		accountName = readS();

		playKeySecond = readD();
		playKeyFirst = readD();
		authKeyFirst = readD();
		authKeySecond = readD();
		return true;
	}

	@Override
	public void run(ClientHandler client) throws Exception {
		if (StringUtils.isEmpty(accountName)) {
			client.closeNow();
			return;
		}

		final ChannelInboundHandler<?> handler = SessionManager.getInstance().getClient(playKeyFirst, playKeySecond, authKeyFirst, authKeySecond);
		if (handler != null) {
			return;
		}

		final SessionInfo of = SessionInfo.of(playKeyFirst, playKeySecond, authKeyFirst, authKeySecond);
		of.setAccountName(accountName);
		client.setSessionInfo(of);

		SessionManager.getInstance().addClient(client, of);
		client.setConnectionState(REGISTERED);

		final ChannelInboundHandler<?> inboundHandler = NetworkConnectionPool.getInstance().getNextChannel(DiscoveryClients.GAME_MASTER_NODE);
		inboundHandler.sendPacket(DiscoveryClients.GAME_SLAVE_NODE,
				DiscoveryClients.GAME_MASTER_NODE,
				of,
				new TryAuthPacket(accountName));
	}
}
