package org.genfork.gameserver.master.config;

import org.genfork.tools.config.abstraction.ConfigPropertiesLoader;
import org.genfork.tools.config.annotation.ConfigClass;
import org.genfork.tools.config.annotation.ConfigField;
import org.genfork.tools.servers.enums.AgeLimit;
import org.genfork.tools.servers.enums.ServerType;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@ConfigClass(fileName = "Server")
public class ServerConfig extends ConfigPropertiesLoader {
	@ConfigField(name = "PlayerServerHost", value = "127.0.0.1")
	public static String PLAYER_SERVER_HOST;

	@ConfigField(name = "PlayerServerPort", value = "7777")
	public static int PLAYER_SERVER_PORT;

	@ConfigField(name = "DiscoveryServerHost", value = "127.0.0.1")
	public static String DISCOVERY_SERVER_HOST;

	@ConfigField(name = "DiscoveryServerPort", value = "5000")
	public static int DISCOVERY_SERVER_PORT;

	@ConfigField(name = "DiscoveryRootClientPort", value = "7777")
	public static int DISCOVERY_ROOT_CLIENT_PORT;

	@ConfigField(name = "AllowedProtocolVersions", value = "240,245")
	public static int[] ALLOWED_PROTOCOL_VERSION;

	@ConfigField(name = "PlayerServerId", value = "3")
	public static short PLAYER_SERVER_ID;

	@ConfigField(name = "PlayerServerLimit", value = "2000")
	public static short PLAYER_SERVER_LIMIT;

	@ConfigField(name = "PlayerServerAgeLimit", value = "NONE")
	public static AgeLimit PLAYER_SERVER_AGE_LIMIT;

	@ConfigField(name = "PlayerServerType", value = "FREE")
	public static ServerType PLAYER_SERVER_TYPE;

	@ConfigField(name = "ServerKey", value = "{bcrypt}$2a$10$QLekRpQ7ib/LoQzaok7wBuNbLAklR5mQWMop8oH8k4PBmB8yCXHIO")
	public static String SERVER_PRIVATE_KEY;
}
