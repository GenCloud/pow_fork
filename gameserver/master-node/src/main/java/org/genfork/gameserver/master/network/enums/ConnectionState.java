package org.genfork.gameserver.master.network.enums;

import org.genfork.tools.network.IConnectionState;

public enum ConnectionState implements IConnectionState {
	REGISTERED,
	CONNECTED
}
