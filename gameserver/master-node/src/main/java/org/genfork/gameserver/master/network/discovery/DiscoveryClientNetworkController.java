package org.genfork.gameserver.master.network.discovery;

import lombok.Getter;
import org.genfork.gameserver.master.async.EventLoopGroupData;
import org.genfork.gameserver.master.config.ServerConfig;
import org.genfork.gameserver.master.network.client.packets.send.auth.RegisterGameServerPacket;
import org.genfork.tools.loader.annotations.InstanceGetter;
import org.genfork.tools.loader.annotations.Load;
import org.genfork.tools.loader.interfaces.PostLoadGroup;
import org.genfork.tools.network.ChannelInboundHandler;
import org.genfork.tools.network.ClientNetworkController;
import org.genfork.tools.network.DiscoveryClients;
import org.genfork.tools.network.NetworkConnectionPool;
import org.genfork.tools.servers.enums.ServerState;

import java.util.Arrays;
import java.util.stream.IntStream;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class DiscoveryClientNetworkController extends ClientNetworkController<DiscoveryServerHandler> {
	@Getter(lazy = true, onMethod = @__(@InstanceGetter))
	private static final DiscoveryClientNetworkController instance = new DiscoveryClientNetworkController();

	public DiscoveryClientNetworkController() {
		super(EventLoopGroupData.getInstance().getWorkerGroup(),
				new DiscoveryServerInitializer(),
				ServerConfig.DISCOVERY_SERVER_HOST,
				ServerConfig.DISCOVERY_SERVER_PORT);
	}

	@Load(group = PostLoadGroup.class)
	private void load() {
		IntStream.range(0, BASIC_NETWORK_POOL)
				.forEach(i -> NetworkConnectionPool.getInstance().initialize(DiscoveryClients.GAME_MASTER_NODE, getBootstrap()));

		final ChannelInboundHandler<?> handler = NetworkConnectionPool.getInstance().getNextChannel(DiscoveryClients.GAME_MASTER_NODE);

		final Byte[] bytes = Arrays
				.stream(ServerConfig.PLAYER_SERVER_HOST.split("\\."))
				.map(Byte::parseByte)
				.toArray(Byte[]::new);
		final int[] ipParts = new int[5];
		ipParts[0] = bytes[0];
		ipParts[1] = bytes[1];
		ipParts[2] = bytes[2];
		ipParts[3] = bytes[3];
		ipParts[4] = ServerConfig.PLAYER_SERVER_PORT;

		final RegisterGameServerPacket result = new RegisterGameServerPacket(ServerConfig.PLAYER_SERVER_ID,
				ipParts,
				ServerConfig.PLAYER_SERVER_LIMIT,
				ServerConfig.PLAYER_SERVER_AGE_LIMIT,
				ServerState.ONLINE,
				ServerConfig.PLAYER_SERVER_TYPE);

		handler.sendPacket(DiscoveryClients.AUTH_NODE,
				DiscoveryClients.GAME_MASTER_NODE,
				result);
	}
}
