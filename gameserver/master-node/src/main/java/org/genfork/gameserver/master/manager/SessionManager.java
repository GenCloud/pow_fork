package org.genfork.gameserver.master.manager;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.genfork.tools.data.StatsSet;
import org.genfork.tools.network.ChannelInboundHandler;
import org.genfork.tools.network.SessionInfo;
import org.redisson.api.RLock;
import org.redisson.api.RMap;
import org.redisson.api.RReadWriteLock;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.genfork.gameserver.master.network.enums.ConnectionState.REGISTERED;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Slf4j
public class SessionManager {
	@Getter(lazy = true)
	private static final SessionManager instance = new SessionManager();

	private static final String SESSION_STORE_BUCKET = "sessions_store";

	private final Map<String, ChannelInboundHandler<?>> clients = new HashMap<>();

	public ChannelInboundHandler<?> getClient(int playKeyFirst, int playKeySecond, int authKeyFirst, int authKeySecond) {
		final RMap<String, StatsSet> map = DataStoreManager.getInstance().getMap(SESSION_STORE_BUCKET);

		SessionInfo sessionInfo = SessionInfo.of(playKeyFirst, playKeySecond, authKeyFirst, authKeySecond);

		final String key = sessionInfo.toString();
		final RReadWriteLock lock = map.getReadWriteLock(key);
		final RLock rLock = lock.readLock();

		ChannelInboundHandler<?> result;

		rLock.lockAsync();
		try {
			result = clients.get(sessionInfo.toString());

			sessionInfo = DataStoreManager.getInstance().getData(SESSION_STORE_BUCKET, sessionInfo.toString());
			if (sessionInfo != null && result != null) {
				result.setSessionInfo(sessionInfo);
			}
		} finally {
			rLock.unlockAsync();
		}

		return result;
	}

	public Collection<ChannelInboundHandler<?>> getAllClients() {
		return Collections.unmodifiableCollection(clients.values());
	}

	public void addClient(ChannelInboundHandler<?> handler, SessionInfo sessionInfo) {
		final RMap<String, StatsSet> map = DataStoreManager.getInstance().getMap(SESSION_STORE_BUCKET);

		final String key = sessionInfo.toString();
		final RReadWriteLock lock = map.getReadWriteLock(key);
		final RLock rLock = lock.writeLock();

		rLock.lockAsync();
		try {
			if (clients.containsKey(key)) {
				log.warn("Client with the same session already exists - reject connection!");
				handler.closeNow();
				return;
			}

			clients.put(key, handler);
			DataStoreManager.getInstance().addData(SESSION_STORE_BUCKET, key, sessionInfo);

			handler.setConnectionState(REGISTERED);

			log.info("Register client [{}]", sessionInfo);
		} finally {
			rLock.unlockAsync();
		}
	}

	public void removeClient(int playKeyFirst, int playKeySecond, int authKeyFirst, int authKeySecond) {
		final RMap<String, StatsSet> map = DataStoreManager.getInstance().getMap(SESSION_STORE_BUCKET);

		final SessionInfo sessionInfo = SessionInfo.of(playKeyFirst, playKeySecond, authKeyFirst, authKeySecond);
		final String key = sessionInfo.toString();
		final RReadWriteLock lock = map.getReadWriteLock(key);
		final RLock rLock = lock.writeLock();
		rLock.lockAsync();
		try {
			clients.remove(key);
			DataStoreManager.getInstance().clearData(SESSION_STORE_BUCKET, key);
		} finally {
			rLock.unlockAsync();
		}
	}
}
