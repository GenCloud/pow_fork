package org.genfork.gameserver.master.network.client.packets.send.auth;

import lombok.RequiredArgsConstructor;
import org.genfork.tools.network.OutgoingPacket;
import org.genfork.tools.servers.enums.AgeLimit;
import org.genfork.tools.servers.enums.ServerState;
import org.genfork.tools.servers.enums.ServerType;

import java.util.Arrays;

import static org.genfork.tools.network.opcodes.ClientsBranchOpcodes.REGISTER_GAME_SERVER_PACKET;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@RequiredArgsConstructor
public class RegisterGameServerPacket extends OutgoingPacket {
	private final short serverId;
	private final int[] ipParts;
	private final int playerLimit;
	private final AgeLimit ageLimit;
	private final ServerState serverState;
	private final ServerType serverType;

	@Override
	public boolean write() {
		REGISTER_GAME_SERVER_PACKET.writeId(this);

		writeC(serverId);

		final int size = ipParts.length;
		writeD(size);

		Arrays.stream(ipParts).forEach(this::writeD);

		writeC(serverState.ordinal());
		writeD(playerLimit);
		writeD(ageLimit.getAge());
		writeD(serverType.getMask());
		return true;
	}
}
