package org.genfork.gameserver.master.config;

import org.genfork.tools.config.abstraction.ConfigPropertiesLoader;
import org.genfork.tools.config.annotation.ConfigClass;
import org.genfork.tools.config.annotation.ConfigField;
import org.genfork.tools.config.annotation.ConfigGroupBeginning;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@ConfigClass(fileName = "DataStore")
public class DataStoreConfig extends ConfigPropertiesLoader {
	@ConfigGroupBeginning(name = "DataStore", comment = {
			"Also: if settings not configured - manager apply connection with default arguments."
	})
	@ConfigField(name = "data.store.redis.host", value = "redis://192.168.133.128", comment = {
			"Non required prop.",
			"Data storage host of redis server database.",
			"Default: 127.0.0.1"
	}, required = false, reloadable = false)
	public static String DATA_STORE_HOST;

	@ConfigField(name = "data.store.redis.port", value = "6379", comment = {
			"Non required prop.",
			"Data storage collection of redis node hosts.",
			"Example: 6379"
	}, required = false, reloadable = false)
	public static int DATA_STORE_PORT;

	@ConfigField(name = "data.store.redis.cluster.hosts", value = "", comment = {
			"Non required prop.",
			"Data storage host of redis servers database.",
			"Default: redis://127.0.0.1:6379, redis://127.0.0.2:6379"
	}, required = false, reloadable = false)
	public static String[] DATA_STORE_CLUSTER_HOSTS;

	@ConfigField(name = "data.store.redis.connection.timeout", value = "60000", comment = {
			"Non required prop.",
			"Data storage connection timeout to server node in millis.",
			"Default: 30000"
	}, required = false, reloadable = false)
	public static long DATA_STORE_CONNECTION_TIMEOUT = 30000;

	@ConfigField(name = "data.store.redis.user", value = "", comment = {
			"Non required prop.",
			"Data storage user name.",
			"Default: admin"
	}, required = false, reloadable = false)
	public static String DATA_STORE_USER;

	@ConfigField(name = "data.store.redis.password", value = "", comment = {
			"Non required prop.",
			"Data storage user password.",
			"Default: admin"
	}, required = false, reloadable = false)
	public static String DATA_STORE_PASSWORD;
}
