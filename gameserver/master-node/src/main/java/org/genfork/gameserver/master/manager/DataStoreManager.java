package org.genfork.gameserver.master.manager;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.genfork.tools.loader.annotations.InstanceGetter;
import org.genfork.tools.loader.annotations.Load;
import org.genfork.tools.loader.interfaces.PreLoadGroup;
import org.redisson.Redisson;
import org.redisson.api.RFuture;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;

import java.io.Serializable;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import static org.genfork.gameserver.master.config.DataStoreConfig.*;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Slf4j
public class DataStoreManager {
	@Getter(lazy = true, onMethod = @__({@InstanceGetter}))
	private static final DataStoreManager instance = new DataStoreManager();

	private static final int DEFAULT_REDIS_PORT = 6379;

	private RedissonClient client;

	@Load(group = PreLoadGroup.class)
	public void load() {
		final Config config = new Config();

		final String[] dataStoreClusterHosts = DATA_STORE_CLUSTER_HOSTS;
		final String dataStoreHost = DATA_STORE_HOST;
		if (dataStoreClusterHosts != null && dataStoreClusterHosts.length > 0) {
			config.useClusterServers().addNodeAddress(dataStoreClusterHosts);
			client = Redisson.create(config);
		} else if (StringUtils.isNotEmpty(dataStoreHost)) {
			final int dataStorePort = DATA_STORE_PORT;
			config.useSingleServer().setAddress(dataStoreHost + ":" + (dataStorePort > 0 ? dataStorePort : DEFAULT_REDIS_PORT));
			client = Redisson.create(config);
		} else {
			config.useSingleServer().setAddress("127.0.0.1:6379");
			client = Redisson.create(config);
		}
	}

	public <O extends Serializable> RMap<String, O> getMap(String bucketName) {
		return client.getMap(bucketName);
	}

	public <O extends Serializable> void addData(String bucketName, String key, O object) {
		if (client != null) {
			final RMap<String, O> rmap = client.getMap(bucketName);
			final RFuture<Boolean> orFuture = rmap.fastPutAsync(key, object);
			sendDataAndCheck(bucketName, key, orFuture);
		}
	}

	private <O extends Serializable> void sendDataAndCheck(String bucketName, String key, RFuture<O> future) {
		try {
			final O result = future.get();
			if (log.isDebugEnabled()) {
				log.debug("Data {} - bucket : {}, key : {}, object : {}", result != null ? "sent" : "not sent", bucketName, key, result);
			}
		} catch (InterruptedException | ExecutionException e) {
			log.error("Cant write data to redis node, client throws exception: {}", e.getLocalizedMessage());
			e.printStackTrace();
		}
	}

	public <O extends Serializable> O getData(String bucketName, String key) {
		if (client != null) {
			final RMap<String, O> rmap = client.getMap(bucketName);
			final RFuture<O> async = rmap.getAsync(key);
			return tryReceiveData(bucketName, key, async);
		}

		return null;
	}

	public <O extends Serializable> Map<String, O> getAllData(String bucketName) {
		if (client != null) {
			final RMap<String, O> rmap = client.getMap(bucketName);
			final Set<String> keys = rmap.readAllKeySet();
			final RFuture<Map<String, O>> hget = rmap.getAllAsync(keys);
			return tryAllReceiveData(bucketName, hget);
		}

		return Collections.emptyMap();
	}

	public Set<String> getKeys(String bucketName) {
		if (client != null) {
			final RMap<String, Serializable> rmap = client.getMap(bucketName);
			final RFuture<Set<String>> future = rmap.readAllKeySetAsync();
			return tryAllReceiveKeys(bucketName, future);
		}

		return Collections.emptySet();
	}

	public <O extends Serializable> void clearData(String bucketName, String key) {
		if (client != null) {
			final RMap<String, Serializable> rmap = client.getMap(bucketName);
			rmap.fastRemoveAsync(key);
		}
	}

	public <O extends Serializable> void clearAllData(String bucketName) {
		if (client != null) {
			final RMap<String, Serializable> rmap = client.getMap(bucketName);
			rmap.clear();
		}
	}

	private Set<String> tryAllReceiveKeys(String bucketName, RFuture<Set<String>> hget) {
		try {
			final Set<String> result = hget.get();
			if (log.isDebugEnabled()) {
				log.debug("Received All Keys - bucket : {}, object : {}", bucketName, result);
			}

			return result;
		} catch (InterruptedException | ExecutionException e) {
			log.error("Cant receiving data from redis node, client throws exception: {}", e.getLocalizedMessage());
			e.printStackTrace();
		}

		return null;
	}

	private <O extends Serializable> Map<String, O> tryAllReceiveData(String bucketName, RFuture<Map<String, O>> hget) {
		try {
			final Map<String, O> result = hget.get();
			if (log.isDebugEnabled()) {
				log.debug("Received All Data - bucket : {}, object : {}", bucketName, result);
			}

			return result;
		} catch (InterruptedException | ExecutionException e) {
			log.error("Cant receiving data from redis node, client throws exception: {}", e.getLocalizedMessage());
			e.printStackTrace();
		}

		return null;
	}

	private <O extends Serializable> O tryReceiveData(String bucketName, String key, RFuture<O> hget) {
		try {
			final O result = hget.get();
			if (log.isDebugEnabled()) {
				log.debug("Received Data - bucket : {}, key : {}, object : {}", bucketName, key, result);
			}

			return result;
		} catch (InterruptedException | ExecutionException e) {
			log.error("Cant receiving data from redis node, client throws exception: {}", e.getLocalizedMessage());
			e.printStackTrace();
		}

		return null;
	}

	public void shutdown() {
		log.info("Trying shutdown data store client...");

		if (client != null) {
			client.shutdown();
		}
	}
}
