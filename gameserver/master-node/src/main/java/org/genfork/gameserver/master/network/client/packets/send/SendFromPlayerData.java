package org.genfork.gameserver.master.network.client.packets.send;

import io.netty.buffer.ByteBuf;
import lombok.RequiredArgsConstructor;
import org.genfork.tools.network.OutgoingPacket;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@RequiredArgsConstructor
public class SendFromPlayerData extends OutgoingPacket {
	private final ByteBuf byteBuf;

	@Override
	public boolean write() {
		getByteBuf().writeBytes(byteBuf);
		byteBuf.clear();
		return true;
	}
}
