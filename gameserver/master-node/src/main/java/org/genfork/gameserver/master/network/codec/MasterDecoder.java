package org.genfork.gameserver.master.network.codec;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import lombok.RequiredArgsConstructor;
import org.genfork.gameserver.master.network.client.ClientHandler;
import org.genfork.gameserver.master.network.client.packets.recv.player.ProtocolVersion;
import org.genfork.gameserver.master.network.client.packets.recv.player.RequestGameAuthPacket;
import org.genfork.gameserver.master.network.client.packets.recv.system.RequestRetransmitDataToSlavePacket;
import org.genfork.tools.network.IncomingPacket;

import java.util.List;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@RequiredArgsConstructor
public class MasterDecoder extends ByteToMessageDecoder {
	private final ClientHandler client;

	@Override
	protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
		if (in == null || !in.isReadable()) {
			return;
		}

		try {
			IncomingPacket<ClientHandler> packet;
			final short opcode = in.readUnsignedByte();
			if (opcode == 0x0E) {
				packet = new ProtocolVersion();
			} else if (opcode == 0x2B) {
				packet = new RequestGameAuthPacket();
			} else {
				in.resetReaderIndex();
				packet = new RequestRetransmitDataToSlavePacket();
			}

			packet.setByteBuf(in);

			if (packet.read(client)) {
				out.add(packet);
			}
		} finally {
			in.readerIndex(in.writerIndex());
		}
	}
}
