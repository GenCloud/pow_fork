package org.genfork.gameserver.master.network.client.packets.send.player;

import lombok.RequiredArgsConstructor;
import org.genfork.gameserver.master.config.ServerConfig;
import org.genfork.tools.network.OutgoingPacket;

import java.util.stream.IntStream;

import static org.genfork.tools.network.opcodes.GameBranchOpcodes.VERSION_CHECK;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@RequiredArgsConstructor
public class VersionCheckPacket extends OutgoingPacket {
	private final byte[] key;
	private final int result;

	@Override
	public boolean write() {
		VERSION_CHECK.writeId(this);

		writeC(result); // 0 - wrong protocol, 1 - protocol ok
		// key
		IntStream.range(0, 8).map(i -> key[i]).forEach(this::writeC);

		writeD(0x01);
		writeD(ServerConfig.PLAYER_SERVER_ID); // server id
		writeC(0x01);
		writeD(0x00); // obfuscation key
		writeC((ServerConfig.PLAYER_SERVER_TYPE.getMask() & 0x400) == 0x400 ? 0x01 : 0x00); // isClassic

		return true;
	}
}
