package org.genfork.gameserver.master.network.client.packets.send.slave;

import lombok.RequiredArgsConstructor;
import org.genfork.tools.network.OutgoingPacket;

import static org.genfork.tools.network.opcodes.ClientsBranchOpcodes.TRY_AUTH_PACKET;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@RequiredArgsConstructor
public class TryAuthPacket extends OutgoingPacket {
	private final String accountName;

	@Override
	public boolean write() {
		TRY_AUTH_PACKET.writeId(this);

		writeS(accountName);
		return true;
	}
}
