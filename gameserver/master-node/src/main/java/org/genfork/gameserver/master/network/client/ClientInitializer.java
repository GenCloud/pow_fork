package org.genfork.gameserver.master.network.client;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.MessageToMessageEncoder;
import org.genfork.gameserver.master.network.codec.MasterDecoder;
import org.genfork.tools.network.codecs.CryptCodec;
import org.genfork.tools.network.codecs.PacketEncoder;

import java.nio.ByteOrder;
import java.util.List;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class ClientInitializer extends ChannelInitializer<SocketChannel> {
	private static final PacketEncoder PACKET_ENCODER = new PacketEncoder(32768 - 2);

	@Override
	protected void initChannel(SocketChannel ch) {
		final ClientHandler clientHandler = new ClientHandler();
		ch.pipeline().addLast("length-decoder", new LengthFieldBasedFrameDecoder(ByteOrder.LITTLE_ENDIAN, 32768 - 2, 0, 2, -2, 2, false));
		ch.pipeline().addLast("length-encoder", new MessageToMessageEncoder<ByteBuf>() {
			@Override
			protected void encode(ChannelHandlerContext ctx, ByteBuf msg, List<Object> out) throws Exception {
				final ByteBuf buf = ctx.alloc().buffer(2);
				final short length = (short) (msg.readableBytes() + 2);
				buf.writeShortLE(length);
				out.add(buf);
				out.add(msg.retain());
			}
		});

		ch.pipeline().addLast("crypt-codec", new CryptCodec(clientHandler.getClientCrypt()));
//		ch.pipeline().addLast(new LoggingHandler(LogLevel.INFO));
		ch.pipeline().addLast("packet-decoder", new MasterDecoder(clientHandler));
		ch.pipeline().addLast("packet-encoder", PACKET_ENCODER);
		ch.pipeline().addLast(clientHandler);
	}
}
