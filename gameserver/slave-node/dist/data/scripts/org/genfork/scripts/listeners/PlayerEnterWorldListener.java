package org.genfork.scripts.listeners;

import org.genfork.gameserver.slave.data.controller.actor.ActorItemContainerDataController;
import org.genfork.gameserver.slave.data.controller.actor.ActorStatController;
import org.genfork.gameserver.slave.data.controller.player.PlayerBroadcastController;
import org.genfork.gameserver.slave.data.controller.world.WorldObjectController;
import org.genfork.gameserver.slave.data.controller.world.WorldObjectsStorageController;
import org.genfork.gameserver.slave.data.repositories.GlobalListenerStoreRepository;
import org.genfork.gameserver.slave.managers.DataStoreManager;
import org.genfork.gameserver.slave.managers.dispatcher.EventDispatcherManager;
import org.genfork.gameserver.slave.model.scripts.annotation.GameScript;
import org.genfork.gameserver.slave.network.client.send.player.info.UserInfo;
import org.genfork.server.dispatcher.events.OnPlayerEnterWorld;
import org.genfork.server.dispatcher.listeners.ConsumerEventListener;
import org.genfork.server.objects.Player;
import org.genfork.server.objects.WorldObject;
import org.genfork.server.objects.component.containers.ListenersContainer;
import org.genfork.tools.network.ChannelInboundHandler;
import org.genfork.tools.network.DiscoveryClients;
import org.genfork.tools.network.NetworkConnectionPool;
import org.genfork.tools.network.SessionInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.function.Consumer;

import static org.genfork.gameserver.slave.managers.dispatcher.EventDispatcherManager.GLOBAL_PLAYERS_CONTAINER_NAME;
import static org.genfork.server.enums.EventType.ON_PLAYER_ENTER_WORLD;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class PlayerEnterWorldListener {
	private static final Logger log = LoggerFactory.getLogger(PlayerEnterWorldListener.class);

	private PlayerEnterWorldListener() {
		final GlobalListenerStoreRepository repository = DataStoreManager.getInstance().getRepository(GlobalListenerStoreRepository.class);
		repository.lockAndAddListenerContainer(GLOBAL_PLAYERS_CONTAINER_NAME, value -> {
			final ListenersContainer playersContainer = EventDispatcherManager.getInstance().getPlayersContainer();

			final boolean exists = playersContainer.hasListener(ON_PLAYER_ENTER_WORLD);
			if (!exists) {
				final ConsumerEventListener listener = new ConsumerEventListener(playersContainer, ON_PLAYER_ENTER_WORLD,
						(Consumer<OnPlayerEnterWorld> & Serializable) PlayerEnterWorldListener::onPlayerEnterWorld, playersContainer);
				playersContainer.addListener(listener);
			}

			return playersContainer;
		});
	}

	@GameScript
	public static void main() {
		new PlayerEnterWorldListener();
	}

	private static void onPlayerEnterWorld(OnPlayerEnterWorld event) {
		SessionInfo sessionInfo = event.getSessionInfo();
		if (sessionInfo == null) {
			return;
		}

		final int[][] trace = event.getTrace();

		final String[] address = new String[5];
		for (int i = 0; i < 5; i++) {
			address[i] = trace[i][0] + "." + trace[i][1] + "." + trace[i][2] + "." + trace[i][3];
		}

		final WorldObject object = WorldObjectsStorageController.getInstance()
				.findObject(sessionInfo.getEnteredPlayerName());
		if (!object.isPlayer()) {
			return;
		}

		final Player player = object.asPlayer();

		final String playerName = player.getName();
		final ChannelInboundHandler<?> handler = NetworkConnectionPool.getInstance().getNextChannel(DiscoveryClients.GAME_SLAVE_NODE);
		if (handler != null) {
			if (log.isDebugEnabled()) {
				log.debug("Try receive player inventory data by player name - {}, clientId - {}", playerName, sessionInfo);
			}

			ActorItemContainerDataController.getInstance().tryPlayerReceiveInventoryInfo(playerName, sessionInfo, handler);
		}


		ActorStatController.getInstance().initActorStats(player);

		WorldObjectController.getInstance().spawnObject(player, player.getRealX(), player.getRealY(), player.getRealZ());
		PlayerBroadcastController.getInstance().broadcastPacket(player, new UserInfo(player));
	}
}
