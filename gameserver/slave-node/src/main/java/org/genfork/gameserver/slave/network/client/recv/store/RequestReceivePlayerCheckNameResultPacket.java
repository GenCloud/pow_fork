package org.genfork.gameserver.slave.network.client.recv.store;

import org.genfork.gameserver.slave.network.client.send.player.lobby.ExCheckPlayerNamePacket;
import org.genfork.gameserver.slave.network.discovery.DiscoveryServerHandler;
import org.genfork.tools.network.DiscoveryClients;
import org.genfork.tools.network.IncomingPacket;

import static org.genfork.gameserver.slave.network.client.recv.player.lobby.RequestPlayerNameCreatablePacket.NAME_ALREADY_EXISTS;
import static org.genfork.gameserver.slave.network.client.recv.player.lobby.RequestPlayerNameCreatablePacket.RESULT_OK;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class RequestReceivePlayerCheckNameResultPacket extends IncomingPacket<DiscoveryServerHandler> {
	private int code;

	@Override
	public boolean read(DiscoveryServerHandler client) {
		code = readD();
		return true;
	}

	@Override
	public void run(DiscoveryServerHandler client) throws Exception {
		if (code == 1) {
			client.sendPacket(DiscoveryClients.GAME_MASTER_NODE,
					getToClientName(),
					getSessionInfo(), new ExCheckPlayerNamePacket(NAME_ALREADY_EXISTS));
		} else {
			client.sendPacket(DiscoveryClients.GAME_MASTER_NODE,
					getToClientName(),
					getSessionInfo(), new ExCheckPlayerNamePacket(RESULT_OK));
		}
	}
}
