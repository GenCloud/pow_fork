package org.genfork.gameserver.slave.network.client.send.auth;

import lombok.RequiredArgsConstructor;
import org.genfork.tools.network.OutgoingPacket;

import static org.genfork.tools.network.opcodes.ClientsBranchOpcodes.REPLY_PLAY_GAME_PACKET;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@RequiredArgsConstructor
public class ReplyPlaySessionPacket extends OutgoingPacket {
	private final String playerClientId;
	private final int sessionId;
	private final short code;

	@Override
	public boolean write() {
		REPLY_PLAY_GAME_PACKET.writeId(this);

		writeS(playerClientId);
		writeD(sessionId);
		writeC(code);
		return true;
	}
}
