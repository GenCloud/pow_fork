package org.genfork.gameserver.slave.network.enums;

import org.genfork.gameserver.slave.network.discovery.DiscoveryServerHandler;
import org.genfork.gameserver.slave.network.discovery.packets.RequestClientReadData;
import org.genfork.tools.network.DiscoveryClients;
import org.genfork.tools.network.IConnectionState;
import org.genfork.tools.network.IIncomingPackets;
import org.genfork.tools.network.IncomingPacket;
import org.genfork.tools.network.opcodes.ClientsBranchOpcodes;
import org.genfork.tools.registry.network.recv.RequestDiscoveryServerHealthCheckPacket;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Supplier;

import static org.genfork.gameserver.slave.network.enums.ConnectionState.CONNECTED;


/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public enum DiscoveryServerIncomingPackets implements IIncomingPackets<DiscoveryServerHandler> {
	REQUEST_DISCOVERY_CLIENT_HEALTH_CHECK(ClientsBranchOpcodes.DISCOVERY_CLIENT_HEALTH_CHECK.getId1(), () -> new RequestDiscoveryServerHealthCheckPacket<>(DiscoveryClients.GAME_SLAVE_NODE), CONNECTED),

	REQUEST_RECEIVE_CLIENT_DATA_PACKET(ClientsBranchOpcodes.DISCOVERY_RETRANSMIT_DATA.getId1(), RequestClientReadData::new, CONNECTED),

	;
	public static final DiscoveryServerIncomingPackets[] PACKET_ARRAY;

	static {
		final short maxPacketId = (short) Arrays.stream(values()).mapToInt(IIncomingPackets::getPacketId).max().orElse(0);
		PACKET_ARRAY = new DiscoveryServerIncomingPackets[maxPacketId + 1];
		for (DiscoveryServerIncomingPackets incomingPacket : values()) {
			PACKET_ARRAY[incomingPacket.getPacketId()] = incomingPacket;
		}
	}

	private short packetId;
	private Supplier<IncomingPacket<DiscoveryServerHandler>> incomingPacketFactory;
	private Set<IConnectionState> connectionStates;

	DiscoveryServerIncomingPackets(int packetId, Supplier<IncomingPacket<DiscoveryServerHandler>> incomingPacketFactory, IConnectionState... connectionStates) {
		// packetId is an unsigned byte
		if (packetId > 0xFF) {
			throw new IllegalArgumentException("packetId must not be bigger than 0xFF");
		}

		this.packetId = (short) packetId;
		this.incomingPacketFactory = incomingPacketFactory != null ? incomingPacketFactory : () -> null;
		this.connectionStates = new HashSet<>(Arrays.asList(connectionStates));
	}

	@Override
	public int getPacketId() {
		return packetId;
	}

	@Override
	public IncomingPacket<DiscoveryServerHandler> newIncomingPacket() {
		return incomingPacketFactory.get();
	}

	@Override
	public Set<IConnectionState> getConnectionStates() {
		return connectionStates;
	}

	@Override
	public IIncomingPackets<DiscoveryServerHandler>[] getArray() {
		return PACKET_ARRAY;
	}
}
