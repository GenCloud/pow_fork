package org.genfork.gameserver.slave.managers.dispatcher;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.genfork.gameserver.slave.data.repositories.GlobalListenerStoreRepository;
import org.genfork.gameserver.slave.managers.DataStoreManager;
import org.genfork.server.dispatcher.interfaces.Event;
import org.genfork.server.dispatcher.listeners.AbstractEventListener;
import org.genfork.server.dispatcher.returns.AbstractEventReturn;
import org.genfork.server.objects.component.containers.ListenersContainer;
import org.genfork.tools.async.ThreadPool;

import java.util.Arrays;
import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.TimeUnit;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Slf4j
public class EventDispatcherManager {
	@Getter(lazy = true)
	private static final EventDispatcherManager instance = new EventDispatcherManager();

	public static final String GLOBAL_CONTAINER_NAME = "GLOBAL_CONTAINER";
	public static final String GLOBAL_PLAYERS_CONTAINER_NAME = "GLOBAL_PLAYERS_CONTAINER";

	private final GlobalListenerStoreRepository listenersRepository;

	private EventDispatcherManager() {
		listenersRepository = DataStoreManager.getInstance().getRepository(GlobalListenerStoreRepository.class);
	}

	public ListenersContainer getGlobalContainer() {
		ListenersContainer container = listenersRepository.getContainer(GLOBAL_CONTAINER_NAME);
		if (container == null) {
			listenersRepository.addListenerContainer(GLOBAL_CONTAINER_NAME, container = new ListenersContainer());
		}
		return container;
	}

	public ListenersContainer getPlayersContainer() {
		ListenersContainer container = listenersRepository.getContainer(GLOBAL_PLAYERS_CONTAINER_NAME);
		if (container == null) {
			listenersRepository.addListenerContainer(GLOBAL_PLAYERS_CONTAINER_NAME, container = new ListenersContainer());
		}
		return container;
	}

	public <T extends AbstractEventReturn> T notifyEvent(Event event) {
		return notifyEvent(event, null, null);
	}

	public <T extends AbstractEventReturn> T notifyEvent(Event event, Class<T> callbackClass) {
		return notifyEvent(event, null, callbackClass);
	}

	public <T extends AbstractEventReturn> T notifyEvent(Event event, ListenersContainer container) {
		return notifyEvent(event, container, null);
	}

	public <T extends AbstractEventReturn> T notifyEvent(Event event, ListenersContainer container, Class<T> callbackClass) {
		try {
			return getGlobalContainer().hasListener(event.getType()) || container != null && container.hasListener(event.getType()) ? notifyEventImpl(event, container, callbackClass) : null;
		} catch (Exception e) {
			log.warn("Couldn't notify event {}", event.getClass().getSimpleName(), e);
		}
		return null;
	}

	public void notifyEventAsync(Event event, ListenersContainer... containers) {
		if (event == null) {
			throw new NullPointerException("Event cannot be null!");
		}

		boolean hasListeners = getGlobalContainer().hasListener(event.getType());
		if (!hasListeners) {
			if (Arrays.stream(containers).anyMatch(container -> container.hasListener(event.getType()))) {
				hasListeners = true;
			}
		}

		if (hasListeners) {
			ThreadPool.execute(() -> notifyEventToMultipleContainers(event, containers, null));
		}
	}

	public void notifyEventAsyncDelayed(Event event, ListenersContainer container, long delay) {
		if (getGlobalContainer().hasListener(event.getType()) || container.hasListener(event.getType())) {
			ThreadPool.schedule(() -> notifyEvent(event, container, null), delay, TimeUnit.MILLISECONDS);
		}
	}

	public void notifyEventAsyncDelayed(Event event, ListenersContainer container, long delay, TimeUnit unit) {
		if (getGlobalContainer().hasListener(event.getType()) || container.hasListener(event.getType())) {
			ThreadPool.schedule(() -> notifyEvent(event, container, null), delay, unit);
		}
	}

	private <T extends AbstractEventReturn> T notifyEventToMultipleContainers(Event event, ListenersContainer[] containers, Class<T> callbackClass) {
		try {
			if (event == null) {
				throw new NullPointerException("Event cannot be null!");
			}

			T callback = null;
			if (containers != null) {
				// Local listeners container first.
				for (ListenersContainer container : containers) {
					if (callback == null || !callback.isAbort()) {
						callback = notifyToListeners(container.getListeners(event.getType()), event, callbackClass, callback);
					}
				}
			}

			// Global listener container.
			if (callback == null || !callback.isAbort()) {
				callback = notifyToListeners(getGlobalContainer().getListeners(event.getType()), event, callbackClass, callback);
			}

			return callback;
		} catch (Exception e) {
			log.warn("Couldn't notify event {}", Objects.requireNonNull(event).getClass().getSimpleName(), e);
		}
		return null;
	}

	private <T extends AbstractEventReturn> T notifyEventImpl(Event event, ListenersContainer container, Class<T> callbackClass) {
		if (event == null) {
			throw new NullPointerException("Event cannot be null!");
		}

		T callback = null;
		if (container != null) {
			callback = notifyToListeners(container.getListeners(event.getType()), event, callbackClass, callback);
		}

		if (callback == null || !callback.isAbort()) {
			callback = notifyToListeners(getGlobalContainer().getListeners(event.getType()), event, callbackClass, callback);
		}

		return callback;
	}

	private <T extends AbstractEventReturn> T notifyToListeners(Queue<AbstractEventListener> listeners, Event event, Class<T> returnBackClass, T callback) {
		for (AbstractEventListener listener : listeners) {
			try {
				final T rb = listener.executeEvent(event, returnBackClass);
				if (rb != null) {
					if (callback == null || rb.isOverride()) {
						callback = rb;
					} else if (rb.isAbort()) {
						break;
					}
				}
			} catch (Exception e) {
				log.warn("Exception during notification of event: {} listener: {}", event.getClass().getSimpleName(), listener.getClass().getSimpleName(), e);
			}
		}

		return callback;
	}
}
