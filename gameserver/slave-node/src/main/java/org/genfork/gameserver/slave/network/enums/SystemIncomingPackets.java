package org.genfork.gameserver.slave.network.enums;

import org.genfork.gameserver.slave.network.client.recv.auth.RequestTryPlayGamePacket;
import org.genfork.gameserver.slave.network.client.recv.master.RequestAuthPacket;
import org.genfork.gameserver.slave.network.client.recv.store.RequestReceiveAccountPlayersDataPacket;
import org.genfork.gameserver.slave.network.client.recv.store.RequestReceiveInventoryPlayerDataPacket;
import org.genfork.gameserver.slave.network.client.recv.store.RequestReceivePlayerCheckNameResultPacket;
import org.genfork.gameserver.slave.network.client.recv.store.RequestReceivePlayerCreationResultPacket;
import org.genfork.gameserver.slave.network.discovery.DiscoveryServerHandler;
import org.genfork.tools.network.IConnectionState;
import org.genfork.tools.network.IIncomingPackets;
import org.genfork.tools.network.IncomingPacket;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Supplier;

import static org.genfork.gameserver.slave.network.enums.ConnectionState.CONNECTED;
import static org.genfork.tools.network.opcodes.ClientsBranchOpcodes.*;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public enum SystemIncomingPackets implements IIncomingPackets<DiscoveryServerHandler> {
	REQUEST_TRY_PLAY_GAME_PACKET(PLAYER_TRY_PLAYER_GAME_PACKET.getId2(), RequestTryPlayGamePacket::new, CONNECTED),

	REQUEST_AUTH_PACKET(TRY_AUTH_PACKET.getId2(), RequestAuthPacket::new, CONNECTED),

	REQUEST_ACCOUNT_PLAYERS_RESULT_PACKET(RECEIVE_ACCOUNT_PLAYERS_RESULT_PACKET.getId2(), RequestReceiveAccountPlayersDataPacket::new, CONNECTED),

	REQUEST_RECEIVE_PLAYER_CREATION_PACKET(PLAYER_CREATION_RESULT_PACKET.getId2(), RequestReceivePlayerCreationResultPacket::new, CONNECTED),

	REQUEST_RECEIVE_PLAYER_CHECK_NAME_RESULT_PACKET(PLAYER_CHECK_NAME_RESULT_PACKET.getId2(), RequestReceivePlayerCheckNameResultPacket::new, CONNECTED),

	REQUEST_RECEIVE_PLAYER_INVENTORY_DATA_PACKET(PLAYER_TRY_RECEIVE_INVENTORY_DATA_RESULT_PACKET.getId2(), RequestReceiveInventoryPlayerDataPacket::new, CONNECTED),
	;


	public static final SystemIncomingPackets[] PACKET_ARRAY;

	static {
		final short maxPacketId = (short) Arrays.stream(values()).mapToInt(IIncomingPackets::getPacketId).max().orElse(0);
		PACKET_ARRAY = new SystemIncomingPackets[maxPacketId + 1];
		for (SystemIncomingPackets incomingPacket : values()) {
			PACKET_ARRAY[incomingPacket.getPacketId()] = incomingPacket;
		}
	}

	private short packetId;
	private Supplier<IncomingPacket<DiscoveryServerHandler>> incomingPacketFactory;
	private Set<IConnectionState> connectionStates;

	SystemIncomingPackets(int packetId, Supplier<IncomingPacket<DiscoveryServerHandler>> incomingPacketFactory, IConnectionState... connectionStates) {
		// packetId is an unsigned byte
		if (packetId > 0xFF) {
			throw new IllegalArgumentException("packetId must not be bigger than 0xFF");
		}

		this.packetId = (short) packetId;
		this.incomingPacketFactory = incomingPacketFactory != null ? incomingPacketFactory : () -> null;
		this.connectionStates = new HashSet<>(Arrays.asList(connectionStates));
	}

	@Override
	public int getPacketId() {
		return packetId;
	}

	@Override
	public IncomingPacket<DiscoveryServerHandler> newIncomingPacket() {
		return incomingPacketFactory.get();
	}

	@Override
	public Set<IConnectionState> getConnectionStates() {
		return connectionStates;
	}

	@Override
	public IIncomingPackets<DiscoveryServerHandler>[] getArray() {
		return PACKET_ARRAY;
	}
}
