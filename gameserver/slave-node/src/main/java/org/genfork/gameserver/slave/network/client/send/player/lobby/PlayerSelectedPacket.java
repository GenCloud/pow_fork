package org.genfork.gameserver.slave.network.client.send.player.lobby;

import lombok.RequiredArgsConstructor;
import org.genfork.server.model.lobby.LobbyInfo;
import org.genfork.tools.network.OutgoingPacket;

import static org.genfork.tools.network.opcodes.GameBranchOpcodes.CHARACTER_SELECTED;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@RequiredArgsConstructor
public class PlayerSelectedPacket extends OutgoingPacket {
	private final LobbyInfo lobbyInfo;
	private final int sessionId;

	@Override
	public boolean write() {
		CHARACTER_SELECTED.writeId(this);

		writeS(lobbyInfo.getPlayerName());
		writeD(lobbyInfo.getPlayerId());
		writeS(lobbyInfo.getTitle());
		writeD(sessionId);
		writeD(lobbyInfo.getClanId());
		writeD(0x00); // ??
		writeD(lobbyInfo.getSex());
		writeD(lobbyInfo.getRace());
		writeD(lobbyInfo.getClassId());
		writeD(0x01); // active ??
		writeD((int) lobbyInfo.getX());
		writeD((int) lobbyInfo.getY());
		writeD((int) lobbyInfo.getZ());
		writeF(lobbyInfo.getCurrentHp());
		writeF(lobbyInfo.getCurrentMp());
		writeQ(lobbyInfo.getCurrentSp());
		writeQ(lobbyInfo.getCurrentExp());
		writeD(lobbyInfo.getPlayerLevel());
		writeD(lobbyInfo.getReputation());
		writeD(lobbyInfo.getPk());
		writeD(0x00); //GameTimeManager.getInstance().getGameTimeInMinutesOfDay()
		writeD(0x00);
		writeD(lobbyInfo.getClassId());

		writeB(new byte[16]);

		writeD(0x00);
		writeD(0x00);
		writeD(0x00);
		writeD(0x00);

		writeD(0x00);

		writeD(0x00);
		writeD(0x00);
		writeD(0x00);
		writeD(0x00);

		writeB(new byte[28]);
		writeD(0x00);

		return true;
	}
}
