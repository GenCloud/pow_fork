package org.genfork.gameserver.slave.data.repositories;

import org.genfork.server.objects.Actor;
import org.genfork.server.objects.component.inventory.ItemContainerData;
import org.genfork.tools.persistence.annotations.DataMethod;
import org.genfork.tools.persistence.annotations.DataRepository;
import org.genfork.tools.persistence.annotations.DataValue;
import org.genfork.tools.persistence.annotations.ExpParam;
import org.genfork.tools.persistence.repository.CrudDataRepository;
import org.genfork.tools.persistence.repository.handler.OperationHandler;

import static org.genfork.tools.persistence.annotations.DataMethod.Type.READ;
import static org.genfork.tools.persistence.annotations.DataMethod.Type.WRITE;

/**
 * @author: ngolubenko@context-it.ru
 * @created: 2020/04
 */
@DataRepository(bucketName = "actor_inventory_store")
public interface ActorItemContainerRepository extends CrudDataRepository {
	@DataMethod(type = READ, keyExpression = "#actor.hashName(T(org.genfork.gameserver.slave.config.ServerConfig).PLAYER_SERVER_ID)")
	ItemContainerData getContainerData(@ExpParam("actor") Actor actor);

	@DataMethod(type = READ, keyExpression = "#actor.hashName(T(org.genfork.gameserver.slave.config.ServerConfig).PLAYER_SERVER_ID)")
	ItemContainerData lockAndGetContainerData(@ExpParam("actor") Actor actor);

	@DataMethod(type = READ, keyExpression = "#actor.hashName(T(org.genfork.gameserver.slave.config.ServerConfig).PLAYER_SERVER_ID)")
	ItemContainerData lockAndGetContainerData(@ExpParam("actor") Actor actor, OperationHandler<? extends ItemContainerData> operationHandler);

	@DataMethod(type = WRITE, keyExpression = "#actor.hashName(T(org.genfork.gameserver.slave.config.ServerConfig).PLAYER_SERVER_ID)")
	ItemContainerData lockAndModifyData(@ExpParam("actor") Actor actor, @DataValue ItemContainerData data, OperationHandler<? extends ItemContainerData> operationHandler);
}
