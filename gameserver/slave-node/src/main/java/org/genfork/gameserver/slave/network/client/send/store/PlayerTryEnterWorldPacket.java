package org.genfork.gameserver.slave.network.client.send.store;

import lombok.RequiredArgsConstructor;
import org.genfork.tools.network.OutgoingPacket;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@RequiredArgsConstructor
public class PlayerTryEnterWorldPacket extends OutgoingPacket {
	private final String accountName;
	private final int lobbySlot;

	@Override
	public boolean write() {

		return true;
	}
}
