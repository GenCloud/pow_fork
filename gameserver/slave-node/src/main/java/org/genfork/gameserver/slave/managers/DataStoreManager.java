package org.genfork.gameserver.slave.managers;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.genfork.tools.loader.annotations.InstanceGetter;
import org.genfork.tools.loader.annotations.Load;
import org.genfork.tools.loader.interfaces.PreLoadGroup;
import org.genfork.tools.persistence.manager.PersistenceManager;
import org.redisson.Redisson;
import org.redisson.api.RLock;
import org.redisson.api.RMap;
import org.redisson.config.Config;

import static org.genfork.gameserver.slave.config.DataStoreConfig.*;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Slf4j
public class DataStoreManager extends PersistenceManager {
	@Getter(lazy = true, onMethod = @__({@InstanceGetter}))
	private static final DataStoreManager instance = new DataStoreManager();

	private static final int DEFAULT_REDIS_PORT = 6379;

	@Load(group = PreLoadGroup.class)
	public void load() {
		final Config config = new Config();

		final String[] dataStoreClusterHosts = DATA_STORE_CLUSTER_HOSTS;
		final String dataStoreHost = DATA_STORE_HOST;

		if (dataStoreClusterHosts != null && dataStoreClusterHosts.length > 0) {
			config.useClusterServers().addNodeAddress(dataStoreClusterHosts);
			client = Redisson.create(config);
		} else if (StringUtils.isNotEmpty(dataStoreHost)) {
			final int dataStorePort = DATA_STORE_PORT;
			config.useSingleServer().setAddress(dataStoreHost + ":" + (dataStorePort > 0 ? dataStorePort : DEFAULT_REDIS_PORT));
			client = Redisson.create(config);
		} else {
			config.useSingleServer().setAddress("127.0.0.1:6379");
			client = Redisson.create(config);
		}
	}

	public RLock openLock(String bucketName, String key) {
		final RMap<Object, Object> map = client.getMap(bucketName);
		final RLock lock = map.getFairLock(key);
		lock.lockAsync();
		return lock;
	}

	public void shutdown() {
		log.info("Trying shutdown data store client...");

		if (client != null) {
			client.shutdown();
		}
	}
}
