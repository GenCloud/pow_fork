package org.genfork.gameserver.slave.network.client.send.player;

import org.genfork.gameserver.slave.network.client.components.IUpdateTypeComponent;
import org.genfork.tools.network.OutgoingPacket;

import java.util.Arrays;

public abstract class AbstractMaskPacket<T extends IUpdateTypeComponent> extends OutgoingPacket {
	protected static final byte[] DEFAULT_FLAG_ARRAY =
			{
					(byte) 0x80,
					0x40,
					0x20,
					0x10,
					0x08,
					0x04,
					0x02,
					0x01
			};

	protected abstract byte[] getMasks();

	protected void onNewMaskAdded(T component) {
	}

	@SafeVarargs
	public final void addComponentType(T... updateComponents) {
		Arrays
				.stream(updateComponents)
				.filter(component ->
						!containsMask(component))
				.forEach(component -> {
					addMask(component.getMask());
					onNewMaskAdded(component);
				});
	}

	protected void addMask(int mask) {
		getMasks()[mask >> 3] |= DEFAULT_FLAG_ARRAY[mask & 7];
	}

	public boolean containsMask(T component) {
		return containsMask(component.getMask());
	}

	public boolean containsMask(int mask) {
		return (getMasks()[mask >> 3] & DEFAULT_FLAG_ARRAY[mask & 7]) != 0;
	}

	public boolean containsMask(int masks, T type) {
		return (masks & type.getMask()) == type.getMask();
	}
}
