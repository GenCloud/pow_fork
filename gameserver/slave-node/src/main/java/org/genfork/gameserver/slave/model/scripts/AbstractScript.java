package org.genfork.gameserver.slave.model.scripts;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public abstract class AbstractScript extends ManagedScript {
	private final String LISTENERS_STORE = "script_listeners_store";
}
