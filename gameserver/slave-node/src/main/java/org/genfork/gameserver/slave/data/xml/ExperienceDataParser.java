package org.genfork.gameserver.slave.data.xml;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.genfork.gameserver.slave.config.ServerConfig;
import org.genfork.tools.loader.annotations.InstanceGetter;
import org.genfork.tools.loader.annotations.Load;
import org.genfork.tools.loader.interfaces.LoadGroup;
import org.genfork.tools.xml.XmlReader;
import org.jdom2.Element;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Slf4j
public class ExperienceDataParser implements XmlReader {
	@Getter(lazy = true, onMethod = @__(@InstanceGetter))
	private static final ExperienceDataParser instance = new ExperienceDataParser();

	private byte MAX_LEVEL;
	private byte MAX_PET_LEVEL;
	private long[] EXPERIENCE_TO_LEVEL;
	private double[] TRAINING_RATE;

	@Load(group = LoadGroup.class)
	public void load() {
		parseFile("data/player/ExperienceData.xml", ServerConfig.DATA_PACK_ROOT);

		log.info("Loaded {} levels.", EXPERIENCE_TO_LEVEL.length - 1);
		log.info("Max Player Level is: {}", (MAX_LEVEL - 1));
		log.info("Max Pet Level is: {}", (MAX_PET_LEVEL - 1));
	}

	@Override
	public void loadFile(Element rootElement) {
		MAX_LEVEL = (byte) (parseByte(rootElement, "max_level") + 1);
		MAX_PET_LEVEL = (byte) (parseByte(rootElement, "max_pet_level") + 1);
		EXPERIENCE_TO_LEVEL = new long[MAX_LEVEL + 1];
		TRAINING_RATE = new double[MAX_LEVEL + 1];

		forEach(rootElement, "experience", expNode -> {
			final byte level = parseByte(expNode, "level");
			if (level <= MAX_LEVEL) {
				EXPERIENCE_TO_LEVEL[level] = parseLong(expNode, "to_level");
				TRAINING_RATE[level] = parseDouble(expNode, "training_rate");
			}
		});
	}

	public int getLoadedElementsCount() {
		return EXPERIENCE_TO_LEVEL.length;
	}

	/**
	 * Gets the exp for level.
	 *
	 * @param level the level required.
	 * @return the experience points required to reach the given level.
	 */
	public long getExpForLevel(int level) {
		return EXPERIENCE_TO_LEVEL[level];
	}

	public double getTrainingRate(int level) {
		return TRAINING_RATE[level];
	}

	/**
	 * Gets the max level.
	 *
	 * @return the maximum level acquirable by a player.
	 */
	public byte getMaxLevel() {
		return MAX_LEVEL;
	}

	/**
	 * Gets the max pet level.
	 *
	 * @return the maximum level acquirable by a pet.
	 */
	public byte getMaxPetLevel() {
		return MAX_PET_LEVEL;
	}
}
