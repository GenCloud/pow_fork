package org.genfork.gameserver.slave.data.xml;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.genfork.gameserver.slave.config.PlayerConfig;
import org.genfork.gameserver.slave.config.ServerConfig;
import org.genfork.server.enums.ClassId;
import org.genfork.server.model.lobby.InventoryItemInfo;
import org.genfork.server.objects.component.items.ItemData;
import org.genfork.tools.loader.annotations.Dependency;
import org.genfork.tools.loader.annotations.InstanceGetter;
import org.genfork.tools.loader.annotations.Load;
import org.genfork.tools.loader.interfaces.LoadGroup;
import org.genfork.tools.xml.XmlReader;
import org.jdom2.Element;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.genfork.server.enums.LocationData.INVENTORY;
import static org.genfork.server.enums.LocationData.PAPER_DOLL;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Slf4j
public class InitialEquipmentDataParser implements XmlReader {
	@Getter(lazy = true, onMethod = @__(@InstanceGetter))
	private static final InitialEquipmentDataParser instance = new InitialEquipmentDataParser();

	private static final String NORMAL = "data/player/equipment/InitialEquipment.xml";
	private static final String EVENT = "data/player/equipment/InitialEquipmentEvent.xml";

	private final Map<ClassId, List<InventoryItemInfo>> equipment = new HashMap<>();

	@Load(group = LoadGroup.class, dependencies = {
			@Dependency(clazz = ItemDataParser.class)
	})
	private void load() throws Exception {
		equipment.clear();
		parseFile(PlayerConfig.INITIAL_EQUIPMENT_EVENT ? EVENT : NORMAL, ServerConfig.DATA_PACK_ROOT);
		log.info("Loaded {} Initial Equipment data.", equipment.size());
	}

	@Override
	public void loadFile(Element rootElement) {
		forEach(rootElement, "equipment", eq -> {
			final ClassId classId = ClassId.getClassId(parseInt(eq, "classId"));
			final List<InventoryItemInfo> equipList = new ArrayList<>();

			forEach(eq, "item", item -> {
				final int itemId = parseInt(item, "id");
				final ItemData data = ItemDataParser.getInstance().getData(itemId);
				if (data == null) {
					log.warn("Item not found with id: {}", itemId);
					return;
				}

				final long amount = parseLong(item, "count");
				final boolean equipped = parseBoolean(item, "equipped", false);
				final int enchant = parseInt(item, "enchant", 0);

				final InventoryItemInfo itemInfo = new InventoryItemInfo();
				itemInfo.setItemId(itemId);
				itemInfo.setAmount(amount);

				if (equipped) {
					itemInfo.setLocationData(PAPER_DOLL);
					itemInfo.setBodyPart(data.getBodyPart());
				} else {
					itemInfo.setLocationData(INVENTORY);
				}

				itemInfo.setEnchant(enchant);
				itemInfo.setTypeFirst(data.getTypeFirst());
				itemInfo.setTypeSecond(data.getTypeSecond());
				itemInfo.setReuseDelay(data.getReuseDelay());
				itemInfo.setLifeTime(data.getTime());

				equipList.add(itemInfo);
			});

			equipment.put(classId, equipList);
		});
	}

	public int getInitialEquipmentCount() {
		return equipment.size();
	}

	public List<InventoryItemInfo> getEquipmentList(ClassId classId) {
		return equipment.get(classId);
	}
}