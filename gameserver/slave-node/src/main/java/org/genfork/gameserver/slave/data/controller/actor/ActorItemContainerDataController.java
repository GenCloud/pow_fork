package org.genfork.gameserver.slave.data.controller.actor;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.genfork.gameserver.slave.data.controller.player.PlayerBroadcastController;
import org.genfork.gameserver.slave.data.controller.world.WorldObjectsStorageController;
import org.genfork.gameserver.slave.data.xml.ItemDataParser;
import org.genfork.gameserver.slave.network.client.send.player.inventory.ExQuestInventoryDataPacket;
import org.genfork.gameserver.slave.network.client.send.player.inventory.InventoryDataPacket;
import org.genfork.gameserver.slave.network.client.send.store.PlayerTryReceiveInventoryDataPacket;
import org.genfork.gameserver.slave.network.discovery.DiscoveryServerHandler;
import org.genfork.server.enums.EtcItemType;
import org.genfork.server.enums.WeaponType;
import org.genfork.server.model.lobby.InventoryItemInfo;
import org.genfork.server.objects.Actor;
import org.genfork.server.objects.Item;
import org.genfork.server.objects.Player;
import org.genfork.server.objects.WorldObject;
import org.genfork.server.objects.component.inventory.ItemContainerData;
import org.genfork.server.objects.component.inventory.PaperdollListener;
import org.genfork.tools.network.ChannelInboundHandler;
import org.genfork.tools.network.DiscoveryClients;
import org.genfork.tools.network.SessionInfo;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;

import static org.genfork.server.objects.component.inventory.InventoryConstants.*;
import static org.genfork.server.objects.component.items.ItemConstants.*;

/**
 * @author: ngolubenko@context-it.ru
 * @created: 2020/04
 */
@Slf4j
public class ActorItemContainerDataController extends AbstractItemContainerDataController {
	@Getter(lazy = true)
	private static final ActorItemContainerDataController instance = new ActorItemContainerDataController();

	public Item addItemWithoutLock(Item item, Player actor, ItemContainerData containerData) {
		Item olditem = getItemByItemIdWithoutLock(containerData, item.getItemId());

		// If stackable item is found in inventory just add to current quantity
		if (olditem != null && olditem.getItemData().isStackable()) {
			long count = item.getAmount();
			if (!validateCount(item.getItemId(), (olditem.getAmount() + count))) {
				return null;
			}

			olditem.changeCount(count);
			olditem.setLastChange(MODIFIED);

// 			And destroys the item
//			TODO: data-server changes
//			ItemData.getInstance().destroyItem(process, item, actor, reference);
//			item.updateDatabase();
			item = olditem;

// 			Updates database
//			float adenaRate = RatesConfig.RATE_DROP_AMOUNT_MULTIPLIER.getOrDefault(Inventory.ADENA_ID, 1f);
//			if ((item.getId() == Inventory.ADENA_ID) && (count < (10000 * adenaRate)))
//			{
//				// Small adena changes won't be saved to database all the time
//				if ((GameTimeManager.getInstance().getGameTicks() % 5) == 0)
//				{
//					item.updateDatabase();
//				}
//			}
//			else
//			{
//				item.updateDatabase();
//			}
		}
		// If item hasn't be found in inventory, create new one
		else {
			item.setOwnerId(actor.getObjectId());
			item.setLocationData(containerData.getBaseLocation());
			item.setLastChange(ADDED);

			// Add item in inventory
			addItemWithoutLock(containerData, item);

// 			Updates database
//			TODO: data-server changes
//			item.updateDatabase();
		}

		refreshWeightWithoutLock(containerData);
		return item;
	}

	private boolean validateCount(int itemId, long count) {
//		TODO: config allowed count
		return count >= 0 && count <= (itemId == ADENA_ID ? Long.MAX_VALUE : Integer.MAX_VALUE);
	}

	/**
	 * Gets the items in paperdoll slots filtered by filter.
	 *
	 * @param filters multiple filters
	 * @return the filtered items in inventory
	 */
	@SafeVarargs
	public final Collection<Item> getPaperdollItems(Actor actor, Predicate<Item>... filters) {
		Predicate<Item> filter = Objects::nonNull;
		for (Predicate<Item> additionalFilter : filters) {
			filter = filter.and(additionalFilter);
		}

		return getItems(actor, i -> i.getPaperdollIndex() != -1, filter);
	}

	/**
	 * @param slot the slot.
	 * @return the item in the paperdoll slot
	 */
	public Item getPaperdollItem(Actor actor, int slot) {
		return getItem(actor, i -> i.getPaperdollIndex() == slot);
	}

	/**
	 * @param slot the slot.
	 * @return the item in the paperdoll slot
	 */
	public Item getPaperdollItem(ItemContainerData containerData, int slot) {
		return getItem(containerData, i -> i.getPaperdollIndex() == slot);
	}

	/**
	 * @param slot the slot.
	 * @return {@code true} if specified paperdoll slot is empty, {@code false} otherwise
	 */
	public boolean isPaperdollSlotEmpty(Actor actor, int slot) {
		return !hasItem(actor, i -> i.getPaperdollIndex() == slot);
	}

	public boolean isItemEquipped(Actor actor, int itemId) {
		return getItemsByItemId(actor, itemId).stream().anyMatch(Item::isEquipped);
	}

	/**
	 * Returns the ID of the item in the paperdoll slot
	 *
	 * @param slot : int designating the slot
	 * @return int designating the ID of the item
	 */
	public int getPaperdollItemId(Actor actor, int slot) {
		final Item item = getItem(actor, i -> i.getPaperdollIndex() == slot);
		if (item != null) {
			return item.getItemId();
		}
		return 0;
	}

	/**
	 * Returns the ID of the item in the paperdoll slot
	 *
	 * @param slot : int designating the slot
	 * @return int designating the ID of the item
	 */
	public int getPaperdollItemDisplayId(Actor actor, int slot) {
		final Item item = getItem(actor, i -> i.getPaperdollIndex() == slot);
		return item != null ? item.getItemData().getDisplayId() : 0;
	}

	/**
	 * Returns the visual id of the item in the paperdoll slot
	 *
	 * @param slot : int designating the slot
	 * @return int designating the ID of the item
	 */
	public int getPaperdollItemVisualId(Actor actor, int slot) {
		final Item item = getItem(actor, i -> i.getPaperdollIndex() == slot);
		return item != null ? item.getVisualId() : 0;
	}

	/**
	 * Returns the objectID associated to the item in the paperdoll slot
	 *
	 * @param slot : int pointing out the slot
	 * @return int designating the objectID
	 */
	public int getPaperdollObjectId(Actor actor, int slot) {
		final Item item = getItem(actor, i -> i.getPaperdollIndex() == slot);
		return item != null ? item.getObjectId() : 0;
	}

	/**
	 * Adds new inventory's paperdoll listener.
	 *
	 * @param listener the new listener
	 */
	public void addPaperdollListener(Actor actor, PaperdollListener listener) {
		actorItemContainerRepository.lockAndModifyData(actor, null, op -> {
			final ItemContainerData containerData = actorItemContainerRepository.getContainerData(actor);
			containerData.getPaperdollListeners().add(listener);
			return containerData;
		});
	}

	/**
	 * Removes a paperdoll listener.
	 *
	 * @param listener the listener to be deleted
	 */
	public void removePaperdollListener(Actor actor, PaperdollListener listener) {
		actorItemContainerRepository.lockAndModifyData(actor, null, op -> {
			final ItemContainerData containerData = actorItemContainerRepository.getContainerData(actor);
			containerData.getPaperdollListeners().remove(listener);
			return containerData;
		});
	}

	private void setPaperdollItem(ItemContainerData containerData, int slot, Item item) {
		final Item oldPaperdollItem = getPaperdollItem(containerData, slot);
		if (oldPaperdollItem != item) {
			if (oldPaperdollItem != null) {
				oldPaperdollItem.setPaperdollIndex(-1);
				// Put old item from paperdoll slot to base location
				oldPaperdollItem.setLocationData(containerData.getBaseLocation());
				oldPaperdollItem.setLastChange(MODIFIED);
				// Get the mask for paperdoll
				int mask = 0;
				for (int i = 0; i < PAPERDOLL_TOTALSLOTS; i++) {
					final Item pi = getPaperdollItem(containerData, i);
					if (pi != null) {
						mask |= pi.getItemData().getItemMask();
					}
				}

				containerData.setWearedMask(mask);

				// Notify all paperdoll listener in order to unequip old item in slot
				containerData.getPaperdollListeners()
						.stream()
						.filter(Objects::nonNull)
						.forEach(listener ->
								listener.notifyUnequiped(slot, oldPaperdollItem, containerData));

//					TODO: to data server
//					old.updateDatabase();
			}

			// Add new item in slot of paperdoll
			if (item != null) {
				item.setPaperdollIndex(slot);
				item.setLocationData(containerData.getEquipLocation(), slot);
				item.setLastChange(MODIFIED);

				containerData.setWearedMask(containerData.getWearedMask() | item.getItemData().getItemMask());
				containerData.getPaperdollListeners()
						.stream()
						.filter(Objects::nonNull)
						.forEach(listener ->
								listener.notifyEquiped(slot, item, containerData));
//					TODO: to data server
//					item.updateDatabase();
			}

//				if (getOwner().isPlayer()) {
//					getOwner().sendPacket(new ExUserInfoEquipSlot(getOwner().getActingPlayer()));
//				}
		}
	}

	/**
	 * Equips item in slot of paperdoll.
	 *
	 * @param item : Item designating the item and slot used.
	 */
	public void equipItem(Actor actor, Item item) {
		//		TODO: move one level up this conditions (handlers/managers/etc.) and rewrite this
//		if ((getOwner() instanceof PlayerInstance) && (((PlayerInstance) getOwner()).getPrivateStoreType() != PrivateStoreType.NONE)) {
//			return;
//		}
//
//		if (getOwner() instanceof PlayerInstance) {
//			PlayerInstance player = (PlayerInstance) getOwner();
//
//			if (!player.canOverrideCond(PcCondOverride.ITEM_CONDITIONS) && !player.isHero() && item.isHeroItem()) {
//				return;
//			}
//		}
//
//		// Check if player is using Formal Wear and item isn't Wedding Bouquet.
//		ItemInstance formal = getPaperdollItem(PAPERDOLL_CHEST);
//		if ((item.getId() != 21163) && (formal != null) && (formal.getItem().getBodyPart() == SLOT_ALLDRESS)) {
//			// only chest target can pass this
//			if (targetSlot == SLOT_LR_HAND || targetSlot == SLOT_L_HAND || targetSlot == SLOT_R_HAND || targetSlot == SLOT_LEGS || targetSlot == SLOT_FEET || targetSlot == SLOT_GLOVES || targetSlot == SLOT_HEAD) {
//				return;
//			}
//		}

		actorItemContainerRepository.lockAndModifyData(actor, null, op -> {
			final ItemContainerData containerData = actorItemContainerRepository.getContainerData(actor);
			final long targetSlot = item.getItemData().getBodyPart();
			// don't care about arrows, listener will unequip them (hopefully)
			// handle full armor
			if (targetSlot == SLOT_LR_HAND) {
				setPaperdollItem(containerData, PAPERDOLL_LHAND, null);
				setPaperdollItem(containerData, PAPERDOLL_RHAND, item);
			} else if (targetSlot == SLOT_L_HAND) {
				final Item rh = getPaperdollItem(containerData, PAPERDOLL_RHAND);
				if (rh != null && rh.getItemData().getBodyPart() == SLOT_LR_HAND
						&& !((rh.getItemData().getItemType() == WeaponType.BOW
						&& item.getItemData().getItemType() == EtcItemType.ARROW)
						|| ((rh.getItemData().getItemType() == WeaponType.CROSSBOW
						|| rh.getItemData().getItemType() == WeaponType.TWOHANDCROSSBOW)
						&& item.getItemData().getItemType() == EtcItemType.BOLT)
						|| (rh.getItemData().getItemType() == WeaponType.FISHINGROD
						&& item.getItemData().getItemType() == EtcItemType.LURE))) {
					setPaperdollItem(containerData, PAPERDOLL_RHAND, null);
				}
				setPaperdollItem(containerData, PAPERDOLL_LHAND, item);
			} else if (targetSlot == SLOT_R_HAND) {
				setPaperdollItem(containerData, PAPERDOLL_RHAND, item);
			} else if (targetSlot == SLOT_L_EAR || targetSlot == SLOT_R_EAR || targetSlot == SLOT_LR_EAR) {
				if (notExistsItem(containerData, i -> i.getPaperdollIndex() == PAPERDOLL_LEAR)) {
					setPaperdollItem(containerData, PAPERDOLL_LEAR, item);
				} else if (notExistsItem(containerData, i -> i.getPaperdollIndex() == PAPERDOLL_REAR)) {
					setPaperdollItem(containerData, PAPERDOLL_REAR, item);
				} else {
					setPaperdollItem(containerData, PAPERDOLL_LEAR, item);
				}
			} else if (targetSlot == SLOT_L_FINGER || targetSlot == SLOT_R_FINGER || targetSlot == SLOT_LR_FINGER) {
				if (notExistsItem(containerData, i -> i.getPaperdollIndex() == PAPERDOLL_LFINGER)) {
					setPaperdollItem(containerData, PAPERDOLL_LFINGER, item);
				} else if (notExistsItem(containerData, i -> i.getPaperdollIndex() == PAPERDOLL_RFINGER)) {
					setPaperdollItem(containerData, PAPERDOLL_RFINGER, item);
				} else {
					setPaperdollItem(containerData, PAPERDOLL_LFINGER, item);
				}
			} else if (targetSlot == SLOT_NECK) {
				setPaperdollItem(containerData, PAPERDOLL_NECK, item);
			} else if (targetSlot == SLOT_FULL_ARMOR) {
				setPaperdollItem(containerData, PAPERDOLL_LEGS, null);
				setPaperdollItem(containerData, PAPERDOLL_CHEST, item);
			} else if (targetSlot == SLOT_CHEST) {
				setPaperdollItem(containerData, PAPERDOLL_CHEST, item);
			} else if (targetSlot == SLOT_LEGS) {
				final Item chest = getPaperdollItem(containerData, PAPERDOLL_CHEST);
				if (chest != null && chest.getItemData().getBodyPart() == SLOT_FULL_ARMOR) {
					setPaperdollItem(containerData, PAPERDOLL_CHEST, null);
				}
				setPaperdollItem(containerData, PAPERDOLL_LEGS, item);
			} else if (targetSlot == SLOT_FEET) {
				setPaperdollItem(containerData, PAPERDOLL_FEET, item);
			} else if (targetSlot == SLOT_GLOVES) {
				setPaperdollItem(containerData, PAPERDOLL_GLOVES, item);
			} else if (targetSlot == SLOT_HEAD) {
				setPaperdollItem(containerData, PAPERDOLL_HEAD, item);
			} else if (targetSlot == SLOT_HAIR) {
				final Item hair = getPaperdollItem(containerData, PAPERDOLL_HAIR);
				if (hair != null && hair.getItemData().getBodyPart() == SLOT_HAIRALL) {
					setPaperdollItem(containerData, PAPERDOLL_HAIR2, null);
				} else {
					setPaperdollItem(containerData, PAPERDOLL_HAIR, null);
				}

				setPaperdollItem(containerData, PAPERDOLL_HAIR, item);
			} else if (targetSlot == SLOT_HAIR2) {
				final Item hair2 = getPaperdollItem(containerData, PAPERDOLL_HAIR);
				if (hair2 != null && hair2.getItemData().getBodyPart() == SLOT_HAIRALL) {
					setPaperdollItem(containerData, PAPERDOLL_HAIR, null);
				} else {
					setPaperdollItem(containerData, PAPERDOLL_HAIR2, null);
				}

				setPaperdollItem(containerData, PAPERDOLL_HAIR2, item);
			} else if (targetSlot == SLOT_HAIRALL) {
				setPaperdollItem(containerData, PAPERDOLL_HAIR2, null);
				setPaperdollItem(containerData, PAPERDOLL_HAIR, item);
			} else if (targetSlot == SLOT_UNDERWEAR) {
				setPaperdollItem(containerData, PAPERDOLL_UNDER, item);
			} else if (targetSlot == SLOT_BACK) {
				setPaperdollItem(containerData, PAPERDOLL_CLOAK, item);
			} else if (targetSlot == SLOT_L_BRACELET) {
				setPaperdollItem(containerData, PAPERDOLL_LBRACELET, item);
			} else if (targetSlot == SLOT_R_BRACELET) {
				setPaperdollItem(containerData, PAPERDOLL_RBRACELET, item);
			} else if (targetSlot == SLOT_BELT) {
				setPaperdollItem(containerData, PAPERDOLL_BELT, item);
			} else if (targetSlot == SLOT_ALLDRESS) { // formal dress
				setPaperdollItem(containerData, PAPERDOLL_LEGS, null);
				setPaperdollItem(containerData, PAPERDOLL_LHAND, null);
				setPaperdollItem(containerData, PAPERDOLL_RHAND, null);
				setPaperdollItem(containerData, PAPERDOLL_RHAND, null);
				setPaperdollItem(containerData, PAPERDOLL_LHAND, null);
				setPaperdollItem(containerData, PAPERDOLL_HEAD, null);
				setPaperdollItem(containerData, PAPERDOLL_FEET, null);
				setPaperdollItem(containerData, PAPERDOLL_GLOVES, null);
				setPaperdollItem(containerData, PAPERDOLL_CHEST, item);
			} else if (targetSlot == SLOT_BROOCH) {
				setPaperdollItem(containerData, PAPERDOLL_BROOCH, item);
			} else if (targetSlot == SLOT_ARTIFACT_BOOK) {
				setPaperdollItem(containerData, PAPERDOLL_ARTIFACT_BOOK, item);
			} else if (targetSlot == SLOT_DECO) {
//				TODO
//				equipTalisman(item);
			} else if (targetSlot == SLOT_BROOCH_JEWEL) {
//				equipBroochJewel(item);
			} else if (targetSlot == SLOT_AGATHION) {
//				equipAgathion(item);
			} else if (targetSlot == SLOT_ARTIFACT) {
//				equipArtifact(item);
			} else {
				log.warn("Unknown body slot {} for Item id: {}", targetSlot, item.getItemId());
			}

			return containerData;
		});
	}

	/**
	 * Unequips item in slot (i.e. equips with default value)
	 *
	 * @param slot : int designating the slot
	 * @return {@link Item} designating the item placed in the slot
	 */
	public void unEquipItemInBodySlot(Actor actor, long slot) {
		actorItemContainerRepository.lockAndModifyData(actor, null, op -> {
			final ItemContainerData containerData = actorItemContainerRepository.getContainerData(actor);
			int pdollSlot = -1;

			if (slot == SLOT_L_EAR) {
				pdollSlot = PAPERDOLL_LEAR;
			} else if (slot == SLOT_R_EAR) {
				pdollSlot = PAPERDOLL_REAR;
			} else if (slot == SLOT_NECK) {
				pdollSlot = PAPERDOLL_NECK;
			} else if (slot == SLOT_R_FINGER) {
				pdollSlot = PAPERDOLL_RFINGER;
			} else if (slot == SLOT_L_FINGER) {
				pdollSlot = PAPERDOLL_LFINGER;
			} else if (slot == SLOT_HAIR) {
				pdollSlot = PAPERDOLL_HAIR;
			} else if (slot == SLOT_HAIR2) {
				pdollSlot = PAPERDOLL_HAIR2;
			} else if (slot == SLOT_HAIRALL) {
				setPaperdollItem(containerData, PAPERDOLL_HAIR, null);
				pdollSlot = PAPERDOLL_HAIR;
			} else if (slot == SLOT_HEAD) {
				pdollSlot = PAPERDOLL_HEAD;
			} else if (slot == SLOT_R_HAND || slot == SLOT_LR_HAND) {
				pdollSlot = PAPERDOLL_RHAND;
			} else if (slot == SLOT_L_HAND) {
				pdollSlot = PAPERDOLL_LHAND;
			} else if (slot == SLOT_GLOVES) {
				pdollSlot = PAPERDOLL_GLOVES;
			} else if (slot == SLOT_CHEST || slot == SLOT_ALLDRESS || slot == SLOT_FULL_ARMOR) {
				pdollSlot = PAPERDOLL_CHEST;
			} else if (slot == SLOT_LEGS) {
				pdollSlot = PAPERDOLL_LEGS;
			} else if (slot == SLOT_BACK) {
				pdollSlot = PAPERDOLL_CLOAK;
			} else if (slot == SLOT_FEET) {
				pdollSlot = PAPERDOLL_FEET;
			} else if (slot == SLOT_UNDERWEAR) {
				pdollSlot = PAPERDOLL_UNDER;
			} else if (slot == SLOT_L_BRACELET) {
				pdollSlot = PAPERDOLL_LBRACELET;
			} else if (slot == SLOT_R_BRACELET) {
				pdollSlot = PAPERDOLL_RBRACELET;
			} else if (slot == SLOT_DECO) {
				pdollSlot = PAPERDOLL_DECO1;
			} else if (slot == SLOT_BELT) {
				pdollSlot = PAPERDOLL_BELT;
			} else if (slot == SLOT_BROOCH) {
				pdollSlot = PAPERDOLL_BROOCH;
			} else if (slot == SLOT_BROOCH_JEWEL) {
				pdollSlot = PAPERDOLL_BROOCH_JEWEL1;
			} else if (slot == SLOT_AGATHION) {
				pdollSlot = PAPERDOLL_AGATHION1;
			} else if (slot == SLOT_ARTIFACT_BOOK) {
				pdollSlot = PAPERDOLL_ARTIFACT_BOOK;
			} else if (slot == SLOT_ARTIFACT) {
				pdollSlot = PAPERDOLL_ARTIFACT1;
			} else {
				log.info("Unhandled slot type: {}", slot);
			}

			if (pdollSlot >= 0) {
				setPaperdollItem(containerData, pdollSlot, null);
			}

			return containerData;
		});
	}

	public void tryPlayerReceiveInventoryInfo(String playerName, SessionInfo sessionInfo, ChannelInboundHandler<?> client) {
		final PlayerTryReceiveInventoryDataPacket packet = new PlayerTryReceiveInventoryDataPacket(playerName);
		client.sendPacket(DiscoveryClients.DATA_STORE_NODE, DiscoveryClients.GAME_SLAVE_NODE, sessionInfo, packet);
	}

	public void restorePlayerInventoryInfo(String playerName, List<InventoryItemInfo> itemInfos) {
		final WorldObject object = WorldObjectsStorageController.getInstance()
				.findObject(playerName);
		if (object.isPlayer()) {
			final Player player = object.asPlayer();

			actorItemContainerRepository.lockAndModifyData(player, null, op -> {
				ItemContainerData containerData = actorItemContainerRepository.getContainerData(player);
				if (containerData == null) {
					containerData = new ItemContainerData();
				}

				for (InventoryItemInfo itemInfo : itemInfos) {
					final Item item = itemInfo.mapItem();
					item.setItemData(ItemDataParser.getInstance().getData(item.getItemId()));

					addItemWithoutLock(item, player, containerData);
				}
				return containerData;
			});
		}
	}

	@Override
	protected void refreshWeightWithoutLock(ItemContainerData containerData) {
		try {
			containerData.setTotalWeight(Math.toIntExact(containerData.getItems().values()
					.stream()
					.filter(Objects::nonNull)
					.mapToLong(Item::getTotalWeight)
					.reduce(Math::addExact)
					.orElse(0L)));
		} catch (ArithmeticException ae) {
			containerData.setTotalWeight(Integer.MAX_VALUE);
		}
	}

	@Override
	public void sendItemList(String enteredPlayerName, DiscoveryServerHandler client) {
		final WorldObject object = WorldObjectsStorageController.getInstance()
				.findObject(enteredPlayerName);
		if (object != null && object.isPlayer()) {
			final Player player = object.asPlayer();
			actorItemContainerRepository.lockAndGetContainerData(player, itemContainerData -> {
				final Collection<Item> normalItems = ActorItemContainerDataController.getInstance().getItemsWithoutLock(itemContainerData, i -> !i.getItemData().isQuestItem());
				final Collection<Item> questItems = ActorItemContainerDataController.getInstance().getItemsWithoutLock(itemContainerData, i -> i.getItemData().isQuestItem());

				final InventoryDataPacket inventoryDataFirst = new InventoryDataPacket(1, itemContainerData, normalItems);
				final InventoryDataPacket inventoryDataSecond = new InventoryDataPacket(2, itemContainerData, normalItems);

				final ExQuestInventoryDataPacket questInventoryDataFirst = new ExQuestInventoryDataPacket(1, itemContainerData, questItems);
				final ExQuestInventoryDataPacket questInventoryDataSecond = new ExQuestInventoryDataPacket(2, itemContainerData, questItems);

				PlayerBroadcastController.getInstance().sendPartPacket(player, inventoryDataFirst, inventoryDataSecond);
				PlayerBroadcastController.getInstance().sendPartPacket(player, questInventoryDataFirst, questInventoryDataSecond);
				log.info("sent count normal: {}, quest: {}", normalItems.size(), questItems.size());
				return itemContainerData;
			});
		}
	}

	@Override
	public void refreshWeight(Actor actor) {
		actorItemContainerRepository.lockAndModifyData(actor, null, op -> {
			final ItemContainerData containerData = actorItemContainerRepository.getContainerData(actor);
			try {
				containerData.setTotalWeight(Math.toIntExact(containerData.getItems().values()
						.stream()
						.filter(Objects::nonNull)
						.mapToLong(Item::getTotalWeight)
						.reduce(Math::addExact)
						.orElse(0L)));
			} catch (ArithmeticException ae) {
				containerData.setTotalWeight(Integer.MAX_VALUE);
			}

			return containerData;
		});
	}
}
