package org.genfork.gameserver.slave.network.discovery;

import lombok.Getter;
import org.genfork.gameserver.slave.async.EventLoopGroupData;
import org.genfork.gameserver.slave.config.ServerConfig;
import org.genfork.tools.loader.annotations.InstanceGetter;
import org.genfork.tools.loader.annotations.Load;
import org.genfork.tools.loader.interfaces.PostLoadGroup;
import org.genfork.tools.network.ClientNetworkController;
import org.genfork.tools.network.DiscoveryClients;
import org.genfork.tools.network.NetworkConnectionPool;

import java.util.stream.IntStream;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class DiscoveryClientNetworkController extends ClientNetworkController<DiscoveryServerHandler> {
	@Getter(lazy = true, onMethod = @__(@InstanceGetter))
	private static final DiscoveryClientNetworkController instance = new DiscoveryClientNetworkController();

	public DiscoveryClientNetworkController() {
		super(EventLoopGroupData.getInstance().getWorkerGroup(),
				new DiscoveryServerInitializer(),
				ServerConfig.DISCOVERY_SERVER_HOST,
				ServerConfig.DISCOVERY_SERVER_PORT);
	}

	@Load(group = PostLoadGroup.class)
	private void load() throws InterruptedException {
		IntStream.range(0, BASIC_NETWORK_POOL)
				.forEach(i -> NetworkConnectionPool.getInstance().initialize(DiscoveryClients.GAME_SLAVE_NODE, getBootstrap()));
	}
}
