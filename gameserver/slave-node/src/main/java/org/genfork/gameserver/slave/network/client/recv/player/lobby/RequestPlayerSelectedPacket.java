package org.genfork.gameserver.slave.network.client.recv.player.lobby;

import org.genfork.gameserver.slave.data.controller.lobby.LobbyDataController;
import org.genfork.gameserver.slave.managers.SessionManager;
import org.genfork.gameserver.slave.network.discovery.DiscoveryServerHandler;
import org.genfork.tools.network.IncomingPacket;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class RequestPlayerSelectedPacket extends IncomingPacket<DiscoveryServerHandler> {
	private int lobbySlot;

	@Override
	public boolean read(DiscoveryServerHandler client) {
		lobbySlot = readD();
		return true;
	}

	@Override
	public void run(DiscoveryServerHandler client) throws Exception {
		SessionManager.getInstance().getManagedSessionInfo(getSessionInfo(), value -> {
			LobbyDataController.getInstance().playerTryEnterWorld(value, getToClientName(), lobbySlot);
			return value;
		});
	}
}
