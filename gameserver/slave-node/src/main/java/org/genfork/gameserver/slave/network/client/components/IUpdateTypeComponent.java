package org.genfork.gameserver.slave.network.client.components;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public interface IUpdateTypeComponent {
	int getMask();
}
