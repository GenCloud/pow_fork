package org.genfork.gameserver.slave.managers;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.genfork.gameserver.slave.data.repositories.SessionStoreRepository;
import org.genfork.tools.network.SessionInfo;
import org.genfork.tools.persistence.repository.handler.OperationHandler;

import java.util.Map;
import java.util.function.Predicate;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Slf4j
public class SessionManager {
	@Getter(lazy = true)
	private static final SessionManager instance = new SessionManager();

	private final SessionStoreRepository repository;

	private SessionManager() {
		repository = DataStoreManager.getInstance().getRepository(SessionStoreRepository.class);
	}

	public void removeSession(SessionInfo sessionInfo) {
		repository.removeSession(sessionInfo);
	}

	public SessionInfo getManagedSessionInfo(Predicate<SessionInfo> p) {
		final Map<String, SessionInfo> all = repository.getAll();
		return all.values().stream().filter(p).findFirst().orElse(null);
	}

	public void getManagedSessionInfo(SessionInfo sessionInfo, OperationHandler<SessionInfo> operationHandler) {
		repository.getSession(sessionInfo, operationHandler);
	}

	public void addSession(SessionInfo sessionInfo) {
		repository.addSession(sessionInfo);
	}

	public void addSession(SessionInfo sessionInfo, OperationHandler<SessionInfo> operationHandler) {
		repository.addSession(sessionInfo, operationHandler);
	}
}
