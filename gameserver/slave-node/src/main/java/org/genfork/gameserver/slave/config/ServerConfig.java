package org.genfork.gameserver.slave.config;

import org.genfork.tools.config.abstraction.ConfigPropertiesLoader;
import org.genfork.tools.config.annotation.ConfigClass;
import org.genfork.tools.config.annotation.ConfigField;

import java.nio.file.Path;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@ConfigClass(fileName = "Server")
public class ServerConfig extends ConfigPropertiesLoader {
	@ConfigField(name = "SlaveServerHost", value = "127.0.0.1")
	public static String SLAVE_SERVER_HOST;

	@ConfigField(name = "SlaveServerPort", value = "8000")
	public static int SLAVE_SERVER_PORT;

	@ConfigField(name = "DiscoveryServerHost", value = "127.0.0.1")
	public static String DISCOVERY_SERVER_HOST;

	@ConfigField(name = "DiscoveryServerPort", value = "5000")
	public static int DISCOVERY_SERVER_PORT;

	@ConfigField(name = "DiscoveryRootClientPort", value = "9014")
	public static int DISCOVERY_ROOT_CLIENT_PORT;

	@ConfigField(name = "PlayerServerId", value = "3")
	public static short PLAYER_SERVER_ID;

	@ConfigField(name = "ServerKey", value = "{bcrypt}$2a$10$QLekRpQ7ib/LoQzaok7wBuNbLAklR5mQWMop8oH8k4PBmB8yCXHIO")
	public static String SERVER_PRIVATE_KEY;

	@ConfigField(name = "DataPackRoot", value = ".", comment =
			{
					"Datapack root directory.",
					"Defaults to current directory from which the server is started unless the below line is uncommented.",
					"WARNING: <u><b><font color='red'>If the specified path is invalid, it will lead to multiple errors!</font></b></u>"
			}, reloadable = false)
	public static Path DATA_PACK_ROOT;
}
