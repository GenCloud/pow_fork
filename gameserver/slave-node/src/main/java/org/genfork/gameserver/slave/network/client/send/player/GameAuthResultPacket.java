package org.genfork.gameserver.slave.network.client.send.player;

import lombok.RequiredArgsConstructor;
import org.genfork.tools.network.OutgoingPacket;

import static org.genfork.tools.network.opcodes.GameBranchOpcodes.LOGIN_RESULT;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@RequiredArgsConstructor
public class GameAuthResultPacket extends OutgoingPacket {
	public static final int NO_TEXT = 0;
	public static final int SYSTEM_ERROR_LOGIN_LATER = 1;
	public static final int PASSWORD_DOES_NOT_MATCH_THIS_ACCOUNT = 2;
	public static final int PASSWORD_DOES_NOT_MATCH_THIS_ACCOUNT2 = 3;
	public static final int ACCESS_FAILED_TRY_LATER = 4;
	public static final int INCORRECT_ACCOUNT_INFO_CONTACT_CUSTOMER_SUPPORT = 5;
	public static final int ACCESS_FAILED_TRY_LATER2 = 6;
	public static final int ACCOUNT_ALREADY_IN_USE = 7;
	public static final int ACCESS_FAILED_TRY_LATER3 = 8;
	public static final int ACCESS_FAILED_TRY_LATER4 = 9;
	public static final int ACCESS_FAILED_TRY_LATER5 = 10;

	public static final GameAuthResultPacket AUTH_SUCCESS = new GameAuthResultPacket(-1, NO_TEXT);

	private final int reason;
	private final int success;

	@Override
	public boolean write() {
		LOGIN_RESULT.writeId(this);

		writeD(success);
		writeD(reason);
		return true;
	}
}
