package org.genfork.gameserver.slave.network.client.send.player;

import org.genfork.tools.network.OutgoingPacket;

import static org.genfork.tools.network.opcodes.GameBranchOpcodes.SERVER_CLOSE;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class ServerClose extends OutgoingPacket {
	public static final ServerClose STATIC = new ServerClose();

	@Override
	public boolean write() {
		SERVER_CLOSE.writeId(this);
		return true;
	}
}
