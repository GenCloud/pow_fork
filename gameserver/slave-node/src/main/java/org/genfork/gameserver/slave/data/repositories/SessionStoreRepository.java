package org.genfork.gameserver.slave.data.repositories;

import org.genfork.tools.jmm.NotThreadSafe;
import org.genfork.tools.network.SessionInfo;
import org.genfork.tools.persistence.annotations.DataMethod;
import org.genfork.tools.persistence.annotations.DataRepository;
import org.genfork.tools.persistence.annotations.DataValue;
import org.genfork.tools.persistence.annotations.ExpParam;
import org.genfork.tools.persistence.repository.CrudDataRepository;
import org.genfork.tools.persistence.repository.handler.OperationHandler;

import java.util.Map;

import static org.genfork.tools.persistence.annotations.DataMethod.Type.*;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@DataRepository(bucketName = "sessions_store")
public interface SessionStoreRepository extends CrudDataRepository {
	@NotThreadSafe
	@DataMethod(type = WRITE, keyExpression = "#sessionInfo.toString()")
	void addSession(@ExpParam("sessionInfo") @DataValue SessionInfo sessionInfo);

	@DataMethod(type = WRITE, keyExpression = "#sessionInfo.toString()")
	void addSession(@ExpParam("sessionInfo") @DataValue SessionInfo sessionInfo, OperationHandler<SessionInfo> operationHandler);

	@DataMethod(type = READ, keyExpression = "#sessionInfo.toString()")
	SessionInfo getSession(@ExpParam("sessionInfo") SessionInfo sessionInfo, OperationHandler<SessionInfo> operationHandler);

	@DataMethod(type = READ_ALL)
	Map<String, SessionInfo> getAll();

	@DataMethod(type = REMOVE, keyExpression = "#sessionInfo.toString()")
	void removeSession(@ExpParam("sessionInfo") SessionInfo sessionInfo);
}
