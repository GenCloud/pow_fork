package org.genfork.gameserver.slave.network.client.recv.store;

import org.genfork.gameserver.slave.data.controller.actor.ActorItemContainerDataController;
import org.genfork.gameserver.slave.network.discovery.DiscoveryServerHandler;
import org.genfork.server.enums.LocationData;
import org.genfork.server.model.lobby.InventoryItemInfo;
import org.genfork.tools.network.IncomingPacket;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

/**
 * @author: ngolubenko@context-it.ru
 * @created: 2020/04
 */
public class RequestReceiveInventoryPlayerDataPacket extends IncomingPacket<DiscoveryServerHandler> {
	private final List<InventoryItemInfo> itemInfos = new ArrayList<>();
	private String playerName;

	@Override
	public boolean read(DiscoveryServerHandler client) {
		playerName = readS();

		final int inventorySize = readD();
		IntStream.range(0, inventorySize)
				.mapToObj(idx -> new InventoryItemInfo())
				.forEach(itemInfo -> {
					itemInfo.setItemObjId(readD());
					itemInfo.setItemPlayerId(readD());
					itemInfo.setItemId(readD());
					itemInfo.setAmount(readQ());
					itemInfo.setEnchant(readD());
					itemInfo.setTypeFirst(readD());
					itemInfo.setTypeSecond(readD());
					final int ordinal = readD();
					itemInfo.setLocationData(LocationData.values()[ordinal]);
					itemInfo.setLocationMask(readD());
					itemInfo.setUseTime(readD());
					itemInfo.setMpLeft(readE());
					itemInfo.setLifeTime(readE());
					itemInfo.setReuseDelay(readD());
					itemInfo.setSysTime(readQ());
					final int elementsSize = readD();
					final int[] elementTypes = IntStream.range(0, elementsSize).map(eidx -> readD()).toArray();
					itemInfo.setElementTypes(elementTypes);
					final int elementValSize = readD();
					final int[] elementValues = IntStream.range(0, elementValSize).map(eidx -> readD()).toArray();
					itemInfo.setElementTypes(elementValues);
					final int abilitiesSize = readD();
					final int[] abilityTypes = IntStream.range(0, abilitiesSize).map(eidx -> readD()).toArray();
					itemInfo.setAbilityTypes(abilityTypes);
					final int abilityIdsSize = readD();
					final int[] abilityIds = IntStream.range(0, abilityIdsSize).map(eidx -> readD()).toArray();
					itemInfo.setAbilityIds(abilityIds);
					final int abilitySlotsSize = readD();
					final int[] abilitySlots = IntStream.range(0, abilitySlotsSize).map(eidx -> readD()).toArray();
					itemInfo.setAbilitySlots(abilitySlots);
					final int variationSize = readD();
					final int[] mineralIds = IntStream.range(0, variationSize).map(eidx -> readD()).toArray();
					itemInfo.setMineralIds(mineralIds);
					final int options1Size = readD();
					final int[] optionsFirst = IntStream.range(0, options1Size).map(eidx -> readD()).toArray();
					itemInfo.setOptionsFirst(optionsFirst);
					final int options2Size = readD();
					final int[] optionsSecond = IntStream.range(0, options2Size).map(eidx -> readD()).toArray();
					itemInfo.setOptionsSecond(optionsSecond);
					itemInfos.add(itemInfo);
				});

		return true;
	}

	@Override
	public void run(DiscoveryServerHandler client) throws Exception {
		ActorItemContainerDataController.getInstance().restorePlayerInventoryInfo(playerName, itemInfos);
	}
}
