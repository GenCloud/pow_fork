package org.genfork.gameserver.slave.network.client.recv.player.lobby;

import org.apache.commons.lang3.StringUtils;
import org.genfork.gameserver.slave.config.PlayerConfig;
import org.genfork.gameserver.slave.network.client.send.player.lobby.ExCheckPlayerNamePacket;
import org.genfork.gameserver.slave.network.client.send.store.PlayerCheckNamePacket;
import org.genfork.gameserver.slave.network.discovery.DiscoveryServerHandler;
import org.genfork.tools.network.DiscoveryClients;
import org.genfork.tools.network.IncomingPacket;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class RequestPlayerNameCreatablePacket extends IncomingPacket<DiscoveryServerHandler> {
	public static int RESULT_OK = -1;
	public static int CHARACTER_CREATE_FAILED = 1;
	public static int NAME_ALREADY_EXISTS = 2;
	public static int INVALID_LENGTH = 3;
	public static int INVALID_NAME = 4;
	public static int CANNOT_CREATE_SERVER = 5;
	private String playerName;

	@Override
	public boolean read(DiscoveryServerHandler client) {
		playerName = readS();
		return true;
	}

	@Override
	public void run(DiscoveryServerHandler client) throws Exception {
		final int length = playerName.length();
		if (length < 1 || length > 16) {
			client.sendPacket(this, new ExCheckPlayerNamePacket(INVALID_LENGTH));
			return;
		}

		if (!StringUtils.isAlphanumeric(playerName) || !PlayerConfig.PLAYER_NAME_TEMPLATE_PATTERN.matcher(playerName).matches()) {
			client.sendPacket(this, new ExCheckPlayerNamePacket(INVALID_NAME));
			return;
		}

		final PlayerCheckNamePacket packet = new PlayerCheckNamePacket(playerName);
		client.sendPacket(DiscoveryClients.DATA_STORE_NODE, DiscoveryClients.GAME_SLAVE_NODE, getSessionInfo(), packet);
	}
}
