package org.genfork.gameserver.slave.model.scripts;

import lombok.extern.slf4j.Slf4j;
import org.genfork.gameserver.slave.data.xml.ScriptsDataManager;
import org.genfork.gameserver.slave.model.scripts.annotation.GameScript;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Slf4j
public abstract class ManagedScript {
	private long lastLoadTime;
	private boolean active;

	public ManagedScript() {
		setLastLoadTime(System.currentTimeMillis());
	}

	/**
	 * Attempts to reload this script and to refresh the necessary bindings with it ScriptControler.<BR>
	 * Subclasses of this class should override this method to properly refresh their bindings when necessary.
	 *
	 * @return true if and only if the script was reloaded, false otherwise.
	 */
	public boolean reload() {
		try {
			ScriptsDataManager.getInstance().runScript(getClass().getName(), GameScript.class);
			return true;
		} catch (Exception e) {
			log.warn("Failed to reload script!", e);
			return false;
		}
	}

	public abstract boolean unload();

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean status) {
		active = status;
	}

	/**
	 * @return Returns the lastLoadTime.
	 */
	protected long getLastLoadTime() {
		return lastLoadTime;
	}

	/**
	 * @param lastLoadTime The lastLoadTime to set.
	 */
	protected void setLastLoadTime(long lastLoadTime) {
		this.lastLoadTime = lastLoadTime;
	}

	public abstract String getScriptName();
}
