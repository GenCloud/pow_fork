package org.genfork.gameserver.slave.network.client.send.store;

import lombok.RequiredArgsConstructor;
import org.genfork.tools.network.OutgoingPacket;

import static org.genfork.tools.network.opcodes.ClientsBranchOpcodes.PLAYER_CHECK_NAME_PACKET;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@RequiredArgsConstructor
public class PlayerCheckNamePacket extends OutgoingPacket {
	private final String playerName;

	@Override
	public boolean write() {
		PLAYER_CHECK_NAME_PACKET.writeId(this);

		writeS(playerName);
		return true;
	}
}
