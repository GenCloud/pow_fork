package org.genfork.gameserver.slave.data.repositories;

import org.genfork.server.model.world.WorldRegion;
import org.genfork.tools.data.StatsSet;
import org.genfork.tools.persistence.annotations.DataMethod;
import org.genfork.tools.persistence.annotations.DataRepository;
import org.genfork.tools.persistence.annotations.DataValue;
import org.genfork.tools.persistence.annotations.ExpParam;
import org.genfork.tools.persistence.repository.CrudDataRepository;
import org.genfork.tools.persistence.repository.handler.OperationHandler;

import static org.genfork.tools.persistence.annotations.DataMethod.Type.READ;
import static org.genfork.tools.persistence.annotations.DataMethod.Type.WRITE;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@DataRepository(bucketName = "world_region_setting_store")
public interface WorldRegionStoreRepository extends CrudDataRepository {
	@DataMethod(type = READ, keyExpression = "#region.getRegionX() + '_' + #region.getRegionY() + '_' + #region.getRegionZ() + '_' + T(org.genfork.gameserver.slave.config.ServerConfig).PLAYER_SERVER_ID")
	StatsSet getRegionOptions(@ExpParam("region") WorldRegion region);

	@DataMethod(type = READ, keyExpression = "#region.getRegionX() + '_' + #region.getRegionY() + '_' + #region.getRegionZ() + '_' + T(org.genfork.gameserver.slave.config.ServerConfig).PLAYER_SERVER_ID")
	StatsSet getRegionOptions(@ExpParam("region") WorldRegion region, OperationHandler<StatsSet> operationHandler);

	@DataMethod(type = WRITE, keyExpression = "#region.getRegionX() + '_' + #region.getRegionY() + '_' + #region.getRegionZ() + '_' + T(org.genfork.gameserver.slave.config.ServerConfig).PLAYER_SERVER_ID")
	void updateOptions(@ExpParam("region") WorldRegion region, OperationHandler<StatsSet> operationHandler);

	@DataMethod(type = WRITE, keyExpression = "#region.getRegionX() + '_' + #region.getRegionY() + '_' + #region.getRegionZ() + '_' + T(org.genfork.gameserver.slave.config.ServerConfig).PLAYER_SERVER_ID")
	void updateOptions(@ExpParam("region") WorldRegion region, @DataValue StatsSet statsSet);
}
