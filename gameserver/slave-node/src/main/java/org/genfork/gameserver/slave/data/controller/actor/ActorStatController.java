package org.genfork.gameserver.slave.data.controller.actor;

import lombok.Getter;
import org.genfork.gameserver.slave.data.repositories.ActorStatRepository;
import org.genfork.gameserver.slave.managers.DataStoreManager;
import org.genfork.server.enums.*;
import org.genfork.server.model.pojo.StatsHolder;
import org.genfork.server.model.skills.Skill;
import org.genfork.server.objects.Actor;
import org.genfork.server.objects.Item;
import org.genfork.server.objects.component.stat.ActorStatData;
import org.genfork.server.stats.ActorStatValue;
import org.genfork.tools.common.MathUtil;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;

import static org.genfork.server.enums.ActorStat.*;

/**
 * @author: ngolubenko@context-it.ru
 * @created: 2020/04
 */
public class ActorStatController {
	@Getter(lazy = true)
	private static final ActorStatController instance = new ActorStatController();

	private final ActorStatRepository actorStatRepository;

	private ActorStatController() {
		actorStatRepository = DataStoreManager.getInstance().getRepository(ActorStatRepository.class);
	}

	public ActorStatData getActorStatData(Actor actor) {
		return actorStatRepository.getStatDataWithLock(actor);
	}

	/**
	 * @return the Accuracy (base+modifier) of the Actor in function of the Weapon Expertise Penalty.
	 */
	public int getPhysicalAccuracy(Actor actor) {
		return (int) getValue(actor, ACCURACY_PHYSICAL);
	}

	/**
	 * @return the Magic Accuracy (base+modifier) of the Actor
	 */
	public int getMagicAccuracy(Actor actor) {
		return (int) getValue(actor, ACCURACY_MAGICAL);
	}

	/**
	 * @return the CON of the Actor (base+modifier).
	 */
	public final int getCON(Actor actor) {
		return (int) getValue(actor, STAT_CON);
	}

	/**
	 * @return the Critical Damage rate (base+modifier) of the Actor.
	 */
	public final double getCriticalDmg(Actor actor, double init) {
		return getValue(actor, CRITICAL_DAMAGE, init);
	}

	/**
	 * @return the Critical Hit rate (base+modifier) of the Actor.
	 */
	public int getPhysicalCriticalRate(Actor actor) {
		return (int) getValue(actor, PHYSICAL_CRITICAL_RATE);
	}

	/**
	 * @return the DEX of the Actor (base+modifier).
	 */
	public final int getDEX(Actor actor) {
		return (int) getValue(actor, STAT_DEX);
	}

	/**
	 * @return the Attack Evasion rate (base+modifier) of the Actor.
	 */
	public int getPhysicalEvasionRate(Actor actor) {
		return (int) getValue(actor, PHYSICAL_EVASION_RATE);
	}

	/**
	 * @return the Attack Evasion rate (base+modifier) of the Actor.
	 */
	public int getMagicEvasionRate(Actor actor) {
		return (int) getValue(actor, MAGICAL_EVASION_RATE);
	}

	public int getMaxCp(Actor actor) {
		return (int) getValue(actor, MAX_CP);
	}

	public int getMaxRecoverableCp(Actor actor) {
		return (int) getValue(actor, MAX_RECOVERABLE_CP, getMaxCp(actor));
	}

	public int getMaxHp(Actor actor) {
		return (int) getValue(actor, MAX_HP);
	}

	public int getMaxRecoverableHp(Actor actor) {
		return (int) getValue(actor, MAX_RECOVERABLE_HP, getMaxHp(actor));
	}

	public int getMaxMp(Actor actor) {
		return (int) getValue(actor, MAX_MP);
	}

	public int getMaxRecoverableMp(Actor actor) {
		return (int) getValue(actor, MAX_RECOVERABLE_MP, getMaxMp(actor));
	}

	/**
	 * Return the MAtk (base+modifier) of the Actor.<br>
	 * <B><U>Example of use</U>: Calculate Magic damage
	 */
	public int getMagicalAttack(Actor actor) {
		return (int) getValue(actor, MAGIC_ATTACK);
	}

	/**
	 * @return the MAtk Speed (base+modifier) of the Actor in function of the Armour Expertise Penalty.
	 */
	public int getMagicalAttackSpeed(Actor actor) {
		return (int) getValue(actor, MAGIC_ATTACK_SPEED);
	}

	/**
	 * @return the Magic Critical Hit rate (base+modifier) of the Actor.
	 */
	public final int getMagicCriticalRate(Actor actor) {
		return (int) getValue(actor, MAGIC_CRITICAL_RATE);
	}

	/**
	 * <B><U>Example of use </U>: Calculate Magic damage.
	 *
	 * @return the MDef (base+modifier) of the Actor against a skill in function of abnormal effects in progress.
	 */
	public int getMagicalDefense(Actor actor) {
		return (int) getValue(actor, MAGICAL_DEFENCE);
	}

	/**
	 * @return the INT of the Creature (base+modifier).
	 */
	public int getINT(Actor actor) {
		return (int) getValue(actor, ActorStat.STAT_INT);
	}

	/**
	 * @return the MEN of the Actor (base+modifier).
	 */
	public final int getMEN(Actor actor) {
		return (int) getValue(actor, STAT_MEN);
	}

	public final int getLUC(Actor actor) {
		return (int) getValue(actor, STAT_LUC);
	}

	public final int getCHA(Actor actor) {
		return (int) getValue(actor, STAT_CHA);
	}

	public double getMovementSpeedMultiplier(Actor actor) {
		double baseSpeed;
//		if (actor.isInsideZone(ZoneId.WATER)) {
//			baseSpeed = actor.getTemplate().getBaseValue(actor.isRunning() ? SWIM_RUN_SPEED : SWIM_WALK_SPEED, 0);
//		} else
		{
			baseSpeed = actor.getData().getBaseValue(actor.isRunning() ? RUN_SPEED : WALK_SPEED, 0);
		}
		return getMoveSpeed(actor) * (1. / baseSpeed);
	}

	public double getMovementSpeedMultiplier(Actor actor, ActorStatData actorStatData) {
		double baseSpeed;
//		if (actor.isInsideZone(ZoneId.WATER)) {
//			baseSpeed = actor.getTemplate().getBaseValue(actor.isRunning() ? SWIM_RUN_SPEED : SWIM_WALK_SPEED, 0);
//		} else
		{
			baseSpeed = actor.getData().getBaseValue(actor.isRunning() ? RUN_SPEED : WALK_SPEED, 0);
		}
		return getMoveSpeed(actor, actorStatData) * (1. / baseSpeed);
	}

	/**
	 * @return the RunSpeed (base+modifier) of the Actor in function of the Armour Expertise Penalty.
	 */
	public double getRunSpeed(Actor actor) {
		return getValue(actor, /*actor.isInsideZone(ZoneId.WATER) ? SWIM_RUN_SPEED : */RUN_SPEED);
	}

	/**
	 * @return the WalkSpeed (base+modifier) of the Actor.
	 */
	public double getWalkSpeed(Actor actor) {
		return getValue(actor, /*actor.isInsideZone(ZoneId.WATER) ? SWIM_WALK_SPEED :*/ WALK_SPEED);
	}

	/**
	 * @return the SwimRunSpeed (base+modifier) of the Actor.
	 */
	public double getSwimRunSpeed(Actor actor) {
		return getValue(actor, SWIM_RUN_SPEED);
	}

	/**
	 * @return the SwimWalkSpeed (base+modifier) of the Actor.
	 */
	public double getSwimWalkSpeed(Actor actor) {
		return getValue(actor, SWIM_WALK_SPEED);
	}

	/**
	 * @return the RunSpeed (base+modifier) or WalkSpeed (base+modifier) of the Actor in function of the movement type.
	 */
	public double getMoveSpeed(Actor actor) {
//		TODO: implement areas
//		if (actor.isInsideZone(ZoneId.WATER)) {
//			return actor.isRunning() ? getSwimRunSpeed() : getSwimWalkSpeed();
//		}

		return actor.isRunning() ? getRunSpeed(actor) : getWalkSpeed(actor);
	}

	public double getMoveSpeed(Actor actor, ActorStatData actorStatData) {
//		TODO: implement areas
//		if (actor.isInsideZone(ZoneId.WATER)) {
//			return actor.isRunning() ? getSwimRunSpeed() : getSwimWalkSpeed();
//		}

		return actor.isRunning() ? getValue(actor, actorStatData, RUN_SPEED) : getValue(actor, actorStatData, WALK_SPEED);
	}

	/**
	 * @return the PAtk (base+modifier) of the Actor.
	 */
	public int getPhysicalAttack(Actor actor) {
		return (int) getValue(actor, PHYSICAL_ATTACK);
	}

	/**
	 * @return the PAtk Speed (base+modifier) of the Actor in function of the Armour Expertise Penalty.
	 */
	public int getPhysicalAttackSpeed(Actor actor) {
		return (int) getValue(actor, PHYSICAL_ATTACK_SPEED);
	}

	/**
	 * @return the PDef (base+modifier) of the Actor.
	 */
	public int getPhysicalDefense(Actor actor) {
		return (int) getValue(actor, PHYSICAL_DEFENCE);
	}

	/**
	 * @return the Physical Attack range (base+modifier) of the Actor.
	 */
	public final int getPhysicalAttackRange(Actor actor) {
		return (int) getValue(actor, PHYSICAL_ATTACK_RANGE);
	}

	public int getPhysicalAttackRadius(Actor actor) {
		return 40;
	}

	public int getPhysicalAttackAngle(Actor actor) {
		return 120;
	}

	/**
	 * @return the weapon reuse modifier.
	 */
	public final double getWeaponReuseModifier(Actor actor) {
		return getValue(actor, ATK_REUSE, 1);
	}

	/**
	 * @return the ShieldDef rate (base+modifier) of the Actor.
	 */
	public final int getShldDef(Actor actor) {
		return (int) getValue(actor, SHIELD_DEFENCE);
	}

	/**
	 * @return the STR of the Actor (base+modifier).
	 */
	public final int getSTR(Actor actor) {
		return (int) getValue(actor, STAT_STR);
	}

	/**
	 * @return the WIT of the Actor (base+modifier).
	 */
	public final int getWIT(Actor actor) {
		return (int) getValue(actor, STAT_WIT);
	}

	public AttributeType getAttackElement(Actor actor) {
//		TODO: elemental
//		final Item weaponInstance = actor.getInventory().getPaperdollItem(InventoryConstants.PAPERDOLL_RHAND);
//		// 1st order - weapon element
//		if (weaponInstance != null && weaponInstance.getAttackAttributeType() != AttributeType.NONE) {
//			return weaponInstance.getAttackAttributeType();
//		}

		//@formatter:off
		// Find the greatest attack element attribute greater than 0.
		return Arrays.stream(AttributeType.ATTRIBUTE_TYPES)
				.filter(a ->
						getAttackElementValue(actor, a) > 0)
				.max(Comparator.comparingInt(e -> getAttackElementValue(actor, e)))
				.orElse(AttributeType.NONE);
		//@formatter:on
	}

	public int getAttackElementValue(Actor actor, AttributeType attackAttribute) {
		switch (attackAttribute) {
			case FIRE:
				return (int) getValue(actor, FIRE_POWER);
			case WATER:
				return (int) getValue(actor, WATER_POWER);
			case WIND:
				return (int) getValue(actor, WIND_POWER);
			case EARTH:
				return (int) getValue(actor, EARTH_POWER);
			case HOLY:
				return (int) getValue(actor, HOLY_POWER);
			case DARK:
				return (int) getValue(actor, DARK_POWER);
			default:
				return 0;
		}
	}

	public int getDefenseElementValue(Actor actor, AttributeType defenseAttribute) {
		switch (defenseAttribute) {
			case FIRE:
				return (int) getValue(actor, FIRE_RES);
			case WATER:
				return (int) getValue(actor, WATER_RES);
			case WIND:
				return (int) getValue(actor, WIND_RES);
			case EARTH:
				return (int) getValue(actor, EARTH_RES);
			case HOLY:
				return (int) getValue(actor, HOLY_RES);
			case DARK:
				return (int) getValue(actor, DARK_RES);
			default:
				return (int) getValue(actor, BASE_ATTRIBUTE_RES);
		}
	}

	public void initActorStats(Actor actor) {
		actorStatRepository.addStatData(actor, new ActorStatData());
	}

	protected void resetStats(Actor actor) {
		actorStatRepository.lockAndModifyData(actor, null, data -> {
			final ActorStatData statData = actorStatRepository.getStatData(actor);
			statData.getDoubleStats().forEach((k, v) -> v.reset(k));

			statData.getBooleanStats().clear();
			statData.getBlockActionsAllowedSkills().clear();
			statData.getBlockActionsAllowedItems().clear();
			statData.getCastingStat().clear();
			statData.getAttackTraits().clear();
			statData.getDefenceTraits().clear();
			statData.getInvulnerableTraits().clear();
			statData.getMpConsumeStat().clear();
			statData.getReuseStat().clear();

			statData.setVampiricSum(0);
			statData.setMaxBuffCount(20);

			Arrays.fill(statData.getAttackTraitValues(), 1f);
			Arrays.fill(statData.getDefenceTraitValues(), 1f);
			return statData;
		});
	}

	/**
	 * @return the Attack Speed multiplier (base+modifier) of the Actor to get proper animations.
	 */
	public final double getAttackSpeedMultiplier(Actor actor) {
		return actorStatRepository.getStatData(actor).getAttackSpeedMultiplier();
	}

	public final double getMAttackSpeedMultiplier(Actor actor) {
		return actorStatRepository.getStatData(actor).getMAttackSpeedMultiplier();
	}

	/**
	 * Adds static value to the 'add' map of the stat everytime recalculation happens
	 */
	public boolean addAdditionalStat(Actor actor, ActorStat stat, double value, BiPredicate<Actor, StatsHolder> condition) {
		final StatsHolder statsHolder = StatsHolder.of(stat, value, condition);
		return setAddAdditionalStat(actor, statsHolder);
	}

	/**
	 * Adds static value to the 'add' map of the stat everytime recalculation happens
	 */
	public boolean addAdditionalStat(Actor actor, ActorStat stat, double value) {
		final StatsHolder statsHolder = StatsHolder.of(stat, value);
		return setAddAdditionalStat(actor, statsHolder);
	}

	private boolean setAddAdditionalStat(Actor actor, StatsHolder statsHolder) {
		final ActorStatData actorStatData = actorStatRepository.lockAndModifyData(actor, null, data -> {
			final ActorStatData statData = actorStatRepository.getStatData(actor);
			statData.getAdditionalAdd().add(statsHolder);
			return statData;
		});

		return actorStatData.getAdditionalAdd().stream().anyMatch(s -> s.equals(statsHolder));
	}

	/**
	 * @return {@code true} if 'add' was removed, {@code false} in case there wasn't such stat and value
	 */
	public boolean removeAddAdditionalStat(Actor actor, ActorStat stat, double value) {
		final ActorStatData actorStatData = actorStatRepository.lockAndModifyData(actor, null, data -> {
			final ActorStatData statData = actorStatRepository.getStatData(actor);
			final Iterator<StatsHolder> it = statData.getAdditionalAdd().iterator();
			while (it.hasNext()) {
				final StatsHolder holder = it.next();
				if (holder.getStat() == stat && holder.getValue() == value) {
					it.remove();
					break;
				}
			}

			return statData;
		});

		return actorStatData.getAdditionalAdd().stream().noneMatch(h -> h.getStat() == stat);
	}

	/**
	 * Adds static multiplier to the 'mul' map of the stat everytime recalculation happens
	 */
	public boolean mulAdditionalStat(Actor actor, ActorStat stat, double value, BiPredicate<Actor, StatsHolder> condition) {
		final StatsHolder statsHolder = StatsHolder.of(stat, value, condition);
		return setMulAdditionalStat(actor, statsHolder);
	}

	/**
	 * Adds static multiplier to the 'mul' map of the stat everytime recalculation happens
	 *
	 * @return {@code true}
	 */
	public boolean mulAdditionalStat(Actor actor, ActorStat stat, double value) {
		final StatsHolder statsHolder = StatsHolder.of(stat, value);
		return setMulAdditionalStat(actor, statsHolder);
	}

	private boolean setMulAdditionalStat(Actor actor, StatsHolder statsHolder) {
		final ActorStatData actorStatData = actorStatRepository.lockAndModifyData(actor, null, data -> {
			final ActorStatData statData = actorStatRepository.getStatData(actor);
			statData.getAdditionalMul().add(statsHolder);
			return statData;
		});

		return actorStatData.getAdditionalMul().stream().anyMatch(s -> s.equals(statsHolder));
	}

	/**
	 * @return {@code true} if 'mul' was removed, {@code false} in case there wasn't such stat and value
	 */
	public boolean removeMulAdditionalStat(Actor actor, ActorStat stat, double value) {
		final ActorStatData actorStatData = actorStatRepository.lockAndModifyData(actor, null, data -> {
			final ActorStatData statData = actorStatRepository.getStatData(actor);
			final Iterator<StatsHolder> it = statData.getAdditionalMul().iterator();
			while (it.hasNext()) {
				final StatsHolder holder = it.next();
				if (holder.getStat() == stat && holder.getValue() == value) {
					it.remove();
					break;
				}
			}

			return statData;
		});

		return actorStatData.getAdditionalMul().stream().noneMatch(h -> h.getStat() == stat);
	}

	/**
	 * @return true if the there wasn't previously set fixed value, {@code false} otherwise
	 */
	public boolean addFixedValue(Actor actor, ActorStat stat, Double value) {
		final ActorStatData actorStatData = actorStatRepository.lockAndModifyData(actor, null, data -> {
			final ActorStatData statData = actorStatRepository.getStatData(actor);
			statData.getFixedValue().put(stat, value);
			return statData;
		});

		return actorStatData.getFixedValue().get(stat) != null;
	}

	/**
	 * @return {@code true} if fixed value is removed, {@code false} otherwise
	 */
	public boolean removeFixedValue(Actor actor, ActorStat stat) {
		final ActorStatData actorStatData = actorStatRepository.lockAndModifyData(actor, null, data -> {
			final ActorStatData statData = actorStatRepository.getStatData(actor);
			statData.getFixedValue().remove(stat);
			return statData;
		});

		return actorStatData.getFixedValue().get(stat) == null;
	}

	public boolean isBlockedActionsAllowedSkill(Actor actor, Skill skill) {
		return actorStatRepository.getStatDataWithLock(actor).getBlockActionsAllowedSkills().contains(skill.getSkillId());
	}

	public void addBlockActionsAllowedSkill(Actor actor, int skillId) {
		actorStatRepository.lockAndModifyData(actor, null, data -> {
			final ActorStatData statData = actorStatRepository.getStatData(actor);
			statData.getBlockActionsAllowedSkills().add(skillId);
			return statData;
		});
	}

	public void removeBlockActionsAllowedSkill(Actor actor, int skillId) {
		actorStatRepository.lockAndModifyData(actor, null, data -> {
			final ActorStatData statData = actorStatRepository.getStatData(actor);
			statData.getBlockActionsAllowedSkills().remove(skillId);
			return statData;
		});
	}

	public void mergeAttackTrait(Actor actor, TraitType traitType, float value) {
		actorStatRepository.lockAndModifyData(actor, null, data -> {
			final ActorStatData statData = actorStatRepository.getStatData(actor);
			statData.setAttackTraitValue(traitType, statData.getAttackTrait(traitType) * value);
			statData.getAttackTraits().add(traitType);
			return statData;
		});
	}

	public float getAttackTrait(Actor actor, TraitType traitType) {
		return actorStatRepository.getStatDataWithLock(actor).getAttackTrait(traitType);
	}

	public boolean hasAttackTrait(Actor actor, TraitType traitType) {
		return actorStatRepository.getStatDataWithLock(actor).getAttackTraits().contains(traitType);
	}

	public void mergeDefenceTrait(Actor actor, TraitType traitType, float value) {
		actorStatRepository.lockAndModifyData(actor, null, data -> {
			final ActorStatData statData = actorStatRepository.getStatData(actor);
			statData.setDefenceTraitValue(traitType, statData.getDefenceTrait(traitType) * value);
			statData.getDefenceTraits().add(traitType);
			return statData;
		});
	}

	public float getDefenceTrait(Actor actor, TraitType traitType) {
		return actorStatRepository.getStatDataWithLock(actor).getDefenceTrait(traitType);
	}

	public boolean hasDefenceTrait(Actor actor, TraitType traitType) {
		return actorStatRepository.getStatDataWithLock(actor).getDefenceTraits().contains(traitType);
	}

	public void mergeInvulnerableTrait(Actor actor, TraitType traitType) {
		actorStatRepository.lockAndModifyData(actor, null, data -> {
			final ActorStatData statData = actorStatRepository.getStatData(actor);
			statData.getInvulnerableTraits().add(traitType);
			return statData;
		});
	}

	public boolean isInvulnerableTrait(Actor actor, TraitType traitType) {
		return actorStatRepository.getStatDataWithLock(actor).getInvulnerableTraits().contains(traitType);
	}

	/**
	 * Gets the maximum buff count.
	 *
	 * @return the maximum buff count
	 */
	public int getMaxBuffCount(Actor actor) {
		return actorStatRepository.getStatDataWithLock(actor).getMaxBuffCount();
	}

	public double getPositionTypeValue(Actor actor, ActorStat stat, Position position) {
		return actorStatRepository.getStatDataWithLock(actor).getPositionStats()
				.getOrDefault(stat, Collections.emptyMap()).getOrDefault(position, 1d);
	}

	public void mergePositionTypeValue(Actor actor, ActorStat stat, Position position, double value, BiFunction<? super Double, ? super Double, ? extends Double> func) {
		actorStatRepository.lockAndModifyData(actor, null, data -> {
			final ActorStatData statData = actorStatRepository.getStatData(actor);
			statData.getPositionStats().computeIfAbsent(stat, key -> new ConcurrentHashMap<>()).merge(position, value, func);
			return statData;
		});
	}

	public double getMoveTypeValue(Actor actor, ActorStat stat, MoveType type) {
		return actorStatRepository.getStatDataWithLock(actor).getMoveTypeStats()
				.getOrDefault(stat, Collections.emptyMap()).getOrDefault(type, 0d);
	}

	public void mergeMoveTypeValue(Actor actor, ActorStat stat, MoveType type, double value) {
		actorStatRepository.lockAndModifyData(actor, null, data -> {
			final ActorStatData statData = actorStatRepository.getStatData(actor);
			statData.getMoveTypeStats().computeIfAbsent(stat, key -> new ConcurrentHashMap<>()).merge(type, value, MathUtil::add);
			return statData;
		});
	}

	public double getReuseTypeValue(Actor actor, int magicType) {
		return actorStatRepository.getStatDataWithLock(actor).getReuseStat().getOrDefault(magicType, 1d);
	}

	public void mergeReuseTypeValue(Actor actor, int magicType, double value, BiFunction<? super Double, ? super Double, ? extends Double> func) {
		actorStatRepository.lockAndModifyData(actor, null, data -> {
			final ActorStatData statData = actorStatRepository.getStatData(actor);
			statData.getReuseStat().merge(magicType, value, func);
			return statData;
		});
	}

	public double getCastChanceValue(Actor actor, int magicType) {
		return actorStatRepository.getStatDataWithLock(actor).getCastingStat().getOrDefault(magicType, 0d);
	}

	public void mergeCastChanceValue(Actor actor, int magicType, double value, BiFunction<? super Double, ? super Double, ? extends Double> func) {
		actorStatRepository.lockAndModifyData(actor, null, data -> {
			final ActorStatData statData = actorStatRepository.getStatData(actor);
			statData.getCastingStat().merge(magicType, value, func);
			return statData;
		});
	}

	public double getMpConsumeTypeValue(Actor actor, int magicType) {
		final Map<Integer, Double> mpConsumeStat = actorStatRepository.getStatDataWithLock(actor).getMpConsumeStat();
		return mpConsumeStat.getOrDefault(magicType, 1d);
	}

	public void mergeMpConsumeTypeValue(Actor actor, int magicType, double value, BiFunction<? super Double, ? super Double, ? extends Double> func) {
		actorStatRepository.lockAndModifyData(actor, null, data -> {
			final ActorStatData statData = actorStatRepository.getStatData(actor);
			statData.getMpConsumeStat().merge(magicType, value, func);
			return statData;
		});
	}

	public double getSkillEvasionTypeValue(Actor actor, int magicType) {
		final Map<Integer, LinkedList<Double>> skillEvasionStat = actorStatRepository.getStatDataWithLock(actor).getSkillEvasionStat();
		final LinkedList<Double> skillEvasions = skillEvasionStat.get(magicType);
		if (skillEvasions != null && !skillEvasions.isEmpty()) {
			return skillEvasions.peekLast();
		}
		return 0d;
	}

	public void addSkillEvasionTypeValue(Actor actor, int magicType, double value) {
		actorStatRepository.lockAndModifyData(actor, null, data -> {
			final ActorStatData statData = actorStatRepository.getStatData(actor);
			statData.getSkillEvasionStat().computeIfAbsent(magicType, k -> new LinkedList<>()).add(value);
			return statData;
		});
	}

	public void removeSkillEvasionTypeValue(Actor actor, int magicType, double value) {
		actorStatRepository.lockAndModifyData(actor, null, data -> {
			final ActorStatData statData = actorStatRepository.getStatData(actor);
			statData.getSkillEvasionStat().computeIfPresent(magicType, (k, v) -> {
				v.remove(value);
				return !v.isEmpty() ? v : null;
			});
			return statData;
		});
	}

	public void setSupportItemBonusRate(Actor actor, double rate) {
		actorStatRepository.lockAndModifyData(actor, null, data -> {
			final ActorStatData statData = actorStatRepository.getStatData(actor);
			statData.setSupportItemBonusRate(rate);
			return statData;
		});
	}

	public double getSupportItemBonus(Actor actor) {
		return actorStatRepository.getStatDataWithLock(actor).getSupportItemBonusRate();
	}

	public void addToVampiricSum(Actor actor, double sum) {
		actorStatRepository.lockAndModifyData(actor, null, data -> {
			final ActorStatData statData = actorStatRepository.getStatData(actor);
			statData.setVampiricSum(statData.getVampiricSum() + sum);
			return statData;
		});
	}

	public double getVampiricSum(Actor actor) {
		return actorStatRepository.getStatDataWithLock(actor).getVampiricSum();
	}

	public void set(Actor actor, BooleanStat stat) {
		actorStatRepository.lockAndModifyData(actor, null, data -> {
			final ActorStatData statData = actorStatRepository.getStatData(actor);
			statData.getBooleanStats().add(stat);
			return statData;
		});
	}

	public boolean has(Actor actor, BooleanStat stat) {
		return actorStatRepository.getStatDataWithLock(actor).getBooleanStats().contains(stat);
	}

	public void addBlockActionsAllowedItem(Actor actor, int itemId) {
		actorStatRepository.lockAndModifyData(actor, null, data -> {
			final ActorStatData statData = actorStatRepository.getStatData(actor);
			statData.getBlockActionsAllowedItems().add(itemId);
			return statData;
		});
	}

	public boolean isBlockedActionsAllowedItem(Actor actor, Item item) {
		return actorStatRepository.getStatDataWithLock(actor).getBlockActionsAllowedItems().contains(item.getItemId());
	}

	/**
	 * Merges the double stat add
	 *
	 * @param doubleStat the double stat
	 * @param value      the value
	 */
	public void mergeAdd(Actor actor, ActorStat doubleStat, double value) {
		actorStatRepository.lockAndModifyData(actor, null, data -> {
			final ActorStatData statData = actorStatRepository.getStatData(actor);
			final Map<ActorStat, ActorStatValue> doubleStats = statData.getDoubleStats();
			final ActorStatValue doubleStatValue = doubleStats.computeIfAbsent(doubleStat, ActorStatValue::new);
			doubleStatValue.setAdd(doubleStat.add(doubleStatValue.getAdd(), value));
			return statData;
		});
	}

	/**
	 * Merges the double stat mul
	 *
	 * @param doubleStat the double stat
	 * @param mul        the mul
	 */
	public void mergeMul(Actor actor, ActorStat doubleStat, double mul) {
		actorStatRepository.lockAndModifyData(actor, null, data -> {
			final ActorStatData statData = actorStatRepository.getStatData(actor);
			final Map<ActorStat, ActorStatValue> doubleStats = statData.getDoubleStats();
			final ActorStatValue doubleStatValue = doubleStats.computeIfAbsent(doubleStat, ActorStatValue::new);
			doubleStatValue.setMul(doubleStat.mul(doubleStatValue.getMul(), mul));
			return statData;
		});
	}

	/**
	 * @return the add value
	 */
	public double getAdd(Actor actor, ActorStat doubleStat) {
		final ActorStatValue actorStatValue = actorStatRepository.getStatDataWithLock(actor).getDoubleStats().get(doubleStat);
		return actorStatValue != null ? actorStatValue.getAdd() : doubleStat.getResetAddValue();
	}

	/**
	 * @return the mul value
	 */
	public double getMul(Actor actor, ActorStat doubleStat) {
		final ActorStatValue actorStatValue = actorStatRepository.getStatDataWithLock(actor).getDoubleStats().get(doubleStat);
		return actorStatValue != null ? actorStatValue.getMul() : doubleStat.getResetMulValue();
	}

	/**
	 * @return the final value of the stat
	 */
	public double getValue(Actor actor, ActorStat stat, double baseValue) {
		final Double fixedValue = actorStatRepository.getStatDataWithLock(actor).getFixedValue().get(stat);
		return fixedValue != null ? fixedValue : stat.finalize(actor, OptionalDouble.of(baseValue).orElse(0.));
	}

	/**
	 * @return the final value of the stat
	 */
	public double getValue(Actor actor, ActorStat stat) {
		final Double fixedValue = actorStatRepository.getStatDataWithLock(actor).getFixedValue().get(stat);
		return fixedValue != null ? fixedValue : stat.finalize(actor, 0.);
	}

	/**
	 * @return the final value of the stat
	 */
	public double getValue(Actor actor, ActorStatData data, ActorStat stat, double baseValue) {
		final Double fixedValue = data.getFixedValue().get(stat);
		return fixedValue != null ? fixedValue : stat.finalize(actor, OptionalDouble.of(baseValue).orElse(0.));
	}

	/**
	 * @return the final value of the stat
	 */
	public double getValue(Actor actor, ActorStatData data, ActorStat stat) {
		final Double fixedValue = data.getFixedValue().get(stat);
		return fixedValue != null ? fixedValue : stat.finalize(actor, 0.);
	}
}
