package org.genfork.gameserver.slave.data.repositories;

import org.genfork.server.objects.WorldObject;
import org.genfork.server.objects.component.stat.ActorStatData;
import org.genfork.tools.persistence.annotations.DataMethod;
import org.genfork.tools.persistence.annotations.DataRepository;
import org.genfork.tools.persistence.annotations.DataValue;
import org.genfork.tools.persistence.annotations.ExpParam;
import org.genfork.tools.persistence.repository.CrudDataRepository;
import org.genfork.tools.persistence.repository.handler.OperationHandler;

import static org.genfork.tools.persistence.annotations.DataMethod.Type.READ;
import static org.genfork.tools.persistence.annotations.DataMethod.Type.WRITE;

/**
 * @author: ngolubenko@context-it.ru
 * @created: 2020/04
 */
@DataRepository(bucketName = "actor_stat_store")
public interface ActorStatRepository extends CrudDataRepository {
	@DataMethod(type = WRITE, keyExpression = "#object.hashName(T(org.genfork.gameserver.slave.config.ServerConfig).PLAYER_SERVER_ID)")
	void addStatData(@ExpParam("object") WorldObject object, @DataValue ActorStatData data);

	@DataMethod(type = WRITE, keyExpression = "#object.hashName(T(org.genfork.gameserver.slave.config.ServerConfig).PLAYER_SERVER_ID)")
	ActorStatData lockAndModifyData(@ExpParam("object") WorldObject object, @DataValue ActorStatData data, OperationHandler<? extends ActorStatData> operationHandler);

	@DataMethod(enableLock = false, type = READ, keyExpression = "#object.hashName(T(org.genfork.gameserver.slave.config.ServerConfig).PLAYER_SERVER_ID)")
	ActorStatData getStatData(@ExpParam("object") WorldObject object);

	@DataMethod(type = READ, keyExpression = "#object.hashName(T(org.genfork.gameserver.slave.config.ServerConfig).PLAYER_SERVER_ID)")
	ActorStatData getStatDataWithLock(@ExpParam("object") WorldObject object);
}
