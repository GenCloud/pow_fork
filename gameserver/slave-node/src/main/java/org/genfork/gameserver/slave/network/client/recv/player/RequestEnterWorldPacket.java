package org.genfork.gameserver.slave.network.client.recv.player;

import org.genfork.gameserver.slave.managers.SessionManager;
import org.genfork.gameserver.slave.managers.dispatcher.EventDispatcherManager;
import org.genfork.gameserver.slave.network.discovery.DiscoveryServerHandler;
import org.genfork.server.dispatcher.events.OnPlayerEnterWorld;
import org.genfork.tools.network.IncomingPacket;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class RequestEnterWorldPacket extends IncomingPacket<DiscoveryServerHandler> {
	private final int[][] trace = new int[5][4];

	@Override
	public boolean read(DiscoveryServerHandler client) {
		for (int i = 0; i < 5; i++) {
			for (int o = 0; o < 4; o++) {
				trace[i][o] = readC();
			}
		}

		readD(); // Unknown Value
		readD(); // Unknown Value
		readD(); // Unknown Value
		readD(); // Unknown Value
		readB(64); // Unknown Byte Array
		readD(); // Unknown Value
		return true;
	}

	@Override
	public void run(DiscoveryServerHandler client) throws Exception {
		final EventDispatcherManager instance = EventDispatcherManager.getInstance();
		SessionManager.getInstance().getManagedSessionInfo(getSessionInfo(), value -> {
			instance.notifyEventAsync(new OnPlayerEnterWorld(value, trace), instance.getPlayersContainer());
			return value;
		});
	}
}
