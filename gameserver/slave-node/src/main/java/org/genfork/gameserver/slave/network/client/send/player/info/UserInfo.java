package org.genfork.gameserver.slave.network.client.send.player.info;

import org.genfork.gameserver.slave.data.controller.actor.ActorStatController;
import org.genfork.gameserver.slave.data.xml.ExperienceDataParser;
import org.genfork.gameserver.slave.enums.UserInfoType;
import org.genfork.gameserver.slave.network.client.send.player.AbstractMaskPacket;
import org.genfork.server.objects.Player;
import org.genfork.server.objects.component.appearance.PlayerAppearance;
import org.genfork.server.objects.component.data.PlayerData;
import org.genfork.server.objects.component.stat.ActorStatData;

import static org.genfork.gameserver.slave.enums.UserInfoType.BASIC_INFO;
import static org.genfork.gameserver.slave.enums.UserInfoType.CLAN;
import static org.genfork.server.enums.ActorStat.*;
import static org.genfork.tools.network.opcodes.GameBranchOpcodes.USER_INFO;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class UserInfo extends AbstractMaskPacket<UserInfoType> {
	private final Player player;
	private final int relation;
	private final int playerLevel;
	private final int runSpd;
	private final int walkSpd;
	private final int swimRunSpd;
	private final int swimWalkSpd;
	private final int flRunSpd;
	private final int flWalkSpd;
	private final int flyRunSpd;
	private final int flyWalkSpd;
	private final int enchantLevel;
	private final int armorEnchant;
	private final double moveMultiplier;
	private final byte[] masks = new byte[]
			{
					(byte) 0x00,
					(byte) 0x00,
					(byte) 0x00,
					(byte) 0x00
			};

	private final double str, dex, con, _int, wit, men, luc, cha;
	private final double pAtk, pAtkSpeed, pDef, pEvasRate, pAcc, pCritRate, mAtk, mAtkSpd, mEvasRate, mDef, mAcc, mCritRate;
	private final double fireRes, waterRes, windRes, earthRes, holyRes, darkRes;
	private final double pAtkSpdMult;

	private String title;
	private int initSize = 5;

	public UserInfo(Player player) {
		this(player, true);
	}

	public UserInfo(Player player, boolean addAll) {
		this.player = player;

//		TODO
//		relation = calculateRelation(player);

		playerLevel = player.getPlayerLevel();
		relation = 0;

		enchantLevel = 24;
		armorEnchant = 10;

		title = player.getTitle();

		final ActorStatData actorStatData = ActorStatController.getInstance().getActorStatData(player);

		moveMultiplier = ActorStatController.getInstance().getMovementSpeedMultiplier(player, actorStatData);
		runSpd = (int) Math.round(ActorStatController.getInstance().getValue(player, actorStatData, RUN_SPEED) / moveMultiplier);
		walkSpd = (int) Math.round(ActorStatController.getInstance().getValue(player, actorStatData, WALK_SPEED) / moveMultiplier);
		swimRunSpd = (int) Math.round(ActorStatController.getInstance().getValue(player, actorStatData, SWIM_RUN_SPEED) / moveMultiplier);
		swimWalkSpd = (int) Math.round(ActorStatController.getInstance().getValue(player, actorStatData, SWIM_WALK_SPEED) / moveMultiplier);

		flRunSpd = runSpd;
		flWalkSpd = walkSpd;
		flyRunSpd = runSpd;
		flyWalkSpd = walkSpd;

		str = ActorStatController.getInstance().getValue(player, actorStatData, STAT_STR);
		dex = ActorStatController.getInstance().getValue(player, actorStatData, STAT_DEX);
		con = ActorStatController.getInstance().getValue(player, actorStatData, STAT_CON);
		_int = ActorStatController.getInstance().getValue(player, actorStatData, STAT_INT);
		wit = ActorStatController.getInstance().getValue(player, actorStatData, STAT_WIT);
		men = ActorStatController.getInstance().getValue(player, actorStatData, STAT_MEN);
		luc = ActorStatController.getInstance().getValue(player, actorStatData, STAT_LUC);
		cha = ActorStatController.getInstance().getValue(player, actorStatData, STAT_CHA);

		pAtk = ActorStatController.getInstance().getValue(player, actorStatData, PHYSICAL_ATTACK);
		pAtkSpeed = ActorStatController.getInstance().getValue(player, actorStatData, PHYSICAL_ATTACK_SPEED);
		pDef = ActorStatController.getInstance().getValue(player, actorStatData, PHYSICAL_DEFENCE);
		pEvasRate = ActorStatController.getInstance().getValue(player, actorStatData, PHYSICAL_EVASION_RATE);
		pAcc = ActorStatController.getInstance().getValue(player, actorStatData, ACCURACY_PHYSICAL);
		pCritRate = ActorStatController.getInstance().getValue(player, actorStatData, PHYSICAL_CRITICAL_RATE);
		mAtk = ActorStatController.getInstance().getValue(player, actorStatData, MAGIC_ATTACK);
		mAtkSpd = ActorStatController.getInstance().getValue(player, actorStatData, MAGIC_ATTACK_SPEED);
		mEvasRate = ActorStatController.getInstance().getValue(player, actorStatData, MAGICAL_EVASION_RATE);
		mDef = ActorStatController.getInstance().getValue(player, actorStatData, MAGICAL_DEFENCE);
		mAcc = ActorStatController.getInstance().getValue(player, actorStatData, ACCURACY_MAGICAL);
		mCritRate = ActorStatController.getInstance().getValue(player, actorStatData, MAGIC_CRITICAL_RATE);

		fireRes = ActorStatController.getInstance().getValue(player, actorStatData, FIRE_RES);
		waterRes = ActorStatController.getInstance().getValue(player, actorStatData, WATER_RES);
		windRes = ActorStatController.getInstance().getValue(player, actorStatData, WIND_RES);
		earthRes = ActorStatController.getInstance().getValue(player, actorStatData, EARTH_RES);
		holyRes = ActorStatController.getInstance().getValue(player, actorStatData, HOLY_RES);
		darkRes = ActorStatController.getInstance().getValue(player, actorStatData, DARK_RES);

		pAtkSpdMult = actorStatData.getAttackSpeedMultiplier();

		if (addAll) {
			addComponentType(UserInfoType.values());
		}
	}

	@Override
	protected byte[] getMasks() {
		return masks;
	}

	@Override
	protected void onNewMaskAdded(UserInfoType component) {
		calcBlockSize(component);
	}

	private void calcBlockSize(UserInfoType type) {
		if (type == BASIC_INFO) {
			initSize += type.getBlockLength() + (player.getName().length() * 2);
		} else if (type == CLAN) {
			initSize += type.getBlockLength() + (title.length() * 2);
		} else {
			initSize += type.getBlockLength();
		}
	}

	@Override
	public boolean write() {
		USER_INFO.writeId(this);

		writeD(player.getObjectId());
		writeD(initSize);
		writeH(27);
		writeB(masks);

		final PlayerData data = player.getData();
		final PlayerAppearance playerAppearance = player.getPlayerAppearance();

		if (containsMask(UserInfoType.RELATION)) {
			writeD(relation);
		}

		if (containsMask(UserInfoType.BASIC_INFO)) {
			writeH(16 + (playerAppearance.getVisibleName().length() * 2));
			writeString(player.getName());
			writeC(0x00); // is builder
			writeC(player.getRace().ordinal());
			writeC(playerAppearance.getSexType().ordinal());
			writeD(player.getClassId().getRootClassId().getId());
			writeD(player.getClassId().getId());
			writeC(playerLevel);
		}

		if (containsMask(UserInfoType.BASE_STATS)) {
			writeH(18);
			writeH((int) str);
			writeH((int) dex);
			writeH((int) con);
			writeH((int) _int);
			writeH((int) wit);
			writeH((int) men);
			writeH((int) luc);
			writeH((int) cha);
		}

		if (containsMask(UserInfoType.MAX_HPCPMP)) {
			writeH(14);
			writeD(player.getMaxHp());
			writeD(player.getMaxMp());
			writeD(player.getMaxCp());
		}

		if (containsMask(UserInfoType.CURRENT_HPMPCP_EXP_SP)) {
			writeH(38);
			writeD(Math.round(player.getCurrentHp()));
			writeD(Math.round(player.getCurrentMp()));
			writeD(Math.round(player.getCurrentCp()));
			writeQ(player.getCurrentSp());
			writeQ(player.getCurrentExp());
			final long expForLevel = ExperienceDataParser.getInstance().getExpForLevel(playerLevel);
			final long expForLevelMod = ExperienceDataParser.getInstance().getExpForLevel(playerLevel + 1);
			writeF((float) (player.getCurrentExp() - expForLevel) / (expForLevelMod - expForLevel));
		}

		if (containsMask(UserInfoType.ENCHANTLEVEL)) {
			writeH(4);
			writeC(enchantLevel);
			writeC(armorEnchant);
		}

		if (containsMask(UserInfoType.APPAREANCE)) {
			writeH(15);
			writeD(playerAppearance.getHairStyle());
			writeD(playerAppearance.getHairColor());
			writeD(playerAppearance.getFace());
//			writeC(player.isHairAccessoryEnabled() ? 0x01 : 0x00);
			writeC(0x01);
		}

		if (containsMask(UserInfoType.STATUS)) {
			writeH(6);
//			writeC(player.getMountType().ordinal());
//			writeC(player.getPrivateStoreType().getId());
//			writeC(player.getCrystallizeGrade() != ItemGrade.NONE ? 1 : 0);
//			writeC(player.getAbilityPoints() - player.getAbilityPointsUsed());

			writeC(0);
			writeC(0);
			writeC(0);
			writeC(36);
		}

		if (containsMask(UserInfoType.STATS)) {
			writeH(56);
			writeH(20);
//			writeH(player.getActiveWeaponItem() != null ? 40 : 20);
			writeD((int) pAtk);
			writeD((int) pAtkSpeed);
			writeD((int) pDef);
			writeD((int) pEvasRate);
			writeD((int) pAcc);
			writeD((int) pCritRate);
			writeD((int) mAtk);
			writeD((int) mAtkSpd);
			writeD((int) pAtkSpeed); // Seems like atk speed - 1
			writeD((int) mEvasRate);
			writeD((int) mDef);
			writeD((int) mAcc);
			writeD((int) mCritRate);
		}

		if (containsMask(UserInfoType.ELEMENTALS)) {
			writeH(14);
			writeH((int) fireRes);
			writeH((int) waterRes);
			writeH((int) windRes);
			writeH((int) earthRes);
			writeH((int) holyRes);
			writeH((int) darkRes);
		}

		if (containsMask(UserInfoType.POSITION)) {
			writeH(18);
			writeD(player.getRealX());
			writeD(player.getRealY());
			writeD(player.getRealZ());
//			writeD(player.isInVehicle() ? player.getVehicle().getObjectId() : 0);
			writeD(0);
		}

		if (containsMask(UserInfoType.SPEED)) {
			writeH(18);
			writeH(runSpd);
			writeH(walkSpd);
			writeH(swimRunSpd);
			writeH(swimWalkSpd);
			writeH(flRunSpd);
			writeH(flWalkSpd);
			writeH(flyRunSpd);
			writeH(flyWalkSpd);
		}

		if (containsMask(UserInfoType.MULTIPLIER)) {
			writeH(18);
			writeF(moveMultiplier);
			writeF(pAtkSpdMult);
		}

		if (containsMask(UserInfoType.COL_RADIUS_HEIGHT)) {
			writeH(18);
			writeF(data.getCollisionHeight());
			writeF(data.getCollisionRadius());
		}

		if (containsMask(UserInfoType.ATK_ELEMENTAL)) {
			writeH(5);
//			final AttributeType attackAttribute = player.getAttackElement();
//			writeC(attackAttribute.getClientId());
//			writeH(player.getAttackElementValue(attackAttribute));
			writeC(0);
			writeH(300);
		}

		if (containsMask(UserInfoType.CLAN)) {
			writeH(32 + (title.length() * 2));
			writeString(title);
//			writeH(player.getPledgeType());
//			writeD(player.getClanId());
//			writeD(player.getClanCrestLargeId());
//			writeD(player.getClanCrestId());
//			writeD(player.getClanPrivileges().getBitmask());
//			writeC(player.isClanLeader() ? 0x01 : 0x00);
//			writeD(player.getAllyId());
//			writeD(player.getAllyCrestId());
//			writeC(player.isInMatchingRoom() ? 0x01 : 0x00);

			writeH(1);
			writeD(0);
			writeD(0);
			writeD(0);
			writeD(0);
			writeC(0);
			writeD(0);
			writeD(0);
			writeC(0);
		}

		if (containsMask(UserInfoType.SOCIAL)) {
			writeH(30);
//			writeC(player.getPvpFlag());
			writeC(0);
			writeD(player.getReputation()); // Reputation
//			writeC(player.getNobleStatus().getClientId());
//			writeC(player.isHero() || (player.isGM() && AdminConfig.GM_HERO_AURA) ? 1 : 0);
			writeC(0);
			writeC(0);

//			writeC(player.getPledgeClass());
			writeC(0);

			writeD(player.getPk());
			writeD(player.getPvp());
			writeH(player.getRecommendationsLeft());
			writeH(player.getRecommendationsHave());

			writeD(0x00); // 196
			writeD(0x00); // 228
		}

		if (containsMask(UserInfoType.VITA_FAME)) {
			writeH(19); //235
			writeD(player.getVitalityPoints());
			writeC(200); // Vita Bonus
			writeD(player.getFame());
			writeD(player.getRaidBossPoints());

			writeC(100); // 196
			writeH(100); // Henna Seal Engraving Gauge
			writeC(100); // 196
		}

		if (containsMask(UserInfoType.SLOTS)) {
			writeH(12); // 152
//			writeC(player.getInventory().getTalismanSlots());
//			writeC(player.getInventory().getBroochJewelSlots());
//			writeC(player.getTeam().getId());
			writeC(0);
			writeC(0);
			writeC(0);

			writeD(0x00); //rounds on floor

//			if (player.getInventory().getAgathionSlots() > 0) {
//				writeC(0x01); // Charm slots
//				writeC(player.getInventory().getAgathionSlots() - 1);
//				writeC(player.getInventory().getArtifactSlots()); // Artifact set slots // 152
//			} else {
//				writeC(0x00); // Charm slots
//				writeC(0x00);
//				writeC(player.getInventory().getArtifactSlots()); // Artifact set slots // 152
//			}

			writeC(0);
			writeC(0);
			writeC(0);
		}

		if (containsMask(UserInfoType.MOVEMENTS)) {
			writeH(4);
//			writeC(player.isInsideZone(ZoneId.WATER) ? 1 : player.isFlyingMounted() ? 2 : 0);
//			writeC(player.isRunning() ? 0x01 : 0x00);

			writeC(0);
			writeC(0);
		}

		if (containsMask(UserInfoType.COLOR)) {
			writeH(10);
			writeD(playerAppearance.getNameColor());
			writeD(playerAppearance.getTitleColor());
		}

		if (containsMask(UserInfoType.INVENTORY_LIMIT)) {
			writeH(13);
			writeH(0x00);
			writeH(0x00);
//			writeH(player.getInventoryLimit());
			writeH(128);

//			final int cursedWeaponEquippedId = player.getCursedWeaponEquippedId();
			writeC(0);
//			writeD(cursedWeaponEquippedId); // 196 - changed nick name to demonic sword name 8689 - Akamanah
			writeD(0);
		}

		if (containsMask(UserInfoType.TRUE_HERO)) {
			writeH(9);
			writeD(0x00);
			writeH(0x00);
//			writeC(player.isTrueHero() ? 100 : 0x00);
			writeC(100);
		}

		if (containsMask(UserInfoType.ATT_SPIRITS)) // 152
		{
			writeH(26);
			writeD(-1);
			writeD(0x00);
			writeD(0x00);
			writeD(0x00);
			writeD(0x00);
			writeD(0x00);
		}

		if (containsMask(UserInfoType.RANKING)) // 196
		{
			writeH(6); // 196
			writeD(1); // 196
		}

		return true;
	}
}
