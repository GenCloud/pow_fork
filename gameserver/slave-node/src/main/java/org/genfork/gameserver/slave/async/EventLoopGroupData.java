package org.genfork.gameserver.slave.async;

import io.netty.channel.nio.NioEventLoopGroup;
import lombok.Getter;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class EventLoopGroupData {
	@Getter(lazy = true)
	private static final EventLoopGroupData instance = new EventLoopGroupData();

	@Getter
	private final NioEventLoopGroup bossGroup = new NioEventLoopGroup(4);
	@Getter
	private final NioEventLoopGroup workerGroup = new NioEventLoopGroup(4);
}
