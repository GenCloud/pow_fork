package org.genfork.gameserver.slave.data.repositories;

import org.genfork.server.objects.WorldObject;
import org.genfork.tools.persistence.annotations.DataMethod;
import org.genfork.tools.persistence.annotations.DataRepository;
import org.genfork.tools.persistence.annotations.DataValue;
import org.genfork.tools.persistence.annotations.ExpParam;
import org.genfork.tools.persistence.repository.CrudDataRepository;
import org.genfork.tools.persistence.repository.handler.OperationHandler;

import java.util.Map;

import static org.genfork.tools.persistence.annotations.DataMethod.Type.*;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@DataRepository(bucketName = "world_objects_store")
public interface ObjectStoreRepository extends CrudDataRepository {
	@DataMethod(type = WRITE, keyExpression = "#object.hashName(T(org.genfork.gameserver.slave.config.ServerConfig).PLAYER_SERVER_ID)")
	void addObject(@ExpParam("object") @DataValue WorldObject object);

	@DataMethod(type = WRITE, keyExpression = "#object.hashName(T(org.genfork.gameserver.slave.config.ServerConfig).PLAYER_SERVER_ID)")
	void lockAndAddObject(@ExpParam("object") @DataValue WorldObject object, OperationHandler<? extends WorldObject> operationHandler);

	@DataMethod(type = READ, keyExpression = "#name + '_' + T(org.genfork.gameserver.slave.config.ServerConfig).PLAYER_SERVER_ID")
	WorldObject lockAndGetObject(@ExpParam("name") String name, OperationHandler<? extends WorldObject> operationHandler);

	@DataMethod(type = READ, keyExpression = "#name + '_' + T(org.genfork.gameserver.slave.config.ServerConfig).PLAYER_SERVER_ID")
	WorldObject getObject(@ExpParam("name") String name);

	@DataMethod(type = READ_ALL)
	Map<String, WorldObject> getObjects();

	@DataMethod(type = REMOVE, keyExpression = "#object.hashName(T(org.genfork.gameserver.slave.config.ServerConfig).PLAYER_SERVER_ID)")
	void removeObject(@ExpParam("object") WorldObject object);
}
