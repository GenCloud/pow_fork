package org.genfork.gameserver.slave.data.controller.lobby;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.genfork.gameserver.slave.config.ServerConfig;
import org.genfork.gameserver.slave.data.controller.world.WorldObjectsStorageController;
import org.genfork.gameserver.slave.data.repositories.LobbyStoreRepository;
import org.genfork.gameserver.slave.data.repositories.ObjectStoreRepository;
import org.genfork.gameserver.slave.data.xml.PlayerDataParser;
import org.genfork.gameserver.slave.managers.DataStoreManager;
import org.genfork.gameserver.slave.managers.SessionManager;
import org.genfork.gameserver.slave.network.client.send.player.ServerClose;
import org.genfork.gameserver.slave.network.client.send.player.lobby.PlayerSelectedPacket;
import org.genfork.server.enums.ClassId;
import org.genfork.server.enums.Race;
import org.genfork.server.model.lobby.LobbyInfo;
import org.genfork.server.objects.Player;
import org.genfork.server.objects.component.appearance.PlayerAppearance;
import org.genfork.server.objects.component.data.PlayerData;
import org.genfork.tools.data.StatsSet;
import org.genfork.tools.network.ChannelInboundHandler;
import org.genfork.tools.network.DiscoveryClients;
import org.genfork.tools.network.NetworkConnectionPool;
import org.genfork.tools.network.SessionInfo;

import java.time.Instant;
import java.util.Collections;
import java.util.List;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Slf4j
public class LobbyDataController {
	private static final String LOBBY_DATA_PARAM = "lobby_data";

	@Getter(lazy = true)
	private static final LobbyDataController instance = new LobbyDataController();

	private final LobbyStoreRepository lobbyStoreRepository;
	private final ObjectStoreRepository objectStoreRepository;

	private LobbyDataController() {
		objectStoreRepository = DataStoreManager.getInstance().getRepository(ObjectStoreRepository.class);
		lobbyStoreRepository = DataStoreManager.getInstance().getRepository(LobbyStoreRepository.class);
	}

	public void addLobbyData(String accountName, List<LobbyInfo> lobbyInfos) {
		lobbyStoreRepository.lockAndAddData(accountName, value -> {
			final StatsSet set = new StatsSet();
			set.set(LOBBY_DATA_PARAM, lobbyInfos);
			return set;
		});
	}

	public List<LobbyInfo> getLobbyData(String accountName) {
		final StatsSet data = lobbyStoreRepository.getData(accountName);
		return data != null ? data.getList(LOBBY_DATA_PARAM, LobbyInfo.class) : Collections.emptyList();
	}

	public void removeLobbyData(String accountName) {
		lobbyStoreRepository.removeData(accountName);
	}

	public void playerTryEnterWorld(SessionInfo sessionInfo, String toClientName, int lobbySlot) {
		final String accountName = sessionInfo.getAccountName();
		final List<LobbyInfo> lobbyData = LobbyDataController.getInstance().getLobbyData(accountName);

		final ChannelInboundHandler<?> handler = NetworkConnectionPool.getInstance().getNextChannel(DiscoveryClients.GAME_SLAVE_NODE);
		if (lobbyData.isEmpty()) {
			handleCloseEvent(sessionInfo, toClientName, handler);
			return;
		}

		final LobbyInfo lobbyInfo = lobbyData.stream().filter(d -> d.getLobbySlot() == lobbySlot).findFirst().orElse(null);
		if (lobbyInfo == null) {
			handleCloseEvent(sessionInfo, toClientName, handler);
			return;
		}

		WorldObjectsStorageController.getInstance()
				.findObject(lobbyInfo.getPlayerName() + "_" + ServerConfig.PLAYER_SERVER_ID,
						value -> {
							if (value instanceof Player) {
								WorldObjectsStorageController.getInstance().removeObject(value.asPlayer());
								handleCloseEvent(sessionInfo, toClientName, handler);
							} else {
								if (lobbyInfo.getAccessLevel() < 0) {
									handleCloseEvent(sessionInfo, toClientName, handler);
									return value;
								}

								final Player player = loadPlayerData(lobbyInfo);
								sessionInfo.setEnteredPlayerId(player.getObjectId());
								sessionInfo.setEnteredPlayerName(player.getName());
								player.setSessionInfo(sessionInfo);

								objectStoreRepository.addObject(player);

								SessionManager.getInstance().addSession(sessionInfo);

								removeLobbyData(accountName);

								handler.sendPacket(DiscoveryClients.GAME_MASTER_NODE,
										toClientName,
										sessionInfo,
										new PlayerSelectedPacket(lobbyInfo, sessionInfo.getPlayKeyFirst()));
								return player;
							}

							return value;
						});
	}

	private Player loadPlayerData(LobbyInfo lobbyInfo) {
		final Player player = new Player();
		player.setObjectId(lobbyInfo.getPlayerId());

		final int classId = lobbyInfo.getClassId();
		final PlayerData playerData = PlayerDataParser.getInstance().getData(classId);
		player.setData(playerData);
		player.setName(lobbyInfo.getPlayerName());
		player.setAccountName(lobbyInfo.getAccountName());

		player.setCreatedDate(lobbyInfo.getCreatedDate());

		player.setAccessLevel(lobbyInfo.getAccessLevel());
		player.setPlayerLevel(lobbyInfo.getPlayerLevel());
		player.setRace(Race.byOrdinal(lobbyInfo.getRace()));
		player.setClassId(ClassId.getClassId(classId));
		player.setBaseClassId(ClassId.getClassId(lobbyInfo.getBaseClassId()));
		player.setMaxCp(lobbyInfo.getMaxCp());
		player.setCurrentCp(lobbyInfo.getCurrentCp());
		player.setMaxHp(lobbyInfo.getMaxHp());
		player.setCurrentHp(lobbyInfo.getCurrentHp());
		player.setMaxMp(lobbyInfo.getMaxMp());
		player.setCurrentMp(lobbyInfo.getCurrentMp());

		final PlayerAppearance appearance = new PlayerAppearance(lobbyInfo.getFace(),
				lobbyInfo.getHairColor(),
				lobbyInfo.getHairStyle(), lobbyInfo.getSex() == 1);
		appearance.setPlayer(player);
		player.setPlayerAppearance(appearance);

		player.setRealX(lobbyInfo.getX());
		player.setRealY(lobbyInfo.getY());
		player.setRealZ(lobbyInfo.getZ());
		player.setRealHeading(lobbyInfo.getH());

		player.setRaidBossPoints(lobbyInfo.getRaidBossPoints());
		player.setFame(lobbyInfo.getFame());
		player.setReputation(lobbyInfo.getReputation());
		player.setRecommendationsHave(lobbyInfo.getRecommendationsHave());
		player.setRecommendationsLeft(lobbyInfo.getRecommendationsLeft());

		player.setPvp(lobbyInfo.getPvp());
		player.setPk(lobbyInfo.getPk());

		player.setCurrentSp(lobbyInfo.getCurrentSp());
		player.setCurrentExp(lobbyInfo.getCurrentExp());
		player.setExpBeforeDeath(lobbyInfo.getExpBeforeDeath());

		player.setTitle(lobbyInfo.getTitle());
		player.setTitleColor(lobbyInfo.getTitleColor());
		player.setOnline(1);
		player.setLastAccess(Instant.now().toEpochMilli());
		player.setNobless(lobbyInfo.getNobless());
		player.setVitalityPoints(lobbyInfo.getVitalityPoints());
		return player;
	}

	private void handleCloseEvent(SessionInfo sessionInfo, String toClientName, ChannelInboundHandler<?> handler) {
		handler.sendPacket(DiscoveryClients.GAME_MASTER_NODE,
				toClientName,
				sessionInfo,
				ServerClose.STATIC);
	}
}
