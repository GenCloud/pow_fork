package org.genfork.gameserver.slave.network.client.recv.store;

import org.genfork.gameserver.slave.network.client.send.player.lobby.PlayerCreateSuccessPacket;
import org.genfork.gameserver.slave.network.discovery.DiscoveryServerHandler;
import org.genfork.tools.network.DiscoveryClients;
import org.genfork.tools.network.IncomingPacket;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class RequestReceivePlayerCreationResultPacket extends IncomingPacket<DiscoveryServerHandler> {
	private int result;

	@Override
	public boolean read(DiscoveryServerHandler client) {
		result = readD();
		return true;
	}

	@Override
	public void run(DiscoveryServerHandler client) throws Exception {
		if (result == 1) {
			client.sendPacket(DiscoveryClients.GAME_MASTER_NODE, DiscoveryClients.GAME_SLAVE_NODE, getSessionInfo(), PlayerCreateSuccessPacket.STATIC);
		}
	}
}
