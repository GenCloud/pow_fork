package org.genfork.gameserver.slave.network.client.send.store;

import lombok.RequiredArgsConstructor;
import org.genfork.tools.network.OutgoingPacket;

import static org.genfork.tools.network.opcodes.ClientsBranchOpcodes.PLAYER_TRY_RECEIVE_INVENTORY_DATA_PACKET;

/**
 * @author: ngolubenko@context-it.ru
 * @created: 2020/04
 */
@RequiredArgsConstructor
public class PlayerTryReceiveInventoryDataPacket extends OutgoingPacket {
	private final String playerName;

	@Override
	public boolean write() {
		PLAYER_TRY_RECEIVE_INVENTORY_DATA_PACKET.writeId(this);

		writeS(playerName);
		return true;
	}
}
