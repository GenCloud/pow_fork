package org.genfork.gameserver.slave.data.controller.player;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import lombok.Getter;
import org.genfork.gameserver.slave.data.controller.world.WorldObjectsStorageController;
import org.genfork.server.objects.Player;
import org.genfork.server.objects.WorldObject;
import org.genfork.tools.network.ChannelInboundHandler;
import org.genfork.tools.network.DiscoveryClients;
import org.genfork.tools.network.NetworkConnectionPool;
import org.genfork.tools.network.OutgoingPacket;
import org.genfork.tools.registry.network.NetworkUtil;
import org.genfork.tools.registry.network.send.PartClientDataPacket;
import org.genfork.tools.registry.network.send.SendClientDataPacket;

import java.util.ArrayList;
import java.util.List;

import static org.genfork.tools.registry.NetworkConstants.*;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class PlayerBroadcastController {
	@Getter(lazy = true)
	private static final PlayerBroadcastController instance = new PlayerBroadcastController();

//	public void broadcastUserInfo() {
//		// Send user info to the current player
//		sendPacket(new UserInfo(this));
//		sendPacket(new ExBrNotifyPremiumState(this));
//
//		// Broadcast char info to known players
//		broadcastCharInfo();
//	}

	//	public void broadcastUserInfo(UserInfoType... types) {
//		// Send user info to the current player
//		UserInfo ui = new UserInfo(this, false);
//		ui.addComponentType(types);
//		sendPacket(ui);
//
//		// Broadcast char info to all known players
//		broadcastCharInfo();
//	}
//
//	public final void broadcastCharInfo() {
//		final IClientOutgoingPacket packet = getPoly().isMorphed() ? new PlayerPolymorphedNpcInfo(this) : new CharInfo(this, false);
//		World.getInstance().forEachVisibleObject(this, PlayerInstance.class, player ->
//		{
//			if (isVisibleFor(player)) {
//				if (isInvisible() && player.canOverrideCond(PcCondOverride.SEE_ALL_PLAYERS)) {
//					player.sendPacket(getPoly().isMorphed() ? new PlayerPolymorphedNpcInfo(this) : new CharInfo(this, true));
//				} else {
//					player.sendPacket(packet);
//				}
//
//				sendRelationChanged(player);
//			}
//		});
//	}
//
//	public final void broadcastTitleInfo() {
//		// Send a Server->Client packet UserInfo to this PlayerInstance
//		UserInfo ui = new UserInfo(this, false);
//		ui.addComponentType(UserInfoType.CLAN);
//		sendPacket(ui);
//
//		// Send a Server->Client packet TitleUpdate to all PlayerInstance in _KnownPlayers of the PlayerInstance
//		broadcastPacket(new NicknameChanged(this));
//	}

	public void sendInfo(WorldObject self, Player target) {
		if (self.isPlayer()) {
//			sendPacket(target, new CharInfo(self.asPlayer()));
		}
	}

	public void broadcastPacket(Player player, OutgoingPacket packet) {
		sendPacket(player, packet);

		WorldObjectsStorageController.getInstance().forEachVisibleObject(player, Player.class, p -> {
			if (!player.isVisibleFor(p)) {
				return;
			}

			sendPacket(p, packet);
		});
	}

	public void broadcastPacket(Player player, OutgoingPacket packet, int radiusInKnownlist) {
		sendPacket(player, packet);

		WorldObjectsStorageController.getInstance().forEachVisibleObjectInRadius(player, Player.class, radiusInKnownlist, p -> {
			if (!player.isVisibleFor(p)) {
				return;
			}

			sendPacket(p, packet);
		});
	}

	public void sendPacket(Player player, OutgoingPacket packet) {
		final ChannelInboundHandler<?> handler = NetworkConnectionPool.getInstance().getNextChannel(DiscoveryClients.GAME_SLAVE_NODE);
		handler.sendPacket(DiscoveryClients.GAME_MASTER_NODE, DiscoveryClients.GAME_SLAVE_NODE, player.getSessionInfo(), packet);
	}

	public void sendPartPacket(Player player, OutgoingPacket... packets) {
		final ChannelInboundHandler<?> handler = NetworkConnectionPool.getInstance().getNextChannel(DiscoveryClients.GAME_SLAVE_NODE);

		final List<ByteBuf> buffs = new ArrayList<>();

		final StringBuilder builder = new StringBuilder();
		builder.append(PACKET_PARTS_PARAM).append(packets.length).append(PACKET_PARAMS_DELIMITER).append(PACKET_PARTS_LENGTH_PARAM);
		for (int i = 0; i < packets.length; i++) {
			OutgoingPacket packet = packets[i];
			ByteBuf byteBuf = packet.getByteBuf();
			if (byteBuf == null) {
				packet.setByteBuf(byteBuf = Unpooled.directBuffer());
			}

			packet.write();

			final int partLength = byteBuf.readableBytes();
			if (i + 1 == packets.length) {
				builder.append(partLength);
			} else {
				builder.append(partLength).append(PACKET_PARTS_DELIMITER);
			}

			buffs.add(byteBuf);
		}

		final PartClientDataPacket dataPacket = new PartClientDataPacket(builder.toString(), buffs);
		final SendClientDataPacket sendClientDataPacket = NetworkUtil.buildPlayerSendData(DiscoveryClients.GAME_MASTER_NODE,
				DiscoveryClients.GAME_SLAVE_NODE,
				player.getSessionInfo(),
				dataPacket);

		final Channel channel = handler.getChannel();
		channel.writeAndFlush(sendClientDataPacket);
	}
}
