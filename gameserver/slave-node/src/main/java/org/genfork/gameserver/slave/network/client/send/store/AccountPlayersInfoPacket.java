package org.genfork.gameserver.slave.network.client.send.store;

import lombok.RequiredArgsConstructor;
import org.genfork.tools.network.OutgoingPacket;

import static org.genfork.tools.network.opcodes.ClientsBranchOpcodes.ACCOUNT_PLAYERS_INFO_PACKET;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@RequiredArgsConstructor
public class AccountPlayersInfoPacket extends OutgoingPacket {
	private final String accountName;
	private final boolean authentication;

	@Override
	public boolean write() {
		ACCOUNT_PLAYERS_INFO_PACKET.writeId(this);
		writeC(authentication ? 1 : 0);
		writeS(accountName);
		return true;
	}
}
