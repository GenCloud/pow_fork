package org.genfork.gameserver.slave.network.enums;

import org.genfork.gameserver.slave.network.client.recv.ExPacket;
import org.genfork.gameserver.slave.network.client.recv.SystemPackets;
import org.genfork.gameserver.slave.network.client.recv.player.RequestEnterWorldPacket;
import org.genfork.gameserver.slave.network.client.recv.player.inventory.RequestInventoryDataPacket;
import org.genfork.gameserver.slave.network.client.recv.player.lobby.RequestNewPlayerPacket;
import org.genfork.gameserver.slave.network.client.recv.player.lobby.RequestPlayerCreatePacket;
import org.genfork.gameserver.slave.network.client.recv.player.lobby.RequestPlayerSelectedPacket;
import org.genfork.gameserver.slave.network.discovery.DiscoveryServerHandler;
import org.genfork.tools.network.IConnectionState;
import org.genfork.tools.network.IIncomingPackets;
import org.genfork.tools.network.IncomingPacket;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Supplier;

public enum ClientIncomingPackets implements IIncomingPackets<DiscoveryServerHandler> {
	SYSTEM_BRANCH(0x0E, SystemPackets::new),

	REQUEST_PLAYER_CREATE_PACKET(0x0C, RequestPlayerCreatePacket::new),

	REQUEST_ENTER_WORLD_PACKET(0x11, RequestEnterWorldPacket::new),
	REQUEST_PLAYER_SELECTED_PACKET(0x12, RequestPlayerSelectedPacket::new),

	REQUEST_NEW_PLAYER_PACKET(0x13, RequestNewPlayerPacket::new),
	REQUEST_INVENTORY_DATA_PACKET(0x14, RequestInventoryDataPacket::new),

	EX_PACKET(0xD0, ExPacket::new);

	public static final ClientIncomingPackets[] PACKET_ARRAY;

	static {
		final short maxPacketId = (short) Arrays.stream(values()).mapToInt(IIncomingPackets::getPacketId).max().orElse(0);
		PACKET_ARRAY = new ClientIncomingPackets[maxPacketId + 1];
		for (ClientIncomingPackets incomingPacket : values()) {
			PACKET_ARRAY[incomingPacket.getPacketId()] = incomingPacket;
		}
	}

	private short packetId;
	private Supplier<IncomingPacket<DiscoveryServerHandler>> incomingPacketFactory;
	private Set<IConnectionState> connectionStates;

	ClientIncomingPackets(int packetId, Supplier<IncomingPacket<DiscoveryServerHandler>> incomingPacketFactory) {
		this(packetId, incomingPacketFactory, ConnectionState.values());
	}

	ClientIncomingPackets(int packetId, Supplier<IncomingPacket<DiscoveryServerHandler>> incomingPacketFactory, IConnectionState... connectionStates) {
		// packetId is an unsigned byte
		if (packetId > 0xFF) {
			throw new IllegalArgumentException("packetId must not be bigger than 0xFF");
		}

		this.packetId = (short) packetId;
		this.incomingPacketFactory = incomingPacketFactory != null ? incomingPacketFactory : () -> null;
		this.connectionStates = new HashSet<>(Arrays.asList(connectionStates));
	}

	@Override
	public int getPacketId() {
		return packetId;
	}

	@Override
	public IncomingPacket<DiscoveryServerHandler> newIncomingPacket() {
		return incomingPacketFactory.get();
	}

	@Override
	public Set<IConnectionState> getConnectionStates() {
		return connectionStates;
	}

	@Override
	public IIncomingPackets<DiscoveryServerHandler>[] getArray() {
		return PACKET_ARRAY;
	}
}
