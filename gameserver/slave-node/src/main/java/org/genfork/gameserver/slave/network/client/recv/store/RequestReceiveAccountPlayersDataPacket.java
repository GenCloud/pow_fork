package org.genfork.gameserver.slave.network.client.recv.store;

import org.genfork.gameserver.slave.data.controller.lobby.LobbyDataController;
import org.genfork.gameserver.slave.managers.SessionManager;
import org.genfork.gameserver.slave.network.client.send.player.GameAuthResultPacket;
import org.genfork.gameserver.slave.network.client.send.player.lobby.PlayerLobbyInfoPacket;
import org.genfork.gameserver.slave.network.discovery.DiscoveryServerHandler;
import org.genfork.server.enums.LocationData;
import org.genfork.server.model.lobby.InventoryItemInfo;
import org.genfork.server.model.lobby.LobbyInfo;
import org.genfork.tools.network.ChannelInboundHandler;
import org.genfork.tools.network.DiscoveryClients;
import org.genfork.tools.network.IncomingPacket;
import org.genfork.tools.network.NetworkConnectionPool;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.IntStream;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class RequestReceiveAccountPlayersDataPacket extends IncomingPacket<DiscoveryServerHandler> {
	private final List<LobbyInfo> result = new ArrayList<>();
	private boolean authentication;

	@Override
	public boolean read(DiscoveryServerHandler client) {
		authentication = readC() == 1;

		final int size = readD();
		if (size > 0) {
			IntStream.range(0, size)
					.mapToObj(idx ->
							new LobbyInfo())
					.forEach(info -> {
						info.setPlayerId(readD());

						info.setPlayerName(readS());
						info.setAccountName(readS());

						info.setCreatedDate(readQ());

						info.setAccessLevel(readD());
						info.setLobbySlot(readD());
						info.setPlayerLevel(readD());
						info.setRace(readD());
						info.setClassId(readD());
						info.setBaseClassId(readD());
						info.setMaxCp(readD());
						info.setCurrentCp(readD());
						info.setMaxHp(readD());
						info.setCurrentHp(readD());
						info.setMaxMp(readD());
						info.setCurrentMp(readD());
						info.setFace(readD());
						info.setHairStyle(readD());
						info.setHairColor(readD());
						info.setSex(readD());

						info.setX(readD());
						info.setY(readD());
						info.setZ(readD());
						info.setH(readD());

						info.setClanId(readD());
						info.setRaidBossPoints(readD());
						info.setFame(readD());
						info.setReputation(readD());
						info.setRecommendationsHave(readD());
						info.setRecommendationsLeft(readD());

						info.setPvp(readD());
						info.setPk(readD());

						info.setCurrentSp(readQ());
						info.setCurrentExp(readQ());
						info.setExpBeforeDeath(readQ());

						info.setTitle(readS());
						info.setTitleColor(readD());
						info.setOnline(readD());
						info.setOnlineTime(readD());
						info.setLastAccess(readQ());
						info.setTransformId(readD());
						info.setDeleteTime(readQ());
						info.setCanCraft(readD());
						info.setNobless(readD());
						info.setVitalityPoints(readD());

						final int inventorySize = readD();

						for (int idx = 0; idx < inventorySize; idx++) {
							final InventoryItemInfo itemInfo = new InventoryItemInfo();
							itemInfo.setItemObjId(readD());
							itemInfo.setItemPlayerId(readD());
							itemInfo.setItemId(readD());
							itemInfo.setAmount(readQ());
							itemInfo.setEnchant(readD());
							itemInfo.setTypeFirst(readD());
							itemInfo.setTypeSecond(readD());
							final int ordinal = readD();
							itemInfo.setLocationData(LocationData.values()[ordinal]);
							itemInfo.setLocationMask(readD());
							itemInfo.setUseTime(readD());
							itemInfo.setMpLeft(readE());
							itemInfo.setLifeTime(readE());
							itemInfo.setReuseDelay(readD());
							itemInfo.setSysTime(readQ());

							final int elementsSize = readD();
							final int[] elementTypes = IntStream.range(0, elementsSize).map(eidx -> readD()).toArray();
							itemInfo.setElementTypes(elementTypes);

							final int elementValSize = readD();
							final int[] elementValues = IntStream.range(0, elementValSize).map(eidx -> readD()).toArray();
							itemInfo.setElementTypes(elementValues);

							final int abilitiesSize = readD();
							final int[] abilityTypes = IntStream.range(0, abilitiesSize).map(eidx -> readD()).toArray();
							itemInfo.setAbilityTypes(abilityTypes);

							final int abilityIdsSize = readD();
							final int[] abilityIds = IntStream.range(0, abilityIdsSize).map(eidx -> readD()).toArray();
							itemInfo.setAbilityIds(abilityIds);

							final int abilitySlotsSize = readD();
							final int[] abilitySlots = IntStream.range(0, abilitySlotsSize).map(eidx -> readD()).toArray();
							itemInfo.setAbilitySlots(abilitySlots);

							final int variationSize = readD();
							final int[] mineralIds = IntStream.range(0, variationSize).map(eidx -> readD()).toArray();
							itemInfo.setMineralIds(mineralIds);

							final int options1Size = readD();
							final int[] optionsFirst = IntStream.range(0, options1Size).map(eidx -> readD()).toArray();
							itemInfo.setOptionsFirst(optionsFirst);

							final int options2Size = readD();
							final int[] optionsSecond = IntStream.range(0, options2Size).map(eidx -> readD()).toArray();
							itemInfo.setOptionsSecond(optionsSecond);

							info.getItemInfos().add(itemInfo);
						}

						result.add(info);
					});

			result.sort((Comparator.comparingInt(LobbyInfo::getLobbySlot)));
		}

		return true;
	}

	@Override
	public void run(DiscoveryServerHandler client) throws Exception {
		SessionManager.getInstance().getManagedSessionInfo(getSessionInfo(), value -> {
			final ChannelInboundHandler<?> handler = NetworkConnectionPool.getInstance().getNextChannel(DiscoveryClients.GAME_SLAVE_NODE);

			if (authentication) {
				handler.sendPacket(DiscoveryClients.GAME_MASTER_NODE,
						getToClientName(),
						value,
						GameAuthResultPacket.AUTH_SUCCESS);
			}

			handler.sendPacket(DiscoveryClients.GAME_MASTER_NODE,
					getToClientName(),
					value,
					new PlayerLobbyInfoPacket(result, value));

			final String accountName = value.getAccountName();
			LobbyDataController.getInstance().addLobbyData(accountName, result);
			return value;
		});
	}
}
