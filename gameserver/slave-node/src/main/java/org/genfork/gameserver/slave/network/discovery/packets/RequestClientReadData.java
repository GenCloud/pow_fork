package org.genfork.gameserver.slave.network.discovery.packets;

import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;
import org.genfork.gameserver.slave.network.discovery.DiscoveryServerHandler;
import org.genfork.gameserver.slave.network.enums.ClientIncomingPackets;
import org.genfork.tools.network.IncomingPacket;
import org.genfork.tools.network.SessionInfo;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Slf4j
public class RequestClientReadData extends IncomingPacket<DiscoveryServerHandler> {
	private String fromClient;
	private String toClient;

	private int authKeyFirst, authKeySecond, playKeyFirst, playKeySecond;

	private ByteBuf byteBuf;
	private boolean fromPlayer;

	@Override
	public boolean read(DiscoveryServerHandler client) {
		fromClient = readS();
		toClient = readS();

		fromPlayer = readC() == 1;
		if (fromPlayer) {
			authKeyFirst = readD();
			authKeySecond = readD();
			playKeyFirst = readD();
			playKeySecond = readD();

			if (log.isDebugEnabled()) {
				log.debug("Retransmit slave data to client by session: {}:{}:{}:{}", authKeyFirst, authKeySecond, playKeyFirst, playKeySecond);
			}
		}

		byteBuf = readB();
		return true;
	}

	@Override
	public void run(DiscoveryServerHandler client) throws Exception {
		final short packetId = byteBuf.readUnsignedByte();
		final ClientIncomingPackets[] packets = ClientIncomingPackets.PACKET_ARRAY;

		if (packetId >= packets.length) {
			log.warn("Unknown packet: {}", Integer.toHexString(packetId));
			return;
		}

		final ClientIncomingPackets incomingPacket = packets[packetId];
		if (incomingPacket == null) {
			log.warn("Unknown packet: {}", Integer.toHexString(packetId));
			return;
		}

		final IncomingPacket<DiscoveryServerHandler> packet = incomingPacket.newIncomingPacket();

		if (packet != null) {
			packet.setByteBuf(byteBuf);

			boolean read;
			if (fromPlayer) {
				final SessionInfo sessionInfo = SessionInfo.of(playKeyFirst, playKeySecond, authKeyFirst, authKeySecond);

				packet.setFromClientName(fromClient);
				packet.setToClientName(toClient);
				packet.setSessionInfo(sessionInfo);

				read = packet.read(client);
			} else {
				packet.setFromClientName(fromClient);
				packet.setToClientName(toClient);

				read = packet.read(client);
			}

			if (!read) {
				log.error("Error read data packet [{}]", packet);
			} else {
				try {
					packet.run(client);
				} catch (Exception e) {
					log.error("Error run data packet [{}]: {}", packet, e.getLocalizedMessage());
				}
			}

			byteBuf.retain();
		}
	}
}
