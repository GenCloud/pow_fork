package org.genfork.gameserver.slave.network.client.send.store;

import lombok.RequiredArgsConstructor;
import org.genfork.server.model.lobby.InventoryItemInfo;
import org.genfork.tools.network.OutgoingPacket;

import java.util.List;
import java.util.stream.IntStream;

import static org.genfork.tools.network.opcodes.ClientsBranchOpcodes.PLAYER_CREATION_PACKET;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@RequiredArgsConstructor
public class PlayerCreationPacket extends OutgoingPacket {
	private final String accountName;
	private final String playerName;
	private final int race;
	private final int classId;
	private final int hairStyle;
	private final int hairColor;
	private final int face;
	private final int sex;
	private final int accessLevel;

	private final int x, y, z, h;

	private final int maxCp, currentCp, maxHp, currentHp, maxMp, currentMp;

	private final int recommendationsLeft;

	private final List<InventoryItemInfo> itemInfos;

	@Override
	public boolean write() {
		PLAYER_CREATION_PACKET.writeId(this);

		writeS(accountName);
		writeS(playerName);
		writeD(race);
		writeD(classId);
		writeD(hairStyle);
		writeD(hairColor);
		writeD(face);
		writeD(sex);
		writeD(accessLevel);

		writeD(x);
		writeD(y);
		writeD(z);
		writeD(h);

		writeD(maxCp);
		writeD(currentCp);
		writeD(maxHp);
		writeD(currentHp);
		writeD(maxMp);
		writeD(currentMp);

		writeD(recommendationsLeft);

		if (itemInfos.isEmpty()) {
			writeD(0);
		} else {
			writeD(itemInfos.size());
			itemInfos.forEach(i -> {
				writeD(i.getItemId());
				writeQ(i.getAmount());
				writeD(i.getEnchant());
				writeD(i.getTypeFirst());
				writeD(i.getTypeSecond());
				writeD(i.getLocationData().ordinal());
				writeD(i.getLocationMask());
				writeD(i.getUseTime());
				writeE(i.getMpLeft());
				writeE(i.getLifeTime());
				writeD(i.getReuseDelay());
				writeQ(i.getSysTime());

				final int[] elementTypes = i.getElementTypes() == null ? new int[0] : i.getElementTypes();
				writeD(elementTypes.length);
				IntStream.range(0, elementTypes.length).forEach(e -> writeD(elementTypes[e]));

				final int[] elementValues = i.getElementValues() == null ? new int[0] : i.getElementValues();
				writeD(elementValues.length);
				IntStream.range(0, elementValues.length).forEach(e -> writeD(elementValues[e]));

				final int[] abilityTypes = i.getAbilityTypes() == null ? new int[0] : i.getAbilityTypes();
				writeD(abilityTypes.length);
				IntStream.range(0, abilityTypes.length).forEach(e -> writeD(abilityTypes[e]));

				final int[] abilityIds = i.getAbilityIds() == null ? new int[0] : i.getAbilityIds();
				writeD(abilityIds.length);
				IntStream.range(0, abilityIds.length).forEach(e -> writeD(abilityIds[e]));

				final int[] abilitySlots = i.getAbilitySlots() == null ? new int[0] : i.getAbilitySlots();
				writeD(abilitySlots.length);
				IntStream.range(0, abilitySlots.length).forEach(e -> writeD(abilitySlots[e]));

				final int[] mineralIds = i.getMineralIds() == null ? new int[0] : i.getMineralIds();
				writeD(mineralIds.length);
				IntStream.range(0, mineralIds.length).forEach(e -> writeD(mineralIds[e]));

				final int[] optionsFirst = i.getOptionsFirst() == null ? new int[0] : i.getOptionsFirst();
				writeD(optionsFirst.length);
				IntStream.range(0, optionsFirst.length).forEach(e -> writeD(optionsFirst[e]));

				final int[] optionsSecond = i.getOptionsSecond() == null ? new int[0] : i.getOptionsSecond();
				writeD(optionsSecond.length);
				IntStream.range(0, optionsSecond.length).forEach(e -> writeD(optionsSecond[e]));
			});
		}
		return true;
	}
}
