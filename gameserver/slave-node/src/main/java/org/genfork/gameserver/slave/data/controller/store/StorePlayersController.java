package org.genfork.gameserver.slave.data.controller.store;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.genfork.gameserver.slave.network.client.send.store.AccountPlayersInfoPacket;
import org.genfork.tools.network.ChannelInboundHandler;
import org.genfork.tools.network.DiscoveryClients;
import org.genfork.tools.network.NetworkConnectionPool;
import org.genfork.tools.network.SessionInfo;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Slf4j
public class StorePlayersController {
	@Getter(lazy = true)
	private static final StorePlayersController instance = new StorePlayersController();

	public void tryReceiveAccountPlayerData(boolean authentication, SessionInfo sessionInfo, String accountName) {
		final ChannelInboundHandler<?> handler = NetworkConnectionPool.getInstance().getNextChannel(DiscoveryClients.GAME_SLAVE_NODE);
		if (handler != null) {
			if (log.isDebugEnabled()) {
				log.debug("Try receive account players data by account name - {}, clientId - {}", accountName, sessionInfo);
			}

			final AccountPlayersInfoPacket result = new AccountPlayersInfoPacket(accountName, authentication);

			handler.sendPacket(DiscoveryClients.DATA_STORE_NODE,
					DiscoveryClients.GAME_SLAVE_NODE,
					sessionInfo,
					result);
		}
	}
}
