package org.genfork.gameserver.slave.network.discovery;

import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;
import org.genfork.gameserver.slave.config.DiscoveryClientConfig;
import org.genfork.tools.network.ChannelInboundHandler;
import org.genfork.tools.network.DiscoveryClients;
import org.genfork.tools.network.IncomingPacket;
import org.genfork.tools.network.NetworkConnectionPool;
import org.genfork.tools.registry.annotation.DiscoveryClient;

import static org.genfork.gameserver.slave.network.enums.ConnectionState.CONNECTED;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Slf4j
@DiscoveryClient(config = DiscoveryClientConfig.class)
public class DiscoveryServerHandler extends ChannelInboundHandler<DiscoveryServerHandler> {
	@Override
	public void channelActive(ChannelHandlerContext ctx) {
		super.channelActive(ctx);

		setConnectionState(CONNECTED);

		NetworkConnectionPool.getInstance().addChannel(DiscoveryClients.GAME_SLAVE_NODE, this);
	}

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, IncomingPacket<DiscoveryServerHandler> packet) throws Exception {
		try {
			packet.run(this);
		} catch (Exception ex) {
			log.warn("Exception for: {} on packet.run: {}", toString(), packet.getClass().getSimpleName(), ex);
		}
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
		log.warn("Network exception caught for: {} : {}", toString(), cause.getLocalizedMessage());
	}
}
