package org.genfork.gameserver.slave.network.client.send.player.lobby;

import org.genfork.tools.network.OutgoingPacket;

import static org.genfork.tools.network.opcodes.GameBranchOpcodes.CHARACTER_CREATE_SUCCESS;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class PlayerCreateSuccessPacket extends OutgoingPacket {
	public static final PlayerCreateSuccessPacket STATIC = new PlayerCreateSuccessPacket();

	@Override
	public boolean write() {
		CHARACTER_CREATE_SUCCESS.writeId(this);
		writeD(0x01);
		return true;
	}
}
