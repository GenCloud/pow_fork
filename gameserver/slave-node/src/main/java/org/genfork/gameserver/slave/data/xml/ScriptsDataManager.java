package org.genfork.gameserver.slave.data.xml;

import com.google.common.collect.ImmutableList;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.genfork.gameserver.slave.config.ServerConfig;
import org.genfork.gameserver.slave.model.scripts.annotation.GameScript;
import org.genfork.tools.loader.annotations.Dependency;
import org.genfork.tools.loader.annotations.InstanceGetter;
import org.genfork.tools.loader.annotations.Load;
import org.genfork.tools.loader.annotations.Reload;
import org.genfork.tools.loader.interfaces.PreLoadGroup;
import org.genfork.tools.loader.interfaces.ScriptLoadGroup;
import org.genfork.tools.path.PathProvider;
import org.genfork.tools.xml.XmlReader;
import org.jdom2.Element;

import javax.tools.*;
import javax.tools.JavaCompiler.CompilationTask;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.time.Duration;
import java.util.*;
import java.util.stream.Stream;

import static javax.tools.Diagnostic.Kind.ERROR;
import static javax.tools.Diagnostic.NOPOS;
import static org.genfork.tools.path.OverrideMode.OVERLAYING;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Slf4j
public class ScriptsDataManager implements XmlReader {
	@Getter(lazy = true, onMethod = @__(@InstanceGetter))
	private static final ScriptsDataManager instance = new ScriptsDataManager();

	private static final String INNER_CLASS_SEPARATOR = "$";

	private final MemoryClassLoader memoryClassLoader
			= AccessController.doPrivileged((PrivilegedAction<MemoryClassLoader>) MemoryClassLoader::new);

	private final Set<String> excludedPackages = new HashSet<>();
	private final Set<String> includedPackages = new HashSet<>();
	private final Set<String> excludedClasses = new HashSet<>();
	private final Set<String> includedClasses = new HashSet<>();

	@Load(group = PreLoadGroup.class)
	public void loadScriptsXml() {
		parseFile("config/" + System.getProperty("active.profile") + "/xml/scripts.xml", ServerConfig.DATA_PACK_ROOT);
	}

	@Load(group = PreLoadGroup.class, dependencies = @Dependency(clazz = ScriptsDataManager.class, method = "loadScriptsXml"))
	public void loadClasses() throws IOException {
		final Collection<File> scriptFiles = new ArrayList<>();

		final PathProvider pathProvider = new PathProvider().setAcceptFilter(this::acceptFilter).setOverrideLogging(overrideSystemLogging());
		pathProvider.resolveOverriddenDirectory(Paths.get("data/scripts"), ServerConfig.DATA_PACK_ROOT, true, OVERLAYING)
				.forEach(file -> {
					final File target = file.toFile();
					if (target.isFile() && target.getName().endsWith(".java")) {
						scriptFiles.add(target);
					}
				});


		if (scriptFiles.isEmpty()) {
			return;
		}

		final long startTime = System.currentTimeMillis();
		compile(scriptFiles);
		log.info("Complete scripts compilation time: {} seconds", Duration.ofMillis(System.currentTimeMillis() - startTime).getSeconds());
	}

	@Reload("scripts")
	@Load(group = ScriptLoadGroup.class)
	public void load() {
		log.info("Loading server script(s) ...");

		ScriptsDataManager.getInstance().runScripts(GameScript.class);

		// report handlers
//		ActionHandler.getInstance().report();
//		ActionShiftHandler.getInstance().report();
//		AdminCommandHandler.getInstance().report();
//		BypassHandler.getInstance().report();
//		ChatHandler.getInstance().report();
//		CommunityBoardHandler.getInstance().report();
//		ItemHandler.getInstance().report();
//		PunishmentHandler.getInstance().report();
//		UserCommandHandler.getInstance().report();
//		VoicedCommandHandler.getInstance().report();

		// report loaded scripts
//		QuestManager.getInstance().report();

		log.info("Server script(s) were loaded successfully .");
	}

	@Override
	public void loadFile(Element rootElement) {
		forEach(rootElement, "excludePackage", excludePackageNode ->
				excludedPackages.add(excludePackageNode.getText().endsWith(".")
						? excludePackageNode.getText() : excludePackageNode.getText() + "."));

		forEach(rootElement, "includePackage", includePackageNode ->
				includedPackages.add(includePackageNode.getText().endsWith(".")
						? includePackageNode.getText() : includePackageNode.getText() + "."));

		forEach(rootElement, "excludeClass", excludeClassesNode ->
				excludedClasses.add(excludeClassesNode.getText()));

		forEach(rootElement, "includeClass", includeClassesNode ->
				includedClasses.add(includeClassesNode.getText()));

		log.info("Loaded {} Excluded Packages {} Included Packages {} Excluded Classes {} Included Classes", excludedPackages.size(), includedPackages.size(), excludedClasses.size(), includedClasses.size());
	}

	public boolean compile(Iterable<File> files) {
		// compiler options
		final JavaCompiler javaCompiler = ToolProvider.getSystemJavaCompiler();
		final List<String> options = ImmutableList.of("-Xlint:all", "-g");
		final DiagnosticCollector<JavaFileObject> diagnostics = new DiagnosticCollector<>();
		final StandardJavaFileManager fileManager = javaCompiler.getStandardFileManager(diagnostics, null, StandardCharsets.UTF_8);
		try (JavaFileManager memFileManager = new MemoryJavaFileManager(fileManager, memoryClassLoader)) {
			final CompilationTask compile = javaCompiler.getTask(null, memFileManager, diagnostics, options, null,
					fileManager.getJavaFileObjectsFromFiles(files));
			return compile.call();
		} catch (IOException e) {
			log.error("Can't compile", e);
			return false;
		} finally {
			diagnostics.getDiagnostics().forEach(diagnostic -> {
				if (diagnostic.getKind() == ERROR) {
					log.error("{}{}: {}", diagnostic.getSource().getName(), diagnostic.getPosition() == NOPOS
									? "" : ":" + diagnostic.getLineNumber() + ',' + diagnostic.getColumnNumber(),
							diagnostic.getMessage(Locale.getDefault()));
				} else {
					final String sourceName = diagnostic.getSource() == null ? "" : diagnostic.getSource().getName();
					log.debug("{}{}: {}", sourceName, diagnostic.getPosition() == NOPOS
									? "" : ":" + diagnostic.getLineNumber() + ',' + diagnostic.getColumnNumber(),
							diagnostic.getMessage(Locale.getDefault()));
				}
			});
		}
	}

	public void runScripts(Class<? extends Annotation> annotation) {
		final Stream<String> loadedClasses = memoryClassLoader.getLoadedClasses()
				.filter(name ->
						!name.contains(INNER_CLASS_SEPARATOR));

		loadedClasses
				.forEach(name -> {
					if (!includedClasses.contains(name)) {
						if (excludedClasses.contains(name)) {
							return;
						}

						if (excludedPackages.stream().anyMatch(name::startsWith)
								&& includedPackages.stream().noneMatch(name::startsWith)) {
							return;
						}
					}

					try {
						final Class<?> loaded = memoryClassLoader.loadClass(name);
						invokeAnnotatedMethod(loaded, annotation);
					} catch (ClassNotFoundException | IllegalAccessException | InvocationTargetException e) {
						log.error("Can't load script class: {}", name, e);
					}
				});
	}

	public void runScript(String className, Class<? extends Annotation> annotation) throws ClassNotFoundException, InvocationTargetException, IllegalAccessException {
		invokeAnnotatedMethod(Class.forName(className, true, memoryClassLoader), annotation);
	}

	private void invokeAnnotatedMethod(Class<?> clazz, Class<? extends Annotation> annotation) throws InvocationTargetException, IllegalAccessException {
		for (Method method : clazz.getDeclaredMethods()) {
			if (!method.isAnnotationPresent(annotation)) {
				continue;
			}

			if (!Modifier.isStatic(method.getModifiers())) {
				throw new RuntimeException();
			}

			if (method.getParameterCount() != 0) {
				throw new RuntimeException();
			}

			method.invoke(null);
		}
	}

	public static class MemoryJavaFileManager extends ForwardingJavaFileManager<StandardJavaFileManager> {
		private final MemoryClassLoader loader;

		public MemoryJavaFileManager(StandardJavaFileManager fileManager, MemoryClassLoader loader) {
			super(fileManager);
			this.loader = loader;
		}

		@Override
		public JavaFileObject getJavaFileForOutput(Location location, String className, javax.tools.JavaFileObject.Kind kind, FileObject sibling) {
			final MemoryByteCode mbc = new MemoryByteCode(className.replace('/', '.').replace('\\', '.'),
					URI.create("file:///" + className.replace('.', '/').replace('\\', '/') + kind.extension));
			loader.addClass(mbc);
			return mbc;
		}

		@Override
		public ClassLoader getClassLoader(final Location location) {
			return loader;
		}
	}

	public static class MemoryByteCode extends SimpleJavaFileObject {
		private final String className;
		private ByteArrayOutputStream outputStream;

		public MemoryByteCode(String className, URI uri) {
			super(uri, Kind.CLASS);
			this.className = className;
		}

		@Override
		public OutputStream openOutputStream() {
			return outputStream = new ByteArrayOutputStream(3 * 1024);
		}

		public byte[] getBytes() {
			return outputStream.toByteArray();
		}

		@Override
		public String getName() {
			return className;
		}
	}

	public static class MemoryClassLoader extends ClassLoader {
		private final Map<String, MemoryByteCode> classes = new HashMap<>();
		private final Map<String, MemoryByteCode> loaded = new HashMap<>();

		@Override
		protected Class<?> findClass(String name) throws ClassNotFoundException {
			MemoryByteCode mbc = classes.get(name);
			if (mbc == null) {
				mbc = classes.get(name);
				if (mbc == null) {
					return super.findClass(name);
				}
			}
			return defineClass(name, mbc.getBytes(), 0, mbc.getBytes().length);
		}

		public void addClass(MemoryByteCode mbc) {
			final String mbcName = mbc.getName();
			classes.put(mbcName, mbc);
			loaded.put(mbcName, mbc);
		}

		public MemoryByteCode getClass(String name) {
			return classes.get(name);
		}

		public Stream<String> getLoadedClasses() {
			return loaded.keySet().stream();
		}

		public void clear() {
			loaded.clear();
		}
	}
}
