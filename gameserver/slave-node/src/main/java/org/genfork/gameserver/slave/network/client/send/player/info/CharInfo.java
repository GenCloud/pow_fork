package org.genfork.gameserver.slave.network.client.send.player.info;

import org.genfork.server.objects.Player;
import org.genfork.tools.network.OutgoingPacket;

import static org.genfork.server.objects.component.inventory.InventoryConstants.*;
import static org.genfork.tools.network.opcodes.GameBranchOpcodes.CHAR_INFO;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class CharInfo extends OutgoingPacket {
	private static final int[] PAPERDOLL_ORDER = new int[]
			{
					PAPERDOLL_UNDER,
					PAPERDOLL_HEAD,
					PAPERDOLL_RHAND,
					PAPERDOLL_LHAND,
					PAPERDOLL_GLOVES,
					PAPERDOLL_CHEST,
					PAPERDOLL_LEGS,
					PAPERDOLL_FEET,
					PAPERDOLL_CLOAK,
					PAPERDOLL_RHAND,
					PAPERDOLL_HAIR,
					PAPERDOLL_HAIR2
			};
	private static final int[] PAPERDOLL_ORDER_AUGMENT = new int[]
			{
					PAPERDOLL_RHAND,
					PAPERDOLL_LHAND,
					PAPERDOLL_RHAND
			};
	private static final int[] PAPERDOLL_ORDER_VISUAL_ID = new int[]
			{
					PAPERDOLL_RHAND,
					PAPERDOLL_LHAND,
					PAPERDOLL_RHAND,
					PAPERDOLL_GLOVES,
					PAPERDOLL_CHEST,
					PAPERDOLL_LEGS,
					PAPERDOLL_FEET,
					PAPERDOLL_HAIR,
					PAPERDOLL_HAIR2
			};
	private final Player player;
	private final int relation;
	private final int runSpd;
	private final int walkSpd;
	private final int swimRunSpd;
	private final int swimWalkSpd;
	private final int flRunSpd;
	private final int flWalkSpd;
	private final int flyRunSpd;
	private final int flyWalkSpd;
	private final int enchantLevel;
	private final int armorEnchant;
	private final double moveMultiplier;
	private String title;

	public CharInfo(Player player) {
		this.player = player;

//		TODO
//		relation = calculateRelation(player);
//		moveMultiplier = player.getMovementSpeedMultiplier();
//		runSpd = (int) Math.round(player.getRunSpeed() / moveMultiplier);
//		walkSpd = (int) Math.round(player.getWalkSpeed() / moveMultiplier);
//		swimRunSpd = (int) Math.round(player.getSwimRunSpeed() / moveMultiplier);
//		swimWalkSpd = (int) Math.round(player.getSwimWalkSpeed() / moveMultiplier);

		relation = 0;
		moveMultiplier = 1;
		runSpd = 260;
		walkSpd = 200;
		swimRunSpd = 0;
		swimWalkSpd = 0;

		flRunSpd = 0;
		flWalkSpd = 0;
		flyRunSpd = 0;
		flyWalkSpd = 0;
		enchantLevel = 24;
		armorEnchant = 10;

		title = player.getTitle();
	}

	@Override
	public boolean write() {
		CHAR_INFO.writeId(this);

		writeC(0x00); // Grand Crusade
		writeD(player.getRealX()); // Confirmed
		writeD(player.getRealY()); // Confirmed
		writeD(player.getRealZ()); // Confirmed

		writeD(0x00); // vehicle id

		writeD(player.getObjectId()); // Confirmed
		writeS(player.getName()); // Confirmed

		writeH(player.getRace().ordinal()); // Confirmed
		writeC(player.getPlayerAppearance().getSexType().ordinal()); // Confirmed
		writeD(player.getBaseClassId().getId()); // Confirmed

		for (int slot : PAPERDOLL_ORDER) {
			writeD(0x00); // item id
		}

		for (int slot : PAPERDOLL_ORDER_AUGMENT) {
//			final VariationInstance augment = player.getInventory().getPaperdollAugmentation(slot);
//			writeD(augment != null ? augment.getOption1Id() : 0); // Confirmed
//			writeD(augment != null ? augment.getOption2Id() : 0); // Confirmed
			writeD(0x00);
			writeD(0x00);
		}

		writeC(10); // armor enchant

		for (int slot : PAPERDOLL_ORDER_VISUAL_ID) {
			writeD(0x00); //visual id
		}

		writeC(0x00); // flagged

		writeD(player.getReputation());

		writeD(333); //m atk spd
		writeD(333); //p atk spd

		writeH(runSpd);
		writeH(walkSpd);
		writeH(swimRunSpd);
		writeH(swimWalkSpd);
		writeH(flyRunSpd);
		writeH(flyWalkSpd);
		writeH(flRunSpd);
		writeH(flWalkSpd);
		writeF(0.2); // move multiplier
		writeF(0.2); // atk spd multiplier

//		writeF(player.getCollisionRadius());
//		writeF(player.getCollisionHeight());
		writeF(16);
		writeF(16);

		writeD(player.getPlayerAppearance().getHairStyle());
		writeD(player.getPlayerAppearance().getHairColor());
		writeD(player.getPlayerAppearance().getFace());

//		writeS(_gmSeeInvis ? "Invisible" : player.getAppearance().getVisibleTitle());
		writeS("");

//		writeD(player.getAppearance().getVisibleClanId());
//		writeD(player.getAppearance().getVisibleClanCrestId());
//		writeD(player.getAppearance().getVisibleAllyId());
//		writeD(player.getAppearance().getVisibleAllyCrestId());
//
//		writeC(player.isSitting() ? 0x00 : 0x01); // Confirmed
//		writeC(player.isRunning() ? 0x01 : 0x00); // Confirmed
//		writeC(player.isInCombat() ? 0x01 : 0x00); // Confirmed

		writeD(0);
		writeD(0);
		writeD(0);
		writeD(0);

		writeC(0x00);
		writeC(0x00);
		writeC(0x00); // Confirmed

//		writeC(!player.isInOlympiadMode() && player.isAlikeDead() ? 0x01 : 0x00); // Confirmed
		writeC(0x00);

		writeC(player.isInvisible() ? 0x01 : 0x00);

//		writeC(player.getMountType().ordinal()); // 1-on Strider, 2-on Wyvern, 3-on Great Wolf, 0-no mount
//		writeC(player.getPrivateStoreType().getId()); // Confirmed
		writeC(0x00);
		writeC(0x00);

//		writeH(player.getCubics().size()); // Confirmed
//		player.getCubics().keySet().forEach(packet::writeH);
		writeH(0x00);

//		writeC(player.isInMatchingRoom() ? 0x01 : 0x00); // Confirmed
		writeC(0x00);

//		writeC(player.isInsideZone(ZoneId.WATER) ? 1 : player.isFlyingMounted() ? 2 : 0);
		writeC(0x00);

		writeH(player.getRecommendationsHave()); // Confirmed
//		writeD(player.getMountNpcId() == 0 ? 0 : player.getMountNpcId() + 1000000);
		writeD(0x00);

		writeD(player.getClassId().getId()); // Confirmed
		writeD(0x00); // TODO: Find me!
//		writeC(player.isMounted() ? 0 : _enchantLevel); // Confirmed
		writeC(10);

//		writeC(player.getTeam().getId()); // Confirmed
		writeC(0x00);

//		writeD(player.getClanCrestLargeId());
//		writeC(player.getNobleStatus().getClientId()); // Confirmed
//		writeC(player.isHero() || (player.isGM() && AdminConfig.GM_HERO_AURA) ? 1 : 0); // Confirmed

		writeD(0x00);
		writeC(0x00);
		writeC(0x00);

//		writeC(player.isFishing() ? 1 : 0); // Confirmed
		writeC(0x00);

//		final ILocational baitLocation = player.getFishing().getBaitLocation();
//		writeD((int) baitLocation.getX()); // Confirmed
//		writeD((int) baitLocation.getY()); // Confirmed
//		writeD((int) baitLocation.getZ()); // Confirmed

		writeD(0x00);
		writeD(0x00);
		writeD(0x00);

//		writeD(player.getAppearance().getNameColor()); // Confirmed
		writeD(0x00);

		writeD(player.getRealHeading()); // Confirmed

//		writeC(player.getPledgeClass());
//		writeH(player.getPledgeType());
		writeC(0x00);
		writeH(0x00);

		writeD(player.getTitleColor()); // Confirmed

//		writeC(player.isCursedWeaponEquipped() ? CursedWeaponsManager.getInstance().getLevel(player.getCursedWeaponEquippedId()) : 0);
		writeC(0);

//		writeD(player.getAppearance().getVisibleClanId() > 0 ? player.getClan().getReputationScore() : 0);
//		writeD(player.getTransformationDisplayId()); // Confirmed
//		writeD(player.getAgathionId()); // Confirmed
		writeD(0x00);
		writeD(0x00);
		writeD(0x00);

		writeC(0x00); // TODO: Find me!

		writeD(Math.round(player.getCurrentCp())); // Confirmed
		writeD(player.getMaxHp()); // Confirmed
		writeD(Math.round(player.getCurrentHp())); // Confirmed
		writeD(player.getMaxMp()); // Confirmed
		writeD(Math.round(player.getCurrentMp())); // Confirmed

		writeC(0x00); // TODO: Find me!
//		final Set<AbnormalVisualEffect> abnormalVisualEffects = player.getEffectList().getCurrentAbnormalVisualEffects();
		writeD(0); // Confirmed
//		for (AbnormalVisualEffect abnormalVisualEffect : abnormalVisualEffects)
//		{
//			writeH(abnormalVisualEffect.getClientId()); // Confirmed
//		}
//		if (_gmSeeInvis)
//		{
//			writeH(AbnormalVisualEffect.STEALTH.getClientId());
//		}

//		writeC(cocPlayer != null ? cocPlayer.getPosition() : player.isTrueHero() ? 100 : 0);
		writeC(100);
//		writeC(player.isHairAccessoryEnabled() ? 0x01 : 0x00); // Hair accessory
//		writeC(player.getAbilityPointsUsed()); // Used Ability Points

		writeC(1);
		writeC(36);
		return true;
	}
}
