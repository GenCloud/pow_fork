package org.genfork.gameserver.slave.data.xml;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.genfork.gameserver.slave.config.ServerConfig;
import org.genfork.server.enums.ItemSkillType;
import org.genfork.server.model.pojo.AutoUseHolder;
import org.genfork.server.model.pojo.CapsuleItemHolder;
import org.genfork.server.model.pojo.ItemSkillHolder;
import org.genfork.server.objects.component.items.ItemData;
import org.genfork.tools.data.StatsSet;
import org.genfork.tools.loader.annotations.Dependency;
import org.genfork.tools.loader.annotations.InstanceGetter;
import org.genfork.tools.loader.annotations.Load;
import org.genfork.tools.loader.annotations.Reload;
import org.genfork.tools.loader.interfaces.LoadGroup;
import org.genfork.tools.xml.XmlReader;
import org.jdom2.Element;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author: ngolubenko@context-it.ru
 * @created: 2020/01
 */
@Slf4j
public class ItemDataParser implements XmlReader {
	@Getter(lazy = true, onMethod = @__({@InstanceGetter}))
	private static final ItemDataParser instance = new ItemDataParser();

	@Getter
	private final Map<Integer, ItemData> templates = new HashMap<>();

	public ItemData getData(int itemId) {
		return templates.get(itemId);
	}

	@Reload("item")
	@Load(group = LoadGroup.class, dependencies = {@Dependency(clazz = AutoUseOptionDataParser.class)})
	public void load() throws IOException {
		templates.clear();
		parseDirectory("data/items/", ServerConfig.DATA_PACK_ROOT, true);
		log.info("Loaded items {} size.", templates.size());
	}

	@Override
	public void loadFile(Element rootElement) {
		forEach(rootElement, "item", item -> {
			final StatsSet statsSet = new StatsSet();
			statsSet.set("item_id", getValue(item, "id"));
			statsSet.set("name", getValue(item, "name"));

			ItemData template;
			try {
				final String type = getValue(item, "type");
				final Constructor<?> itemClass = Class.forName(ItemData.class.getPackage().getName() + "." + type + "Template").getConstructor(StatsSet.class);
				template = (ItemData) itemClass.newInstance(statsSet);
			} catch (NoSuchMethodException | ClassNotFoundException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
				e.printStackTrace();
				return;
			}

			forEach(item, node -> {
				final String nodeName = node.getName();
				switch (nodeName) {
					case "conditions":
						loadConditions(template, node);
						break;
					case "capsuled_items":
						forEach(node, "item", itemsNode ->
								loadCapsuledItems(template, itemsNode));
						break;
					case "skills":
						forEach(node, "skill", skillsNode ->
								loadItemSkills(template, skillsNode));
						break;
					case "stats":
						forEach(node, statNode ->
								loadItemStats(template, statNode));
						break;
					default:
						statsSet.set(nodeName, node.getText());
						break;
				}
			});

			template.set(statsSet);

			final AutoUseHolder option = AutoUseOptionDataParser.getInstance().getOption(template.getItemId(), true);
			if (option != null) {
				template.setAutoUseType(option.getAutoUseType());
			}

			templates.put(template.getItemId(), template);
		});
	}

	private void loadItemStats(ItemData template, Element node) {
//		TODO: refresh stat naming in items
//		final ActorStat type = ActorStat.valueOfXml(node.getName());
//		final double value = Double.parseDouble(node.getText());
//		final FuncTemplate funcTemplate = new FuncTemplate(null, null, "add", 0x00, type, value);
//		template.addFunctionTemplate(funcTemplate);
	}

	private void loadItemSkills(ItemData template, Element skillsNode) {
		final ItemSkillHolder holder = new ItemSkillHolder();
		forEach(skillsNode, skillNode -> {
			final String skillNodeName = skillNode.getName();
			if ("id".equals(skillNodeName)) {
				holder.setSkillId(parseInt(skillNode));
			} else if ("level".equals(skillNodeName)) {
				holder.setSkillLevel(parseInt(skillNode));
			} else if ("subLevel".equals(skillNodeName)) {
				holder.setSkillSubLevel(parseInt(skillNode, 0));
			} else if ("type".equals(skillNodeName)) {
				holder.setType(parseEnum(skillNode, ItemSkillType.class, ItemSkillType.NORMAL));
			} else if ("type_chance".equals(skillNodeName)) {
				holder.setChance(parseInt(skillNode, 0));
			} else if ("type_value".equals(skillNodeName)) {
				holder.setValue(parseInt(skillNode, 0));
			}
		});

		template.addSkill(holder);
	}

	private void loadCapsuledItems(ItemData template, Element itemsNode) {
		final CapsuleItemHolder holder = new CapsuleItemHolder();
		forEach(itemsNode, itemNode -> {
			final String itemNodeName = itemNode.getName();
			if ("id".equals(itemNodeName)) {
				holder.setId(parseInt(itemNode));
			} else if ("min".equals(itemNodeName)) {
				holder.setMin(parseInt(itemNode));
			} else if ("max".equals(itemNodeName)) {
				holder.setMax(parseInt(itemNode, 0));
			} else if ("chance".equals(itemNodeName)) {
				holder.setChance(parseDouble(itemNode));
			} else if ("minEnchant".equals(itemNodeName)) {
				holder.setMinEnchant(parseInt(itemNode, 0));
			} else if ("maxEnchant".equals(itemNodeName)) {
				holder.setMaxEnchant(parseInt(itemNode, 0));
			}
		});

		template.addCapsuledItem(holder);
	}

	private void loadConditions(ItemData template, Element conditionsNode) {
//		TODO: add conditions
//		final List<Element> conds = conditionsNode.getChildren();
//
//		Integer msgId = null;
//		String message = null;
//		boolean addName = false;
//
//		final Element msgIdNode = conds.stream().filter(e -> e.getName().equals("msgId")).findFirst().orElse(null);
//		final Element msgNode = conds.stream().filter(e -> e.getName().equals("msg")).findFirst().orElse(null);
//		final Element addNameNode = conds.stream().filter(e -> e.getName().equals("addName")).findFirst().orElse(null);
//
//		if (msgIdNode != null) {
//			msgId = parseInt(msgIdNode);
//		}
//
//		if (msgNode != null) {
//			message = msgNode.getText();
//		}
//
//		if (addNameNode != null) {
//			addName = true;
//		}
//
//		for (Element conditionNode : conds) {
//			final String conditionNodeName = conditionNode.getName();
//			if ("cond".equals(conditionNodeName)) {
//				final StatsSet conditionSet = new StatsSet();
//				conditionNode
//						.getChildren()
//						.forEach(conditionParam ->
//								conditionSet.set(conditionParam.getName(), conditionParam.getText()));
//				final String conditionName = conditionSet.getString("name");
//
//				try {
//					final Constructor<?> conditionClass = Class.forName(Condition.class.getPackage().getName() + "." + conditionName).getConstructor(StatsSet.class);
//					final Condition condition = (Condition) conditionClass.newInstance(conditionSet);
//					if (msgId != null) {
//						condition.setMessageId(msgId);
//					}
//
//					if (message != null) {
//						condition.setMessage(message);
//					}
//
//					if (addName) {
//						condition.setAddName(true);
//					}
//
//					template.attachCondition(condition);
//				} catch (NoSuchMethodException | ClassNotFoundException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
//					log.error("Condition with the same name {} have exception - {}.", conditionName, e.getMessage(), e);
//				}
//			}
//		}
	}

//	/**
//	 * Create the ItemInstance corresponding to the Item Identifier and quantitiy add logs the activity. <B><U> Actions</U> :</B>
//	 * <li>Create and Init the ItemInstance corresponding to the Item Identifier and quantity</li>
//	 * <li>Add the ItemInstance object to _allObjects of L2world</li>
//	 * <li>Logs Item creation according to log settings</li>
//	 *
//	 * @param process   : String Identifier of process triggering this action
//	 * @param itemId    : int Item Identifier of the item to be created
//	 * @param count     : int Quantity of items to be created for stackable items
//	 * @param actor     : PlayerInstance Player requesting the item creation
//	 * @param reference : Object Object referencing current action like NPC selling item or previous item in transformation
//	 * @return ItemInstance corresponding to the new item
//	 */
//	public ItemInstance createItem(String process, int itemId, long count, PlayerInstance actor, Object reference) {
//		// Create and Init the ItemInstance corresponding to the Item Identifier
//		ItemInstance item = new ItemInstance(IdFactory.getInstance().getNextId(), itemId);
//
//		if (process.equalsIgnoreCase("loot")) {
//			ScheduledFuture<?> itemLootShedule;
//			if ((reference instanceof Attackable) && ((Attackable) reference).isRaid()) // loot privilege for raids
//			{
//				Attackable raid = (Attackable) reference;
//				// if in CommandChannel and was killing a World/RaidBoss
//				if ((raid.getFirstCommandChannelAttacked() != null) && !PlayerConfig.AUTO_LOOT_RAIDS) {
//					item.setOwnerId(raid.getFirstCommandChannelAttacked().getLeaderObjectId());
//					itemLootShedule = ThreadPool.schedule(new ResetOwner(item), PlayerConfig.LOOT_RAIDS_PRIVILEGE_INTERVAL, TimeUnit.MILLISECONDS);
//					item.setItemLootShedule(itemLootShedule);
//					item.setInstance(actor.getInstanceWorld());
//				}
//			} else if (!PlayerConfig.AUTO_LOOT || ((reference instanceof EventMonsterInstance) && ((EventMonsterInstance) reference).eventDropOnGround())) {
//				item.setOwnerId(actor.getObjectId());
//				itemLootShedule = ThreadPool.schedule(new ResetOwner(item), 15000, TimeUnit.MILLISECONDS);
//				item.setItemLootShedule(itemLootShedule);
//				item.setInstance(actor.getInstanceWorld());
//			}
//		}
//
//		if (GeneralConfig.DEBUG) {
//			log.debug("Item created: {}", item);
//		}
//
//		// Add the ItemInstance object to _allObjects of L2world
//		World.getInstance().addObject(item);
//
//		// Set Item parameters
//		if (item.isStackable() && (count > 1)) {
//			item.setCount(count);
//		}
//
//		if (GeneralConfig.LOG_ITEMS && !process.equals("Reset")) {
//			if (!GeneralConfig.LOG_ITEMS_SMALL_LOG || item.isEquipable() || item.getId() == ADENA_ID) {
//				if (item.getEnchantLevel() > 0) {
//					log.info("CREATE:{}, item {}:+{} {}({}), {}, {}", process, item.getObjectId(), item.getEnchantLevel(), item.getItem().getName(), item.getCount(), actor, reference);
//				} else {
//					log.info("CREATE:{}, item {}:{}({}), {}, {}", process, item.getObjectId(), item.getItem().getName(), item.getCount(), actor, reference);
//				}
//			}
//		}
//
//		if (actor != null) {
//			if (actor.isGM()) {
//				String referenceName = "no-reference";
//				if (reference instanceof WorldObject) {
//					referenceName = (((WorldObject) reference).getName() != null ? ((WorldObject) reference).getName() : "no-name");
//				} else if (reference instanceof String) {
//					referenceName = (String) reference;
//				}
//				String targetName = (actor.getTarget() != null ? actor.getTarget().getName() : "no-target");
//				if (GeneralConfig.GMAUDIT) {
//					GMAudit.auditGMAction(actor.getName() + " [" + actor.getObjectId() + "]", process + "(id: " + itemId + " count: " + count + " name: " + item.getItemName() + " objId: " + item.getObjectId() + ")", targetName, "WorldObject referencing this action is: " + referenceName);
//				}
//			}
//		}
//
//		// Notify to scripts
//		EventDispatcher.getInstance().notifyEventAsync(new OnItemCreate(process, item, actor, reference), item.getItem());
//		return item;
//	}
//
//	/**
//	 * Destroys the ItemInstance.<br>
//	 * <B><U> Actions</U> :</B>
//	 * <ul>
//	 * <li>Sets ItemInstance parameters to be unusable</li>
//	 * <li>Removes the ItemInstance object to _allObjects of L2world</li>
//	 * <li>Logs Item deletion according to log settings</li>
//	 * </ul>
//	 *
//	 * @param process   a string identifier of process triggering this action.
//	 * @param item      the item instance to be destroyed.
//	 * @param actor     the player requesting the item destroy.
//	 * @param reference the object referencing current action like NPC selling item or previous item in transformation.
//	 */
//	public void destroyItem(String process, ItemInstance item, PlayerInstance actor, Object reference) {
//		synchronized (item) {
//			long old = item.getCount();
//			item.setCount(0);
//			item.setOwnerId(0);
//			item.setItemLocation(ItemLocation.VOID);
//			item.setLastChange(ItemInstance.REMOVED);
//
//			World.getInstance().removeObject(item);
//			IdFactory.getInstance().releaseId(item.getObjectId());
//
//			if (GeneralConfig.LOG_ITEMS) {
//				if (!GeneralConfig.LOG_ITEMS_SMALL_LOG || item.isEquipable() || item.getId() == ADENA_ID) {
//					if (item.getEnchantLevel() > 0) {
//						log.info("DELETE:{}, item {}:+{} {}({}), PrevCount({}), {}, {}", process, item.getObjectId(), item.getEnchantLevel(), item.getItem().getName(), item.getCount(), old, actor, reference);
//					} else {
//						log.info("DELETE:{}, item {}:{}({}), PrevCount({}), {}, {}", process, item.getObjectId(), item.getItem().getName(), item.getCount(), old, actor, reference);
//					}
//				}
//			}
//
//			if (actor != null) {
//				if (actor.isGM()) {
//					String referenceName = "no-reference";
//					if (reference instanceof WorldObject) {
//						referenceName = (((WorldObject) reference).getName() != null ? ((WorldObject) reference).getName() : "no-name");
//					} else if (reference instanceof String) {
//						referenceName = (String) reference;
//					}
//					String targetName = (actor.getTarget() != null ? actor.getTarget().getName() : "no-target");
//					if (GeneralConfig.GMAUDIT) {
//						GMAudit.auditGMAction(actor.getName() + " [" + actor.getObjectId() + "]", process + "(id: " + item.getId() + " count: " + item.getCount() + " itemObjId: " + item.getObjectId() + ")", targetName, "WorldObject referencing this action is: " + referenceName);
//					}
//				}
//			}
//
//			// if it's a pet control item, delete the pet as well
//			if (item.getItem().isPetItem()) {
//				try (Connection con = DatabaseFactory.getInstance().getConnection();
//					 PreparedStatement statement = con.prepareStatement("DELETE FROM pets WHERE item_obj_id=?")) {
//					// Delete the pet in db
//					statement.setInt(1, item.getObjectId());
//					statement.execute();
//				} catch (Exception e) {
//					log.warn("could not delete pet objectid:", e);
//				}
//			}
//		}
//	}
//
//	protected static class ResetOwner implements Runnable {
//		ItemInstance _item;
//
//		public ResetOwner(ItemInstance item) {
//			_item = item;
//		}
//
//		@Override
//		public void run() {
//			_item.setOwnerId(0);
//			_item.setItemLootShedule(null);
//		}
//	}
}
