package org.genfork.gameserver.slave.data.xml;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.genfork.gameserver.slave.config.ServerConfig;
import org.genfork.server.enums.AutoUseType;
import org.genfork.server.model.pojo.AutoUseHolder;
import org.genfork.tools.loader.annotations.InstanceGetter;
import org.genfork.tools.loader.annotations.Load;
import org.genfork.tools.loader.interfaces.LoadGroup;
import org.genfork.tools.xml.XmlReader;
import org.jdom2.Element;
import org.jdom2.JDOMException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author: ngolubenko@context-it.ru
 * @created: 2020/01
 */
@Slf4j
public class AutoUseOptionDataParser implements XmlReader {
	@Getter(lazy = true, onMethod = @__({@InstanceGetter}))
	private static final AutoUseOptionDataParser instance = new AutoUseOptionDataParser();

	private final List<AutoUseHolder> autoUseHolders = new ArrayList<>();

	public AutoUseHolder getOption(int id, boolean skillOption) {
		return autoUseHolders
				.parallelStream()
				.filter(i ->
						skillOption && i.getSkillOption() == 1)
				.filter(i ->
						i.getId() == id)
				.findFirst()
				.orElse(null);
	}

	@Load(group = LoadGroup.class)
	public void load() throws JDOMException, IOException {
		parseFile("data/options/AutoUseData.xml", ServerConfig.DATA_PACK_ROOT);
		log.info("Loaded auto-use options {} size.", autoUseHolders.size());
	}

	@Override
	public void loadFile(Element rootElement) {
		forEach(rootElement, "auto_option", option -> {
			final Integer skillOption = parseInt(option, "skillOption");
			final Integer id = parseInt(option, "id");
			final AutoUseType autoUseType = parseEnum(option, "type", AutoUseType.class);

			final AutoUseHolder holder = new AutoUseHolder();
			holder.setSkillOption(skillOption);
			holder.setId(id);
			holder.setAutoUseType(autoUseType);

			autoUseHolders.add(holder);
		});
	}
}
