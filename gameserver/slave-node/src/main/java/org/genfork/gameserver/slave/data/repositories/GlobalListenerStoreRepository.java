package org.genfork.gameserver.slave.data.repositories;

import org.genfork.server.objects.component.containers.ListenersContainer;
import org.genfork.tools.persistence.annotations.DataMethod;
import org.genfork.tools.persistence.annotations.DataRepository;
import org.genfork.tools.persistence.annotations.DataValue;
import org.genfork.tools.persistence.annotations.ExpParam;
import org.genfork.tools.persistence.repository.CrudDataRepository;
import org.genfork.tools.persistence.repository.handler.OperationHandler;

import static org.genfork.tools.persistence.annotations.DataMethod.Type.READ;
import static org.genfork.tools.persistence.annotations.DataMethod.Type.WRITE;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@DataRepository(bucketName = "global_listeners_store")
public interface GlobalListenerStoreRepository extends CrudDataRepository {
	@DataMethod(enableLock = false, type = WRITE, keyExpression = "#name + '_' + T(org.genfork.gameserver.slave.config.ServerConfig).PLAYER_SERVER_ID")
	void lockAndAddListenerContainer(@ExpParam("name") String name, OperationHandler<ListenersContainer> operationHandler);

	@DataMethod(enableLock = false, type = WRITE, keyExpression = "#name + '_' + T(org.genfork.gameserver.slave.config.ServerConfig).PLAYER_SERVER_ID")
	void addListenerContainer(@ExpParam("name") String name, @DataValue ListenersContainer container);

	@DataMethod(enableLock = false, type = READ, keyExpression = "#name + '_' + T(org.genfork.gameserver.slave.config.ServerConfig).PLAYER_SERVER_ID")
	ListenersContainer getContainer(@ExpParam("name") String name);
}
