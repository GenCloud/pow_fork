package org.genfork.gameserver.slave.config;

import org.genfork.tools.config.abstraction.ConfigPropertiesLoader;
import org.genfork.tools.config.annotation.ConfigClass;
import org.genfork.tools.config.annotation.ConfigField;

import java.util.regex.Pattern;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@ConfigClass(fileName = "Player")
public class PlayerConfig extends ConfigPropertiesLoader {
	@ConfigField(name = "PlayerNameTemplate", value = ".*", comment =
			{
					"Player name template.",
					"Examples:",
					"PlayerNameTemplate = [A-Z][a-z]{3,3}[A-Za-z0-9]*",
					"The above setting will allow names with first capital letter, next three small letters,",
					"and any letter (case insensitive) or number, like OmfgWTF1",
					"PlayerNameTemplate = [A-Z][a-z]*",
					"The above setting will allow names only of letters with first one capital, like Omfgwtf",
					"The .* (allows any symbol)",
			})
	public static Pattern PLAYER_NAME_TEMPLATE_PATTERN;

	@ConfigField(name = "PlayerMaxNumber", value = "7", comment =
			{
					"Maximum number of chars per account."
			})
	public static int MAX_CHARACTERS_NUMBER_PER_ACCOUNT;

	@ConfigField(name = "InitialEquipmentEvent", value = "false", comment =
			{
					"Initial Equipment Events is to enable a special settings for the items that a new character starts with."
			})
	public static boolean INITIAL_EQUIPMENT_EVENT;
}
