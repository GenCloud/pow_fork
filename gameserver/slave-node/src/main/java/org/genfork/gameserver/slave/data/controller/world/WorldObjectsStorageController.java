package org.genfork.gameserver.slave.data.controller.world;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.genfork.gameserver.slave.data.controller.player.PlayerBroadcastController;
import org.genfork.gameserver.slave.data.repositories.ObjectStoreRepository;
import org.genfork.gameserver.slave.managers.DataStoreManager;
import org.genfork.server.model.world.WorldRegion;
import org.genfork.server.objects.Player;
import org.genfork.server.objects.WorldObject;
import org.genfork.server.utils.Util;
import org.genfork.tools.loader.annotations.Dependency;
import org.genfork.tools.loader.annotations.InstanceGetter;
import org.genfork.tools.loader.annotations.Load;
import org.genfork.tools.loader.interfaces.PreLoadGroup;
import org.genfork.tools.persistence.repository.handler.OperationHandler;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Slf4j
public class WorldObjectsStorageController {
	/**
	 * Gracia border Flying objects not allowed to the east of it.
	 */
	public static final int GRACIA_MAX_X = -166168;
	public static final int GRACIA_MAX_Z = 6105;
	public static final int GRACIA_MIN_Z = -895;
	/**
	 * Bit shift, defines number of regions note, shifting by 15 will result in regions corresponding to map tiles shifting by 11 divides one tile to 16x16 regions.
	 */
	public static final int SHIFT_BY = 11;
	public static final int SHIFT_BY_Z = 10;
	/**
	 * Map dimensions
	 */
	public static final int TILE_X_MIN = 11;
	public static final int TILE_Y_MIN = 10;
	public static final int TILE_X_MAX = 28;
	public static final int TILE_Y_MAX = 26;
	public static final int TILE_ZERO_COORD_X = 20;
	public static final int TILE_ZERO_COORD_Y = 18;

	private static final int TILE_SIZE = 32768;
	public static final int MAP_MIN_X = (TILE_X_MIN - TILE_ZERO_COORD_X) * TILE_SIZE;
	public static final int MAP_MIN_Y = (TILE_Y_MIN - TILE_ZERO_COORD_Y) * TILE_SIZE;
	public static final int MAP_MIN_Z = -TILE_SIZE / 2;
	public static final int MAP_MAX_X = ((TILE_X_MAX - TILE_ZERO_COORD_X) + 1) * TILE_SIZE;
	public static final int MAP_MAX_Y = ((TILE_Y_MAX - TILE_ZERO_COORD_Y) + 1) * TILE_SIZE;
	public static final int MAP_MAX_Z = TILE_SIZE / 2;
	public static final int REGION_MIN_DIMENSION = Math.min(TILE_SIZE / (TILE_SIZE >> SHIFT_BY_Z), TILE_SIZE / (TILE_SIZE >> SHIFT_BY));
	public static final int OFFSET_X = Math.abs(MAP_MIN_X >> SHIFT_BY);
	public static final int OFFSET_Y = Math.abs(MAP_MIN_Y >> SHIFT_BY);
	public static final int OFFSET_Z = Math.abs(MAP_MIN_Z >> SHIFT_BY_Z);

	public static final int REGIONS_X = (MAP_MAX_X >> SHIFT_BY) + OFFSET_X;
	public static final int REGIONS_Y = (MAP_MAX_Y >> SHIFT_BY) + OFFSET_Y;

	public static final int REGIONS_Z = (MAP_MAX_Z >> SHIFT_BY_Z) + OFFSET_Z;

	@Getter(lazy = true, onMethod = @__(@InstanceGetter))
	private static final WorldObjectsStorageController instance = new WorldObjectsStorageController();

	private final ObjectStoreRepository objectStoreRepository;

	private WorldObjectsStorageController() {
		objectStoreRepository = DataStoreManager.getInstance().getRepository(ObjectStoreRepository.class);
	}

	@Load(group = PreLoadGroup.class, dependencies = @Dependency(clazz = DataStoreManager.class))
	private void load() {
		log.info("({} by {} by {}) World Region Grid set up.", REGIONS_X, REGIONS_Y, REGIONS_Z);
	}

	public void findAndAccept(Predicate<WorldObject> predicate, Consumer<WorldObject> c) {
		getVisibleObjects().values().stream().filter(predicate).forEach(c);
	}

	public WorldObject findObject(Predicate<WorldObject> predicate) {
		return getVisibleObjects().values().stream().filter(predicate).findFirst().orElse(null);
	}

	public void findObject(String hashName, OperationHandler<? extends WorldObject> operationHandler) {
		objectStoreRepository.lockAndGetObject(hashName, operationHandler);
	}

	public WorldObject findObject(String hashName) {
		return objectStoreRepository.getObject(hashName);
	}

	public Map<String, WorldObject> getVisibleObjects() {
		return objectStoreRepository.getObjects();
	}

	public void removeObject(WorldObject object) {
		objectStoreRepository.removeObject(object);
	}

	public void publishVisibleObjectToOther(WorldObject object, WorldRegion newRegion) {
		if (!WorldRegionController.getInstance().getActiveOption(newRegion)) {
			return;
		}

		forEachVisibleObject(object, WorldObject.class, 1, wo -> {
			if (object.isPlayer() && wo.isVisibleFor((Player) object)) {
				final Player player = object.asPlayer();
				PlayerBroadcastController.getInstance().sendInfo(wo, player);
			}

			if (wo.isPlayer() && object.isVisibleFor((Player) wo)) {
				final Player player = wo.asPlayer();
				PlayerBroadcastController.getInstance().sendInfo(object, player);
			}
		});
	}

	private <T extends WorldObject> void forEachVisibleObject(WorldObject object, Class<T> clazz, int depth, Consumer<T> c) {
		if (object == null) {
			return;
		}

		final WorldRegion centerWorldRegion = WorldRegionController.getInstance().getRegion(object);
		if (centerWorldRegion == null) {
			return;
		}

		for (int x = Math.max(centerWorldRegion.getRegionX() - depth, 0); x <= Math.min(centerWorldRegion.getRegionX() + depth, REGIONS_X); x++) {
			for (int y = Math.max(centerWorldRegion.getRegionY() - depth, 0); y <= Math.min(centerWorldRegion.getRegionY() + depth, REGIONS_Y); y++) {
				for (int z = Math.max(centerWorldRegion.getRegionZ() - depth, 0); z <= Math.min(centerWorldRegion.getRegionZ() + depth, REGIONS_Z); z++) {
					final WorldRegion region = WorldRegionController.getInstance().getRegions(x, y, z);
					final Map<Integer, WorldObject> visibleObjects = WorldRegionController.getInstance().getObjectsOption(region);
					for (WorldObject visibleObject : visibleObjects.values()) {
						if (visibleObject == null || visibleObject == object || !clazz.isInstance(visibleObject)) {
							continue;
						}

//						TODO
//						if (visibleObject.getInstanceWorld() != object.getInstanceWorld()) {
//							continue;
//						}

						c.accept(clazz.cast(visibleObject));
					}
				}
			}
		}
	}

	public <T extends WorldObject> void forEachVisibleObject(WorldObject object, Class<T> clazz, Consumer<T> c) {
		forEachVisibleObject(object, clazz, 1, c);
	}

	public <T extends WorldObject> void forEachVisibleObjectInRadius(WorldObject object, Class<T> clazz, int radius, Consumer<T> c) {
		if (object == null) {
			return;
		}

		final WorldRegion centerWorldRegion = WorldRegionController.getInstance().getRegion(object);
		if (centerWorldRegion == null) {
			return;
		}

		final int depth = (radius / REGION_MIN_DIMENSION) + 1;
		for (int x = Math.max(centerWorldRegion.getRegionX() - depth, 0); x <= Math.min(centerWorldRegion.getRegionX() + depth, REGIONS_X); x++) {
			for (int y = Math.max(centerWorldRegion.getRegionY() - depth, 0); y <= Math.min(centerWorldRegion.getRegionY() + depth, REGIONS_Y); y++) {
				for (int z = Math.max(centerWorldRegion.getRegionZ() - depth, 0); z <= Math.min(centerWorldRegion.getRegionZ() + depth, REGIONS_Z); z++) {
					final int x1 = (x - OFFSET_X) << SHIFT_BY;
					final int y1 = (y - OFFSET_Y) << SHIFT_BY;
					final int z1 = (z - OFFSET_Z) << SHIFT_BY_Z;
					final int x2 = ((x + 1) - OFFSET_X) << SHIFT_BY;
					final int y2 = ((y + 1) - OFFSET_Y) << SHIFT_BY;
					final int z2 = ((z + 1) - OFFSET_Z) << SHIFT_BY_Z;
					if (Util.cubeIntersectsSphere(x1, y1, z1, x2, y2, z2, object.getRealX(), object.getRealY(), object.getRealZ(), radius)) {
						final WorldRegion region = WorldRegionController.getInstance().getRegions(x, y, z);
						final Map<Integer, WorldObject> visibleObjects = WorldRegionController.getInstance().getObjectsOption(region);
						for (WorldObject visibleObject : visibleObjects.values()) {
							if ((visibleObject == null) || (visibleObject == object) || !clazz.isInstance(visibleObject)) {
								continue;
							}

//							TODO
//							if (visibleObject.getInstanceWorld() != object.getInstanceWorld()) {
//								continue;
//							}

							if (visibleObject.isInRadius3d(object, radius)) {
								c.accept(clazz.cast(visibleObject));
							}
						}
					}
				}
			}
		}
	}

	public <T extends WorldObject> List<T> getVisibleObjects(WorldObject object, Class<T> clazz) {
		final List<T> result = new LinkedList<>();
		forEachVisibleObject(object, clazz, result::add);
		return result;
	}

	public <T extends WorldObject> List<T> getVisibleObjects(WorldObject object, Class<T> clazz, Predicate<T> predicate) {
		final List<T> result = new LinkedList<>();
		forEachVisibleObject(object, clazz, o -> {
			if (predicate.test(o)) {
				result.add(o);
			}
		});
		return result;
	}

	public <T extends WorldObject> List<T> getVisibleObjects(WorldObject object, Class<T> clazz, int radius) {
		final List<T> result = new LinkedList<>();
		forEachVisibleObjectInRadius(object, clazz, radius, result::add);
		return result;
	}

	public <T extends WorldObject> List<T> getVisibleObjects(WorldObject object, Class<T> clazz, int radius, Predicate<T> predicate) {
		final List<T> result = new LinkedList<>();
		forEachVisibleObjectInRadius(object, clazz, radius, o -> {
			if (predicate.test(o)) {
				result.add(o);
			}
		});
		return result;
	}
}
