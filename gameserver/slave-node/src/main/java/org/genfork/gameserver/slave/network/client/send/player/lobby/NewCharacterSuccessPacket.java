package org.genfork.gameserver.slave.network.client.send.player.lobby;

import lombok.RequiredArgsConstructor;
import org.genfork.server.objects.component.data.PlayerData;
import org.genfork.tools.network.OutgoingPacket;

import java.util.List;

import static org.genfork.tools.network.opcodes.GameBranchOpcodes.NEW_CHARACTER_SUCCESS;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@RequiredArgsConstructor
public class NewCharacterSuccessPacket extends OutgoingPacket {
	private final List<PlayerData> playerData;

	@Override
	public boolean write() {
		NEW_CHARACTER_SUCCESS.writeId(this);

		writeD(playerData.size());
		for (PlayerData template : playerData) {
			if (template == null) {
				continue;
			}

			writeD(template.getRace().ordinal());
			writeD(template.getClassId().getId());

			writeD(99);
			writeD(template.getBaseSTR());
			writeD(1);

			writeD(99);
			writeD(template.getBaseDEX());
			writeD(1);

			writeD(99);
			writeD(template.getBaseCON());
			writeD(1);

			writeD(99);
			writeD(template.getBaseINT());
			writeD(1);

			writeD(99);
			writeD(template.getBaseWIT());
			writeD(1);

			writeD(99);
			writeD(template.getBaseMEN());
			writeD(1);

			writeD(99);
			writeD(template.getBaseLUC());
			writeD(1);

			writeD(99);
			writeD(template.getBaseCHA());
			writeD(1);
		}
		return true;
	}
}
