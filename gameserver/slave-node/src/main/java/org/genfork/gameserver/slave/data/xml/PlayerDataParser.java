package org.genfork.gameserver.slave.data.xml;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.genfork.gameserver.slave.config.ServerConfig;
import org.genfork.server.enums.ClassId;
import org.genfork.server.location.Location;
import org.genfork.server.objects.component.data.PlayerData;
import org.genfork.tools.data.StatsSet;
import org.genfork.tools.loader.annotations.InstanceGetter;
import org.genfork.tools.loader.annotations.Load;
import org.genfork.tools.loader.interfaces.LoadGroup;
import org.genfork.tools.xml.XmlReader;
import org.jdom2.Element;

import java.util.*;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Slf4j
public class PlayerDataParser implements XmlReader {
	@Getter(lazy = true, onMethod = @__(@InstanceGetter))
	private static final PlayerDataParser instance = new PlayerDataParser();

	private final Map<Integer, PlayerData> playersData = new HashMap<>();
	private List<PlayerData> creationList;

	public List<PlayerData> getCreationList() {
		return creationList;
	}

	public PlayerData getData(ClassId classId) {
		return playersData.get(classId.getId());
	}

	public PlayerData getData(Integer classId) {
		return playersData.get(classId);
	}

	public boolean existsData(Integer classId) {
		return playersData.containsKey(classId);
	}

	@Load(group = LoadGroup.class)
	public void load() throws Exception {
		parseDirectory("data/player/templates", ServerConfig.DATA_PACK_ROOT, false);
		log.info("Loaded {} player templates.", playersData.size());

		creationList = Arrays
				.asList(getData(ClassId.FIGHTER), // Human Figther
						getData(ClassId.MAGE), // Human Mystic
						getData(ClassId.ELVEN_FIGHTER), // Elven Fighter
						getData(ClassId.ELVEN_MAGE), // Elven Mystic
						getData(ClassId.DARK_FIGHTER), // Dark Fighter
						getData(ClassId.DARK_MAGE), // Dark Mystic
						getData(ClassId.ORC_FIGHTER), // Orc Fighter
						getData(ClassId.ORC_MAGE), // Orc Mystic
						getData(ClassId.DWARVEN_FIGHTER), // Dwarf Fighter
						getData(ClassId.MALE_SOLDIER), // Male Kamael Soldier
						getData(ClassId.FEMALE_SOLDIER), // Female Kamael Soldier
						getData(ClassId.ERTHEIA_FIGHTER), // Ertheia Fighter
						getData(ClassId.ERTHEIA_WIZARD));// Ertheia Wizard
	}

	@Override
	public void loadFile(Element rootElement) {
		final StatsSet set = new StatsSet();
		forEach(rootElement, node -> {
			switch (node.getName()) {
				case "class_id": {
					set.set("class_id", Integer.parseInt(node.getText()));
					break;
				}
				case "static_data": {
					forEach(node, staticDataNode ->
					{
						switch (staticDataNode.getName()) {
							case "creation_points": {
								final List<Location> creationPoints = new ArrayList<>();
								forEach(staticDataNode, "node", innerNode -> {
									final int x = parseInt(innerNode, "x");
									final int y = parseInt(innerNode, "y");
									final int z = parseInt(innerNode, "z");
									creationPoints.add(new Location(x, y, z));
								});

								set.set("creation_points", creationPoints);
								break;
							}
							case "physical_defence": {
								forEach(staticDataNode, innerNode -> {
									switch (innerNode.getName()) {
										case "chest":
										case "legs":
										case "head":
										case "feet":
										case "gloves":
										case "underwear":
										case "cloak":
											set.set(staticDataNode.getName() + "_" + innerNode.getName(), innerNode.getText());
											break;
									}
								});
								break;
							}
							case "magical_defence": {
								forEach(staticDataNode, innerNode -> {
									switch (innerNode.getName()) {
										case "rear":
										case "lear":
										case "rfinger":
										case "lfinger":
										case "neck":
											set.set(staticDataNode.getName() + "_" + innerNode.getName(), innerNode.getText());
											break;
									}
								});
								break;
							}
							case "damage_range": {
								forEach(staticDataNode, innerNode -> {
									switch (innerNode.getName()) {
										case "vertical_direction":
										case "horizontal_direction":
										case "distance":
										case "width":
											set.set(staticDataNode.getName() + "_" + innerNode.getName(), innerNode.getText());
											break;
									}
								});
								break;
							}
							case "move_speed": {
								forEach(staticDataNode, innerNode -> {
									switch (innerNode.getName()) {
										case "walk":
										case "run":
										case "walk_swim":
										case "run_swim":
											set.set(staticDataNode.getName() + "_" + innerNode.getName(), innerNode.getText());
											break;
									}
								});
								break;
							}
							case "collision_male": {
								parseCollision(set, staticDataNode);
								break;
							}
							case "collision_female": {
								parseCollision(set, staticDataNode);
							}
							default: {
								set.set(staticDataNode.getName(), staticDataNode.getText());
								break;
							}
						}
					});

					// calculate total pdef and mdef from parts
					set.set("physical_defence", set.getInt("physical_defence_chest", 0)
							+ set.getInt("physical_defence_legs", 0)
							+ set.getInt("physical_defence_head", 0)
							+ set.getInt("physical_defence_feet", 0)
							+ set.getInt("physical_defence_gloves", 0)
							+ set.getInt("physical_defence_underwear", 0)
							+ set.getInt("physical_defence_cloak", 0));

					set.set("magical_defence", set.getInt("magical_defence_rear", 0)
							+ set.getInt("magical_defence_lear", 0)
							+ set.getInt("magical_defence_rfinger", 0)
							+ set.getInt("magical_defence_rfinger", 0)
							+ set.getInt("magical_defence_neck", 0));

					playersData.compute(set.getInt("class_id"), (key, value) -> new PlayerData(set));
					break;
				}
				case "lvl_up_gain": {
					forEach(node, "level", lvlUpNode ->
					{
						final int level = parseInt(lvlUpNode, "val");
						final PlayerData template = playersData.get(set.getInt("class_id"));

						forEach(lvlUpNode, innerNode -> {
							switch (innerNode.getName()) {
								case "hp":
								case "mp":
								case "cp":
								case "hp_regen":
								case "mp_regen":
								case "cp_regen": {
									template.setUpGainValue(innerNode.getName(), level, Double.parseDouble(innerNode.getText()));
									break;
								}
							}
						});
					});
					break;
				}
			}
		});
	}

	private void parseCollision(StatsSet set, Element staticDataNode) {
		forEach(staticDataNode, innerNode -> {
			switch (innerNode.getName()) {
				case "radius":
				case "height":
					set.set(staticDataNode.getName() + "_" + innerNode.getName(), innerNode.getText());
					break;
			}
		});
	}
}
