package org.genfork.gameserver.slave.network.enums;

import org.genfork.tools.network.IConnectionState;

public enum ConnectionState implements IConnectionState {
	REGISTERED,
	CONNECTED,
	DISCONNECTED,
	CLOSING,
	AUTHENTICATED,
	IN_GAME
}
