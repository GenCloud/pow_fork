package org.genfork.gameserver.slave.network.client.recv.player.lobby;

import org.genfork.gameserver.slave.data.xml.PlayerDataParser;
import org.genfork.gameserver.slave.network.client.send.player.lobby.NewCharacterSuccessPacket;
import org.genfork.gameserver.slave.network.discovery.DiscoveryServerHandler;
import org.genfork.server.objects.component.data.PlayerData;
import org.genfork.tools.network.IncomingPacket;

import java.util.List;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class RequestNewPlayerPacket extends IncomingPacket<DiscoveryServerHandler> {
	@Override
	public boolean read(DiscoveryServerHandler client) {
		return true;
	}

	@Override
	public void run(DiscoveryServerHandler client) throws Exception {
		final List<PlayerData> creationList = PlayerDataParser.getInstance().getCreationList();
		client.sendPacket(this, new NewCharacterSuccessPacket(creationList));
	}
}
