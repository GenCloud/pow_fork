package org.genfork.gameserver.slave.network.client.send.player.inventory;

import lombok.RequiredArgsConstructor;
import org.genfork.server.objects.Item;
import org.genfork.server.objects.component.inventory.ItemContainerData;

import java.util.Collection;

import static org.genfork.tools.network.opcodes.GameBranchOpcodes.EX_QUEST_ITEMLIST;

/**
 * @author: ngolubenko@context-it.ru
 * @created: 2020/04
 */
@RequiredArgsConstructor
public class ExQuestInventoryDataPacket extends AbstractInventoryPacket {
	private final int sendType;
	private final ItemContainerData itemContainerData;
	private final Collection<Item> items;

	@Override
	public boolean write() {
		EX_QUEST_ITEMLIST.writeId(this);

		writeC(sendType);
		if (sendType == 2) {
			writeD(items.size());
		} else {
			writeH(0);
		}

		writeD(items.size());
		items.forEach(this::writeItem);

		writeInventoryBlock(itemContainerData);
		return true;
	}
}
