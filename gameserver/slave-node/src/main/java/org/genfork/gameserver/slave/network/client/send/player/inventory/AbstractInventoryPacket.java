package org.genfork.gameserver.slave.network.client.send.player.inventory;

import org.genfork.gameserver.slave.enums.ItemListType;
import org.genfork.gameserver.slave.network.client.send.player.AbstractMaskPacket;
import org.genfork.server.objects.Item;
import org.genfork.server.objects.component.inventory.ItemContainerData;

/**
 * @author: ngolubenko@context-it.ru
 * @created: 2020/04
 */
public abstract class AbstractInventoryPacket extends AbstractMaskPacket<ItemListType> {
	private static final byte[] MASKS =
			{
					0x00
			};

	//	TODO
	protected static int calculateMask(Item item) {
		int mask = 0;
//		if (item.getVariationInstance() != null) {
//			mask |= ItemListType.AUGMENT_BONUS.getMask();
//		}

//		if (item.getElementAttackType() >= 0
//				|| item.getAttributeDefence(AttributeType.FIRE) > 0
//				|| item.getAttributeDefence(AttributeType.WATER) > 0
//				|| item.getAttributeDefence(AttributeType.WIND) > 0
//				|| item.getAttributeDefence(AttributeType.EARTH) > 0
//				|| item.getAttributeDefence(AttributeType.HOLY) > 0
//				|| item.getAttributeDefence(AttributeType.DARK) > 0) {
//			mask |= ItemListType.ELEMENTAL_ATTRIBUTE.getMask();
//		}

//		if (item.getEnchantOptions() != null) {
//			if (Arrays.stream(item.getEnchantOptions()).anyMatch(id -> id > 0)) {
//				mask |= ItemListType.ENCHANT_EFFECT.getMask();
//			}
//		}

		if (item.getVisualId() > 0) {
			mask |= ItemListType.VISUAL_ID.getMask();
		}

//		if ((item.getSoulCrystalOptions() != null && !item.getSoulCrystalOptions().isEmpty())
//				|| (item.getSoulCrystalSpecialOptions() != null && !item.getSoulCrystalSpecialOptions().isEmpty())) {
//			mask |= ItemListType.SOUL_CRYSTAL.getMask();
//		}

//		mask |= 0x12;
//		mask |= 0x14;
//		if (player != null && player.getItemRemainingReuseTime(item.getObjectId()) > 0) {
//			mask |= ItemListType.REUSE.getMask();
//		}

		return mask;
	}

	@Override
	protected byte[] getMasks() {
		return MASKS;
	}

	protected void writeItem(Item item) {
		final int mask = calculateMask(item);
		// cddcQcchQccddc
		writeC(mask);
		writeD(item.getObjectId()); // ObjectId
		writeD(item.getItemData().getDisplayId()); // ItemId
		writeC(item.getItemData().isQuestItem() || item.isEquipped() ? 0xFF : item.getLocationMask()); // T1
		writeQ(item.getAmount()); // Quantity
		writeC(item.getItemData().getTypeSecond()); // Item Type 2 : 00-weapon, 01-shield/armor, 02-ring/earring/necklace, 03-questitem, 04-adena, 05-item
		writeC(0); // Filler (always 0) item.getTypeFirst()
		writeH(item.isEquipped() ? 1 : 0); // Equipped : 00-No, 01-yes
		writeQ(item.getItemData().getBodyPart()); // Slot : 0006-lr.ear, 0008-neck, 0030-lr.finger, 0040-head, 0100-l.hand, 0200-gloves, 0400-chest, 0800-pants, 1000-feet, 4000-r.hand, 8000-r.hand
		writeC(item.getEnchant()); // Enchant level (pet level shown in control item)
		writeC(item.getTypeSecond());
		writeD(item.getMpLeft());
		writeD(/*item.getTime()*/ -1);

		writeC(0x01); // GOD Item enabled = 1 disabled (red) = 0
		writeC(0x00);
		writeC(0x00);

		if (containsMask(mask, ItemListType.AUGMENT_BONUS)) {
			writeItemAugment(item);
		}

		if (containsMask(mask, ItemListType.ELEMENTAL_ATTRIBUTE)) {
			writeItemElemental(item);
		}

		if (containsMask(mask, ItemListType.ENCHANT_EFFECT)) {
			writeItemEnchantEffect(item);
		}

		if (containsMask(mask, ItemListType.VISUAL_ID)) {
			writeD(item.getVisualId()); // Item remodel visual ID
		}

		if (containsMask(mask, ItemListType.SOUL_CRYSTAL)) {
			writeItemEnsoulOptions(item);
		}

//		if (containsMask(mask, ItemListType.REUSE) && player != null && player.getItemRemainingReuseTime(item.getObjectId()) > 0) {
//			writeItemReuseData(player.getItemRemainingReuseTime(item.getObjectId()));
//		}
	}

	protected void writeItemAugment(Item item) {
//		if (item != null && item.getVariationInstance() != null) {
//			writeD(item.getVariationInstance().getOption1Id());
//			writeD(item.getVariationInstance().getOption2Id());
//		} else
		{
			writeD(0);
			writeD(0);
		}
	}

	@SuppressWarnings("unused")
	protected void writeItemElementalAndEnchant(Item item) {
		writeItemElemental(item);
		writeItemEnchantEffect(item);
	}

	protected void writeItemElemental(Item item) {
//		if (item != null) {
//			writeH(item.getElementAttackType());
//			writeH(item.getElementAttackPower());
//			writeH(item.getAttributeDefence(AttributeType.FIRE));
//			writeH(item.getAttributeDefence(AttributeType.WATER));
//			writeH(item.getAttributeDefence(AttributeType.WIND));
//			writeH(item.getAttributeDefence(AttributeType.EARTH));
//			writeH(item.getAttributeDefence(AttributeType.HOLY));
//			writeH(item.getAttributeDefence(AttributeType.DARK));
//		} else {
		writeH(0);
		writeH(0);
		writeH(0);
		writeH(0);
		writeH(0);
		writeH(0);
		writeH(0);
		writeH(0);
//		}
	}

	protected void writeItemEnchantEffect(Item item) {
		writeD(0x00);
		// Enchant Effects
//		Arrays.stream(item.getEnchantOptions()).forEach(packet::writeD);
	}

	protected void writeItemEnsoulOptions(Item item) {
//		if (item != null) {
//			writeC(item.getSoulCrystalOptions().size()); // Size of regular soul crystal options.
//			// Regular Soul Crystal Ability ID.
//			item.getSoulCrystalOptions().stream().mapToInt(EnsoulOption::getId).forEach(packet::writeD);
//
//			writeC(item.getSoulCrystalSpecialOptions().size()); // Size of special soul crystal options.
//			// Special Soul Crystal Ability ID.
//			item.getSoulCrystalSpecialOptions().stream().mapToInt(EnsoulOption::getId).forEach(packet::writeD);
//		} else {
		writeC(0); // Size of regular soul crystal options.
		writeC(0); // Size of special soul crystal options.
//		}
	}

	protected void writeInventoryBlock(ItemContainerData containerData) {
		if (containerData.hasInventoryBlock()) {
			writeH(containerData.getBlockItems().size());
			writeC(containerData.getBlockMode().getClientId());
			containerData.getBlockItems().forEach(this::writeD);
		} else {
			writeH(0x00);
		}
	}

	protected void writeItemReuseData(long reuseDelay) {
		writeD((int) reuseDelay);
	}
}
