package org.genfork.gameserver.slave.data.controller.world;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.genfork.gameserver.slave.data.controller.player.PlayerBroadcastController;
import org.genfork.gameserver.slave.data.repositories.WorldRegionStoreRepository;
import org.genfork.gameserver.slave.managers.DataStoreManager;
import org.genfork.server.model.world.WorldRegion;
import org.genfork.server.objects.Player;
import org.genfork.server.objects.WorldObject;
import org.genfork.server.objects.component.ILocational;
import org.genfork.tools.data.StatsSet;
import org.genfork.tools.loader.annotations.InstanceGetter;
import org.genfork.tools.loader.annotations.Load;
import org.genfork.tools.loader.interfaces.PreLoadGroup;
import org.genfork.tools.persistence.repository.handler.OperationHandler;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Predicate;

import static org.genfork.gameserver.slave.data.controller.world.WorldObjectsStorageController.*;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Slf4j
public class WorldRegionController {
	@Getter(lazy = true, onMethod = @__(@InstanceGetter))
	private static final WorldRegionController instance = new WorldRegionController();

	private final WorldRegion[][][] regions = new WorldRegion[REGIONS_X + 1][REGIONS_Y + 1][REGIONS_Z + 1];

	private final WorldRegionStoreRepository worldRegionStoreRepository;

	private WorldRegionController() {
		worldRegionStoreRepository = DataStoreManager.getInstance().getRepository(WorldRegionStoreRepository.class);
	}

	@Load(group = PreLoadGroup.class)
	private void load() {
		for (short x = 0; x <= REGIONS_X; x++) {
			for (short y = 0; y <= REGIONS_Y; y++) {
				for (short z = 0; z <= REGIONS_Z; z++) {
					regions[x][y][z] = new WorldRegion(x, y, z);
				}
			}
		}
	}

	public void switchRegion(WorldObject object, WorldRegion newRegion) {
		final WorldRegion oldRegion = object.getCurrentRegion();
		if (oldRegion == null || oldRegion == newRegion) {
			return;
		}

		forEachSurroundingRegion(oldRegion, w -> {
			if (!isSurroundingRegion(newRegion, w)) {
				final StatsSet regionOptions = getRegionOptions(w);
				for (WorldObject wo : getObjectsOption(w).values()) {
					if (wo == object) {
						continue;
					}
				}
			}
			return true;
		});


		forEachSurroundingRegion(newRegion, w -> {
			if (!isSurroundingRegion(oldRegion, w)) {
				for (WorldObject wo : getObjectsOption(w).values()) {
					if (object.isPlayer() && wo.isVisibleFor((Player) object)) {
						final Player player = object.asPlayer();
						PlayerBroadcastController.getInstance().sendInfo(wo, player);
					}

					if (wo.isPlayer() && object.isVisibleFor((Player) wo)) {
						final Player player = object.asPlayer();
						PlayerBroadcastController.getInstance().sendInfo(object, player);
					}
				}
			}
			return true;
		});
	}

	public Map<Integer, WorldObject> getObjectsOption(WorldRegion region) {
		final StatsSet regionOptions = getRegionOptions(region);
		return regionOptions.getMap("objects", Integer.class, WorldObject.class);
	}

	public boolean getActiveOption(WorldRegion region) {
		final StatsSet regionOptions = getRegionOptions(region);
		return regionOptions.getBoolean("active");
	}

	public StatsSet getRegionOptions(WorldRegion region) {
		StatsSet value = worldRegionStoreRepository.getRegionOptions(region);
		if (value == null) {
			value = new StatsSet();
			value.set("objects", new HashMap<Integer, WorldObject>());
			value.set("active", false);

			worldRegionStoreRepository.updateOptions(region, value);
		}

		return value;
	}

	public void updateOptions(WorldRegion region, OperationHandler<StatsSet> operationHandler) {
		worldRegionStoreRepository.updateOptions(region, operationHandler);
	}

	public WorldRegion getRegion(ILocational object) {
		return getRegion(object.getRealX(), object.getRealY(), object.getRealZ());
	}

	public WorldRegion getRegion(double x, double y, double z) {
		try {
			return regions[((int) x >> SHIFT_BY) + OFFSET_X][((int) y >> SHIFT_BY) + OFFSET_Y][((int) z >> SHIFT_BY_Z) + OFFSET_Z];
		} catch (ArrayIndexOutOfBoundsException e) {
			log.warn("Incorrect world region X: {} Y: {} Z: {} for coordinates x: {} y: {} z: {}", (((int) x >> SHIFT_BY) + OFFSET_X), (((int) y >> SHIFT_BY) + OFFSET_Y), (((int) z >> SHIFT_BY_Z) + OFFSET_Z), x, y, z);
			return null;
		}
	}

	public WorldRegion getRegions(int x, int y, int z) {
		try {
			return regions[x][y][z];
		} catch (ArrayIndexOutOfBoundsException e) {
			log.warn("Incorrect world region X: {} Y: {} Z: {} for coordinates x: {} y: {} z: {}", (((int) x >> SHIFT_BY) + OFFSET_X), (((int) y >> SHIFT_BY) + OFFSET_Y), (((int) z >> SHIFT_BY_Z) + OFFSET_Z), x, y, z);
			return null;
		}
	}

	public void addVisibleObject(WorldRegion owner, WorldObject object) {
		updateOptions(owner, value -> {
			final StatsSet options = getRegionOptions(owner);
			try {
				options.getMap("objects", Integer.class, WorldObject.class).put(object.getObjectId(), object);

				if (object.isPlayable()) {
					if (!options.getBoolean("active")) {
						options.set("active", true);
						setRegionState(owner, true);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}


			return options;
		});
	}

	public void removeVisibleObject(WorldRegion owner, WorldObject object) {
		updateOptions(owner, value -> {
			final StatsSet options = getRegionOptions(owner);
			options.getMap("objects", Integer.class, WorldObject.class).remove(object.getObjectId());

			if (object.isPlayable()) {
				if (areNeighborsEmpty(owner)) {
					setRegionState(owner, false);
				}
			}

			return options;
		});
	}

	public boolean isSurroundingRegion(WorldRegion owner, WorldRegion region) {
		return region != null
				&& owner.getRegionX() >= region.getRegionX() - 1
				&& owner.getRegionX() <= region.getRegionX() + 1
				&& owner.getRegionY() >= region.getRegionY() - 1
				&& owner.getRegionY() <= region.getRegionY() + 1
				&& owner.getRegionZ() >= region.getRegionZ() - 1
				&& owner.getRegionZ() <= region.getRegionZ() + 1;
	}

	private void setRegionState(WorldRegion owner, boolean activating) {
		forEachSurroundingRegion(owner, w -> {
			updateOptions(w, value -> {
				StatsSet options = getRegionOptions(w);
				if (options == null) {
					options = new StatsSet();
					options.set("objects", new HashMap<Integer, WorldObject>());
					options.set("active", false);
				}

				if (activating || areNeighborsEmpty(w)) {
					options.set("active", activating);
				}

				return options;
			});

			return true;
		});
	}

	public boolean forEachSurroundingRegion(WorldRegion owner, Predicate<WorldRegion> p) {
		for (int x = owner.getRegionX() - 1; x <= owner.getRegionX() + 1; x++) {
			for (int y = owner.getRegionY() - 1; y <= owner.getRegionY() + 1; y++) {
				for (int z = owner.getRegionZ() - 1; z <= (owner.getRegionZ() + 1); z++) {
					if (validRegion(x, y, z)) {
						final WorldRegion worldRegion = getRegions(x, y, z);
						if (!p.test(worldRegion)) {
							return false;
						}
					}
				}
			}
		}
		return true;
	}

	public boolean areNeighborsEmpty(WorldRegion owner) {
		return forEachSurroundingRegion(owner, w -> !getActiveOption(w) && getObjectsOption(w).values().stream().anyMatch(WorldObject::isPlayable));
	}

	public boolean validRegion(int x, int y, int z) {
		return x >= 0 && x <= REGIONS_X && y >= 0 && y <= REGIONS_Y && z >= 0 && z <= REGIONS_Z;
	}
}
