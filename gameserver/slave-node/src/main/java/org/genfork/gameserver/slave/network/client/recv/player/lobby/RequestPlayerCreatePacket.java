package org.genfork.gameserver.slave.network.client.recv.player.lobby;

import org.apache.commons.lang3.StringUtils;
import org.genfork.gameserver.slave.config.PlayerConfig;
import org.genfork.gameserver.slave.data.xml.InitialEquipmentDataParser;
import org.genfork.gameserver.slave.data.xml.PlayerDataParser;
import org.genfork.gameserver.slave.managers.SessionManager;
import org.genfork.gameserver.slave.network.client.send.player.lobby.PlayerCreateFailPacket;
import org.genfork.gameserver.slave.network.client.send.store.PlayerCreationPacket;
import org.genfork.gameserver.slave.network.discovery.DiscoveryServerHandler;
import org.genfork.server.enums.ClassId;
import org.genfork.server.location.Location;
import org.genfork.server.model.lobby.InventoryItemInfo;
import org.genfork.server.objects.component.data.PlayerData;
import org.genfork.server.objects.component.inventory.InventoryConstants;
import org.genfork.tools.network.DiscoveryClients;
import org.genfork.tools.network.IncomingPacket;

import java.util.List;

import static org.genfork.gameserver.slave.network.client.send.player.lobby.PlayerCreateFailPacket.*;
import static org.genfork.server.enums.LocationData.PAPER_DOLL;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class RequestPlayerCreatePacket extends IncomingPacket<DiscoveryServerHandler> {
	private String playerName;
	private int race;
	private byte sex;
	private int classId;
	private int intelligent;
	private int straight;
	private int concentration;
	private int mentality;
	private int dexterity;
	private int withes;
	private int luc;
	private int cha;
	private byte hairStyle;
	private byte hairColor;
	private byte face;

	@Override
	public boolean read(DiscoveryServerHandler client) {
		playerName = readS();
		race = readD();
		sex = (byte) readD();
		classId = readD();
		intelligent = readD();
		straight = readD();
		concentration = readD();
		mentality = readD();
		dexterity = readD();
		withes = readD();
		hairStyle = (byte) readD();
		hairColor = (byte) readD();
		face = (byte) readD();
		return true;
	}

	@Override
	public void run(DiscoveryServerHandler client) throws Exception {
		final int length = playerName.length();
		if (length < 1 || length > 16) {
			client.sendPacket(this, new PlayerCreateFailPacket(REASON_16_ENG_CHARS));
			return;
		}

		if (!StringUtils.isAlphanumeric(playerName) || !PlayerConfig.PLAYER_NAME_TEMPLATE_PATTERN.matcher(playerName).matches()) {
			client.sendPacket(this, new PlayerCreateFailPacket(REASON_INCORRECT_NAME));
			return;
		}

		if (face > 2 || face < 0) {
			client.sendPacket(this, new PlayerCreateFailPacket(REASON_CREATION_FAILED));
			return;
		}

		if (hairStyle < 0 || (sex == 0 && hairStyle > 4) || (sex != 0 && hairStyle > 6)) {
			client.sendPacket(this, new PlayerCreateFailPacket(REASON_CREATION_FAILED));
			return;
		}

		if (hairColor > 3 || hairColor < 0) {
			client.sendPacket(this, new PlayerCreateFailPacket(REASON_CREATION_FAILED));
			return;
		}

		final PlayerData template = PlayerDataParser.getInstance().getData(classId);
		if (template == null) {
			client.sendPacket(this, new PlayerCreateFailPacket(REASON_CREATION_FAILED));
			return;
		}

		SessionManager.getInstance().getManagedSessionInfo(getSessionInfo(), value -> {
			if (value == null) {
				client.sendPacket(this, new PlayerCreateFailPacket(REASON_CREATION_FAILED));
			} else {
				final Location creationPoint = template.getCreationPoint();
				final float baseCpMax = template.getBaseCpMax(1);
				final float baseHpMax = template.getBaseHpMax(1);
				final float baseMpMax = template.getBaseMpMax(1);

				final List<InventoryItemInfo> initialItems = InitialEquipmentDataParser.getInstance().getEquipmentList(ClassId.getClassId(classId));
				if (initialItems != null) {
					initialItems.forEach(i -> {
						if (i.getLocationData() == PAPER_DOLL) {
							final int locationMask = InventoryConstants.getPaperdollIdFromItem(i.getBodyPart());
							i.setLocationMask(locationMask);
						}
					});
				}

				final PlayerCreationPacket packet = new PlayerCreationPacket(
						value.getAccountName(),
						playerName,
						race,
						classId,
						hairStyle,
						hairColor,
						face,
						sex,
						100,
						creationPoint.getRealX(),
						creationPoint.getRealY(),
						creationPoint.getRealZ(),
						creationPoint.getRealHeading(),
						(int) baseCpMax,
						(int) (baseCpMax * 0.7),
						(int) baseHpMax,
						(int) (baseHpMax * 0.7),
						(int) baseMpMax,
						(int) (baseMpMax * 0.7),
						20,
						initialItems
				);

				client.sendPacket(DiscoveryClients.DATA_STORE_NODE, DiscoveryClients.GAME_SLAVE_NODE, value, packet);
			}

			return value;
		});

	}
}
