package org.genfork.gameserver.slave.network.client.recv.auth;

import org.genfork.gameserver.slave.data.controller.world.WorldObjectsStorageController;
import org.genfork.gameserver.slave.managers.SessionManager;
import org.genfork.gameserver.slave.network.client.send.auth.ReplyPlaySessionPacket;
import org.genfork.gameserver.slave.network.client.send.player.ServerClose;
import org.genfork.gameserver.slave.network.discovery.DiscoveryServerHandler;
import org.genfork.server.objects.WorldObject;
import org.genfork.tools.network.DiscoveryClients;
import org.genfork.tools.network.IncomingPacket;
import org.genfork.tools.network.SessionInfo;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class RequestTryPlayGamePacket extends IncomingPacket<DiscoveryServerHandler> {
	private String playerClientId;
	private String accountName;

	@Override
	public boolean read(DiscoveryServerHandler client) {
		playerClientId = readS();
		accountName = readS();
		return true;
	}

	@Override
	public void run(DiscoveryServerHandler client) throws Exception {
		final WorldObject object = WorldObjectsStorageController.getInstance()
				.findObject(w ->
						w.isPlayer() && w.asPlayer().getAccountName().equals(accountName));

		final SessionInfo originSession = getSessionInfo();
		final SessionInfo managedSessionInfo = SessionManager.getInstance().getManagedSessionInfo(s ->
				s.getAccountName().equals(accountName) && !s.equals(originSession));

		ReplyPlaySessionPacket packet;
		if (object != null) {
			packet = new ReplyPlaySessionPacket(playerClientId, originSession.getAuthKeyFirst(), (short) 0);

			final SessionInfo sessionInfo = object.asPlayer().getSessionInfo();
			WorldObjectsStorageController.getInstance().removeObject(object);
			SessionManager.getInstance().removeSession(sessionInfo);

			client.sendPacket(DiscoveryClients.GAME_MASTER_NODE,
					DiscoveryClients.GAME_SLAVE_NODE,
					sessionInfo,
					ServerClose.STATIC);
		} else if (managedSessionInfo != null) {
			packet = new ReplyPlaySessionPacket(playerClientId, originSession.getAuthKeyFirst(), (short) 0);
			SessionManager.getInstance().removeSession(managedSessionInfo);
			client.sendPacket(DiscoveryClients.GAME_MASTER_NODE,
					DiscoveryClients.GAME_SLAVE_NODE,
					managedSessionInfo,
					ServerClose.STATIC);
		} else {
			packet = new ReplyPlaySessionPacket(playerClientId, originSession.getAuthKeyFirst(), (short) 1);
		}

		client.sendPacket(getToClientName(),
				getFromClientName(),
				originSession,
				packet);
	}
}
