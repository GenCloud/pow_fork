package org.genfork.gameserver.slave.network.client.recv.player.inventory;

import org.genfork.gameserver.slave.data.controller.actor.ActorItemContainerDataController;
import org.genfork.gameserver.slave.managers.SessionManager;
import org.genfork.gameserver.slave.network.discovery.DiscoveryServerHandler;
import org.genfork.tools.network.IncomingPacket;

/**
 * @author: ngolubenko@context-it.ru
 * @created: 2020/04
 */
public class RequestInventoryDataPacket extends IncomingPacket<DiscoveryServerHandler> {
	@Override
	public boolean read(DiscoveryServerHandler client) {
		return true;
	}

	@Override
	public void run(DiscoveryServerHandler client) throws Exception {
		SessionManager.getInstance().getManagedSessionInfo(getSessionInfo(), value -> {
			final String enteredPlayerName = value.getEnteredPlayerName();
			ActorItemContainerDataController.getInstance().sendItemList(enteredPlayerName, client);
			return value;
		});
	}
}
