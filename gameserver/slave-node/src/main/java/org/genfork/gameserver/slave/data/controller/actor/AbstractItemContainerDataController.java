package org.genfork.gameserver.slave.data.controller.actor;

import org.genfork.gameserver.slave.data.repositories.ActorItemContainerRepository;
import org.genfork.gameserver.slave.managers.DataStoreManager;
import org.genfork.gameserver.slave.network.discovery.DiscoveryServerHandler;
import org.genfork.server.objects.Actor;
import org.genfork.server.objects.Item;
import org.genfork.server.objects.component.inventory.ItemContainerData;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * @author: ngolubenko@context-it.ru
 * @created: 2020/04
 */
public abstract class AbstractItemContainerDataController {
	protected final ActorItemContainerRepository actorItemContainerRepository;

	public AbstractItemContainerDataController() {
		actorItemContainerRepository = DataStoreManager.getInstance().getRepository(ActorItemContainerRepository.class);
	}

	protected Map<Integer, Item> getItemsLock(Actor actor) {
		return actorItemContainerRepository.lockAndGetContainerData(actor).getItems();
	}

	protected Map<Integer, Item> getItemsSimple(Actor actor) {
		return actorItemContainerRepository.getContainerData(actor).getItems();
	}

	protected void refreshWeight(Actor actor) {
	}

	protected void refreshWeightWithoutLock(ItemContainerData containerData) {

	}

	public boolean validateCapacity(Actor actor, long slots) {
		return true;
	}

	public boolean validateWeight(Actor actor, long weight) {
		return true;
	}

	public int getSize(Actor actor) {
		return getItemsLock(actor).size();
	}

	@SafeVarargs
	public final int getSize(Actor actor, Predicate<Item> filter, Predicate<Item>... filters) {
		for (Predicate<Item> additionalFilter : filters) {
			filter = filter.and(additionalFilter);
		}
		return (int) getItemsLock(actor).values().stream().filter(filter).count();
	}

	/**
	 * Gets the items in inventory.
	 *
	 * @return the items in inventory.
	 */
	public Collection<Item> getItems(Actor actor) {
		return getItems(actor, i -> true);
	}

	/**
	 * Gets the items in inventory filtered by filter.
	 *
	 * @param filter  the filter
	 * @param filters multiple filters
	 * @return the filtered items in inventory
	 */
	@SafeVarargs
	public final Collection<Item> getItems(Actor actor, Predicate<Item> filter, Predicate<Item>... filters) {
		for (Predicate<Item> additionalFilter : filters) {
			filter = filter.and(additionalFilter);
		}

		return getItemsLock(actor).values().stream().filter(filter).collect(Collectors.toCollection(LinkedList::new));
	}

	/**
	 * Gets the items in inventory filtered by filter.
	 *
	 * @param filter  the filter
	 * @param filters multiple filters
	 * @return the filtered items in inventory
	 */
	@SafeVarargs
	public final Collection<Item> getItemsWithoutLock(ItemContainerData itemContainerData, Predicate<Item> filter, Predicate<Item>... filters) {
		for (Predicate<Item> additionalFilter : filters) {
			filter = filter.and(additionalFilter);
		}

		return itemContainerData.getItems().values().stream().filter(filter).collect(Collectors.toCollection(LinkedList::new));
	}

	public final Item getItem(Actor actor, Predicate<Item> filter) {
		return getItemsLock(actor).values().stream().filter(filter).findFirst().orElse(null);
	}

	public final Item getItem(ItemContainerData containerData, Predicate<Item> filter) {
		return containerData.getItems().values().stream().filter(filter).findFirst().orElse(null);
	}

	public final boolean hasItem(Actor actor, Predicate<Item> filter) {
		return getItemsLock(actor).values().stream().anyMatch(filter);
	}

	public final boolean notExistsItem(ItemContainerData containerData, Predicate<Item> filter) {
		return containerData.getItems().values().stream().noneMatch(filter);
	}

	/**
	 * @param itemId the item Id
	 * @return the item from inventory by itemId
	 */
	public Item getItemByItemId(Actor actor, int itemId) {
		return getItemsLock(actor).values().stream().filter(item -> item.getItemId() == itemId).findFirst().orElse(null);
	}

	/**
	 * @param itemId the item Id
	 * @return the item from inventory by itemId
	 */
	public Item getItemByItemIdWithoutLock(ItemContainerData containerData, int itemId) {
		return containerData.getItems().values().stream().filter(item -> item.getItemId() == itemId).findFirst().orElse(null);
	}

	/**
	 * @param itemId the item Id
	 * @return the items list from inventory by using its itemId
	 */
	public Collection<Item> getItemsByItemId(Actor actor, int itemId) {
		return getItems(actor, i -> i.getItemId() == itemId);
	}

	/**
	 * @param objectId the item object Id
	 * @return item from inventory by objectId
	 */
	public Item getItemByObjectId(Actor actor, int objectId) {
		return getItemsLock(actor).get(objectId);
	}

	/**
	 * Gets the inventory item count by item Id and enchant level including equipped items.
	 *
	 * @param itemId       the item Id
	 * @param enchantLevel the item enchant level, use -1 to match any enchant level
	 * @return the inventory item count
	 */
	public long getInventoryItemCount(Actor actor, int itemId, int enchantLevel) {
		return getInventoryItemCount(actor, itemId, enchantLevel, true);
	}

	/**
	 * Gets the inventory item count by item Id and enchant level, may include equipped items.
	 *
	 * @param itemId          the item Id
	 * @param enchantLevel    the item enchant level, use -1 to match any enchant level
	 * @param includeEquipped if {@code true} includes equipped items in the result
	 * @return the inventory item count
	 */
	public long getInventoryItemCount(Actor actor, int itemId, int enchantLevel, boolean includeEquipped) {
		long count = 0;

		for (Item item : getItemsLock(actor).values()) {
			if (item.getItemId() == itemId
					&& (item.getEnchant() == enchantLevel || enchantLevel < 0)
					&& (includeEquipped || !item.isEquipped())) {
				if (item.getItemData().isStackable()) {
					return item.getAmount();
				}

				count++;
			}
		}
		return count;
	}

	/**
	 * @return true if player got item for self resurrection
	 */
	public boolean haveItemForSelfResurrection(Actor actor) {
		return getItemsLock(actor).values().stream().anyMatch(item -> item.getItemData().isAllowSelfResurrection());
	}

	/**
	 * Adds item to inventory for further adjustments.
	 *
	 * @param item : ItemInstance to be added from inventory
	 */
	protected void addItem(Actor actor, Item item) {
		actorItemContainerRepository.lockAndModifyData(actor, null, op -> {
			final ItemContainerData container = actorItemContainerRepository.getContainerData(actor);
			container.getItems().put(item.getObjectId(), item);
			return container;
		});
	}

	/**
	 * Adds item to inventory for further adjustments.
	 *
	 * @param item : ItemInstance to be added from inventory
	 */
	protected void addItemWithoutLock(ItemContainerData container, Item item) {
		container.getItems().put(item.getObjectId(), item);
	}

	/**
	 * Removes item from inventory for further adjustments.
	 *
	 * @param item : ItemInstance to be removed from inventory
	 */
	protected boolean removeItem(Actor actor, Item item) {
		final ItemContainerData itemContainerData = actorItemContainerRepository.lockAndModifyData(actor, null, op -> {
			final ItemContainerData container = actorItemContainerRepository.getContainerData(actor);
			container.getItems().remove(item.getObjectId(), item);
			return container;
		});

		return !itemContainerData.getItems().containsKey(item.getObjectId());
	}

	public abstract void sendItemList(String enteredPlayerName, DiscoveryServerHandler client);
}
