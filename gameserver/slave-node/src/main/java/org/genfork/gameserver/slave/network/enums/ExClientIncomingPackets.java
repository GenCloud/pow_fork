package org.genfork.gameserver.slave.network.enums;

import org.genfork.gameserver.slave.network.client.recv.player.lobby.RequestGoToLobby;
import org.genfork.gameserver.slave.network.client.recv.player.lobby.RequestPlayerNameCreatablePacket;
import org.genfork.gameserver.slave.network.discovery.DiscoveryServerHandler;
import org.genfork.tools.network.IConnectionState;
import org.genfork.tools.network.IIncomingPackets;
import org.genfork.tools.network.IncomingPacket;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Supplier;

import static org.genfork.gameserver.slave.network.enums.ConnectionState.CONNECTED;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public enum ExClientIncomingPackets implements IIncomingPackets<DiscoveryServerHandler> {
	REQUEST_GO_TO_LOBBY_PACKET(0x33, RequestGoToLobby::new, CONNECTED),

	REQUEST_PLAYER_NAME_CREATABLE_PACKET(0xA9, RequestPlayerNameCreatablePacket::new, CONNECTED);

	public static final ExClientIncomingPackets[] PACKET_ARRAY;

	static {
		final short maxPacketId = (short) Arrays.stream(values()).mapToInt(IIncomingPackets::getPacketId).max().orElse(0);
		PACKET_ARRAY = new ExClientIncomingPackets[maxPacketId + 1];
		for (ExClientIncomingPackets incomingPacket : values()) {
			PACKET_ARRAY[incomingPacket.getPacketId()] = incomingPacket;
		}
	}

	private short packetId;
	private Supplier<IncomingPacket<DiscoveryServerHandler>> incomingPacketFactory;
	private Set<IConnectionState> connectionStates;

	ExClientIncomingPackets(int packetId, Supplier<IncomingPacket<DiscoveryServerHandler>> incomingPacketFactory, IConnectionState... connectionStates) {
		// packetId is an unsigned byte
		if (packetId > 0xFF) {
			throw new IllegalArgumentException("packetId must not be bigger than 0xFF");
		}

		this.packetId = (short) packetId;
		this.incomingPacketFactory = incomingPacketFactory != null ? incomingPacketFactory : () -> null;
		this.connectionStates = new HashSet<>(Arrays.asList(connectionStates));
	}

	@Override
	public int getPacketId() {
		return packetId;
	}

	@Override
	public IncomingPacket<DiscoveryServerHandler> newIncomingPacket() {
		return incomingPacketFactory.get();
	}

	@Override
	public Set<IConnectionState> getConnectionStates() {
		return connectionStates;
	}

	@Override
	public IIncomingPackets<DiscoveryServerHandler>[] getArray() {
		return PACKET_ARRAY;
	}
}
