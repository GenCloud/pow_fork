package org.genfork.gameserver.slave.network.client.recv.player.lobby;

import org.genfork.gameserver.slave.data.controller.store.StorePlayersController;
import org.genfork.gameserver.slave.managers.SessionManager;
import org.genfork.gameserver.slave.network.discovery.DiscoveryServerHandler;
import org.genfork.tools.network.IncomingPacket;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class RequestGoToLobby extends IncomingPacket<DiscoveryServerHandler> {
	@Override
	public boolean read(DiscoveryServerHandler client) {
		return true;
	}

	@Override
	public void run(DiscoveryServerHandler client) throws Exception {
		SessionManager.getInstance().getManagedSessionInfo(getSessionInfo(), value -> {
			if (value == null) {
				client.closeNow();
			} else {
				StorePlayersController.getInstance().tryReceiveAccountPlayerData(false, value, value.getAccountName());
			}

			return value;
		});
	}
}
