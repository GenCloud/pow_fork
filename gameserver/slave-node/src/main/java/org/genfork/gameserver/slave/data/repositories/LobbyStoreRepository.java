package org.genfork.gameserver.slave.data.repositories;

import org.genfork.tools.data.StatsSet;
import org.genfork.tools.persistence.annotations.DataMethod;
import org.genfork.tools.persistence.annotations.DataRepository;
import org.genfork.tools.persistence.annotations.ExpParam;
import org.genfork.tools.persistence.repository.CrudDataRepository;
import org.genfork.tools.persistence.repository.handler.OperationHandler;

import static org.genfork.tools.persistence.annotations.DataMethod.Type.*;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@DataRepository(bucketName = "prepared_objects_store")
public interface LobbyStoreRepository extends CrudDataRepository {
	@DataMethod(type = WRITE, keyExpression = "#accountName + '_' + T(org.genfork.gameserver.slave.config.ServerConfig).PLAYER_SERVER_ID")
	void lockAndAddData(@ExpParam("accountName") String accountName, OperationHandler<StatsSet> operationHandler);

	@DataMethod(type = READ, keyExpression = "#accountName + '_' + T(org.genfork.gameserver.slave.config.ServerConfig).PLAYER_SERVER_ID")
	StatsSet getData(@ExpParam("accountName") String accountName);

	@DataMethod(type = REMOVE, keyExpression = "#accountName + '_' + T(org.genfork.gameserver.slave.config.ServerConfig).PLAYER_SERVER_ID")
	void removeData(@ExpParam("accountName") String accountName);
}
