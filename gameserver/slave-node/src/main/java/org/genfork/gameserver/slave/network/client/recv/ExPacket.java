package org.genfork.gameserver.slave.network.client.recv;

import lombok.extern.slf4j.Slf4j;
import org.genfork.gameserver.slave.network.discovery.DiscoveryServerHandler;
import org.genfork.gameserver.slave.network.enums.ExClientIncomingPackets;
import org.genfork.tools.network.IncomingPacket;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Slf4j
public class ExPacket extends IncomingPacket<DiscoveryServerHandler> {
	@Override
	public boolean read(DiscoveryServerHandler client) {
		return true;
	}

	@Override
	public void run(DiscoveryServerHandler client) throws Exception {
		final int packetId = readH() & 0xFFFF;
		final ExClientIncomingPackets[] packets = ExClientIncomingPackets.PACKET_ARRAY;

		if (packetId >= packets.length) {
			log.warn("Unknown packet: {}", Integer.toHexString(packetId));
			return;
		}

		final ExClientIncomingPackets incomingPacket = packets[packetId];
		if (incomingPacket == null) {
			log.warn("Unknown packet: {}", Integer.toHexString(packetId));
			return;
		}

		final IncomingPacket<DiscoveryServerHandler> packet = incomingPacket.newIncomingPacket();

		if (packet != null) {
			if (getSessionInfo() != null) {
				packet.setFromClientName(getFromClientName());
				packet.setToClientName(getToClientName());
				packet.setSessionInfo(getSessionInfo());
			}

			packet.setByteBuf(getByteBuf());

			if (!packet.read(client)) {
				log.error("Error read data packet [{}]", packet);
			} else {
				try {
					packet.run(client);
				} catch (Exception e) {
					log.error("Error run data packet [{}]: {}", packet, e.getLocalizedMessage());
				}
			}
		}
	}
}
