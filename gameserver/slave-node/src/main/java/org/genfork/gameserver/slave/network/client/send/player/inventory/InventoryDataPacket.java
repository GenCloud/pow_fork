package org.genfork.gameserver.slave.network.client.send.player.inventory;

import lombok.RequiredArgsConstructor;
import org.genfork.server.objects.Item;
import org.genfork.server.objects.component.inventory.ItemContainerData;

import java.util.Collection;

import static org.genfork.tools.network.opcodes.GameBranchOpcodes.ITEMLIST;

/**
 * @author: ngolubenko@context-it.ru
 * @created: 2020/04
 */
@RequiredArgsConstructor
public class InventoryDataPacket extends AbstractInventoryPacket {
	private final int sendType;
	private final ItemContainerData itemContainerData;
	private final Collection<Item> items;

	@Override
	public boolean write() {
		ITEMLIST.writeId(this);

		if (sendType == 2) {
			writeC(sendType);
			writeD(items.size());
			writeD(items.size());

			items.forEach(this::writeItem);
		} else {
			writeC(0x01);
			writeD(0x00);
			writeD(items.size());
		}

		writeInventoryBlock(itemContainerData);
		return true;
	}
}
