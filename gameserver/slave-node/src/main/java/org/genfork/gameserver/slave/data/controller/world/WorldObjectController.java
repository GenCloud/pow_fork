package org.genfork.gameserver.slave.data.controller.world;

import lombok.Getter;
import org.genfork.gameserver.slave.data.repositories.ObjectStoreRepository;
import org.genfork.gameserver.slave.managers.DataStoreManager;
import org.genfork.server.location.Location;
import org.genfork.server.model.world.WorldRegion;
import org.genfork.server.objects.WorldObject;

import java.util.concurrent.locks.ReentrantLock;

import static org.genfork.gameserver.slave.data.controller.world.WorldObjectsStorageController.*;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class WorldObjectController {
	@Getter(lazy = true)
	private static final WorldObjectController instance = new WorldObjectController();

	private final ReentrantLock lock = new ReentrantLock();

	private final ObjectStoreRepository objectStoreRepository;

	private WorldObjectController() {
		objectStoreRepository = DataStoreManager.getInstance().getRepository(ObjectStoreRepository.class);
	}

	public void setXYZ(WorldObject object, int x, int y, int z) {
		object.setRealX(x);
		object.setRealY(y);
		object.setRealZ(z);

		if (object.getSpawned()) {
			final WorldRegion oldRegion = object.getCurrentRegion();
			final WorldRegion newRegion = WorldRegionController.getInstance().getRegion(object);
			if (newRegion != oldRegion) {
				if (oldRegion != null) {
					WorldRegionController.getInstance().removeVisibleObject(oldRegion, object);
				}

				WorldRegionController.getInstance().addVisibleObject(newRegion, object);
				WorldRegionController.getInstance().switchRegion(object, newRegion);
				object.setCurrentRegion(newRegion);
			}
		}
	}

	public void spawnObject(WorldObject object, int x, int y, int z) {
		lock.lock();
		try {
			if (x > MAP_MAX_X) {
				x = MAP_MAX_X - 5000;
			}
			if (x < MAP_MIN_X) {
				x = MAP_MIN_X + 5000;
			}
			if (y > MAP_MAX_Y) {
				y = MAP_MAX_Y - 5000;
			}
			if (y < MAP_MIN_Y) {
				y = MAP_MIN_Y + 5000;
			}
			if (z > MAP_MAX_Z) {
				z = MAP_MAX_Z - 1000;
			}
			if (z < MAP_MIN_Z) {
				z = MAP_MIN_Z + 1000;
			}

			setXYZ(object, x, y, z);
		} finally {
			lock.unlock();
		}

		spawnObject(object);
	}

	public void spawnObject(WorldObject object) {
		lock.lock();
		try {
			final Location location = new Location(object.getRealX(), object.getRealY(), object.getRealZ());
			final WorldRegion region = WorldRegionController.getInstance().getRegion(location);

			object.setCurrentRegion(region);

			objectStoreRepository.addObject(object);

			WorldRegionController.getInstance().addVisibleObject(region, object);

			WorldObjectsStorageController.getInstance().publishVisibleObjectToOther(object, region);

			object.setSpawned(true);
		} finally {
			lock.unlock();
		}
	}
}
