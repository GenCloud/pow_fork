package org.genfork.gameserver.slave.network.client.send.player.lobby;

import lombok.RequiredArgsConstructor;
import org.genfork.gameserver.slave.data.xml.ExperienceDataParser;
import org.genfork.server.model.lobby.LobbyInfo;
import org.genfork.tools.network.OutgoingPacket;
import org.genfork.tools.network.SessionInfo;

import java.time.Duration;
import java.util.Arrays;
import java.util.List;

import static org.genfork.server.objects.component.inventory.InventoryConstants.*;
import static org.genfork.tools.network.opcodes.GameBranchOpcodes.CHARACTER_SELECTION_INFO;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@RequiredArgsConstructor
public class PlayerLobbyInfoPacket extends OutgoingPacket {
	private final List<LobbyInfo> infos;
	private final SessionInfo sessionInfo;

	@Override
	public boolean write() {
		CHARACTER_SELECTION_INFO.writeId(this);

		final int size = infos.size();
		writeD(size);

		writeD(7); //max players on account
		writeC(0x00); // if 1 can't create new char
		writeC(0x01); // play mode, if 1 can create only 2 char in regular lobby
		writeD(0x02); // if 1, korean client
		writeC(0x00); // Gift message for inactive accounts // 152
		writeC(0x00); // if 1 suggest premium account Balthus creation

		int activeId = -1;
		long lastAccess = 0L;
		for (int i = 0; i < size; i++) {
			if (lastAccess < infos.get(i).getLastAccess()) {
				lastAccess = infos.get(i).getLastAccess();
				activeId = i;
			}
		}

		for (int i = 0; i < infos.size(); i++) {
			final LobbyInfo info = infos.get(i);
			writeS(info.getPlayerName()); // char name
			writeD(info.getPlayerId()); // char id
			writeS(info.getAccountName()); // login
			writeD(sessionInfo.getPlayKeyFirst()); // session id
			writeD(0x00); // ??
			writeD(0x00); // ??

			writeD(info.getSex()); // sex
			writeD(info.getRace()); // race

			if (info.getClassId() == info.getBaseClassId()) {
				writeD(info.getClassId());
			} else {
				writeD(info.getBaseClassId());
			}

			writeD(0x01); // server id ??

			writeD(info.getX());
			writeD(info.getY());
			writeD(info.getZ());
			writeF(info.getCurrentHp());
			writeF(info.getCurrentMp());

			final long currentExp = info.getCurrentExp();
			writeQ(info.getCurrentSp());
			writeQ(currentExp);

			final int playerLevel = info.getPlayerLevel();
			final long expForLevel = ExperienceDataParser.getInstance().getExpForLevel(playerLevel);
			final long expForLevelMod = ExperienceDataParser.getInstance().getExpForLevel(playerLevel + 1);
			writeF((float) (currentExp - expForLevel) / (expForLevelMod - expForLevel));
			writeD(playerLevel);

			writeD(info.getReputation()); // reputation
			writeD(info.getPk());
			writeD(info.getPvp());

			writeD(0x00);
			writeD(0x00);
			writeD(0x00);
			writeD(0x00);
			writeD(0x00);
			writeD(0x00);
			writeD(0x00);

			writeD(0x00); // Ertheia
			writeD(0x00); // Ertheia

			Arrays.stream(PAPERDOLL_ORDER)
					.map(info::getPaperdollItemId)
					.forEach(this::writeD);

			Arrays.stream(PAPERDOLL_ORDER_VISUAL_ID)
					.map(info::getPaperdollItemVisualId)
					.forEach(this::writeD);

			Arrays.stream(PAPERDOLL_ORDER_ENCHANT_LEVEL)
					.map(info::getPaperdollEnchantLevel)
					.forEach(this::writeH);

			writeD(info.getHairStyle());
			writeD(info.getHairColor());
			writeD(info.getFace());

			writeF(info.getMaxHp());
			writeF(info.getMaxMp());

			writeD(info.getDeleteTime() > 0 ? (int) ((info.getDeleteTime() - System.currentTimeMillis()) / 1000) : 0);
			writeD(info.getClassId());
			writeD(i == activeId ? 1 : 0); // active char id 1 - yes, 0 - no

			writeC(Math.min(info.getPaperdollEnchantLevel(PAPERDOLL_RHAND), 127)); // writeC(info.getEnchantEffect() > 127 ? 127 : info.getEnchantEffect());
			writeD(info.getPaperdollVariationOptionFirst(PAPERDOLL_RHAND)); // writeD(info.getAugmentation() != null ? info.getAugmentation().getOption1Id() : 0);
			writeD(info.getPaperdollVariationOptionSecond(PAPERDOLL_RHAND)); // writeD(info.getAugmentation() != null ? info.getAugmentation().getOption2Id() : 0);

			writeD(0x00); // Currently on retail when you are on character select you don't see your transformation.

			writeD(0x00); // npdid - 16024 Tame Tiny Baby Kookaburra A9E89C
			writeD(0x00); // level
			writeD(0x00); // ?
			writeD(0x00); // food? - 1200
			writeF(0x00); // max Hp
			writeF(0x00); // cur Hp

			writeD(info.getVitalityPoints());
			//			TODO
			writeD(350); // writeD((int) RatesConfig.RATE_VITALITY_EXP_MULTIPLIER * 100); // Vitality Exp Bonus
			writeD(0);// writeD(info.getVitalityItemsUsed()); // Vitality items used, such as potion
			writeD(info.getAccessLevel() == -100 ? 0x00 : 0x01); // Char is active or not
			writeC(info.getNobless());
			//			TODO
			writeC(0x01); // writeC(Hero.getInstance().isHero(info.getObjectId()) ? 0x01 : 0x00); // hero glow
			writeC(0x01); // writeC(info.isHairAccessoryEnabled() ? 0x01 : 0x00); // show hair accessory if enabled

			writeD(info.getAccessLevel() == -100 ? 0x01 : 0x00); //char is blocked or no
			writeD((int) Duration.ofMillis(info.getLastAccess()).getSeconds()); //last access
		}
		return true;
	}
}
