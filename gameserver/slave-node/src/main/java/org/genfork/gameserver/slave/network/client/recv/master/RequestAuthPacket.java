package org.genfork.gameserver.slave.network.client.recv.master;

import org.genfork.gameserver.slave.data.controller.store.StorePlayersController;
import org.genfork.gameserver.slave.network.discovery.DiscoveryServerHandler;
import org.genfork.tools.network.IncomingPacket;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class RequestAuthPacket extends IncomingPacket<DiscoveryServerHandler> {
	private String accountName;

	@Override
	public boolean read(DiscoveryServerHandler client) {
		accountName = readS();
		return true;
	}

	@Override
	public void run(DiscoveryServerHandler client) throws Exception {
		StorePlayersController.getInstance().tryReceiveAccountPlayerData(true, getSessionInfo(), accountName);
	}
}
