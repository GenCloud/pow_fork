package org.genfork.gameserver.slave.enums;

import lombok.Getter;
import org.genfork.gameserver.slave.network.client.components.IUpdateTypeComponent;

/**
 * @author: ngolubenko@context-it.ru
 * @created: 2020/04
 */
public enum ItemListType implements IUpdateTypeComponent {
	AUGMENT_BONUS(0x01),
	ELEMENTAL_ATTRIBUTE(0x02),
	ENCHANT_EFFECT(0x04),
	VISUAL_ID(0x08),
	SOUL_CRYSTAL(0x10),
	REUSE(0x14);

	@Getter
	private final int mask;

	ItemListType(int mask) {
		this.mask = mask;
	}
}
