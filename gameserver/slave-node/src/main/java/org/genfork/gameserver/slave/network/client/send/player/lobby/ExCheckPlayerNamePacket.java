package org.genfork.gameserver.slave.network.client.send.player.lobby;

import lombok.RequiredArgsConstructor;
import org.genfork.tools.network.OutgoingPacket;

import static org.genfork.tools.network.opcodes.GameBranchOpcodes.EX_CHECK_CHAR_NAME;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@RequiredArgsConstructor
public class ExCheckPlayerNamePacket extends OutgoingPacket {
	private final int code;

	@Override
	public boolean write() {
		EX_CHECK_CHAR_NAME.writeId(this);
		writeD(code);
		return true;
	}
}
