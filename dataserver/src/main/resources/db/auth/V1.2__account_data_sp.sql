SELECT pg_catalog.set_config('search_path', 'public', FALSE);

CREATE OR REPLACE VIEW account_data_view
AS
/***
    History:
        Author        Date        Description
        ngolubenko    10-02-2020
***/
SELECT accountname,
       password,
       email,
       created_time,
       last_ip,
       last_server_id,
       banned,
       ban_expire_time,
       premium_points,
       premium_type
FROM account_data acc
         INNER JOIN account_info inf
                    ON acc.id = inf.id;

CREATE OR REPLACE FUNCTION account_data_insert_update(i_accountname VARCHAR,
                                                      i_password VARCHAR,
                                                      i_email VARCHAR = 'mail@mail',
                                                      i_created_time TIMESTAMP = CURRENT_TIMESTAMP,
                                                      i_last_ip VARCHAR = '',
                                                      i_last_server_id SMALLINT = 1,
                                                      i_banned BOOLEAN = false,
                                                      i_ban_expire_time TIMESTAMP = NULL,
                                                      i_premium_points INT = -1,
                                                      i_premium_type SMALLINT = -1)
    RETURNS VOID
AS
/***
    History:
        Author        Date        Description
        ngolubenko    10-02-2020
***/
$function$
DECLARE
    o_id INT;
BEGIN
    IF i_created_time IS NULL THEN
        i_created_time := current_timestamp;
    END IF;

    IF NOT EXISTS(SELECT 1
                  FROM account_data AS d
                  WHERE d.accountname = i_accountname) THEN
        INSERT INTO account_data(accountname,
                                 password,
                                 email,
                                 created_time,
                                 last_ip,
                                 last_server_id)
        VALUES (i_accountname,
                i_password,
                i_email,
                i_created_time,
                i_last_ip,
                i_last_server_id)
        RETURNING id INTO o_id;

        INSERT INTO account_info(id,
                                 banned,
                                 ban_expire_time,
                                 premium_points,
                                 premium_type)
        VALUES (o_id,
                i_banned,
                i_ban_expire_time,
                i_premium_points,
                i_premium_type);
    ELSE
        UPDATE account_data
        SET accountname    = COALESCE(i_accountname, accountname),
            password       = COALESCE(i_password, password),
            email          = COALESCE(i_email, email),
            last_ip        = COALESCE(i_last_ip, last_ip),
            last_server_id = COALESCE(i_last_server_id, last_server_id)
        WHERE accountname = i_accountname
        RETURNING id INTO o_id;

        UPDATE account_info
        SET banned          = COALESCE(i_banned, banned),
            ban_expire_time = COALESCE(i_ban_expire_time, ban_expire_time),
            premium_points  = COALESCE(i_premium_points, premium_points),
            premium_type    = COALESCE(i_premium_type, premium_type)
        WHERE id = o_id;
    END IF;
END;
$function$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION account_data_get(i_accountname VARCHAR)
    RETURNS TABLE
            (
                accountname     VARCHAR,
                password        VARCHAR,
                email           VARCHAR,
                created_time    TIMESTAMP,
                last_ip         VARCHAR,
                last_server_id  SMALLINT,
                banned          BOOLEAN,
                ban_expire_time TIMESTAMP,
                premium_points  INT,
                premium_type    SMALLINT
            )
AS
/***
    History:
        Author        Date        Description
        ngolubenko    10-02-2020
***/
$function$
BEGIN
    RETURN QUERY
        SELECT v.accountname,
               v.password,
               v.email,
               v.created_time,
               v.last_ip,
               v.last_server_id,
               v.banned,
               v.ban_expire_time,
               v.premium_points,
               v.premium_type
        FROM account_data_view v
        WHERE v.accountname = i_accountname;
END;
$function$ LANGUAGE plpgsql;

