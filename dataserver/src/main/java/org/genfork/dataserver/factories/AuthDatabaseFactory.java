package org.genfork.dataserver.factories;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.genfork.dataserver.config.AuthDataBaseConfig;
import org.genfork.tools.database.JdbcHelper;
import org.genfork.tools.database.factory.AbstractDatabaseFactory;
import org.genfork.tools.database.flyway.DatabaseMigrations;
import org.genfork.tools.loader.annotations.InstanceGetter;
import org.genfork.tools.loader.annotations.Load;
import org.genfork.tools.loader.interfaces.PreLoadGroup;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Slf4j
public class AuthDatabaseFactory extends AbstractDatabaseFactory {
	@Getter(lazy = true, onMethod = @__(@InstanceGetter))
	private static final AuthDatabaseFactory instance = new AuthDatabaseFactory();

	private static final String scriptsPath = "db/auth";

	@Getter
	private JdbcHelper jdbcHelper;

	@Load(group = PreLoadGroup.class)
	public void load() {
		// trigger
	}

	@Override
	public void preInit() {
		start(AuthDataBaseConfig.URL,
				AuthDataBaseConfig.USERNAME,
				AuthDataBaseConfig.PASSWORD,
				AuthDataBaseConfig.MAX_POOL_SIZE,
				AuthDataBaseConfig.MIN_IDLE_CONNECTIONS,
				AuthDataBaseConfig.CONNECTION_TIMEOUT,
				AuthDataBaseConfig.IDLE_CONNECTION_TIMOUT);

		jdbcHelper = new JdbcHelper(getConnectionPool());
	}

	@Override
	public void postInit() {
		DatabaseMigrations.startMigration(this, scriptsPath);

		retrieveFunctionsMetaData();

		log.info("Auth database initialized");
	}
}
