package org.genfork.dataserver;

import lombok.extern.slf4j.Slf4j;
import org.genfork.dataserver.async.DataThreadPoolInitializer;
import org.genfork.dataserver.config.ConfigMarker;
import org.genfork.tools.common.CommonUtil;
import org.genfork.tools.loader.AppLoader;
import org.genfork.tools.loader.Loader;
import org.genfork.tools.loader.interfaces.*;
import org.genfork.tools.management.ShutdownManager;
import org.genfork.tools.management.TerminationStatus;
import org.genfork.tools.network.ServerNetworkController;
import org.genfork.tools.path.BasePathProvider;
import org.genfork.tools.versioning.VersionInfo;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.concurrent.CompletableFuture;

import static org.genfork.tools.loader.AppLoader.ApplicationMode.DATA;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Slf4j
public class AppInit {
	private AppInit() {
		AppLoader.defaultInit(DATA, ConfigMarker.class.getPackage().getName(), DataThreadPoolInitializer.class);

		final Loader loader = new Loader(getClass().getPackage().getName());
		CompletableFuture<Void> sideLoadCompletableFuture;
		try {
			sideLoadCompletableFuture = loader.runAsync(SideLoadGroup.class);
			loader.runAsync(PreLoadGroup.class, LoadGroup.class, ScriptLoadGroup.class, ClientAccessLoadGroup.class, PostLoadGroup.class).join();
		} catch (Exception e) {
			log.warn("", e);
			ShutdownManager.halt(TerminationStatus.RUNTIME_INITIALIZATION_FAILURE);
			return;
		}

//		ShutdownHooks.init();

		AppLoader.defaultPostInit(VersionInfo.of(AppLoader.class, ServerNetworkController.class));

		try {
			sideLoadCompletableFuture.join();
		} catch (Exception e) {
			log.warn("", e);
		}

		try {
			loader.writeDependencyTreeToFile(Files.createDirectories(BasePathProvider.resolvePath(Paths.get("log", "loader-stats"))).resolve(LocalDateTime.now().format(CommonUtil.getFilenameDateTimeFormatter()) + ".txt"), SideLoadGroup.class, PreLoadGroup.class, LoadGroup.class, ScriptLoadGroup.class, ClientAccessLoadGroup.class, PostLoadGroup.class);
		} catch (Exception e) {
			log.warn("", e);
		}
	}

	public static void main(String[] args) {
		new AppInit();
	}
}
