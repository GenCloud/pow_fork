package org.genfork.dataserver.data.game.repository;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.genfork.dataserver.data.AbstractRepository;
import org.genfork.dataserver.factories.GameDatabaseFactory;
import org.genfork.server.model.lobby.InventoryItemInfo;
import org.genfork.tools.data.StatsSet;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Slf4j
public class GamePlayersDataRepository extends AbstractRepository {
	public static final String ID_PARAM = "id";
	public static final String ACCOUNT_NAME_PARAM = "account_name";
	public static final String PLAYER_NAME_PARAM = "player_name";
	public static final String CREATED_TIME_PARAM = "created_time";
	public static final String ACCESS_LEVEL_PARAM = "access_level";
	public static final String LOBBY_SLOT_PARAM = "lobby_slot";
	public static final String LEVEL_PARAM = "level";

	public static final String RACE_PARAM = "race";
	public static final String CLASS_ID_PARAM = "class_id";
	public static final String BASE_CLASS_PARAM = "base_class";
	public static final String MAX_CP_PARAM = "max_cp";
	public static final String CURRENT_CP_PARAM = "current_cp";
	public static final String MAX_HP_PARAM = "max_hp";
	public static final String CURRENT_HP_PARAM = "current_hp";
	public static final String MAX_MP_PARAM = "max_mp";
	public static final String CURRENT_MP_PARAM = "current_mp";
	public static final String FACE_PARAM = "face";
	public static final String HAIR_STYLE_PARAM = "hair_style";
	public static final String HAIR_COLOR_PARAM = "hair_color";
	public static final String SEX_PARAM = "sex";
	public static final String X_PARAM = "x";
	public static final String Y_PARAM = "y";
	public static final String Z_PARAM = "z";
	public static final String H_PARAM = "h";
	public static final String CLAN_ID_PARAM = "clan_id";
	public static final String RAID_BOSS_POINTS_PARAM = "raid_boss_points";
	public static final String FAME_PARAM = "fame";
	public static final String REPUTATION_PARAM = "reputation";
	public static final String RECOMMENDATIONS_HAVE_PARAM = "recommendations_have";
	public static final String RECOMMENDATIONS_LEFT_PARAM = "recommendations_left";
	public static final String PVP_PARAM = "pvp_kills";
	public static final String PK_PARAM = "pk_kills";
	public static final String SP_PARAM = "sp";
	public static final String EXP_PARAM = "experience";
	public static final String EXP_BEFORE_DEATH_PARAM = "exp_before_death";
	public static final String TITLE_PARAM = "title";
	public static final String TITLE_COLOR_PARAM = "title_color";
	public static final String ONLINE_PARAM = "online";
	public static final String ONLINE_TIME_PARAM = "online_time";
	public static final String LAST_ACCESS_PARAM = "last_access";
	public static final String TRANSFORM_ID_PARAM = "transform_id";
	public static final String DELETE_TIME_PARAM = "delete_time";
	public static final String CAN_CRAFT_PARAM = "can_craft";
	public static final String NOBLESS_PARAM = "nobless";
	public static final String VITALITY_POINTS_PARAM = "vitality_points";

	public static final String EXISTS_PARAM = "exists";

	public static final String INVENTORY_PARAM = "inventory_data";

	@Getter(lazy = true)
	private static final GamePlayersDataRepository instance = new GamePlayersDataRepository();

	private static final String CHECK_PLAYER_NAME_EXISTS_FUNCTION = "players_name_exists";
	private static final String SELECT_PLAYERS_BY_ACCOUNT_FUNCTION = "players_data_by_account_get";
	private static final String SELECT_PLAYER_BY_NAME_FUNCTION = "players_data_by_name_get";
	private static final String INSERT_UPDATE_PLAYER_DATA = "players_data_insert_update";

	public GamePlayersDataRepository() {
		super(GameDatabaseFactory.getInstance(), GameDatabaseFactory.getInstance().getJdbcHelper());
	}

	public boolean existsPlayerName(String playerName) {
		if (log.isDebugEnabled()) {
			log.debug("Check player name existence - {}", playerName);
		}

		final StatsSet set = new StatsSet();

		executeCall(CHECK_PLAYER_NAME_EXISTS_FUNCTION, rs -> set.set(EXISTS_PARAM, rs.getBoolean(1)), playerName);

		return set.getBoolean(EXISTS_PARAM);
	}

	public void createPlayer(String playerName,
							 String accountName,
							 int race,
							 int classId,
							 int hairStyle,
							 int hairColor,
							 int face,
							 int sex,
							 int accessLevel,
							 int x,
							 int y,
							 int z,
							 int h,
							 int maxCp,
							 int currentCp,
							 int maxHp,
							 int currentHp,
							 int maxMp,
							 int currentMp,
							 int recommendationsLeft,
							 List<InventoryItemInfo> itemInfos) {
		if (log.isDebugEnabled()) {
			log.debug("Create player info by name - {}, account name: {}, access level: {}", playerName, accountName, accessLevel);
		}

		final List<StatsSet> sets = loadPlayersByAccount(accountName);
		final int lobbySlot = sets.isEmpty() ? 0 : sets.size();

		final StatsSet set = new StatsSet();
		executeCall(INSERT_UPDATE_PLAYER_DATA, rs ->
						set.set(ID_PARAM, rs.getInt(1)),
				playerName,
				accountName,
				lobbySlot,
				race,
				classId,
				classId,
				hairStyle,
				hairColor,
				face,
				sex,
				accessLevel,
				1,
				new Timestamp(Instant.now().toEpochMilli()),
				maxCp,
				currentCp,
				maxHp,
				currentHp,
				maxMp,
				currentMp,
				x, y, z, h,
				null,
				null,
				null,
				null,
				null,
				recommendationsLeft);

		final int playerId = set.getInt(ID_PARAM);

		itemInfos.forEach(i ->
				GamePlayerInventoryDataRepository.getInstance().createUpdateItem(playerId, i));
	}

	public StatsSet loadPlayerByName(String playerName) {
		final StatsSet set = new StatsSet();

		if (log.isDebugEnabled()) {
			log.debug("Loading player info by name - {}", playerName);
		}

		final boolean exists = executeCall(SELECT_PLAYER_BY_NAME_FUNCTION, rs -> playerMapIntoSet(set, rs), playerName);
		final int playerId = set.getInt(ID_PARAM);
		final List<StatsSet> inventoryData = GamePlayerInventoryDataRepository.getInstance().loadItems(playerId, false);
		set.set(INVENTORY_PARAM, inventoryData);

		set.set(EXISTS_PARAM, exists);
		return set;
	}

	public List<StatsSet> loadPlayersByAccount(String accountName) {
		if (log.isDebugEnabled()) {
			log.debug("Loading all players info by account name - {}", accountName);
		}

		final List<StatsSet> sets = new ArrayList<>();
		executeCall(SELECT_PLAYERS_BY_ACCOUNT_FUNCTION, rs -> {
			final StatsSet set = new StatsSet(new LinkedHashMap<>());
			playerMapIntoSet(set, rs);
			sets.add(set);
		}, accountName);

		sets.forEach(set -> {
			final int playerId = set.getInt(ID_PARAM);
			final List<StatsSet> inventoryData = GamePlayerInventoryDataRepository.getInstance().loadItems(playerId, true);
			set.set(INVENTORY_PARAM, inventoryData);
		});

		return sets;
	}

	private void playerMapIntoSet(StatsSet set, ResultSet rs) throws SQLException {
		set.set(ID_PARAM, rs.getInt(ID_PARAM));
		set.set(PLAYER_NAME_PARAM, rs.getString(PLAYER_NAME_PARAM));
		set.set(ACCOUNT_NAME_PARAM, rs.getString(ACCOUNT_NAME_PARAM));

		set.set(CREATED_TIME_PARAM, rs.getTimestamp(CREATED_TIME_PARAM) != null ? rs.getTimestamp(CREATED_TIME_PARAM).getTime() : -1);

		set.set(ACCESS_LEVEL_PARAM, rs.getInt(ACCESS_LEVEL_PARAM));
		set.set(LOBBY_SLOT_PARAM, rs.getInt(LOBBY_SLOT_PARAM));
		set.set(LEVEL_PARAM, rs.getInt(LEVEL_PARAM));

		set.set(RACE_PARAM, rs.getInt(RACE_PARAM));

		set.set(CLASS_ID_PARAM, rs.getInt(CLASS_ID_PARAM));
		set.set(BASE_CLASS_PARAM, rs.getInt(BASE_CLASS_PARAM));

		set.set(MAX_CP_PARAM, rs.getInt(MAX_CP_PARAM));
		set.set(CURRENT_CP_PARAM, rs.getInt(CURRENT_CP_PARAM));
		set.set(MAX_HP_PARAM, rs.getInt(MAX_HP_PARAM));
		set.set(CURRENT_HP_PARAM, rs.getInt(CURRENT_HP_PARAM));
		set.set(MAX_MP_PARAM, rs.getInt(MAX_MP_PARAM));
		set.set(CURRENT_MP_PARAM, rs.getInt(CURRENT_MP_PARAM));

		set.set(FACE_PARAM, rs.getInt(FACE_PARAM));
		set.set(HAIR_STYLE_PARAM, rs.getInt(HAIR_STYLE_PARAM));
		set.set(HAIR_COLOR_PARAM, rs.getInt(HAIR_COLOR_PARAM));
		set.set(SEX_PARAM, rs.getInt(SEX_PARAM));

		set.set(X_PARAM, rs.getInt(X_PARAM));
		set.set(Y_PARAM, rs.getInt(Y_PARAM));
		set.set(Z_PARAM, rs.getInt(Z_PARAM));
		set.set(H_PARAM, rs.getInt(H_PARAM));

		set.set(CLAN_ID_PARAM, rs.getInt(CLAN_ID_PARAM));
		set.set(RAID_BOSS_POINTS_PARAM, rs.getInt(RAID_BOSS_POINTS_PARAM));
		set.set(FAME_PARAM, rs.getInt(FAME_PARAM));
		set.set(REPUTATION_PARAM, rs.getInt(REPUTATION_PARAM));
		set.set(RECOMMENDATIONS_HAVE_PARAM, rs.getInt(RECOMMENDATIONS_HAVE_PARAM));
		set.set(RECOMMENDATIONS_LEFT_PARAM, rs.getInt(RECOMMENDATIONS_LEFT_PARAM));

		set.set(PVP_PARAM, rs.getInt(PVP_PARAM));
		set.set(PK_PARAM, rs.getInt(PK_PARAM));
		set.set(SP_PARAM, rs.getLong(SP_PARAM));
		set.set(EXP_PARAM, rs.getLong(EXP_PARAM));
		set.set(EXP_BEFORE_DEATH_PARAM, rs.getLong(EXP_BEFORE_DEATH_PARAM));

		set.set(TITLE_PARAM, rs.getString(TITLE_PARAM) == null ? "" : rs.getString(TITLE_PARAM));
		set.set(TITLE_COLOR_PARAM, rs.getInt(TITLE_COLOR_PARAM));

		set.set(ONLINE_PARAM, rs.getInt(ONLINE_PARAM));
		set.set(ONLINE_TIME_PARAM, rs.getInt(ONLINE_TIME_PARAM));
		set.set(LAST_ACCESS_PARAM, rs.getLong(LAST_ACCESS_PARAM));

		set.set(TRANSFORM_ID_PARAM, rs.getInt(TRANSFORM_ID_PARAM));

		set.set(DELETE_TIME_PARAM, rs.getLong(DELETE_TIME_PARAM));

		set.set(CAN_CRAFT_PARAM, rs.getInt(CAN_CRAFT_PARAM));
		set.set(NOBLESS_PARAM, rs.getInt(NOBLESS_PARAM));
		set.set(VITALITY_POINTS_PARAM, rs.getInt(VITALITY_POINTS_PARAM));
	}
}
