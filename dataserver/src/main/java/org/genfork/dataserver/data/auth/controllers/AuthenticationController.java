package org.genfork.dataserver.data.auth.controllers;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.genfork.dataserver.data.auth.repository.AuthAccountRepository;
import org.genfork.dataserver.network.clients.send.auth.AccountSelectResultPacket;
import org.genfork.dataserver.network.discovery.DiscoveryServerHandler;
import org.genfork.tools.crypto.Encoder;
import org.genfork.tools.data.StatsSet;

import java.time.Instant;

import static org.genfork.dataserver.data.auth.repository.AuthAccountRepository.*;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Slf4j
public class AuthenticationController {
	public static final int SERVER_ERROR = 1;
	public static final int ACCOUNT_VALID_CODE = 0;
	public static final int ACCOUNT_NOT_VALID_CODE = 2;
	public static final int ACCOUNT_DOES_NOT_EXISTS = 3;
	public static final int ACCOUNT_NOT_MATCHES_PASSWORD_CODE = 4;
	public static final int ACCOUNT_BANNED = 5;

	@Getter(lazy = true)
	private static final AuthenticationController instance = new AuthenticationController();

	public void checkAccountAndSendResult(DiscoveryServerHandler clientHandler,
										  String fromClient,
										  String toClient,
										  boolean autoCreation,
										  String playerClientId,
										  String ipAddress,
										  String accountName,
										  String password) {
		StatsSet set = AuthAccountRepository.getInstance().loadAccount(accountName);
		if (!set.getBoolean(EXISTS_PARAM)) {
			if (autoCreation) {
				final String encodedPassword = Encoder.getPasswordEncoder().encode(password);
				AuthAccountRepository.getInstance().saveAccount(accountName, encodedPassword, ipAddress);
				set = AuthAccountRepository.getInstance().loadAccount(accountName);
			} else {
				final AccountSelectResultPacket result = new AccountSelectResultPacket(playerClientId, ACCOUNT_DOES_NOT_EXISTS);

				clientHandler.sendPacket(
						fromClient,
						toClient,
						result);
				return;
			}
		}

		if (set == null) {
			final AccountSelectResultPacket result = new AccountSelectResultPacket(playerClientId, SERVER_ERROR);

			clientHandler.sendPacket(fromClient,
					toClient,
					result);
			return;
		}

		final boolean banned = set.getBoolean(BANNED_PARAM);

		if (banned) {
			final long banExpireTime = set.getLong(BAN_EXPIRE_PARAM);
			if (banExpireTime > Instant.now().toEpochMilli()) {
				final AccountSelectResultPacket result = new AccountSelectResultPacket(playerClientId, ACCOUNT_BANNED);

				clientHandler.sendPacket(fromClient,
						toClient,
						result);
				return;
			}

			AuthAccountRepository.getInstance().updateAccount(accountName, null, null, null, null,
					false, null, null, null);
		}

		final String originPassword = set.getString(PASSWORD_PARAM);
		if (!Encoder.getPasswordEncoder().matches(password, originPassword)) {
			final AccountSelectResultPacket result = new AccountSelectResultPacket(playerClientId, ACCOUNT_NOT_MATCHES_PASSWORD_CODE);

			clientHandler.sendPacket(fromClient,
					toClient,
					result);
			return;
		}

		final String email = set.getString(EMAIL_PARAM, null);
		final String lastIp = set.getString(LAST_IP_PARAM, null);
		final int lastServerId = set.getInt(LAST_SERVER_ID_PARAM, -1);

		final AccountSelectResultPacket result = new AccountSelectResultPacket(playerClientId, ACCOUNT_VALID_CODE, accountName, email, lastIp, lastServerId);

		clientHandler.sendPacket(fromClient,
				toClient,
				result);
	}
}
