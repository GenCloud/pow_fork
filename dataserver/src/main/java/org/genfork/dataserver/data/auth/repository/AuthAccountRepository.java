package org.genfork.dataserver.data.auth.repository;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.genfork.dataserver.data.AbstractRepository;
import org.genfork.dataserver.factories.AuthDatabaseFactory;
import org.genfork.tools.data.StatsSet;

import java.time.Instant;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Slf4j
public class AuthAccountRepository extends AbstractRepository {
	public static final String ACCOUNT_NAME_PARAM = "accountname";
	public static final String PASSWORD_PARAM = "password";
	public static final String EMAIL_PARAM = "email";
	public static final String CREATED_TIME_PARAM = "created_time";
	public static final String LAST_IP_PARAM = "last_ip";
	public static final String LAST_SERVER_ID_PARAM = "last_server_id";
	public static final String BANNED_PARAM = "banned";
	public static final String BAN_EXPIRE_PARAM = "ban_expire_time";
	public static final String PREMIUM_POINTS_PARAM = "premium_points";
	public static final String PREMIUM_TYPE_PARAM = "premium_type";
	public static final String EXISTS_PARAM = "exists";

	@Getter(lazy = true)
	private static final AuthAccountRepository instance = new AuthAccountRepository();

	private static final String LOAD_ACCOUNT_FUNCTION = "account_data_get";
	private static final String INSERT_UPDATE_ACCOUNT_DATA = "account_data_insert_update";

	public AuthAccountRepository() {
		super(AuthDatabaseFactory.getInstance(), AuthDatabaseFactory.getInstance().getJdbcHelper());
	}

	public StatsSet loadAccount(String accountName) {
		final StatsSet set = new StatsSet();

		if (log.isDebugEnabled()) {
			log.debug("Loading account info by name - {}", accountName);
		}

		final boolean exists = executeCall(LOAD_ACCOUNT_FUNCTION, rs -> {
			set.set(ACCOUNT_NAME_PARAM, rs.getString(ACCOUNT_NAME_PARAM));
			set.set(PASSWORD_PARAM, rs.getString(PASSWORD_PARAM));
			set.set(EMAIL_PARAM, rs.getString(EMAIL_PARAM));
			set.set(CREATED_TIME_PARAM, rs.getTimestamp(CREATED_TIME_PARAM));
			set.set(LAST_IP_PARAM, rs.getString(LAST_IP_PARAM));
			set.set(LAST_SERVER_ID_PARAM, rs.getShort(LAST_SERVER_ID_PARAM));
			set.set(BANNED_PARAM, rs.getBoolean(BANNED_PARAM));
			set.set(BAN_EXPIRE_PARAM, rs.getTimestamp(BAN_EXPIRE_PARAM) != null ? rs.getTimestamp(BAN_EXPIRE_PARAM).getTime() : -1);
			set.set(PREMIUM_POINTS_PARAM, rs.getInt(PREMIUM_POINTS_PARAM));
			set.set(PREMIUM_TYPE_PARAM, rs.getShort(PREMIUM_TYPE_PARAM));
		}, accountName);

		set.set(EXISTS_PARAM, exists);
		return set;
	}

	public void saveAccount(String accountName, String password, String ip) {
		if (log.isDebugEnabled()) {
			log.debug("Save account info by name - {}, password: {}, ipAddress: {}", accountName, password, ip);
		}

		executeCall(INSERT_UPDATE_ACCOUNT_DATA, accountName, password, null, Instant.now().toEpochMilli(), ip);
	}

	public void updateAccount(String accountName,
							  String password,
							  String email,
							  String ipAddress,
							  Integer lastServerId,
							  Boolean banned,
							  Long banExpireTime,
							  Integer premiumPoints,
							  Integer premiumType) {
		if (log.isDebugEnabled()) {
			log.debug("Update account info by name - {}, ipAddress: {}, serverId: {}", accountName, ipAddress, lastServerId);
		}

		executeCall(INSERT_UPDATE_ACCOUNT_DATA, accountName, password, email, null, ipAddress, lastServerId, banned, banExpireTime, premiumPoints, premiumType);
	}
}
