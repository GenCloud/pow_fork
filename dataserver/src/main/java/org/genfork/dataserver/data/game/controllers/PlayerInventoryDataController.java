package org.genfork.dataserver.data.game.controllers;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.genfork.dataserver.data.game.repository.GamePlayersDataRepository;
import org.genfork.dataserver.network.clients.send.game.PlayerInventoryDataResultPacket;
import org.genfork.dataserver.network.discovery.DiscoveryServerHandler;
import org.genfork.tools.data.StatsSet;
import org.genfork.tools.network.SessionInfo;

import java.util.Collections;
import java.util.List;

import static org.genfork.dataserver.data.game.repository.GamePlayersDataRepository.EXISTS_PARAM;
import static org.genfork.dataserver.data.game.repository.GamePlayersDataRepository.INVENTORY_PARAM;

/**
 * @author: ngolubenko@context-it.ru
 * @created: 2020/04
 */
@Slf4j
public class PlayerInventoryDataController {
	@Getter(lazy = true)
	private static final PlayerInventoryDataController instance = new PlayerInventoryDataController();

	public void restorePlayerInventory(String playerName, String fromClient, String toClient, SessionInfo sessionInfo, DiscoveryServerHandler client) {
		if (log.isDebugEnabled()) {
			log.debug("Loading player info by name - {}", playerName);
		}

		final StatsSet set = GamePlayersDataRepository.getInstance().loadPlayerByName(playerName);
		final boolean exists = set.getBoolean(EXISTS_PARAM);
		final List<StatsSet> list = exists ? set.getList(INVENTORY_PARAM, StatsSet.class) : Collections.emptyList();
		final PlayerInventoryDataResultPacket result = new PlayerInventoryDataResultPacket(playerName, list);
		client.sendPacket(fromClient,
				toClient,
				sessionInfo,
				result);
	}
}
