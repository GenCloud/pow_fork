package org.genfork.dataserver.data;

import lombok.extern.slf4j.Slf4j;
import org.genfork.tools.database.*;
import org.genfork.tools.database.factory.AbstractDatabaseFactory;
import org.genfork.tools.database.store.FunctionFieldData;
import org.genfork.tools.database.store.FunctionMetadata;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.*;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.IntStream;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Slf4j
public abstract class AbstractRepository {
	private static final String SELECTION_LITERAL = "SELECT * FROM ";
	private static final String ONE_PARAMETER_LITERAL = "?";
	private static final String WITH_NEXT_PARAMETER_LITERAL = "?,";
	private static final String OPEN_PARAMS_BLOCK_LITERAL = "(";
	private static final String CLOSE_PARAMS_BLOCK_LITERAL = ")";

	private final AbstractDatabaseFactory databaseFactory;
	private final JdbcHelper helper;

	public AbstractRepository(AbstractDatabaseFactory databaseFactory, JdbcHelper helper) {
		this.databaseFactory = databaseFactory;
		this.helper = helper;
	}

	public boolean executeCall(String functionName, Object... params) {
		return executeCall(functionName, null, params);
	}

	public boolean executeCall(String functionName, ResultSetHandler handler, Object... params) {
		final Map<String, FunctionMetadata> functions = databaseFactory.getFunctions();
		final FunctionMetadata functionMetadata = functions.get(functionName);
		if (functionMetadata == null) {
			log.warn("Can't exec function with name [{}] - not found in database metadata!", functionName);
			return false;
		}

		final LinkedHashMap<String, FunctionFieldData> functionParameters = functionMetadata.getParams();

		final int size = functionParameters.size();
		final int length = params.length;
		final Object[] objects = new Object[size];
		if (length != size) {
			for (String key : functionParameters.keySet()) {
				final FunctionFieldData functionFieldData = functionParameters.get(key);
				final int currentPos = functionFieldData.getIdx();
				final Object definedValue = objects[currentPos];
				if (definedValue != null) {
					log.warn("Parameter value already defined in array, check it!");
					return false;
				}

				if (currentPos + 1 > length) {
					objects[currentPos] = null;
				} else {
					objects[currentPos] = params[currentPos];
				}
			}
		} else {
			System.arraycopy(params, 0, objects, 0, length);
		}

		final FunctionFieldData[] fieldsData = functionParameters.values().toArray(new FunctionFieldData[0]);
		final String callString = createCallString(functionName.toLowerCase(), fieldsData);

		helper.beginTransaction();

		boolean exists = false;
		boolean rollBack = false;

		Connection connection;
		CallableStatement callableStatement = null;
		ResultSet resultSet = null;

		try {
			connection = helper.getConnection();
			callableStatement = connection.prepareCall(callString);

			for (int idx = 0; idx < objects.length; idx++) {
				final FunctionFieldData procedureData = fieldsData[idx];
				final Object value = objects[idx];

				buildParametrizedStatement(connection, procedureData, callableStatement, value, idx + 1);
			}

			resultSet = callableStatement.executeQuery();
			if (resultSet != null) {
				exists = resultSet.isBeforeFirst();

				if (exists && handler != null) {
					while (resultSet.next()) {
						handler.processRow(resultSet);
					}
				}
			}
		} catch (SQLException ex) {
			rollBack = helper.isInTransaction();
			throw new JdbcException("Error executing call:\n" + callString + "\n\nError: " + ex.getMessage(), ex);
		} catch (NoResultException ex) {
			exists = false;
		} finally {
			JdbcUtil.close(callableStatement, resultSet);
			if (rollBack) {
				helper.rollbackTransaction();
			} else {
				helper.commitTransaction();
			}
		}

		return exists;
	}

	public void execute(String query, Object... params) {
		helper.beginTransaction();
		helper.execute(query, params);
	}

	public boolean query(String query, ResultSetHandler handler, Object... params) {
		helper.beginTransaction();
		return helper.query(query, handler, params);
	}

	private void buildParametrizedStatement(Connection connection, FunctionFieldData procedureData, CallableStatement callableStatement, Object value, int parameterIdx) throws SQLException {
		final String stringValue = String.valueOf(value);

		final Class<?> type = procedureData.getType();

		if (value == null) {
			final int sqlType = toSqlType(type);
			callableStatement.setNull(parameterIdx, sqlType);
		} else {
			try {
				if (type == BigDecimal.class || type == BigInteger.class) {
					callableStatement.setBigDecimal(parameterIdx, new BigDecimal(stringValue));
				} else if (type == Integer.class || type == int.class) {
					callableStatement.setInt(parameterIdx, Integer.parseInt(stringValue));
				} else if (type == Short.class || type == short.class) {
					callableStatement.setShort(parameterIdx, Short.parseShort(stringValue));
				} else if (type == Float.class || type == float.class) {
					callableStatement.setFloat(parameterIdx, Float.parseFloat(stringValue));
				} else if (type == Double.class || type == double.class) {
					callableStatement.setDouble(parameterIdx, Double.parseDouble(stringValue));
				} else if (type == Long.class || type == long.class) {
					callableStatement.setLong(parameterIdx, Long.parseLong(stringValue));
				} else if (type == Boolean.class || type == boolean.class) {
					callableStatement.setBoolean(parameterIdx, Boolean.parseBoolean(stringValue));
				} else if (type == String.class) {
					callableStatement.setString(parameterIdx, stringValue);
				} else if (type == Byte.class || type == byte.class) {
					callableStatement.setByte(parameterIdx, Byte.parseByte(stringValue));
				} else if (type == Byte[].class) {
					callableStatement.setBytes(parameterIdx, (byte[]) value);
				} else if (type == Timestamp.class) {
					final boolean assign = value.getClass().isAssignableFrom(Long.class) || value.getClass().isAssignableFrom(long.class);
					if (assign) {
						callableStatement.setTimestamp(parameterIdx, new Timestamp((long) value));
					} else {
						callableStatement.setTimestamp(parameterIdx, (Timestamp) value);
					}
				} else if (type == Array.class) {
					final String typeString = procedureData.getTypeString();
					Object[] array;
					if (value.getClass() == long[].class
							|| value.getClass() == int[].class
							|| value.getClass() == float[].class
							|| value.getClass() == boolean[].class) {
						array = IntStream.range(0, java.lang.reflect.Array.getLength(value))
								.mapToObj(i -> java.lang.reflect.Array.get(value, i))
								.toArray();
					} else if (value instanceof Object[]) {
						array = (Object[]) value;
					} else {
						array = new Object[1];
						array[0] = value;
					}

					if (typeString.equals("_int4")) {
						array = Arrays.stream(array).map(e -> Integer.parseInt(String.valueOf(e))).toArray();
					} else if (typeString.equals("_varchar")) {
						array = Arrays.stream(array).map(String::valueOf).toArray();
					}

					final Array arrayOf = connection.createArrayOf(typeString.replace("_", ""), array);
					callableStatement.setArray(parameterIdx, arrayOf);
				}
			} catch (Exception ex) {
				log.warn("Can't exec procedure - procedure param [{}] is not valid type! Available type: {}", value, type.getSimpleName());
				throw ex;
			}
		}
	}

	private int toSqlType(Class<?> type) {
		int result = Types.NULL;
		if (type == String.class) {
			result = Types.VARCHAR;
		} else if (type == BigDecimal.class) {
			result = Types.DECIMAL;
		} else if (type == Boolean.class) {
			result = Types.BOOLEAN;
		} else if (type == Byte.class) {
			result = Types.TINYINT;
		} else if (type == Short.class) {
			result = Types.SMALLINT;
		} else if (type == Integer.class) {
			result = Types.INTEGER;
		} else if (type == Long.class) {
			result = Types.BIGINT;
		} else if (type == Float.class) {
			result = Types.FLOAT;
		} else if (type == Double.class) {
			result = Types.DOUBLE;
		} else if (type == Byte[].class) {
			result = Types.BINARY;
		} else if (type == Date.class) {
			result = Types.DATE;
		} else if (type == Time.class) {
			result = Types.TIME;
		} else if (type == Timestamp.class) {
			result = Types.TIMESTAMP;
		}

		return result;
	}

	private String createCallString(String procedureName, FunctionFieldData[] values) {
		final StringBuilder sb = new StringBuilder();
		sb.append(SELECTION_LITERAL);
		sb.append(procedureName);
		sb.append(OPEN_PARAMS_BLOCK_LITERAL);

		final int size = values.length;
		IntStream.range(0, size).forEach(i -> {
			final FunctionFieldData argumentInfo = values[i];
			if (argumentInfo.isInputParameter() || argumentInfo.isInOutParameter()) {
				sb.append(i + 1 != size ? WITH_NEXT_PARAMETER_LITERAL : ONE_PARAMETER_LITERAL);
			}
		});

		sb.append(CLOSE_PARAMS_BLOCK_LITERAL);
		return sb.toString();
	}
}
