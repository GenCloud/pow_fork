package org.genfork.dataserver.data.game.repository;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.genfork.dataserver.data.AbstractRepository;
import org.genfork.dataserver.factories.GameDatabaseFactory;
import org.genfork.server.enums.LocationData;
import org.genfork.server.model.lobby.InventoryItemInfo;
import org.genfork.tools.data.StatsSet;

import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Slf4j
public class GamePlayerInventoryDataRepository extends AbstractRepository {
	public static final String ID_PARAM = "id"; //             INT,
	public static final String PLAYER_ID_PARAM = "player_id"; //         INT,
	public static final String ITEM_ID_PARAM = "item_id"; //        INT,
	public static final String ITEM_COUNT_PARAM = "item_count"; //     BIGINT,
	public static final String ENCHANT_PARAM = "enchant"; //        INT,
	public static final String TYPE_1_PARAM = "type_first"; //     INT,
	public static final String TYPE_2_PARAM = "type_second"; //    INT,
	public static final String LOC_DATA_PARAM = "location_data"; //  loc,
	public static final String LOC_MASK_PARAM = "location_mask"; //  INT,
	public static final String USE_TIME_PARAM = "time_of_use"; //    INT,
	public static final String MP_LEFT_PARAM = "mp_left"; //        DECIMAL,
	public static final String LIFE_TIME_PARAM = "life_time"; //      DECIMAL,
	public static final String REUSE_DELAY_PARAM = "reuse_delay"; //    INT,
	public static final String SYS_TIME_PARAM = "sys_time"; //       BIGINT,
	public static final String ELEMENT_TYPES_PARAM = "element_types"; //  INT[],
	public static final String ELEMENT_VALUES_PARAM = "element_values"; // INT[],
	public static final String ABILITY_TYPES_PARAM = "ability_types"; //  INT[],
	public static final String ABILITY_IDS_PARAM = "ability_ids"; //    INT[],
	public static final String ABILITY_SLOTS_PARAM = "ability_slots"; //  INT[],
	public static final String MINERAL_IDS_PARAM = "mineral_ids"; //    INT[],
	public static final String OPTIONS_1_PARAM = "options_first"; //  INT[],
	public static final String OPTIONS_2_PARAM = "options_second"; // INT[]

	@Getter(lazy = true)
	private static final GamePlayerInventoryDataRepository instance = new GamePlayerInventoryDataRepository();

	private static final String RESTORE_INVENTORY_FUNCTION = "players_inventory_data_get";
	private static final String INSERT_UPDATE_INVENTORY_FUNCTION = "players_inventory_data_insert_update";

	public GamePlayerInventoryDataRepository() {
		super(GameDatabaseFactory.getInstance(), GameDatabaseFactory.getInstance().getJdbcHelper());
	}

	public List<StatsSet> loadItems(int playerId, boolean equipped) {
		final List<StatsSet> result = new ArrayList<>();

		if (log.isDebugEnabled()) {
			log.debug("Load inventory by player id - {}", playerId);
		}

		executeCall(RESTORE_INVENTORY_FUNCTION, rs -> {
			final StatsSet set = mapItemToSet(rs);
			result.add(set);
		}, playerId, equipped ? LocationData.PAPER_DOLL.name() : null);

		return result;
	}

	public void createUpdateItem(int playerId, InventoryItemInfo itemInfo) {
		if (log.isDebugEnabled()) {
			log.debug("Create/update inventory item by player id - {}", playerId);
		}

		executeCall(INSERT_UPDATE_INVENTORY_FUNCTION,
				playerId, itemInfo.getItemId(),
				itemInfo.getAmount(),
				itemInfo.getEnchant(),
				itemInfo.getLocationData().name(),
				itemInfo.getLocationMask(),
				itemInfo.getTypeFirst(),
				itemInfo.getTypeSecond(),
				itemInfo.getUseTime(),
				itemInfo.getMpLeft(),
				itemInfo.getLifeTime(),
				itemInfo.getReuseDelay(),
				itemInfo.getSysTime(),
				itemInfo.getElementTypes(),
				itemInfo.getElementValues(),
				itemInfo.getAbilityTypes(),
				itemInfo.getAbilityIds(),
				itemInfo.getAbilitySlots(),
				itemInfo.getMineralIds(),
				itemInfo.getOptionsFirst(),
				itemInfo.getOptionsSecond());
	}

	private StatsSet mapItemToSet(ResultSet rs) throws SQLException {
		final StatsSet set = new StatsSet(new LinkedHashMap<>());
		set.set(ID_PARAM, rs.getInt(ID_PARAM));
		set.set(PLAYER_ID_PARAM, rs.getInt(PLAYER_ID_PARAM));
		set.set(ITEM_ID_PARAM, rs.getInt(ITEM_ID_PARAM));
		set.set(ITEM_COUNT_PARAM, rs.getLong(ITEM_COUNT_PARAM));
		set.set(ENCHANT_PARAM, rs.getInt(ENCHANT_PARAM));
		set.set(TYPE_1_PARAM, rs.getInt(TYPE_1_PARAM));
		set.set(TYPE_2_PARAM, rs.getInt(TYPE_2_PARAM));
		set.set(LOC_DATA_PARAM, LocationData.valueOf(rs.getString(LOC_DATA_PARAM)).ordinal());
		set.set(LOC_MASK_PARAM, rs.getInt(LOC_MASK_PARAM));
		set.set(USE_TIME_PARAM, rs.getInt(USE_TIME_PARAM));
		set.set(MP_LEFT_PARAM, rs.getFloat(MP_LEFT_PARAM));
		set.set(LIFE_TIME_PARAM, rs.getFloat(LIFE_TIME_PARAM));
		set.set(REUSE_DELAY_PARAM, rs.getInt(REUSE_DELAY_PARAM));
		set.set(SYS_TIME_PARAM, rs.getLong(SYS_TIME_PARAM));

		final Array elementTypes = rs.getArray(ELEMENT_TYPES_PARAM);
		set.set(ELEMENT_TYPES_PARAM, elementTypes == null ? new int[0] : elementTypes.getArray());

		final Array elementValues = rs.getArray(ELEMENT_VALUES_PARAM);
		set.set(ELEMENT_VALUES_PARAM, elementValues == null ? new int[0] : elementValues.getArray());

		final Array abilityTypes = rs.getArray(ABILITY_TYPES_PARAM);
		set.set(ABILITY_TYPES_PARAM, abilityTypes == null ? new int[0] : abilityTypes.getArray());
		final Array abilityIds = rs.getArray(ABILITY_IDS_PARAM);
		set.set(ABILITY_IDS_PARAM, abilityIds == null ? new int[0] : abilityIds.getArray());
		final Array abilitySlots = rs.getArray(ABILITY_SLOTS_PARAM);
		set.set(ABILITY_SLOTS_PARAM, abilitySlots == null ? new int[0] : abilitySlots.getArray());

		final Array mineralIds = rs.getArray(MINERAL_IDS_PARAM);
		set.set(MINERAL_IDS_PARAM, mineralIds == null ? new int[0] : mineralIds.getArray());
		final Array optionsFirst = rs.getArray(OPTIONS_1_PARAM);
		set.set(OPTIONS_1_PARAM, optionsFirst == null ? new int[0] : optionsFirst.getArray());
		final Array optionsSecond = rs.getArray(OPTIONS_2_PARAM);
		set.set(OPTIONS_2_PARAM, optionsSecond == null ? new int[0] : optionsSecond.getArray());
		return set;
	}
}
