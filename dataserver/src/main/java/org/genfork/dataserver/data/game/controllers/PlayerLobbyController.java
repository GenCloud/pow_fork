package org.genfork.dataserver.data.game.controllers;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.genfork.dataserver.data.game.repository.GamePlayersDataRepository;
import org.genfork.dataserver.network.clients.send.game.AccountPlayersDataResultPacket;
import org.genfork.dataserver.network.clients.send.game.PlayerCreationResultPacket;
import org.genfork.dataserver.network.discovery.DiscoveryServerHandler;
import org.genfork.server.model.lobby.InventoryItemInfo;
import org.genfork.tools.data.StatsSet;
import org.genfork.tools.network.SessionInfo;

import java.util.List;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Slf4j
public class PlayerLobbyController {
	@Getter(lazy = true)
	private static final PlayerLobbyController instance = new PlayerLobbyController();

	public void tryReceiveAccountPlayerInfo(boolean authentication, String fromClient, String toClient, SessionInfo sessionInfo, DiscoveryServerHandler client, String accountName) {
		final List<StatsSet> sets = GamePlayersDataRepository.getInstance().loadPlayersByAccount(accountName);

		final AccountPlayersDataResultPacket result = new AccountPlayersDataResultPacket(sets, authentication);

		client.sendPacket(fromClient,
				toClient,
				sessionInfo,
				result);
	}

	public void tryCreatePlayer(String fromClient,
								String toClient,
								SessionInfo sessionInfo,
								DiscoveryServerHandler client,
								String playerName,
								String accountName,
								int race,
								int classId,
								int hairStyle,
								int hairColor,
								int face,
								int sex,
								int accessLevel,
								int x,
								int y,
								int z,
								int h,
								int maxCp,
								int currentCp,
								int maxHp,
								int currentHp,
								int maxMp,
								int currentMp,
								int recommendationsLeft,
								List<InventoryItemInfo> itemInfos) {
		GamePlayersDataRepository.getInstance().createPlayer(playerName,
				accountName,
				race,
				classId,
				hairStyle,
				hairColor,
				face,
				sex,
				accessLevel,
				x,
				y,
				z,
				h,
				maxCp,
				currentCp,
				maxHp,
				currentHp,
				maxMp,
				currentMp,
				recommendationsLeft,
				itemInfos);

		final PlayerCreationResultPacket result = new PlayerCreationResultPacket(1);
		client.sendPacket(fromClient,
				toClient,
				sessionInfo,
				result);
	}
}
