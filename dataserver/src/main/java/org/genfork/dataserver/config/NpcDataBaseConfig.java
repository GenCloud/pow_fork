package org.genfork.dataserver.config;

import org.genfork.tools.config.abstraction.ConfigPropertiesLoader;
import org.genfork.tools.config.annotation.ConfigClass;
import org.genfork.tools.config.annotation.ConfigField;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@ConfigClass(fileName = "NpcDatabase")
public class NpcDataBaseConfig extends ConfigPropertiesLoader {
	@ConfigField(name = "npc.database.url", value = "jdbc:postgresql://localhost:5432/npc_data")
	public static String URL;

	@ConfigField(name = "npc.database.username", value = "postgres")
	public static String USERNAME;

	@ConfigField(name = "npc.database.password", value = "postgres")
	public static String PASSWORD;

	@ConfigField(name = "npc.database.minIdleConnections", value = "4")
	public static int MIN_IDLE_CONNECTIONS;

	@ConfigField(name = "npc.database.maxPoolSize", value = "64")
	public static int MAX_POOL_SIZE;

	@ConfigField(name = "npc.database.connectionTimeout", value = "60000")
	public static int CONNECTION_TIMEOUT;

	@ConfigField(name = "npc.database.idleConnectionTimeout", value = "30000")
	public static int IDLE_CONNECTION_TIMOUT;
}
