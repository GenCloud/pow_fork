package org.genfork.dataserver.network.clients.send.game;

import lombok.RequiredArgsConstructor;
import org.genfork.tools.data.StatsSet;
import org.genfork.tools.network.OutgoingPacket;

import java.util.List;

import static org.genfork.tools.network.opcodes.ClientsBranchOpcodes.PLAYER_RECEIVE_INVENTORY_DATA_PACKET;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@RequiredArgsConstructor
public class PlayerSendInventoryDataPacket extends OutgoingPacket {
	private final List<StatsSet> sets;

	@Override
	public boolean write() {
		PLAYER_RECEIVE_INVENTORY_DATA_PACKET.writeId(this);

		writeD(sets.size());
		sets.forEach(set -> set.writeToPacket(this));
		return true;
	}
}
