package org.genfork.dataserver.network.clients.recv.game;

import org.genfork.dataserver.data.game.controllers.PlayerLobbyController;
import org.genfork.dataserver.network.discovery.DiscoveryServerHandler;
import org.genfork.server.enums.LocationData;
import org.genfork.server.model.lobby.InventoryItemInfo;
import org.genfork.tools.network.IncomingPacket;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class RequestPlayerCreationPacket extends IncomingPacket<DiscoveryServerHandler> {
	private String accountName;
	private String playerName;
	private int race;
	private int classId;
	private int hairStyle;
	private int hairColor;
	private int face;
	private int sex;
	private int accessLevel;

	private final List<InventoryItemInfo> itemInfos = new ArrayList<>();
	private int x, y, z, h;
	private int maxCp, currentCp, maxHp, currentHp, maxMp, currentMp;
	private int recommendationsLeft;

	@Override
	public boolean read(DiscoveryServerHandler client) {
		accountName = readS();
		playerName = readS();
		race = readD();
		classId = readD();
		hairStyle = readD();
		hairColor = readD();
		face = readD();
		sex = readD();
		accessLevel = readD();

		x = readD();
		y = readD();
		z = readD();
		h = readD();

		maxCp = readD();
		currentCp = readD();
		maxHp = readD();
		currentHp = readD();
		maxMp = readD();
		currentMp = readD();

		recommendationsLeft = readD();

		final int itemsSize = readD();
		if (itemsSize > 0) {
			for (int idx = 0; idx < itemsSize; idx++) {
				final InventoryItemInfo itemInfo = new InventoryItemInfo();
				itemInfo.setItemId(readD());
				itemInfo.setAmount(readQ());
				itemInfo.setEnchant(readD());
				itemInfo.setTypeFirst(readD());
				itemInfo.setTypeSecond(readD());
				final int ordinal = readD();
				itemInfo.setLocationData(LocationData.values()[ordinal]);
				itemInfo.setLocationMask(readD());
				itemInfo.setUseTime(readD());
				itemInfo.setMpLeft(readE());
				itemInfo.setLifeTime(readE());
				itemInfo.setReuseDelay(readD());
				itemInfo.setSysTime(readQ());

				final int elementsSize = readD();
				final int[] elementTypes = IntStream.range(0, elementsSize).map(eidx -> readD()).toArray();
				itemInfo.setElementTypes(elementTypes);

				final int elementValSize = readD();
				final int[] elementValues = IntStream.range(0, elementValSize).map(eidx -> readD()).toArray();
				itemInfo.setElementTypes(elementValues);

				final int abilitiesSize = readD();
				final int[] abilityTypes = IntStream.range(0, abilitiesSize).map(eidx -> readD()).toArray();
				itemInfo.setAbilityTypes(abilityTypes);

				final int abilityIdsSize = readD();
				final int[] abilityIds = IntStream.range(0, abilityIdsSize).map(eidx -> readD()).toArray();
				itemInfo.setAbilityIds(abilityIds);

				final int abilitySlotsSize = readD();
				final int[] abilitySlots = IntStream.range(0, abilitySlotsSize).map(eidx -> readD()).toArray();
				itemInfo.setAbilitySlots(abilitySlots);

				final int variationSize = readD();
				final int[] mineralIds = IntStream.range(0, variationSize).map(eidx -> readD()).toArray();
				itemInfo.setMineralIds(mineralIds);

				final int options1Size = readD();
				final int[] optionsFirst = IntStream.range(0, options1Size).map(eidx -> readD()).toArray();
				itemInfo.setOptionsFirst(optionsFirst);

				final int options2Size = readD();
				final int[] optionsSecond = IntStream.range(0, options2Size).map(eidx -> readD()).toArray();
				itemInfo.setOptionsSecond(optionsSecond);

				itemInfos.add(itemInfo);
			}
		}

		return true;
	}

	@Override
	public void run(DiscoveryServerHandler client) throws Exception {
		PlayerLobbyController.getInstance().tryCreatePlayer(
				getFromClientName(),
				getToClientName(),
				getSessionInfo(),
				client,
				playerName,
				accountName,
				race,
				classId,
				hairStyle,
				hairColor,
				face,
				sex,
				accessLevel,
				x,
				y,
				z,
				h,
				maxCp,
				currentCp,
				maxHp,
				currentHp,
				maxMp,
				currentMp,
				recommendationsLeft,
				itemInfos);
	}
}
