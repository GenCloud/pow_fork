package org.genfork.dataserver.network.clients.send.auth;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.genfork.tools.network.OutgoingPacket;

import static org.genfork.dataserver.data.auth.controllers.AuthenticationController.ACCOUNT_VALID_CODE;
import static org.genfork.tools.network.opcodes.ClientsBranchOpcodes.ACCOUNT_SELECT_RESULT_PACKET;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@RequiredArgsConstructor
@AllArgsConstructor
public class AccountSelectResultPacket extends OutgoingPacket {
	private final String playerClientId;
	private final int code;

	private String accountName;
	private String email;
	private String ipAddress;
	private int lastServerId;

	@Override
	public boolean write() {
		ACCOUNT_SELECT_RESULT_PACKET.writeId(this);

		writeS(playerClientId);
		writeC(code);

		if (code == ACCOUNT_VALID_CODE) {
			writeS(accountName);
			writeS(email);
			writeS(ipAddress);
			writeD(lastServerId);
		}
		return true;
	}
}
