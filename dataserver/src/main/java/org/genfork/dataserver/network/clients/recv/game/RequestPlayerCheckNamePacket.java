package org.genfork.dataserver.network.clients.recv.game;

import org.genfork.dataserver.data.game.repository.GamePlayersDataRepository;
import org.genfork.dataserver.network.clients.send.game.PlayerCheckNameResultPacket;
import org.genfork.dataserver.network.discovery.DiscoveryServerHandler;
import org.genfork.tools.network.IncomingPacket;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class RequestPlayerCheckNamePacket extends IncomingPacket<DiscoveryServerHandler> {
	private String playerName;

	@Override
	public boolean read(DiscoveryServerHandler client) {
		playerName = readS();
		return true;
	}

	@Override
	public void run(DiscoveryServerHandler client) throws Exception {
		final boolean exists = GamePlayersDataRepository.getInstance().existsPlayerName(playerName);
		final PlayerCheckNameResultPacket result = new PlayerCheckNameResultPacket(exists ? 1 : 0);

		client.sendPacket(getFromClientName(),
				getToClientName(),
				getSessionInfo(),
				result);
	}
}
