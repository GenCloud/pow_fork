package org.genfork.dataserver.network.clients.send.game;

import lombok.RequiredArgsConstructor;
import org.genfork.tools.network.OutgoingPacket;

import static org.genfork.tools.network.opcodes.ClientsBranchOpcodes.PLAYER_CREATION_RESULT_PACKET;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@RequiredArgsConstructor
public class PlayerCreationResultPacket extends OutgoingPacket {
	private final int result;

	@Override
	public boolean write() {
		PLAYER_CREATION_RESULT_PACKET.writeId(this);

		writeD(result);
		return true;
	}
}
