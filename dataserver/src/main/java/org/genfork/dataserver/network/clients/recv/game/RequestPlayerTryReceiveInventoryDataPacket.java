package org.genfork.dataserver.network.clients.recv.game;

import org.genfork.dataserver.data.game.controllers.PlayerInventoryDataController;
import org.genfork.dataserver.network.discovery.DiscoveryServerHandler;
import org.genfork.tools.network.IncomingPacket;

/**
 * @author: ngolubenko@context-it.ru
 * @created: 2020/04
 */
public class RequestPlayerTryReceiveInventoryDataPacket extends IncomingPacket<DiscoveryServerHandler> {
	private String playerName;

	@Override
	public boolean read(DiscoveryServerHandler client) {
		playerName = readS();
		return true;
	}

	@Override
	public void run(DiscoveryServerHandler client) throws Exception {
		PlayerInventoryDataController.getInstance()
				.restorePlayerInventory(playerName,
						getFromClientName(),
						getToClientName(),
						getSessionInfo(),
						client);
	}
}
