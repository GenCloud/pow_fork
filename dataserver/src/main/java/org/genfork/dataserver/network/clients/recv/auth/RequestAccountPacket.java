package org.genfork.dataserver.network.clients.recv.auth;

import org.apache.commons.lang3.StringUtils;
import org.genfork.dataserver.data.auth.controllers.AuthenticationController;
import org.genfork.dataserver.network.clients.send.auth.AccountSelectResultPacket;
import org.genfork.dataserver.network.discovery.DiscoveryServerHandler;
import org.genfork.tools.network.IncomingPacket;

import static org.genfork.dataserver.data.auth.controllers.AuthenticationController.ACCOUNT_NOT_VALID_CODE;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class RequestAccountPacket extends IncomingPacket<DiscoveryServerHandler> {
	private String playerClientId;
	private String accountName;
	private String accountPassword;
	private boolean autoCreation;
	private String ipAddress;

	@Override
	public boolean read(DiscoveryServerHandler clientHandler) {
		playerClientId = readS();
		autoCreation = readC() == 1;
		accountName = readS();
		accountPassword = readS();
		ipAddress = readS();
		return true;
	}

	@Override
	public void run(DiscoveryServerHandler clientHandler) throws Exception {
		if (StringUtils.isEmpty(accountName) || StringUtils.isEmpty(accountPassword)) {
			clientHandler.sendPacket(new AccountSelectResultPacket(playerClientId, ACCOUNT_NOT_VALID_CODE));
			return;
		}

		AuthenticationController.getInstance().checkAccountAndSendResult(clientHandler,
				getFromClientName(),
				getToClientName(),
				autoCreation,
				playerClientId,
				ipAddress,
				accountName,
				accountPassword);
	}
}
