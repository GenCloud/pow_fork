package org.genfork.dataserver.network.clients.send.game;

import lombok.RequiredArgsConstructor;
import org.genfork.tools.network.OutgoingPacket;

import static org.genfork.tools.network.opcodes.ClientsBranchOpcodes.PLAYER_CHECK_NAME_RESULT_PACKET;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@RequiredArgsConstructor
public class PlayerCheckNameResultPacket extends OutgoingPacket {
	private final int code;

	@Override
	public boolean write() {
		PLAYER_CHECK_NAME_RESULT_PACKET.writeId(this);

		writeD(code); // 1 - exists, 0 - non exists
		return true;
	}
}
