package org.genfork.dataserver.network.clients.recv.game;

import org.genfork.dataserver.data.game.controllers.PlayerLobbyController;
import org.genfork.dataserver.network.discovery.DiscoveryServerHandler;
import org.genfork.tools.network.IncomingPacket;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class RequestAccountPlayersInfoPacket extends IncomingPacket<DiscoveryServerHandler> {
	private String accountName;
	private boolean authentication;

	@Override
	public boolean read(DiscoveryServerHandler clientHandler) {
		authentication = readC() == 1;
		accountName = readS();
		return true;
	}

	@Override
	public void run(DiscoveryServerHandler client) throws Exception {
		PlayerLobbyController.getInstance().tryReceiveAccountPlayerInfo(authentication,
				getFromClientName(),
				getToClientName(),
				getSessionInfo(),
				client,
				accountName);
	}
}
