package org.genfork.dataserver.network.clients.send.game;

import lombok.RequiredArgsConstructor;
import org.genfork.tools.data.StatsSet;
import org.genfork.tools.network.OutgoingPacket;

import java.util.List;

import static org.genfork.tools.network.opcodes.ClientsBranchOpcodes.PLAYER_TRY_RECEIVE_INVENTORY_DATA_RESULT_PACKET;

/**
 * @author: ngolubenko@context-it.ru
 * @created: 2020/04
 */
@RequiredArgsConstructor
public class PlayerInventoryDataResultPacket extends OutgoingPacket {
	private final String playerName;
	private final List<StatsSet> inventoryInfo;

	@Override
	public boolean write() {
		PLAYER_TRY_RECEIVE_INVENTORY_DATA_RESULT_PACKET.writeId(this);

		writeS(playerName);

		writeD(inventoryInfo.size());

		inventoryInfo.forEach(info -> info.writeToPacket(this));

		return true;
	}
}
