package org.genfork.dataserver.network.clients.recv.auth;

import org.genfork.dataserver.data.auth.repository.AuthAccountRepository;
import org.genfork.dataserver.network.discovery.DiscoveryServerHandler;
import org.genfork.tools.network.IncomingPacket;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class RequestAccountUpdateInfoPacket extends IncomingPacket<DiscoveryServerHandler> {
	private String accountName;
	private String ipAddress;
	private int lastServerId;

	@Override
	public boolean read(DiscoveryServerHandler clientHandler) {
		accountName = readS();
		ipAddress = readS();
		lastServerId = readD();
		return true;
	}

	@Override
	public void run(DiscoveryServerHandler clientHandler) throws Exception {
		AuthAccountRepository.getInstance().updateAccount(accountName, null, null, ipAddress, lastServerId,
				null, null, null, null);
	}
}
