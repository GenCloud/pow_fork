package org.genfork.dataserver.network.enums;

import org.genfork.dataserver.network.clients.recv.auth.RequestAccountPacket;
import org.genfork.dataserver.network.clients.recv.auth.RequestAccountUpdateInfoPacket;
import org.genfork.dataserver.network.clients.recv.game.*;
import org.genfork.dataserver.network.discovery.DiscoveryServerHandler;
import org.genfork.tools.network.IConnectionState;
import org.genfork.tools.network.IIncomingPackets;
import org.genfork.tools.network.IncomingPacket;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Supplier;

import static org.genfork.dataserver.network.enums.ConnectionState.REGISTERED;
import static org.genfork.tools.network.opcodes.ClientsBranchOpcodes.*;

public enum DataServerIncomingPackets implements IIncomingPackets<DiscoveryServerHandler> {
	REQUEST_ACCOUNT_PACKET(ACCOUNT_AUTH_PACKET.getId1(), RequestAccountPacket::new, REGISTERED),
	REQUEST_ACCOUNT_UPDATE_INFO_PACKET(ACCOUNT_UPDATE_INFO_PACKET.getId1(), RequestAccountUpdateInfoPacket::new, REGISTERED),
	REQUEST_ACCOUNT_PLAYERS_INFO_PACKET(ACCOUNT_PLAYERS_INFO_PACKET.getId1(), RequestAccountPlayersInfoPacket::new, REGISTERED),

	REQUEST_PLAYER_CREATION_PACKET(PLAYER_CREATION_PACKET.getId1(), RequestPlayerCreationPacket::new, REGISTERED),

	REQUEST_PLAYER_CHECK_NAME_PACKET(PLAYER_CHECK_NAME_PACKET.getId1(), RequestPlayerCheckNamePacket::new, REGISTERED),

	REQUEST_PLAYER_GET_INVENTORY_DATA_PACKET(PLAYER_GET_INVENTORY_DATA_PACKET.getId1(), RequestPlayerGetInventoryDataPacket::new, REGISTERED),

	REQUEST_PLAYER_TRY_RECEIVE_INVENTORY_DATA_PACKET(PLAYER_TRY_RECEIVE_INVENTORY_DATA_PACKET.getId1(), RequestPlayerTryReceiveInventoryDataPacket::new, REGISTERED),
	;
	public static final DataServerIncomingPackets[] PACKET_ARRAY;

	static {
		final short maxPacketId = (short) Arrays.stream(values()).mapToInt(IIncomingPackets::getPacketId).max().orElse(0);
		PACKET_ARRAY = new DataServerIncomingPackets[maxPacketId + 1];
		for (DataServerIncomingPackets incomingPacket : values()) {
			PACKET_ARRAY[incomingPacket.getPacketId()] = incomingPacket;
		}
	}

	private short packetId;
	private Supplier<IncomingPacket<DiscoveryServerHandler>> incomingPacketFactory;
	private Set<IConnectionState> connectionStates;

	DataServerIncomingPackets(int packetId, Supplier<IncomingPacket<DiscoveryServerHandler>> incomingPacketFactory, IConnectionState... connectionStates) {
		// packetId is an unsigned byte
		if (packetId > 0xFF) {
			throw new IllegalArgumentException("packetId must not be bigger than 0xFF");
		}

		this.packetId = (short) packetId;
		this.incomingPacketFactory = incomingPacketFactory != null ? incomingPacketFactory : () -> null;
		this.connectionStates = new HashSet<>(Arrays.asList(connectionStates));
	}

	@Override
	public int getPacketId() {
		return packetId;
	}

	@Override
	public IncomingPacket<DiscoveryServerHandler> newIncomingPacket() {
		return incomingPacketFactory.get();
	}

	@Override
	public Set<IConnectionState> getConnectionStates() {
		return connectionStates;
	}

	@Override
	public IIncomingPackets<DiscoveryServerHandler>[] getArray() {
		return PACKET_ARRAY;
	}
}
