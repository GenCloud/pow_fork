package org.genfork.dataserver.network.clients.send.game;

import lombok.RequiredArgsConstructor;
import org.genfork.tools.data.StatsSet;
import org.genfork.tools.network.OutgoingPacket;

import java.util.List;

import static org.genfork.tools.network.opcodes.ClientsBranchOpcodes.RECEIVE_ACCOUNT_PLAYERS_RESULT_PACKET;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@RequiredArgsConstructor
public class AccountPlayersDataResultPacket extends OutgoingPacket {
	private final List<StatsSet> sets;
	private final boolean authentication;

	@Override
	public boolean write() {
		RECEIVE_ACCOUNT_PLAYERS_RESULT_PACKET.writeId(this);

		writeC(authentication ? 1 : 0);
		writeD(sets.size());

		sets.forEach(set -> set.writeToPacket(this));

		return true;
	}
}
