package org.genfork.dataserver.network.discovery;

import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;
import org.genfork.dataserver.config.DiscoveryClientConfig;
import org.genfork.tools.network.ChannelInboundHandler;
import org.genfork.tools.network.IncomingPacket;
import org.genfork.tools.network.OutgoingPacket;
import org.genfork.tools.registry.annotation.DiscoveryClient;

import static org.genfork.dataserver.network.enums.ConnectionState.CONNECTED;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Slf4j
@DiscoveryClient(config = DiscoveryClientConfig.class)
public class DiscoveryServerHandler extends ChannelInboundHandler<DiscoveryServerHandler> {
	@Override
	public void channelActive(ChannelHandlerContext ctx) {
		super.channelActive(ctx);

		setConnectionState(CONNECTED);
	}

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, IncomingPacket<DiscoveryServerHandler> packet) throws Exception {
		try {
			packet.run(this);
		} catch (Exception ex) {
			log.warn("Exception for: {} on packet.run: {}", toString(), packet.getClass().getSimpleName(), ex);
		}
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
		log.warn("Network exception caught for: {} : {}", toString(), cause.getLocalizedMessage());
	}

	public void closeNow() {
		if (channel != null) {
			channel.close();
		}
	}

	public void sendPacket(OutgoingPacket packet) {
		if (packet == null) {
			return;
		}

		channel.writeAndFlush(packet);
	}
}
