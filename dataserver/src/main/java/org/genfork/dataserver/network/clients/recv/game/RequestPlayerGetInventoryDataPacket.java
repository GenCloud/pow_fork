package org.genfork.dataserver.network.clients.recv.game;

import org.genfork.dataserver.data.game.repository.GamePlayerInventoryDataRepository;
import org.genfork.dataserver.network.clients.send.game.PlayerSendInventoryDataPacket;
import org.genfork.dataserver.network.discovery.DiscoveryServerHandler;
import org.genfork.tools.data.StatsSet;
import org.genfork.tools.network.IncomingPacket;

import java.util.List;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class RequestPlayerGetInventoryDataPacket extends IncomingPacket<DiscoveryServerHandler> {
	private int playerId;
	private boolean equippedItems;

	@Override
	public boolean read(DiscoveryServerHandler client) {
		playerId = readD();
		equippedItems = readC() == 1;
		return true;
	}

	@Override
	public void run(DiscoveryServerHandler client) throws Exception {
		final List<StatsSet> sets = GamePlayerInventoryDataRepository.getInstance().loadItems(playerId, equippedItems);
		client.sendPacket(this, new PlayerSendInventoryDataPacket(sets));
	}
}
