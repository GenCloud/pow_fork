package org.genfork.dataserver.network.enums;

import org.genfork.tools.network.IConnectionState;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public enum ConnectionState implements IConnectionState {
	CONNECTED,
	REGISTERED
}
