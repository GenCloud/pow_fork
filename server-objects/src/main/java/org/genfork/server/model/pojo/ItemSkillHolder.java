package org.genfork.server.model.pojo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.genfork.server.enums.ItemSkillType;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ItemSkillHolder extends SkillHolder {
	private ItemSkillType type;
	private int chance;
	private int value;

	public ItemSkillHolder() {
		super(0, 0, 0);
	}

	public ItemSkillHolder(int skillId, int skillLvl, int skillSubLevel, ItemSkillType type, int chance, int value) {
		super(skillId, skillLvl, skillSubLevel);
		this.type = type;
		this.chance = chance;
		this.value = value;
	}
}
