package org.genfork.server.model.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.genfork.server.enums.AttributeType;

import java.io.Serializable;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@AllArgsConstructor
@Data
public class AttributeHolder implements Serializable {
	private final AttributeType type;
	private int value;
}
