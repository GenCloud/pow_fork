package org.genfork.server.model.pojo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.genfork.tools.data.StatsSet;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@EqualsAndHashCode(callSuper = true)
public class ItemChanceHolder extends ItemHolder {
	@Getter
	private final double chance;

	public ItemChanceHolder(StatsSet params) {
		this(params.getInt("id"), params.getDouble("chance"), params.getLong("count"));
	}

	public ItemChanceHolder(int id, double chance) {
		this(id, chance, 1);
	}

	public ItemChanceHolder(int id, double chance, long count) {
		super(id, count);
		this.chance = chance;
	}
}
