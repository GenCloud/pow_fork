package org.genfork.server.model.pojo;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.io.Serializable;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@RequiredArgsConstructor
@Data
public class ItemHolder implements Serializable {
	private final int id;
	private final long count;
}
