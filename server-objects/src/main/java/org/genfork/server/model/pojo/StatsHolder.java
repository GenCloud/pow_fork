package org.genfork.server.model.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.genfork.server.enums.ActorStat;
import org.genfork.server.objects.Actor;

import java.io.Serializable;
import java.util.Objects;
import java.util.function.BiPredicate;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@RequiredArgsConstructor
@AllArgsConstructor
@Data
public class StatsHolder implements Serializable {
	private final ActorStat stat;
	private final double value;
	private BiPredicate<Actor, StatsHolder> condition;

	public boolean verifyCondition(Actor actor) {
		return condition == null || condition.test(actor, this);
	}

	public static StatsHolder of(ActorStat stat, double value) {
		return new StatsHolder(stat, value);
	}

	public static StatsHolder of(ActorStat stat, double value, BiPredicate<Actor, StatsHolder> condition) {
		return new StatsHolder(stat, value, condition);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		StatsHolder that = (StatsHolder) o;
		return Double.compare(that.value, value) == 0 &&
				stat == that.stat;
	}

	@Override
	public int hashCode() {
		return Objects.hash(stat, value);
	}
}
