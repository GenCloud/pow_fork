package org.genfork.server.model.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CapsuleItemHolder {
	private int id;
	private int min;
	private int max;
	private double chance;
	private int minEnchant;
	private int maxEnchant;

	public double getChance() {
		return chance * 1000;
	}
}
