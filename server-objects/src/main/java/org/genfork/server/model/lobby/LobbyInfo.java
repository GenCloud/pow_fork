package org.genfork.server.model.lobby;

import lombok.Data;
import org.genfork.server.enums.LocationData;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.genfork.server.objects.component.inventory.InventoryConstants.PAPERDOLL_TOTALSLOTS;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Data
public class LobbyInfo implements Serializable {
	private final List<InventoryItemInfo> itemInfos = new ArrayList<>();
	private int accessLevel;
	private int lobbySlot;
	private int playerLevel;
	private int playerId;
	private int race;
	private int classId;
	private int baseClassId;
	private int maxCp;
	private int currentCp;
	private int maxHp;
	private int currentHp;
	private int maxMp;
	private int currentMp;
	private int face;
	private int hairStyle;
	private int hairColor;
	private int sex;
	private int x;
	private int y;
	private int z;
	private int h;

	private int clanId;
	private int raidBossPoints;
	private int fame;
	private int reputation;
	private int recommendationsHave;
	private int recommendationsLeft;

	private int pvp;
	private int pk;
	private int titleColor;
	private int online;
	private int onlineTime;
	private int transformId;
	private int canCraft;
	private int nobless;
	private int vitalityPoints;
	private long createdDate;
	private long currentSp;
	private long currentExp;
	private long expBeforeDeath;
	private long lastAccess;
	private long deleteTime;
	private String title;
	private String playerName;
	private String accountName;
	private int[][] paperdoll;

	public int getPaperdollObjectId(int slot) {
		if (paperdoll == null || paperdoll.length == 0) {
			paperdoll = restoreVisibleInventory();
		}

		return paperdoll[slot][0];
	}

	public int getPaperdollItemId(int slot) {
		if (paperdoll == null || paperdoll.length == 0) {
			paperdoll = restoreVisibleInventory();
		}

		return paperdoll[slot][1];
	}

	public int getPaperdollEnchantLevel(int slot) {
		if (paperdoll == null || paperdoll.length == 0) {
			paperdoll = restoreVisibleInventory();
		}

		return paperdoll[slot][2];
	}

	public int getPaperdollVariationOptionFirst(int slot) {
		if (paperdoll == null || paperdoll.length == 0) {
			paperdoll = restoreVisibleInventory();
		}

		return paperdoll[slot][4];
	}

	public int getPaperdollVariationOptionSecond(int slot) {
		if (paperdoll == null || paperdoll.length == 0) {
			paperdoll = restoreVisibleInventory();
		}

		return paperdoll[slot][5];
	}

	public int getPaperdollItemVisualId(int slot) {
		if (paperdoll == null || paperdoll.length == 0) {
			paperdoll = restoreVisibleInventory();
		}

		return paperdoll[slot][3];
	}

	private int[][] restoreVisibleInventory() {
		final List<InventoryItemInfo> collect = itemInfos
				.stream()
				.filter(i ->
						i.getLocationData() == LocationData.PAPER_DOLL)
				.collect(Collectors.toList());

		final int[][] paperdoll = new int[PAPERDOLL_TOTALSLOTS][6];

		collect.forEach(i -> {
			final int slot = i.getLocationMask();
			paperdoll[slot][0] = i.getItemObjId();
			paperdoll[slot][1] = i.getItemId();
			paperdoll[slot][2] = i.getEnchant();
			paperdoll[slot][3] = 0; // TODO: visual id
			paperdoll[slot][4] = i.getOptionsFirst() != null && i.getOptionsFirst().length > 0 ? i.getOptionsFirst()[0] : 0;
			paperdoll[slot][5] = i.getOptionsSecond() != null && i.getOptionsSecond().length > 0 ? i.getOptionsSecond()[0] : 0;
		});

		return paperdoll;
	}
}
