package org.genfork.server.model.skills;

import lombok.Data;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Data
public class Skill {
	private int skillId;
}
