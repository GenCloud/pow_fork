package org.genfork.server.model.lobby;

import lombok.Data;
import org.genfork.server.enums.LocationData;
import org.genfork.server.objects.Item;

import java.io.Serializable;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Data
public class InventoryItemInfo implements Serializable {
	private int itemObjId;
	private int itemPlayerId;
	private int itemId;
	private int enchant;
	private int typeFirst;
	private int typeSecond;
	private int locationMask;
	private int useTime;
	private int reuseDelay;

	private long bodyPart;
	private long amount;
	private long sysTime;

	private float mpLeft;
	private float lifeTime;

	private LocationData locationData;

	private int[] elementTypes;
	private int[] elementValues;

	private int[] abilityTypes;
	private int[] abilityIds;
	private int[] abilitySlots;

	private int[] mineralIds;
	private int[] optionsFirst;
	private int[] optionsSecond;

	//	TODO
	public Item mapItem() {
		final Item item = new Item();
		item.setItemId(itemId);
		item.setObjectId(itemObjId);

		item.setAmount(amount);
		item.setEnchant(enchant);
		item.setTypeFirst(typeFirst);
		item.setTypeSecond(typeSecond);
		item.setLocationMask(locationMask);
		item.setLocationData(locationData);
		item.setMpLeft((int) mpLeft);
		return item;
	}
}
