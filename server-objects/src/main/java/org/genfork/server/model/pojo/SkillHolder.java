package org.genfork.server.model.pojo;

import lombok.Data;

import java.io.Serializable;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Data
public class SkillHolder implements Serializable {
	private int skillId;
	private int skillLevel;
	private int skillSubLevel;

	public SkillHolder(int skillId, int skillLevel) {
		this.skillId = skillId;
		this.skillLevel = skillLevel;
		skillSubLevel = 0;
	}

	public SkillHolder(int skillId, int skillLevel, int skillSubLevel) {
		this.skillId = skillId;
		this.skillLevel = skillLevel;
		this.skillSubLevel = skillSubLevel;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = (prime * result) + skillId;
		result = (prime * result) + skillLevel;
		result = (prime * result) + skillSubLevel;
		return result;
	}
}
