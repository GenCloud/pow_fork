package org.genfork.server.model.world;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@RequiredArgsConstructor
@Data
@Slf4j
public class WorldRegion implements Serializable {
	private final short regionX;
	private final short regionY;
	private final short regionZ;

//	private boolean active;
//
//	public void setActive(boolean active) {
//		if (this.active == active) {
//			return;
//		}
//
//		this.active = active;
//
//		if (log.isDebugEnabled()) {
//			log.debug("{} Grid {}", active ? "Starting" : "Stopping", this);
//		}
//	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		WorldRegion that = (WorldRegion) o;
		return regionX == that.regionX &&
				regionY == that.regionY &&
				regionZ == that.regionZ;// &&
//				active == that.active;
	}

	@Override
	public int hashCode() {
		return Objects.hash(regionX, regionY, regionZ/*, active*/);
	}
}
