package org.genfork.server.model.pojo;

import lombok.Data;
import org.genfork.server.enums.AutoUseType;

/**
 * @author: ngolubenko@context-it.ru
 * @created: 2020/01
 */
@Data
public class AutoUseHolder {
	private int skillOption;
	private int id;
	private AutoUseType autoUseType;
}
