package org.genfork.server.enums;

import org.apache.commons.lang3.ArrayUtils;
import org.genfork.server.dispatcher.events.OnPlayerEnterWorld;
import org.genfork.server.dispatcher.interfaces.Event;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public enum EventType {
	ON_PLAYER_ENTER_WORLD(OnPlayerEnterWorld.class, void.class),

	;
	private final Class<? extends Event> eventClass;
	private final Class<?>[] returnClass;

	EventType(Class<? extends Event> eventClass, Class<?>... returnClass) {
		this.eventClass = eventClass;
		this.returnClass = returnClass;
	}

	public boolean isEventClass(Class<?> eventClass) {
		return this.eventClass == eventClass;
	}

	public boolean isReturnClass(Class<?> clazz) {
		return ArrayUtils.contains(returnClass, clazz);
	}
}
