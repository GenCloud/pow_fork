package org.genfork.server.enums;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public enum ArmorType implements ItemType {
	NONE,
	LIGHT,
	HEAVY,
	MAGIC,
	SIGIL,
	SHIELD;

	@Override
	public int mask() {
		return 1 << (ordinal() + WeaponType.values().length);
	}
}
