package org.genfork.server.enums;

import java.util.Arrays;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public enum Race {
	HUMAN,
	ELF,
	DARK_ELF,
	ORC,
	DWARF,
	KAMAEL,
	ERTHEIA;

	public static Race byOrdinal(int id) {
		return Arrays.stream(values()).filter(race -> race.ordinal() == id).findFirst().orElse(null);
	}
}
