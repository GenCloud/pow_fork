package org.genfork.server.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@RequiredArgsConstructor
public enum MaterialType {
	OTHER(0),
	FISH(1),
	MITHRIL(2),
	GOLD(3),
	SILVER(4),
	BRONZE(6),
	STEEL(8),
	WOOD(13),
	BONE(14),
	CLOTH(17),
	PAPER(18),
	LEATHER(19),
	CRYSTAL(23),
	COTTON(33),
	COBWEB(37),
	DYESTUFF(38),
	SCALE_OF_DRAGON(46),
	ADAMANTAITE(47),
	BLOOD_STEEL(48),
	CHRYSOLITE(49),
	DAMASCUS(50),
	FINE_STEEL(51),
	HORN(52),
	LIQUID(53);

	@Getter
	private final int clientId;

	public static MaterialType byClientId(int ordinal) {
		return Arrays.stream(values()).filter(type -> type.getClientId() == ordinal).findFirst().orElse(null);
	}
}
