package org.genfork.server.enums;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public enum ActionType {
	action_none,
	action_calc,
	action_call_skill,
	action_capsule,
	action_create_mpcc,
	action_dice,
	action_equip,
	action_fishingshot,
	action_harvest,
	action_hide_name,
	action_keep_exp,
	action_nick_color,
	action_peel,
	action_recipe,
	action_seed,
	action_show_adventurer_guide_book,
	action_show_html,
	action_show_ssq_status,
	action_skill_maintain,
	action_skill_reduce,
	action_soulshot,
	action_spiritshot,
	action_start_quest,
	action_summon_soulshot,
	action_summon_spiritshot,
	action_xmas_open,
	action_skill_reduce_on_skill_success,
	action_show_tutorial
}
