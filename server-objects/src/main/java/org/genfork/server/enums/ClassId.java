package org.genfork.server.enums;

import lombok.Getter;

import java.util.*;

import static org.genfork.server.enums.Race.*;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public enum ClassId {
	FIGHTER(0, false, HUMAN, null),

	WARRIOR(1, false, HUMAN, FIGHTER),
	GLADIATOR(2, false, HUMAN, WARRIOR),
	WARLORD(3, false, HUMAN, WARRIOR),
	KNIGHT(4, false, HUMAN, FIGHTER),
	PALADIN(5, false, HUMAN, KNIGHT),
	DARK_AVENGER(6, false, HUMAN, KNIGHT),
	ROGUE(7, false, HUMAN, FIGHTER),
	TREASURE_HUNTER(8, false, HUMAN, ROGUE),
	HAWKEYE(9, false, HUMAN, ROGUE),

	MAGE(10, true, HUMAN, null),
	WIZARD(11, true, HUMAN, MAGE),
	SORCERER(12, true, HUMAN, WIZARD),
	NECROMANCER(13, true, HUMAN, WIZARD),
	WARLOCK(14, true, true, HUMAN, WIZARD),
	CLERIC(15, true, HUMAN, MAGE),
	BISHOP(16, true, HUMAN, CLERIC),
	PROPHET(17, true, HUMAN, CLERIC),

	ELVEN_FIGHTER(18, false, ELF, null),
	ELVEN_KNIGHT(19, false, ELF, ELVEN_FIGHTER),
	TEMPLE_KNIGHT(20, false, ELF, ELVEN_KNIGHT),
	SWORDSINGER(21, false, ELF, ELVEN_KNIGHT),
	ELVEN_SCOUT(22, false, ELF, ELVEN_FIGHTER),
	PLAINS_WALKER(23, false, ELF, ELVEN_SCOUT),
	SILVER_RANGER(24, false, ELF, ELVEN_SCOUT),

	ELVEN_MAGE(25, true, ELF, null),
	ELVEN_WIZARD(26, true, ELF, ELVEN_MAGE),
	SPELLSINGER(27, true, ELF, ELVEN_WIZARD),
	ELEMENTAL_SUMMONER(28, true, true, ELF, ELVEN_WIZARD),
	ORACLE(29, true, ELF, ELVEN_MAGE),
	ELDER(30, true, ELF, ORACLE),

	DARK_FIGHTER(31, false, DARK_ELF, null),
	PALUS_KNIGHT(32, false, DARK_ELF, DARK_FIGHTER),
	SHILLIEN_KNIGHT(33, false, DARK_ELF, PALUS_KNIGHT),
	BLADEDANCER(34, false, DARK_ELF, PALUS_KNIGHT),
	ASSASSIN(35, false, DARK_ELF, DARK_FIGHTER),
	ABYSS_WALKER(36, false, DARK_ELF, ASSASSIN),
	PHANTOM_RANGER(37, false, DARK_ELF, ASSASSIN),

	DARK_MAGE(38, true, DARK_ELF, null),
	DARK_WIZARD(39, true, DARK_ELF, DARK_MAGE),
	SPELLHOWLER(40, true, DARK_ELF, DARK_WIZARD),
	PHANTOM_SUMMONER(41, true, true, DARK_ELF, DARK_WIZARD),
	SHILLIEN_ORACLE(42, true, DARK_ELF, DARK_MAGE),
	SHILLIEN_ELDER(43, true, DARK_ELF, SHILLIEN_ORACLE),

	ORC_FIGHTER(44, false, ORC, null),
	ORC_RAIDER(45, false, ORC, ORC_FIGHTER),
	DESTROYER(46, false, ORC, ORC_RAIDER),
	ORC_MONK(47, false, ORC, ORC_FIGHTER),
	TYRANT(48, false, ORC, ORC_MONK),

	ORC_MAGE(49, true, ORC, null),
	ORC_SHAMAN(50, true, ORC, ORC_MAGE),
	OVERLORD(51, true, ORC, ORC_SHAMAN),
	WARCRYER(52, true, ORC, ORC_SHAMAN),

	DWARVEN_FIGHTER(53, false, DWARF, null),
	SCAVENGER(54, false, DWARF, DWARVEN_FIGHTER),
	BOUNTY_HUNTER(55, false, DWARF, SCAVENGER),
	ARTISAN(56, false, DWARF, DWARVEN_FIGHTER),
	WARSMITH(57, false, DWARF, ARTISAN),

	DUMMY_1(58, false, null, null),
	DUMMY_2(59, false, null, null),
	DUMMY_3(60, false, null, null),
	DUMMY_4(61, false, null, null),
	DUMMY_5(62, false, null, null),
	DUMMY_6(63, false, null, null),
	DUMMY_7(64, false, null, null),
	DUMMY_8(65, false, null, null),
	DUMMY_9(66, false, null, null),
	DUMMY_10(67, false, null, null),
	DUMMY_11(68, false, null, null),
	DUMMY_12(69, false, null, null),
	DUMMY_13(70, false, null, null),
	DUMMY_14(71, false, null, null),
	DUMMY_15(72, false, null, null),
	DUMMY_16(73, false, null, null),
	DUMMY_17(74, false, null, null),
	DUMMY_18(75, false, null, null),
	DUMMY_19(76, false, null, null),
	DUMMY_20(77, false, null, null),
	DUMMY_21(78, false, null, null),
	DUMMY_22(79, false, null, null),
	DUMMY_23(80, false, null, null),
	DUMMY_24(81, false, null, null),
	DUMMY_25(82, false, null, null),
	DUMMY_26(83, false, null, null),
	DUMMY_27(84, false, null, null),
	DUMMY_28(85, false, null, null),
	DUMMY_29(86, false, null, null),
	DUMMY_30(87, false, null, null),

	DUELIST(88, false, HUMAN, GLADIATOR),
	DREADNOUGHT(89, false, HUMAN, WARLORD),
	PHOENIX_KNIGHT(90, false, HUMAN, PALADIN),
	HELL_KNIGHT(91, false, HUMAN, DARK_AVENGER),
	SAGITTARIUS(92, false, HUMAN, HAWKEYE),
	ADVENTURER(93, false, HUMAN, TREASURE_HUNTER),
	ARCHMAGE(94, true, HUMAN, SORCERER),
	SOULTAKER(95, true, HUMAN, NECROMANCER),
	ARCANA_LORD(96, true, true, HUMAN, WARLOCK),
	CARDINAL(97, true, HUMAN, BISHOP),
	HIEROPHANT(98, true, HUMAN, PROPHET),

	EVA_TEMPLAR(99, false, ELF, TEMPLE_KNIGHT),
	SWORD_MUSE(100, false, ELF, SWORDSINGER),
	WIND_RIDER(101, false, ELF, PLAINS_WALKER),
	MOONLIGHT_SENTINEL(102, false, ELF, SILVER_RANGER),
	MYSTIC_MUSE(103, true, ELF, SPELLSINGER),
	ELEMENTAL_MASTER(104, true, true, ELF, ELEMENTAL_SUMMONER),
	EVA_SAINT(105, true, ELF, ELDER),

	SHILLIEN_TEMPLAR(106, false, DARK_ELF, SHILLIEN_KNIGHT),
	SPECTRAL_DANCER(107, false, DARK_ELF, BLADEDANCER),
	GHOST_HUNTER(108, false, DARK_ELF, ABYSS_WALKER),
	GHOST_SENTINEL(109, false, DARK_ELF, PHANTOM_RANGER),
	STORM_SCREAMER(110, true, DARK_ELF, SPELLHOWLER),
	SPECTRAL_MASTER(111, true, true, DARK_ELF, PHANTOM_SUMMONER),
	SHILLIEN_SAINT(112, true, DARK_ELF, SHILLIEN_ELDER),

	TITAN(113, false, ORC, DESTROYER),
	GRAND_KHAVATARI(114, false, ORC, TYRANT),
	DOMINATOR(115, true, ORC, OVERLORD),
	DOOMCRYER(116, true, ORC, WARCRYER),

	FORTUNE_SEEKER(117, false, DWARF, BOUNTY_HUNTER),
	MAESTRO(118, false, DWARF, WARSMITH),

	DUMMY_31(119, false, null, null),
	DUMMY_32(120, false, null, null),
	DUMMY_33(121, false, null, null),
	DUMMY_34(122, false, null, null),

	MALE_SOLDIER(123, false, KAMAEL, null),
	FEMALE_SOLDIER(124, false, KAMAEL, null),
	TROOPER(125, false, KAMAEL, MALE_SOLDIER),
	WARDER(126, false, KAMAEL, FEMALE_SOLDIER),
	BERSERKER(127, false, KAMAEL, TROOPER),
	MALE_SOULBREAKER(128, false, KAMAEL, TROOPER),
	FEMALE_SOULBREAKER(129, false, KAMAEL, WARDER),
	ARBALESTER(130, false, KAMAEL, WARDER),
	DOOMBRINGER(131, false, KAMAEL, BERSERKER),
	MALE_SOUL_HOUND(132, false, KAMAEL, MALE_SOULBREAKER),
	FEMALE_SOUL_HOUND(133, false, KAMAEL, FEMALE_SOULBREAKER),
	TRICKSTER(134, false, KAMAEL, ARBALESTER),
	INSPECTOR(135, false, KAMAEL, WARDER),
	JUDICATOR(136, false, KAMAEL, INSPECTOR),

	DUMMY_35(137, false, null, null),
	DUMMY_36(138, false, null, null),

	SIGEL_KNIGHT(139, false, null, null),
	TYRR_WARRIOR(140, false, null, null),
	OTHELL_ROGUE(141, false, null, null),
	YUL_ARCHER(142, false, null, null),
	FEOH_WIZARD(143, false, null, null),
	ISS_ENCHANTER(144, false, null, null),
	WYNN_SUMMONER(145, false, null, null),
	AEORE_HEALER(146, false, null, null),

	DUMMY_37(147, false, null, null),

	SIGEL_PHOENIX_KNIGHT(148, false, HUMAN, PHOENIX_KNIGHT),
	SIGEL_HELL_KNIGHT(149, false, HUMAN, HELL_KNIGHT),
	SIGEL_EVA_TEMPLAR(150, false, ELF, EVA_TEMPLAR),
	SIGEL_SHILLIEN_TEMPLAR(151, false, DARK_ELF, SHILLIEN_TEMPLAR),
	TYRR_DUELIST(152, false, HUMAN, DUELIST),
	TYRR_DREADNOUGHT(153, false, HUMAN, DREADNOUGHT),
	TYRR_TITAN(154, false, ORC, TITAN),
	TYRR_GRAND_KHAVATARI(155, false, ORC, GRAND_KHAVATARI),
	TYRR_MAESTRO(156, false, DWARF, MAESTRO),
	TYRR_DOOMBRINGER(157, false, KAMAEL, DOOMBRINGER),
	OTHELL_ADVENTURER(158, false, HUMAN, ADVENTURER),
	OTHELL_WIND_RIDER(159, false, ELF, WIND_RIDER),
	OTHELL_GHOST_HUNTER(160, false, DARK_ELF, GHOST_HUNTER),
	OTHELL_FORTUNE_SEEKER(161, false, DWARF, FORTUNE_SEEKER),
	YUL_SAGITTARIUS(162, false, HUMAN, SAGITTARIUS),
	YUL_MOONLIGHT_SENTINEL(163, false, ELF, MOONLIGHT_SENTINEL),
	YUL_GHOST_SENTINEL(164, false, DARK_ELF, GHOST_SENTINEL),
	YUL_TRICKSTER(165, false, KAMAEL, TRICKSTER),
	FEOH_ARCHMAGE(166, true, HUMAN, ARCHMAGE),
	FEOH_SOULTAKER(167, true, HUMAN, SOULTAKER),
	FEOH_MYSTIC_MUSE(168, true, ELF, MYSTIC_MUSE),
	FEOH_STORM_SCREAMER(169, true, DARK_ELF, STORM_SCREAMER),
	FEOH_SOUL_HOUND(170, true, KAMAEL, MALE_SOUL_HOUND), // fix me ?
	ISS_HIEROPHANT(171, true, HUMAN, HIEROPHANT),
	ISS_SWORD_MUSE(172, false, ELF, SWORD_MUSE),
	ISS_SPECTRAL_DANCER(173, false, DARK_ELF, SPECTRAL_DANCER),
	ISS_DOMINATOR(174, true, ORC, DOMINATOR),
	ISS_DOOMCRYER(175, true, ORC, DOOMCRYER),
	WYNN_ARCANA_LORD(176, true, true, HUMAN, ARCANA_LORD),
	WYNN_ELEMENTAL_MASTER(177, true, true, ELF, ELEMENTAL_MASTER),
	WYNN_SPECTRAL_MASTER(178, true, true, DARK_ELF, SPECTRAL_MASTER),
	AEORE_CARDINAL(179, true, HUMAN, CARDINAL),
	AEORE_EVA_SAINT(180, true, ELF, EVA_SAINT),
	AEORE_SHILLIEN_SAINT(181, true, DARK_ELF, SHILLIEN_SAINT),

	ERTHEIA_FIGHTER(182, false, ERTHEIA, null),
	ERTHEIA_WIZARD(183, true, ERTHEIA, null),

	MARAUDER(184, false, ERTHEIA, ERTHEIA_FIGHTER),
	CLOUD_BREAKER(185, true, ERTHEIA, ERTHEIA_WIZARD),

	RIPPER(186, false, ERTHEIA, MARAUDER),
	STRATOMANCER(187, true, ERTHEIA, CLOUD_BREAKER),

	EVISCERATOR(188, false, ERTHEIA, RIPPER),
	SAYHA_SEER(189, true, ERTHEIA, STRATOMANCER);

	private static Map<Integer, ClassId> classIdMap = new HashMap<>(ClassId.values().length);

	static {
		Arrays.stream(ClassId.values()).forEach(classId -> classIdMap.put(classId.getId(), classId));
	}

	/**
	 * The Identifier of the Class
	 */
	@Getter
	private final int id;
	/**
	 * True if the class is a mage class
	 */
	@Getter
	private final boolean magician;
	/**
	 * True if the class is a summoner class
	 */
	@Getter
	private final boolean summoner;
	/**
	 * The Race object of the class
	 */
	@Getter
	private final Race race;
	/**
	 * The parent ClassId or null if this class is a root
	 */
	private final ClassId parent;
	/**
	 * List of available Class for next transfer
	 **/
	@Getter
	private final Set<ClassId> nextClassIds = new HashSet<>(1);

	/**
	 * Class constructor.
	 *
	 * @param id       the class Id.
	 * @param magician {code true} if the class is mage class.
	 * @param race     the race related to the class.
	 * @param parent   the parent class Id.
	 */
	ClassId(int id, boolean magician, Race race, ClassId parent) {
		this.id = id;
		this.magician = magician;
		this.race = race;
		this.parent = parent;

		summoner = false;

		if (this.parent != null) {
			this.parent.addNextClassId(this);
		}
	}

	/**
	 * Class constructor.
	 *
	 * @param id       the class Id.
	 * @param magician {code true} if the class is mage class.
	 * @param summoner {code true} if the class is summoner class.
	 * @param race     the race related to the class.
	 * @param parent   the parent class Id.
	 */
	ClassId(int id, boolean magician, boolean summoner, Race race, ClassId parent) {
		this.id = id;
		this.magician = magician;
		this.summoner = summoner;
		this.race = race;
		this.parent = parent;

		if (this.parent != null) {
			this.parent.addNextClassId(this);
		}
	}

	public static ClassId getClassId(int classId) {
		return classIdMap.get(classId);
	}

	/**
	 * @param classId the parent ClassId to check.
	 * @return {code true} if this Class is a child of the selected ClassId.
	 */
	public boolean childOf(ClassId classId) {
		if (parent == null) {
			return false;
		}

		if (parent == classId) {
			return true;
		}

		return parent.childOf(classId);
	}

	/**
	 * @param classId the parent ClassId to check.
	 * @return {code true} if this Class is equal to the selected ClassId or a child of the selected ClassId.
	 */
	public boolean equalsOrChildOf(ClassId classId) {
		return this == classId || childOf(classId);
	}

	/**
	 * @return the child level of this Class (0=root, 1=child leve 1...)
	 */
	public int level() {
		if (parent == null) {
			return 0;
		}

		return 1 + parent.level();
	}

	public ClassId getRootClassId() {
		if (parent != null) {
			return parent.getRootClassId();
		}
		return this;
	}

	private void addNextClassId(ClassId cId) {
		nextClassIds.add(cId);
	}
}
