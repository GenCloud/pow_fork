package org.genfork.server.enums;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public enum MoveType {
	WALKING,
	RUNNING,
	SITTING,
	STANDING
}
