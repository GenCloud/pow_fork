package org.genfork.server.enums;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public enum ItemSkillType {
	NORMAL,
	ON_ENCHANT,
	ON_EQUIP,
	ON_UNEQUIP,
	ON_CRITICAL_SKILL,
	ON_MAGIC_SKILL
}
