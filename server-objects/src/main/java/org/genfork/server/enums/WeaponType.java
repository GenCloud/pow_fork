package org.genfork.server.enums;

import lombok.Getter;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public enum WeaponType implements ItemType {
	NONE(TraitType.NONE),
	SWORD(TraitType.SWORD),
	BLUNT(TraitType.BLUNT),
	DAGGER(TraitType.DAGGER),
	POLE(TraitType.POLE),
	FIST(TraitType.FIST),
	BOW(TraitType.BOW),
	ETC(TraitType.ETC),
	DUAL(TraitType.DUAL),
	DUALFIST(TraitType.DUALFIST),
	FISHINGROD(TraitType.NONE),
	RAPIER(TraitType.RAPIER),
	CROSSBOW(TraitType.CROSSBOW),
	ANCIENTSWORD(TraitType.ANCIENTSWORD),
	DUALDAGGER(TraitType.DUALDAGGER),
	TWOHANDCROSSBOW(TraitType.TWOHANDCROSSBOW),
	DUALBLUNT(TraitType.DUALBLUNT);

	private final int mask;
	@Getter
	private final TraitType traitType;

	WeaponType(TraitType traitType) {
		this.traitType = traitType;
		mask = 1 << ordinal();
	}

	/**
	 * @return the ID of the item after applying the mask.
	 */
	@Override
	public int mask() {
		return mask;
	}

	public boolean isRanged() {
		return this == BOW || this == CROSSBOW || this == TWOHANDCROSSBOW;
	}

	public boolean isCrossbow() {
		return this == CROSSBOW || this == TWOHANDCROSSBOW;
	}

	public boolean isDual() {
		return this == DUALFIST || this == DUAL || this == DUALDAGGER || this == DUALBLUNT;
	}
}
