package org.genfork.server.enums;

import lombok.Getter;

import java.util.Arrays;

/**
 * @author: ngolubenko@context-it.ru
 * @created: 2020/01
 */
public enum AutoUseType {
	DISABLED(0),
	ITEM_SKILL(1),
	POTION(2);

	@Getter
	private final int clientId;

	AutoUseType(int clientId) {
		this.clientId = clientId;
	}

	public static AutoUseType ofClientId(int clientId) {
		return Arrays.stream(AutoUseType.values())
				.filter(autoUseType -> autoUseType.clientId == clientId)
				.findFirst()
				.orElse(null);
	}
}
