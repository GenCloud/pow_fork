package org.genfork.server.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@RequiredArgsConstructor
//TODO: move to xml
public enum CrystalType {
	NONE(0, 0, 0, 0),
	D(1, 1458, 11, 90),
	C(2, 1459, 6, 45),
	B(3, 1460, 11, 67),
	A(4, 1461, 20, 145),
	S(5, 1462, 25, 250),
	S80(6, 1462, 25, 250),
	S84(7, 1462, 25, 250),
	R(8, 17371, 30, 500),
	R95(9, 17371, 30, 500),
	R99(10, 17371, 30, 500),
	R110(11, 17371, 30, 500),
	EVENT(12, 0, 0, 0);

	@Getter
	private final int level;
	@Getter
	private final int crystalId;
	@Getter
	private final int crystalEnchantBonusArmor;
	@Getter
	private final int crystalEnchantBonusWeapon;
}
