package org.genfork.server.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author: ngolubenko@context-it.ru
 * @created: 2020/04
 */
@RequiredArgsConstructor
public enum InventoryBlockType {
	NONE(-1),
	BLACKLIST(0),
	WHITELIST(1);

	@Getter
	private final int clientId;
}
