package org.genfork.server.enums;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public enum LocationData {
	VOID,
	INVENTORY,
	PAPER_DOLL,
	WAREHOUSE,
	CLAN_WAREHOUSE,
	PET,
	PET_EQUIP,
	LEASE,
	REFUND,
	MAIL,
	FREIGHT,
	COMMISSION
}
