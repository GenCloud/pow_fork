package org.genfork.server.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@RequiredArgsConstructor
public enum StatFunction {
	ADD("Add", 30),
	DIV("Div", 20),
	ENCHANT("Enchant", 0),
	ENCHANTHP("EnchantHp", 40),
	MUL("Mul", 20),
	SET("Set", 0),
	SUB("Sub", 30);

	@Getter
	private final String name;
	@Getter
	private final int order;
}
