package org.genfork.server.enums;

import lombok.Getter;

import java.util.Arrays;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public enum AttributeType {
	NONE(-2),
	FIRE(0),
	WATER(1),
	WIND(2),
	EARTH(3),
	HOLY(4),
	DARK(5);

	public static final AttributeType[] ATTRIBUTE_TYPES =
			{
					FIRE,
					WATER,
					WIND,
					EARTH,
					HOLY,
					DARK
			};

	@Getter
	private final byte clientId;

	AttributeType(int clientId) {
		this.clientId = (byte) clientId;
	}

	/**
	 * Finds an attribute type by its name.
	 *
	 * @param attributeName the attribute name
	 * @return An {@code AttributeType} if attribute type was found, {@code null} otherwise
	 */
	public static AttributeType findByName(String attributeName) {
		return Arrays.stream(values()).filter(attributeType -> attributeType.name().equalsIgnoreCase(attributeName)).findFirst().orElse(null);
	}

	/**
	 * Finds an attribute type by its client id.
	 *
	 * @param clientId the client id
	 * @return An {@code AttributeType} if attribute type was found, {@code null} otherwise
	 */
	public static AttributeType findByClientId(int clientId) {
		return Arrays.stream(values()).filter(attributeType -> attributeType.getClientId() == clientId).findFirst().orElse(null);
	}

	/**
	 * Gets the opposite.
	 *
	 * @return the opposite
	 */
	public AttributeType getOpposite() {
		return ATTRIBUTE_TYPES[((getClientId() % 2) == 0) ? (getClientId() + 1) : (getClientId() - 1)];
	}
}
