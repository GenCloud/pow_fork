package org.genfork.server.enums;

import lombok.Getter;
import org.genfork.server.objects.Actor;
import org.genfork.server.stats.IStatCalculator;
import org.genfork.server.stats.RegenHpRateStatCalculator;
import org.genfork.server.stats.finalizers.*;
import org.genfork.tools.common.MathUtil;

import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.function.DoubleBinaryOperator;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public enum ActorStat {
	// HP, MP & CP
	MAX_HP("max_hp", new MaxHpStatCalculator()),
	MAX_MP("max_mp", new MaxMpStatCalculator()),
	MAX_CP("max_cp", new MaxCpStatCalculator()),

	MAX_RECOVERABLE_HP("max_recoverable_hp"), // The maximum HP that is able to be recovered trough heals
	MAX_RECOVERABLE_MP("max_recoverable_mp"),
	MAX_RECOVERABLE_CP("maxR_recoverable_cp"),

	REGENERATE_HP_RATE("regenerate_hp_rate", new RegenHpRateStatCalculator()),
	REGENERATE_CP_RATE("regenerate_cp_rate", new RegenCpRateStatCalculator()),
	REGENERATE_MP_RATE("regenerate_mp_rate", new RegenMpRateStatCalculator()),

	ADDITIONAL_POTION_HP("additional_potion_hp"),
	ADDITIONAL_POTION_MP("additional_potion_mp"),
	ADDITIONAL_POTION_CP("additional_potion_cp"),

	MP_CHARGE("mp_charge"),

	HEAL_EFFECT("heal_effect"),
	HEAL_EFFECT_ADD("heal_effect_add"),

	// ATTACK & DEFENCE
	PHYSICAL_DEFENCE("physical_defence", new PysicalDefenseStatCalculator()),
	MAGICAL_DEFENCE("magical_defence", new MagicalDefenseStatCalculator()),
	PHYSICAL_ATTACK("physical_attack", new PhysicalAttackStatCalculator()),
	MAGIC_ATTACK("magical_attack", new MagicalAttackStatCalculator()),
	MAGIC_ATTACK_ADD("magical_attack_add"),
	PHYSICAL_ATTACK_SPEED("physical_attack_speed", new PhysicalAttackSpeedStatCalculator()),
	MAGIC_ATTACK_SPEED("magical_attack_speed", new MagicalAttackSpeedStatCalculator()), // Magic Skill Casting Time Rate
	ATK_REUSE("attack_reuse"), // Bows Hits Reuse Rate
	SHIELD_DEFENCE("shield_defense", new ShieldDefenceStatCalculator()),
	CRITICAL_DAMAGE("critical_damage"),
	CRITICAL_DAMAGE_ADD("critical_damage_add"), // this is another type for special critical damage mods - vicious stance, critical power and critical damage SA
	HATE_ATTACK("hate_attack"),

	// PVP BONUS
	PVP_PHYSICAL_ATTACK_DAMAGE("pvp_physical_attack_damage"),

	PVP_MAGICAL_SKILL_DAMAGE("pvp_magical_skill_damage"),
	PVP_PHYSICAL_SKILL_DAMAGE("pvp_physical_skill_damage"),

	PVP_PHYSICAL_ATTACK_DEFENCE("pvp_physical_attack_defence"),

	PVP_MAGICAL_SKILL_DEFENCE("pvp_magical_skill_defence"),
	PVP_PHYSICAL_SKILL_DEFENCE("pvp_physical_skill_defence"),

	// PVE BONUS
	PVE_PHYSICAL_ATTACK_DAMAGE("pve_physical_attack_damage"),

	PVE_PHYSICAL_SKILL_DAMAGE("pve_physical_skill_damage"),
	PVE_MAGICAL_SKILL_DAMAGE("pve_magical_skill_damage"),

	PVE_PHYSICAL_ATTACK_DEFENCE("pve_physical_attack_defence"),

	PVE_PHYSICAL_SKILL_DEFENCE("pve_physical_skill_defence"),
	PVE_MAGICAL_SKILL_DEFENCE("pve_magical_skill_defence"),

	// FIXED BONUS
	PVP_DAMAGE_TAKEN("pvp_damage_taken"),
	PVE_DAMAGE_TAKEN("pve_damage_taken"),

	// ATTACK & DEFENCE RATES
	MAGIC_CRITICAL_DAMAGE("magic_critical_damage"),
	PHYSICAL_SKILL_POWER("physical_skill_power"), // Adding skill power (not multipliers) results in points added directly to final value unmodified by defence, traits, elements, criticals etc.
	// Even when damage is 0 due to general trait immune multiplier, added skill power is active and clearly visible (damage not being 0 but at the value of added skill power).
	MAGICAL_SKILL_POWER("magical_skill_power"),
	CRITICAL_DAMAGE_SKILL("critical_damage_skill"),
	CRITICAL_DAMAGE_SKILL_ADD("critical_damage_skill_add"),
	MAGIC_CRITICAL_DAMAGE_ADD("magic_critical_damage_add"),
	PHYSICAL_CRITICAL_RATE("physical_critical_rate", new PCriticalRateStatCalculator(), MathUtil::add, MathUtil::add, 0, 1d),
	CRITICAL_RATE_SKILL("critical_rate_skill", ActorStat::defaultValue, MathUtil::add, MathUtil::add, 0, 1d),
	MAGIC_CRITICAL_RATE("magic_critical_rate", new MCritRateStatCalculator()),

	SHIELD_DEFENCE_RATE("shield_defence_rate", new ShieldDefenceRateStatCalculator()),

	BLOW_RATE("blow_rate"),

	DEFENCE_CRITICAL_RATE("defence_critical_rate"),
	DEFENCE_CRITICAL_RATE_ADD("defence_critical_rate_add"),

	DEFENCE_MAGIC_CRITICAL_RATE("defence_magic_critical_rate"),
	DEFENCE_MAGIC_CRITICAL_RATE_ADD("defence_magic_critical_rate_add"),

	DEFENCE_CRITICAL_DAMAGE("defence_critical_damage"),
	DEFENCE_CRITICAL_DAMAGE_ADD("defence_critical_damage_add"), // Resistance to critical damage in value (Example: +100 will be 100 more critical damage, NOT 100% more).

	DEFENCE_MAGIC_CRITICAL_DAMAGE("defence_magic_critical_damage"),
	DEFENCE_MAGIC_CRITICAL_DAMAGE_ADD("defence_magic_critical_damage_add"),

	DEFENCE_CRITICAL_DAMAGE_SKILL("defence_critical_damage_skill"),
	DEFENCE_CRITICAL_DAMAGE_SKILL_ADD("defence_critical_damage_skill_add"),

	INSTANT_KILL_RESIST("instant_kill_resitst"),

	ACTIVE_EXP_RATE("active_exp_rate"),
	PASSIVE_EXP_RATE("passive_exp_rate"),

	SP_RATE("sp_rate"),

	BONUS_EXP("bonus_exp"),
	BONUS_SP("bonus_sp"),
	BONUS_DROP("bonus_drop"),
	BONUS_SPOIL("bonus_spoil"),
	BONUS_ADENA("bonus_adena"),
	ATTACK_CANCEL("attack_cancel"),

	// ACCURACY & RANGE
	ACCURACY_PHYSICAL("accuracy_physical", new PhysicalAccuracyStatCalculator()),
	ACCURACY_MAGICAL("accuracy_magical", new MagicalAccuracyStatCalculator()),
	PHYSICAL_EVASION_RATE("physical_evasion_rate", new PhysicalEvasionRateStatCalculator()),
	MAGICAL_EVASION_RATE("magical_evasion_rate", new MagicalEvasionRateStatCalculator()),
	PHYSICAL_ATTACK_RANGE("physical_attack_range", new PhysicalAttackRangeStatCalculator()),
	MAGIC_ATTACK_RANGE("magical_attack_range"),
	ATTACK_COUNT_MAX("attack_count_max"),

	// Run speed, walk & escape speed are calculated proportionally, magic speed is a buff
	RUN_SPEED("move_speed_run", new SpeedStatCalculator()),
	WALK_SPEED("move_speed_walk", new SpeedStatCalculator()),
	SWIM_RUN_SPEED("move_speed_run_swim", new SpeedStatCalculator()),
	SWIM_WALK_SPEED("move_speed_walk_swim", new SpeedStatCalculator()),
	FLY_RUN_SPEED("move_speed_fly_run", new SpeedStatCalculator()),
	FLY_WALK_SPEED("move_speed_fly_walk", new SpeedStatCalculator()),

	// BASIC STATS
	STAT_STR("STR", new BaseStatsStatCalculator()),
	STAT_CON("CON", new BaseStatsStatCalculator()),
	STAT_DEX("DEX", new BaseStatsStatCalculator()),
	STAT_INT("INT", new BaseStatsStatCalculator()),
	STAT_WIT("WIT", new BaseStatsStatCalculator()),
	STAT_MEN("MEN", new BaseStatsStatCalculator()),
	STAT_LUC("LUC", new BaseStatsStatCalculator()),
	STAT_CHA("CHA", new BaseStatsStatCalculator()),

	// Special stats, share one slot in Calculator

	// VARIOUS
	BREATH("breath"),
	FALL("fall"),

	// VULNERABILITIES
	DAMAGE_ZONE_VULN("damage_zone_vulnerability"),
	RESIST_DISPEL_BUFF("cancel_vulnerability"), // Resistance for cancel type skills
	RESIST_ABNORMAL_DEBUFF("debuff_vulnerability"),

	// RESISTANCES
	FIRE_RES("fire_resist", new AttributeStatCalculator(AttributeType.FIRE, false)),
	WIND_RES("wind_resist", new AttributeStatCalculator(AttributeType.WIND, false)),
	WATER_RES("water_resist", new AttributeStatCalculator(AttributeType.WATER, false)),
	EARTH_RES("earth_resist", new AttributeStatCalculator(AttributeType.EARTH, false)),
	HOLY_RES("holy_resist", new AttributeStatCalculator(AttributeType.HOLY, false)),
	DARK_RES("dark_resist", new AttributeStatCalculator(AttributeType.DARK, false)),
	BASE_ATTRIBUTE_RES("base_attribute_resist"),
	MAGIC_SUCCESS_RES("magic_success_resist"),
	ABNORMAL_RESIST_PHYSICAL("abnormal_resist_physical"),
	ABNORMAL_RESIST_MAGICAL("abnormal_resist_magical"),

	// ELEMENT POWER
	FIRE_POWER("fire_power", new AttributeStatCalculator(AttributeType.FIRE, true)),
	WATER_POWER("water_power", new AttributeStatCalculator(AttributeType.WATER, true)),
	WIND_POWER("wind_power", new AttributeStatCalculator(AttributeType.WIND, true)),
	EARTH_POWER("earth_power", new AttributeStatCalculator(AttributeType.EARTH, true)),
	HOLY_POWER("holy_power", new AttributeStatCalculator(AttributeType.HOLY, true)),
	DARK_POWER("dark_power", new AttributeStatCalculator(AttributeType.DARK, true)),

	// PROFICIENCY
	REFLECT_DAMAGE_PERCENT("reflect_damage_percent"),
	REFLECT_DAMAGE_PERCENT_DEFENSE("reflect_damage_percent_defence"),

	REFLECT_SKILL_MAGIC("reflect_skill_magical"), // Need rework
	REFLECT_SKILL_PHYSIC("reflect_skill_physical"), // Need rework

	VENGEANCE_SKILL_MAGIC_DAMAGE("vengeance_skill_magic_defence"),
	VENGEANCE_SKILL_PHYSICAL_DAMAGE("vengeance_skill_physical_defence"),
	ABSORB_DAMAGE_PERCENT("absorb_damage_percent"),
	ABSORB_DAMAGE_CHANCE("absorb_damage_chance", new VampiricChanceStatCalculator()),
	ABSORB_DAMAGE_DEFENCE("absorb_damage_defence"),

	TRANSFER_DAMAGE_SUMMON_PERCENT("transfer_damage_summon_percent"),

	MP_SHIELD_PERCENT("mp_chield_percent"),

	TRANSFER_DAMAGE_TO_PLAYER("transfer_damage_player_percent"),

	ABSORB_MP_DAMAGE_PERCENT("absorb_mp_damage_percent"),

	WEIGHT_LIMIT("weight_limit"),
	WEIGHT_PENALTY("weight_penalty"),

	// ExSkill
	INVENTORY_NORMAL("inventory_limit"),
	STORAGE_PRIVATE("storage_limit"),
	TRADE_SELL("trade_sell_limit"),
	TRADE_BUY("trade_buy_limit"),
	RECIPE_DWARF("dwarf_recipe_limit"),
	RECIPE_COMMON("common_recipe_limit"),

	// Skill mastery
	SKILL_CRITICAL("skill_critical_rate", ActorStat::defaultValue, MathUtil::add, MathUtil::mul, -1, 1),
	SKILL_CRITICAL_PROBABILITY("skill_critical_rate_probability"),

	// Vitality
	VITALITY_POINTS_RATE("vitality_points_rate"),
	VITALITY_EXP_RATE("vitality_exp_rate"),

	// Souls
	MAX_SOULS("max_souls"),

	REDUCE_EXP_LOST_BY_PVP("reduce_exp_lost_pvp"),
	REDUCE_EXP_LOST_BY_MOB("reduce_exp_lost_mob"),
	REDUCE_EXP_LOST_BY_RAID("reduce_exp_lost_raid"),

	REDUCE_DEATH_PENALTY_BY_PVP("reduce_death_penalty_pvp"),
	REDUCE_DEATH_PENALTY_BY_MOB("reduce_death_penalty_mob"),
	REDUCE_DEATH_PENALTY_BY_RAID("reduce_death_penalty_raid"),

	// Brooches
	BROOCH_JEWELS("brooch_jewel_count"),
	AGATHION_SLOTS("agathion_slot_count"),
	ARTIFACT_SLOTS("artifact_slot_count"),

	// Summon Points
	MAX_SUMMON_POINTS("summon_points"),

	// Cubic Count
	MAX_CUBIC("cubic_count"),

	// The maximum allowed range to be damaged/debuffed from.
	SPHERIC_BARRIER_RANGE("spheric_barrier_range"),

	// Blocks given amount of debuffs.
	DEBUFF_BLOCK("debuff_block"),

	// Affects the random weapon damage.
	RANDOM_DAMAGE("random_damage", new RandomDamageStatCalculator()),

	// Affects the random weapon damage.
	DAMAGE_LIMIT("damage_limit"),

	// Maximun momentum one can charge
	MAX_MOMENTUM("max_momentum"),

	// Which base stat ordinal should alter skill critical formula.
	STAT_BONUS_SKILL_CRITICAL("bonus_skill_critical", ActorStat::defaultValue, MathUtil::add, MathUtil::mul, -1, 1),
	STAT_BONUS_SPEED("bonus_speed", ActorStat::defaultValue, MathUtil::add, MathUtil::mul, -1, 1),
	CRAFTING_CRITICAL("crafting_critical"),

	SHOTS_BONUS("shot_bonus", new ShotsBonusStatCalculator()),

	WORLD_CHAT_POINTS("world_chat_points"),
	ATTACK_DAMAGE("attack_damage");

	public static final int NUM_STATS = values().length;

	@Getter
	private final String value;
	@Getter
	private final IStatCalculator valueFinalizer;
	@Getter
	private final DoubleBinaryOperator addFunction;
	@Getter
	private final DoubleBinaryOperator mulFunction;
	@Getter
	private final double resetAddValue;
	@Getter
	private final double resetMulValue;

	ActorStat(String value) {
		this(value, ActorStat::defaultValue, MathUtil::add, MathUtil::mul, 0, 1);
	}

	ActorStat(String value, IStatCalculator valueFinalizer) {
		this(value, valueFinalizer, MathUtil::add, MathUtil::mul, 0, 1);
	}

	ActorStat(String value, IStatCalculator valueFinalizer, DoubleBinaryOperator addFunction, DoubleBinaryOperator mulFunction, double resetAddValue, double resetMulValue) {
		this.value = value;
		this.valueFinalizer = valueFinalizer;
		this.addFunction = addFunction;
		this.mulFunction = mulFunction;
		this.resetAddValue = resetAddValue;
		this.resetMulValue = resetMulValue;
	}

	public static ActorStat valueOfXml(String name) {
		return Arrays.stream(values())
				.filter(s ->
						s.getValue().equals(name.intern()))
				.findFirst()
				.orElseThrow(() -> new NoSuchElementException("Unknown name '" + name + "' for enum " + ActorStat.class.getSimpleName()));
	}

	public static double weaponBaseValue(Actor creature, ActorStat stat) {
		return 0.0;
//		return stat._valueFinalizer.calcWeaponBaseValue(creature, stat);
	}

	public static double defaultValue(Actor creature, double base, ActorStat stat) {
		return 0.0;
//		final double mul = creature.getStat().getMul(stat);
//		final double add = creature.getStat().getAdd(stat);
//		return base.isPresent() ? defaultValue(creature, stat, base.getAsDouble()) : mul * (add + creature.getStat().getMoveTypeValue(stat, creature.getMoveType()));
	}

	public static double defaultValue(Actor creature, ActorStat stat, double baseValue) {
		return 0.0;
//		final double mul = creature.getStat().getMul(stat);
//		final double add = creature.getStat().getAdd(stat);
//		return (baseValue * mul) + add + creature.getStat().getMoveTypeValue(stat, creature.getMoveType());
	}

	/**
	 * @param creature
	 * @param baseValue
	 * @return the final value
	 */
	public double finalize(Actor creature, double baseValue) {
		try {
			return valueFinalizer.calc(creature, baseValue, this);
		} catch (Exception e) {
//			LOGGER.warn("Exception during finalization for : {} stat: {} : ", creature, toString(), e);
			return defaultValue(creature, baseValue, this);
		}
	}

	public double add(double oldValue, double value) {
		return addFunction.applyAsDouble(oldValue, value);
	}

	public double mul(double oldValue, double value) {
		return mulFunction.applyAsDouble(oldValue, value);
	}
}
