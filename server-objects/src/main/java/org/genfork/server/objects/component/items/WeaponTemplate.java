package org.genfork.server.objects.component.items;

import org.genfork.tools.data.StatsSet;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class WeaponTemplate extends ItemData {
	public WeaponTemplate(StatsSet set) {
		super(set);
	}
}
