package org.genfork.server.objects.component.inventory;

import lombok.Data;
import org.genfork.server.enums.InventoryBlockType;
import org.genfork.server.enums.LocationData;
import org.genfork.server.objects.Item;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Data
public class ItemContainerData implements Serializable {
	private final Map<Integer, Item> items = new ConcurrentHashMap<>();

	private final List<PaperdollListener> paperdollListeners = new ArrayList<>();

	private Collection<Integer> blockItems = null;
	private InventoryBlockType blockMode = InventoryBlockType.NONE;

	private int totalWeight;
	private int wearedMask;
	private int blockedItemSlotsMask;

	private LocationData equipLocation;
	private LocationData baseLocation;

	public boolean hasInventoryBlock() {
		return blockMode != InventoryBlockType.NONE && blockItems != null && !blockItems.isEmpty();
	}
}
