package org.genfork.server.objects;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public abstract class Playable extends Actor {
	private static final long serialVersionUID = 1L;

	@Override
	public boolean isPlayable() {
		return true;
	}
}
