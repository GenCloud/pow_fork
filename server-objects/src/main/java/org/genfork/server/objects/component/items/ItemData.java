package org.genfork.server.objects.component.items;

import lombok.Data;
import org.genfork.server.enums.*;
import org.genfork.server.model.pojo.AttributeHolder;
import org.genfork.server.model.pojo.CapsuleItemHolder;
import org.genfork.server.model.pojo.ItemChanceHolder;
import org.genfork.server.model.pojo.ItemSkillHolder;
import org.genfork.server.objects.component.conditions.Condition;
import org.genfork.server.stats.functions.FuncAdd;
import org.genfork.server.stats.functions.FuncSet;
import org.genfork.server.stats.functions.FuncTemplate;
import org.genfork.tools.data.StatsSet;

import java.io.Serializable;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static org.genfork.server.enums.ActionType.action_none;
import static org.genfork.server.enums.AttributeType.*;
import static org.genfork.server.enums.AutoUseType.DISABLED;
import static org.genfork.server.enums.MaterialType.STEEL;
import static org.genfork.server.objects.component.items.ItemConstants.ITEM_SLOTS;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Data
public class ItemData implements Serializable {
	protected List<FuncTemplate> funcTemplates;
	protected List<Condition> conditions;
	private String name;
	private String icon;
	private String html;
	private int itemId;
	private int displayId;
	private int weight;
	private int equipReuseDelay;
	private int duration;
	private int time;
	private int autoDestroyTime;
	private int enchantable;
	private int defaultEnchantLevel;
	private int crystalCount;
	private int typeFirst; // needed for item list (inventory)
	private int typeSecond; // different lists for armor, weapon, etc
	private int useSkillDisTime;
	private int reuseDelay;
	private int sharedReuseGroup;
	private int allowedEnsoulSlots;
	private int extractableCountMin;
	private int extractableCountMax;
	private long bodyPart;
	private long referencePrice;
	private boolean stackable;
	private boolean sellable;
	private boolean dropable;
	private boolean destroyable;
	private boolean tradeable;
	private boolean depositable;
	private boolean elementable;
	private boolean questItem;
	private boolean freightable;
	private boolean allowSelfResurrection;
	private boolean olyRestricted;
	private boolean cocRestricted;
	private boolean forNpc;
	private boolean heroItem;
	private boolean pvpItem;
	private boolean immediateEffect;
	private boolean exImmediateEffect;
	private boolean combatFlag;
	private boolean appearanceable;
	private boolean blessed;
	private boolean bigJewelStone;
	private boolean enableEnsoul;
	private MaterialType materialType;
	private CrystalType crystalType;
	private ActionType defaultAction;
	private CommissionItemType commissionItemType;
	private AutoUseType autoUseType;
	private ItemType itemType;
	private StatsSet set;
	private List<CapsuleItemHolder> capsuledItems;
	private List<ItemSkillHolder> skills;
	private List<ItemChanceHolder> itemCreations;
	private List<AttributeHolder> attributes;

	public ItemData(StatsSet set) {
		set(set);
	}

	public void set(StatsSet set) {
		this.set = set;
		itemId = set.getInt("item_id");
		displayId = set.getInt("displayId", itemId);
		name = set.getString("name");
		icon = set.getString("icon", null);
		weight = set.getInt("weight", 0);
		materialType = set.getEnum("material", MaterialType.class, STEEL);
		equipReuseDelay = set.getInt("equip_reuse_delay", 0) * 1000;
		duration = set.getInt("duration", -1);
		time = set.getInt("time", -1);
		autoDestroyTime = set.getInt("auto_destroy_time", -1) * 1000;
		bodyPart = ITEM_SLOTS.get(set.getString("bodypart", "none"));
		referencePrice = set.getLong("price", 0);
		crystalType = set.getEnum("crystal_type", CrystalType.class, CrystalType.NONE);
		crystalCount = set.getInt("crystal_count", 0);

		stackable = set.getBoolean("is_stackable", false);
		sellable = set.getBoolean("is_sellable", true);
		dropable = set.getBoolean("is_dropable", true);
		destroyable = set.getBoolean("is_destroyable", true);
		tradeable = set.getBoolean("is_tradable", true);
		depositable = set.getBoolean("is_depositable", true);
		elementable = set.getBoolean("element_enabled", false);
		enchantable = set.getInt("enchant_enabled", 0);
		questItem = set.getBoolean("is_questitem", false);
		freightable = set.getBoolean("is_freightable", false);
		allowSelfResurrection = set.getBoolean("allow_self_resurrection", false);
		olyRestricted = set.getBoolean("is_oly_restricted", false);
		cocRestricted = set.getBoolean("is_coc_restricted", false);
		forNpc = set.getBoolean("for_npc", false);
		appearanceable = set.getBoolean("isAppearanceable", false);
		blessed = set.getBoolean("blessed", false);

		immediateEffect = set.getBoolean("immediate_effect", false);
		exImmediateEffect = set.getBoolean("ex_immediate_effect", false);

		defaultAction = set.getEnum("default_action", ActionType.class, action_none);
		html = set.getString("html", "data/html/item/" + itemId + ".htm");
		useSkillDisTime = set.getInt("useSkillDisTime", 0);
		defaultEnchantLevel = set.getInt("enchanted", 0);
		reuseDelay = set.getInt("reuse_delay", 0);
		sharedReuseGroup = set.getInt("shared_reuse_group", 0);
		commissionItemType = set.getEnum("commissionItemType", CommissionItemType.class, CommissionItemType.OTHER_ITEM);
		autoUseType = set.getEnum("auto_use_type", AutoUseType.class, DISABLED);
		combatFlag = set.getBoolean("combat_flag", false);
		bigJewelStone = set.getBoolean("is_big_jewel", false);
		enableEnsoul = set.getBoolean("is_ensoul", false);

		extractableCountMin = set.getInt("extractableCountMin", 0);
		extractableCountMax = set.getInt("extractableCountMax", 0);

		final int ensoulSlotCount = set.getInt("ensoul_slot_count", 0);
		allowedEnsoulSlots = ensoulSlotCount == 0 ? 1 : ensoulSlotCount == 1 ? 2 : ensoulSlotCount == 2 ? 3 : 0;

		heroItem = ((itemId >= 6611) && (itemId <= 6621)) || ((itemId >= 9388) && (itemId <= 9390)) || (itemId == 6842);
	}

	public int getItemMask() {
		return itemType.mask();
	}

	public Collection<AttributeHolder> getAttributes() {
		return attributes;
	}

	public void setAttributes(AttributeHolder holder) {
		if (attributes == null) {
			attributes = new LinkedList<>();
			attributes.add(holder);
		} else {
			final AttributeHolder attribute = getAttribute(holder.getType());
			if (attribute != null) {
				attribute.setValue(holder.getValue());
			} else {
				attributes.add(holder);
			}
		}
	}

	public AttributeHolder getAttribute(AttributeType type) {
		return attributes.stream().filter(t -> t.getType() == type).findFirst().orElse(null);
	}

	public void addFunctionTemplate(FuncTemplate template) {
		switch (template.getActorStat()) {
			case FIRE_RES:
			case FIRE_POWER: {
				setAttributes(new AttributeHolder(FIRE, (int) template.getValue()));
				break;
			}
			case WATER_RES:
			case WATER_POWER: {
				setAttributes(new AttributeHolder(WATER, (int) template.getValue()));
				break;
			}
			case WIND_RES:
			case WIND_POWER: {
				setAttributes(new AttributeHolder(WIND, (int) template.getValue()));
				break;
			}
			case EARTH_RES:
			case EARTH_POWER: {
				setAttributes(new AttributeHolder(EARTH, (int) template.getValue()));
				break;
			}
			case HOLY_RES:
			case HOLY_POWER: {
				setAttributes(new AttributeHolder(HOLY, (int) template.getValue()));
				break;
			}
			case DARK_RES:
			case DARK_POWER: {
				setAttributes(new AttributeHolder(DARK, (int) template.getValue()));
				break;
			}
		}

		if (funcTemplates == null) {
			funcTemplates = new ArrayList<>();
		}

		funcTemplates.add(template);
	}

	public double getStats(ActorStat stat, double defaultValue) {
		if (funcTemplates != null) {
			final FuncTemplate template = funcTemplates
					.stream()
					.filter(func ->
							func.getActorStat() == stat
									&& (func.getFunctionClass() == FuncAdd.class || func.getFunctionClass() == FuncSet.class))
					.findFirst()
					.orElse(null);
			if (template != null) {
				return template.getValue();
			}
		}
		return defaultValue;
	}

	public boolean isConditionNotAttached() {
		return conditions == null || conditions.isEmpty();
	}

	public void attachCondition(Condition c) {
		if (conditions == null) {
			conditions = new ArrayList<>();
		}
		conditions.add(c);
	}

	/**
	 * @return {@code List} of {@link ItemSkillHolder} if item has skills and matches the condition, {@code null} otherwise
	 */
	public final List<ItemSkillHolder> getSkills(Predicate<ItemSkillHolder> condition) {
		return skills != null ? skills.stream().filter(condition).collect(Collectors.toList()) : null;
	}

	/**
	 * @return {@code List} of {@link ItemSkillHolder} if item has skills, {@code null} otherwise
	 */
	public final List<ItemSkillHolder> getSkills(ItemSkillType type) {
		return skills != null ? skills.stream().filter(sk -> sk.getType() == type).collect(Collectors.toList()) : null;
	}

	/**
	 * Executes the action on each item skill with the specified type (If there are skills at all)
	 */
	public final void forEachSkill(ItemSkillType type, Consumer<ItemSkillHolder> action) {
		if (skills != null) {
			skills.stream().filter(sk -> sk.getType() == type).forEach(action);
		}
	}

	public void addSkill(ItemSkillHolder holder) {
		if (skills == null) {
			skills = new ArrayList<>();
		}

		skills.add(holder);
	}

	public List<ItemChanceHolder> getCreateItems() {
		return itemCreations != null ? itemCreations : Collections.emptyList();
	}

	public void addCreateItem(ItemChanceHolder item) {
		if (itemCreations == null) {
			itemCreations = new ArrayList<>();
		}

		itemCreations.add(item);
	}

	public List<CapsuleItemHolder> getCapsuledItems() {
		return capsuledItems != null ? capsuledItems : Collections.emptyList();
	}

	public void addCapsuledItem(CapsuleItemHolder extractableProductHolder) {
		if (capsuledItems == null) {
			capsuledItems = new ArrayList<>();
		}

		capsuledItems.add(extractableProductHolder);
	}
}
