package org.genfork.server.objects.component.inventory;

import lombok.experimental.UtilityClass;
import org.genfork.server.objects.Item;

import static org.genfork.server.objects.component.items.ItemConstants.*;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@UtilityClass
public class InventoryConstants {
	public static final int ADENA_ID = 57;
	public static final int ANCIENT_ADENA_ID = 5575;
	public static final int BEAUTY_TICKET_ID = 36308;
	public static final int AIR_STONE_ID = 39461;
	public static final int TEMPEST_STONE_ID = 39592;
	public static final int ELCYUM_CRYSTAL_ID = 36514;
	public static final int FORTUNE_READING_TICKET = 23767;
	public static final int LUXURY_FORTUNE_READING_TICKET = 23768;

	public static final int PAPERDOLL_UNDER = 0;
	public static final int PAPERDOLL_HEAD = 1;
	public static final int PAPERDOLL_HAIR = 2;
	public static final int PAPERDOLL_HAIR2 = 3;
	public static final int PAPERDOLL_NECK = 4;
	public static final int PAPERDOLL_RHAND = 5;
	public static final int PAPERDOLL_CHEST = 6;
	public static final int PAPERDOLL_LHAND = 7;
	public static final int PAPERDOLL_REAR = 8;
	public static final int PAPERDOLL_LEAR = 9;
	public static final int PAPERDOLL_GLOVES = 10;
	public static final int PAPERDOLL_LEGS = 11;
	public static final int PAPERDOLL_FEET = 12;
	public static final int PAPERDOLL_RFINGER = 13;
	public static final int PAPERDOLL_LFINGER = 14;
	public static final int PAPERDOLL_LBRACELET = 15;
	public static final int PAPERDOLL_RBRACELET = 16;
	public static final int PAPERDOLL_AGATHION1 = 17;
	public static final int PAPERDOLL_AGATHION2 = 18;
	public static final int PAPERDOLL_AGATHION3 = 19;
	public static final int PAPERDOLL_AGATHION4 = 20;
	public static final int PAPERDOLL_AGATHION5 = 21;
	public static final int PAPERDOLL_DECO1 = 22;
	public static final int PAPERDOLL_DECO2 = 23;
	public static final int PAPERDOLL_DECO3 = 24;
	public static final int PAPERDOLL_DECO4 = 25;
	public static final int PAPERDOLL_DECO5 = 26;
	public static final int PAPERDOLL_DECO6 = 27;
	public static final int PAPERDOLL_CLOAK = 28;
	public static final int PAPERDOLL_BELT = 29;
	public static final int PAPERDOLL_BROOCH = 30;
	public static final int PAPERDOLL_BROOCH_JEWEL1 = 31;
	public static final int PAPERDOLL_BROOCH_JEWEL2 = 32;
	public static final int PAPERDOLL_BROOCH_JEWEL3 = 33;
	public static final int PAPERDOLL_BROOCH_JEWEL4 = 34;
	public static final int PAPERDOLL_BROOCH_JEWEL5 = 35;
	public static final int PAPERDOLL_BROOCH_JEWEL6 = 36;
	public static final int PAPERDOLL_ARTIFACT_BOOK = 37;
	public static final int PAPERDOLL_ARTIFACT1 = 38; // Artifact Balance
	public static final int PAPERDOLL_ARTIFACT2 = 39; // Artifact Balance
	public static final int PAPERDOLL_ARTIFACT3 = 40; // Artifact Balance
	public static final int PAPERDOLL_ARTIFACT4 = 41; // Artifact Balance
	public static final int PAPERDOLL_ARTIFACT5 = 42; // Artifact Balance
	public static final int PAPERDOLL_ARTIFACT6 = 43; // Artifact Balance
	public static final int PAPERDOLL_ARTIFACT7 = 44; // Artifact Balance
	public static final int PAPERDOLL_ARTIFACT8 = 45; // Artifact Balance
	public static final int PAPERDOLL_ARTIFACT9 = 46; // Artifact Balance
	public static final int PAPERDOLL_ARTIFACT10 = 47; // Artifact Balance
	public static final int PAPERDOLL_ARTIFACT11 = 48; // Artifact Balance
	public static final int PAPERDOLL_ARTIFACT12 = 49; // Artifact Balance
	public static final int PAPERDOLL_ARTIFACT13 = 50; // Artifact Spirit
	public static final int PAPERDOLL_ARTIFACT14 = 51; // Artifact Spirit
	public static final int PAPERDOLL_ARTIFACT15 = 52; // Artifact Spirit
	public static final int PAPERDOLL_ARTIFACT16 = 53; // Artifact Protection
	public static final int PAPERDOLL_ARTIFACT17 = 54; // Artifact Protection
	public static final int PAPERDOLL_ARTIFACT18 = 55; // Artifact Protection
	public static final int PAPERDOLL_ARTIFACT19 = 56; // Artifact Support
	public static final int PAPERDOLL_ARTIFACT20 = 57; // Artifact Support
	public static final int PAPERDOLL_ARTIFACT21 = 58; // Artifact Support

	public static final int PAPERDOLL_TOTALSLOTS = 59;

	// Speed percentage mods
	public static final double MAX_ARMOR_WEIGHT = 12000;

	public static final int[] PAPERDOLL_ORDER_VISUAL_ID = new int[]
			{
					PAPERDOLL_RHAND,
					PAPERDOLL_LHAND,
					PAPERDOLL_GLOVES,
					PAPERDOLL_CHEST,
					PAPERDOLL_LEGS,
					PAPERDOLL_FEET,
					PAPERDOLL_RHAND,
					PAPERDOLL_HAIR,
					PAPERDOLL_HAIR2,
			};

	public static final int[] PAPERDOLL_ORDER_ENCHANT_LEVEL = {
			PAPERDOLL_CHEST,
			PAPERDOLL_LEGS,
			PAPERDOLL_HEAD,
			PAPERDOLL_GLOVES,
			PAPERDOLL_FEET
	};

	public static int[] PAPERDOLL_ORDER = new int[]
			{
					PAPERDOLL_UNDER,
					PAPERDOLL_REAR,
					PAPERDOLL_LEAR,
					PAPERDOLL_NECK,
					PAPERDOLL_RFINGER,
					PAPERDOLL_LFINGER,
					PAPERDOLL_HEAD,
					PAPERDOLL_RHAND,
					PAPERDOLL_LHAND,
					PAPERDOLL_GLOVES,
					PAPERDOLL_CHEST,
					PAPERDOLL_LEGS,
					PAPERDOLL_FEET,
					PAPERDOLL_CLOAK,
					PAPERDOLL_RHAND,
					PAPERDOLL_HAIR,
					PAPERDOLL_HAIR2,
					PAPERDOLL_RBRACELET,
					PAPERDOLL_LBRACELET,
					PAPERDOLL_AGATHION1, // 152
					PAPERDOLL_AGATHION2, // 152
					PAPERDOLL_AGATHION3, // 152
					PAPERDOLL_AGATHION4, // 152
					PAPERDOLL_AGATHION5, // 152
					PAPERDOLL_DECO1,
					PAPERDOLL_DECO2,
					PAPERDOLL_DECO3,
					PAPERDOLL_DECO4,
					PAPERDOLL_DECO5,
					PAPERDOLL_DECO6,
					PAPERDOLL_BELT,
					PAPERDOLL_BROOCH,
					PAPERDOLL_BROOCH_JEWEL1,
					PAPERDOLL_BROOCH_JEWEL2,
					PAPERDOLL_BROOCH_JEWEL3,
					PAPERDOLL_BROOCH_JEWEL4,
					PAPERDOLL_BROOCH_JEWEL5,
					PAPERDOLL_BROOCH_JEWEL6,
					PAPERDOLL_ARTIFACT_BOOK, // 152
					PAPERDOLL_ARTIFACT1, // 152
					PAPERDOLL_ARTIFACT2, // 152
					PAPERDOLL_ARTIFACT3, // 152
					PAPERDOLL_ARTIFACT4, // 152
					PAPERDOLL_ARTIFACT5, // 152
					PAPERDOLL_ARTIFACT6, // 152
					PAPERDOLL_ARTIFACT7, // 152
					PAPERDOLL_ARTIFACT8, // 152
					PAPERDOLL_ARTIFACT9, // 152
					PAPERDOLL_ARTIFACT10, // 152
					PAPERDOLL_ARTIFACT11, // 152
					PAPERDOLL_ARTIFACT12, // 152
					PAPERDOLL_ARTIFACT13, // 152
					PAPERDOLL_ARTIFACT14, // 152
					PAPERDOLL_ARTIFACT15, // 152
					PAPERDOLL_ARTIFACT16, // 152
					PAPERDOLL_ARTIFACT17, // 152
					PAPERDOLL_ARTIFACT18, // 152
					PAPERDOLL_ARTIFACT19, // 152
					PAPERDOLL_ARTIFACT20, // 152
					PAPERDOLL_ARTIFACT21 // 152

			};

	public static int[] PAPERDOLL_ORDER_AUGMENT = new int[]
			{
					PAPERDOLL_RHAND,
					PAPERDOLL_LHAND,
					PAPERDOLL_RHAND
			};

	public int getPaperdollIdFromItem(long targetSlot) {
		int slot = -1;

		if (targetSlot == SLOT_LR_HAND) {
			slot = PAPERDOLL_RHAND;
		} else if (targetSlot == SLOT_L_HAND) {
			slot = PAPERDOLL_LHAND;
		} else if (targetSlot == SLOT_R_HAND) {
			slot = PAPERDOLL_RHAND;
		} else if (targetSlot == SLOT_L_EAR) {
			slot = PAPERDOLL_LEAR;
		} else if (targetSlot == SLOT_R_EAR) {
			slot = PAPERDOLL_REAR;
		} else if (targetSlot == SLOT_L_FINGER) {
			slot = PAPERDOLL_LFINGER;
		} else if (targetSlot == SLOT_R_FINGER) {
			slot = PAPERDOLL_RFINGER;
		} else if (targetSlot == SLOT_NECK) {
			slot = PAPERDOLL_NECK;
		} else if (targetSlot == SLOT_FULL_ARMOR) {
			slot = PAPERDOLL_CHEST;
		} else if (targetSlot == SLOT_CHEST) {
			slot = PAPERDOLL_CHEST;
		} else if (targetSlot == SLOT_LEGS) {
			slot = PAPERDOLL_LEGS;
		} else if (targetSlot == SLOT_FEET) {
			slot = PAPERDOLL_FEET;
		} else if (targetSlot == SLOT_GLOVES) {
			slot = PAPERDOLL_GLOVES;
		} else if (targetSlot == SLOT_HEAD) {
			slot = PAPERDOLL_HEAD;
		} else if (targetSlot == SLOT_HAIR) {
			slot = PAPERDOLL_HAIR;
		} else if (targetSlot == SLOT_HAIR2) {
			slot = PAPERDOLL_HAIR2;
		} else if (targetSlot == SLOT_HAIRALL) {
			slot = PAPERDOLL_HAIR;
		} else if (targetSlot == SLOT_UNDERWEAR) {
			slot = PAPERDOLL_UNDER;
		} else if (targetSlot == SLOT_BACK) {
			slot = PAPERDOLL_CLOAK;
		} else if (targetSlot == SLOT_L_BRACELET) {
			slot = PAPERDOLL_LBRACELET;
		} else if (targetSlot == SLOT_R_BRACELET) {
			slot = PAPERDOLL_RBRACELET;
		} else if (targetSlot == SLOT_BELT) {
			slot = PAPERDOLL_BELT;
		} else if (targetSlot == SLOT_ALLDRESS) {// formal dress
			slot = PAPERDOLL_CHEST;
		} else if (targetSlot == SLOT_BROOCH) {
			slot = PAPERDOLL_BROOCH;
		} else if (targetSlot == SLOT_ARTIFACT_BOOK) {
			slot = PAPERDOLL_ARTIFACT_BOOK;
		}

		return slot;
	}

	public long getSlotFromItem(Item item) {
		long slot = -1;
		final int location = item.getLocationMask();
		if (location == PAPERDOLL_UNDER) {
			slot = SLOT_UNDERWEAR;
		} else if (location == PAPERDOLL_LEAR) {
			slot = SLOT_L_EAR;
		} else if (location == PAPERDOLL_REAR) {
			slot = SLOT_R_EAR;
		} else if (location == PAPERDOLL_NECK) {
			slot = SLOT_NECK;
		} else if (location == PAPERDOLL_RFINGER) {
			slot = SLOT_R_FINGER;
		} else if (location == PAPERDOLL_LFINGER) {
			slot = SLOT_L_FINGER;
		} else if (location == PAPERDOLL_HAIR) {
			slot = SLOT_HAIR;
		} else if (location == PAPERDOLL_HAIR2) {
			slot = SLOT_HAIR2;
		} else if (location == PAPERDOLL_HEAD) {
			slot = SLOT_HEAD;
		} else if (location == PAPERDOLL_RHAND) {
			slot = SLOT_R_HAND;
		} else if (location == PAPERDOLL_LHAND) {
			slot = SLOT_L_HAND;
		} else if (location == PAPERDOLL_GLOVES) {
			slot = SLOT_GLOVES;
		} else if (location == PAPERDOLL_CHEST) {
			slot = item.getItemData().getBodyPart();
		} else if (location == PAPERDOLL_LEGS) {
			slot = SLOT_LEGS;
		} else if (location == PAPERDOLL_CLOAK) {
			slot = SLOT_BACK;
		} else if (location == PAPERDOLL_FEET) {
			slot = SLOT_FEET;
		} else if (location == PAPERDOLL_LBRACELET) {
			slot = SLOT_L_BRACELET;
		} else if (location == PAPERDOLL_RBRACELET) {
			slot = SLOT_R_BRACELET;
		} else if (location == PAPERDOLL_DECO1 || location == PAPERDOLL_DECO2 || location == PAPERDOLL_DECO3 || location == PAPERDOLL_DECO4 || location == PAPERDOLL_DECO5 || location == PAPERDOLL_DECO6) {
			slot = SLOT_DECO;
		} else if (location == PAPERDOLL_BELT) {
			slot = SLOT_BELT;
		} else if (location == PAPERDOLL_BROOCH) {
			slot = SLOT_BROOCH;
		} else if (location == PAPERDOLL_BROOCH_JEWEL1 || location == PAPERDOLL_BROOCH_JEWEL2 || location == PAPERDOLL_BROOCH_JEWEL3 || location == PAPERDOLL_BROOCH_JEWEL4 || location == PAPERDOLL_BROOCH_JEWEL5 || location == PAPERDOLL_BROOCH_JEWEL6) {
			slot = SLOT_BROOCH_JEWEL;
		} else if (location == PAPERDOLL_AGATHION1 || location == PAPERDOLL_AGATHION2 || location == PAPERDOLL_AGATHION3 || location == PAPERDOLL_AGATHION4 || location == PAPERDOLL_AGATHION5) {
			slot = SLOT_AGATHION;
		} else if (location == PAPERDOLL_ARTIFACT_BOOK) {
			slot = SLOT_ARTIFACT_BOOK;
		} else if (location == PAPERDOLL_ARTIFACT1 || location == PAPERDOLL_ARTIFACT2 || location == PAPERDOLL_ARTIFACT3
				|| location == PAPERDOLL_ARTIFACT4 || location == PAPERDOLL_ARTIFACT5 || location == PAPERDOLL_ARTIFACT6
				|| location == PAPERDOLL_ARTIFACT7 || location == PAPERDOLL_ARTIFACT8 || location == PAPERDOLL_ARTIFACT9
				|| location == PAPERDOLL_ARTIFACT10 || location == PAPERDOLL_ARTIFACT11 || location == PAPERDOLL_ARTIFACT12
				|| location == PAPERDOLL_ARTIFACT13 || location == PAPERDOLL_ARTIFACT14 || location == PAPERDOLL_ARTIFACT15
				|| location == PAPERDOLL_ARTIFACT16 || location == PAPERDOLL_ARTIFACT17 || location == PAPERDOLL_ARTIFACT18
				|| location == PAPERDOLL_ARTIFACT19 || location == PAPERDOLL_ARTIFACT20 || location == PAPERDOLL_ARTIFACT21) {
			slot = SLOT_ARTIFACT;
		}
		return slot;
	}

	public int getPaperdollIndex(long slot) {
		if (slot == SLOT_UNDERWEAR) {
			return PAPERDOLL_UNDER;
		} else if (slot == SLOT_R_EAR) {
			return PAPERDOLL_REAR;
		} else if (slot == SLOT_LR_EAR || slot == SLOT_L_EAR) {
			return PAPERDOLL_LEAR;
		} else if (slot == SLOT_NECK) {
			return PAPERDOLL_NECK;
		} else if (slot == SLOT_R_FINGER || slot == SLOT_LR_FINGER) {
			return PAPERDOLL_RFINGER;
		} else if (slot == SLOT_L_FINGER) {
			return PAPERDOLL_LFINGER;
		} else if (slot == SLOT_HEAD) {
			return PAPERDOLL_HEAD;
		} else if (slot == SLOT_R_HAND || slot == SLOT_LR_HAND) {
			return PAPERDOLL_RHAND;
		} else if (slot == SLOT_L_HAND) {
			return PAPERDOLL_LHAND;
		} else if (slot == SLOT_GLOVES) {
			return PAPERDOLL_GLOVES;
		} else if (slot == SLOT_CHEST || slot == SLOT_FULL_ARMOR || slot == SLOT_ALLDRESS) {
			return PAPERDOLL_CHEST;
		} else if (slot == SLOT_LEGS) {
			return PAPERDOLL_LEGS;
		} else if (slot == SLOT_FEET) {
			return PAPERDOLL_FEET;
		} else if (slot == SLOT_BACK) {
			return PAPERDOLL_CLOAK;
		} else if (slot == SLOT_HAIR || slot == SLOT_HAIRALL) {
			return PAPERDOLL_HAIR;
		} else if (slot == SLOT_HAIR2) {
			return PAPERDOLL_HAIR2;
		} else if (slot == SLOT_R_BRACELET) {
			return PAPERDOLL_RBRACELET;
		} else if (slot == SLOT_L_BRACELET) {
			return PAPERDOLL_LBRACELET;
		} else if (slot == SLOT_DECO) {
			return PAPERDOLL_DECO1; // return first we deal with it later
		} else if (slot == SLOT_BELT) {
			return PAPERDOLL_BELT;
		} else if (slot == SLOT_BROOCH) {
			return PAPERDOLL_BROOCH;
		} else if (slot == SLOT_BROOCH_JEWEL) {
			return PAPERDOLL_BROOCH_JEWEL1;
		} else if (slot == SLOT_AGATHION) {
			return PAPERDOLL_AGATHION1;
		} else if (slot == SLOT_ARTIFACT_BOOK) {
			return PAPERDOLL_ARTIFACT_BOOK;
		} else if (slot == SLOT_ARTIFACT) {
			return PAPERDOLL_ARTIFACT1;
		}
		return -1;
	}
}
