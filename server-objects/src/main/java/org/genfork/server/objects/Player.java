package org.genfork.server.objects;

import lombok.Getter;
import lombok.Setter;
import org.genfork.server.enums.ClassId;
import org.genfork.server.enums.Race;
import org.genfork.server.objects.component.appearance.PlayerAppearance;
import org.genfork.tools.network.SessionInfo;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class Player extends Playable {
	private static final long serialVersionUID = 1L;

	@Getter
	@Setter
	private String accountName;

	@Getter
	@Setter
	private String title;

	@Getter
	@Setter
	private int accessLevel;

	@Getter
	@Setter
	private int playerLevel;

	@Getter
	@Setter
	private Race race;

	@Getter
	@Setter
	private ClassId classId;

	@Getter
	@Setter
	private ClassId baseClassId;

	@Getter
	@Setter
	private int maxCp;

	@Getter
	@Setter
	private int currentCp;

	@Getter
	@Setter
	private int maxHp;

	@Getter
	@Setter
	private int currentHp;

	@Getter
	@Setter
	private int maxMp;

	@Getter
	@Setter
	private int currentMp;

	@Getter
	@Setter
	private int raidBossPoints;
	@Getter
	@Setter
	private int fame;
	@Getter
	@Setter
	private int reputation;
	@Getter
	@Setter
	private int recommendationsHave;
	@Getter
	@Setter
	private int recommendationsLeft;

	@Getter
	@Setter
	private int pvp;
	@Getter
	@Setter
	private int pk;
	@Getter
	@Setter
	private int titleColor;
	@Getter
	@Setter
	private int online;
	@Getter
	@Setter
	private int onlineTime;
	@Getter
	@Setter
	private int nobless;

	@Getter
	@Setter
	private int vitalityPoints;

	@Getter
	@Setter
	private long createdDate;

	@Getter
	@Setter
	private long currentSp;

	@Getter
	@Setter
	private long currentExp;

	@Getter
	@Setter
	private long expBeforeDeath;

	@Getter
	@Setter
	private long lastAccess;

	@Getter
	@Setter
	private PlayerAppearance playerAppearance;

	@Getter
	@Setter
	private SessionInfo sessionInfo;

	@Override
	public Player asPlayer() {
		return this;
	}

	@Override
	public boolean isPlayer() {
		return true;
	}

	@Override
	public String toString() {
		return "Player{" +
				"accountName='" + accountName + '\'' +
				", title='" + title + '\'' +
				", accessLevel=" + accessLevel +
				", playerLevel=" + playerLevel +
				", race=" + race +
				", classId=" + classId +
				", baseClassId=" + baseClassId +
				", maxCp=" + maxCp +
				", currentCp=" + currentCp +
				", maxHp=" + maxHp +
				", currentHp=" + currentHp +
				", maxMp=" + maxMp +
				", currentMp=" + currentMp +
				", raidBossPoints=" + raidBossPoints +
				", fame=" + fame +
				", reputation=" + reputation +
				", recommendationsHave=" + recommendationsHave +
				", recommendationsLeft=" + recommendationsLeft +
				", pvp=" + pvp +
				", pk=" + pk +
				", titleColor=" + titleColor +
				", online=" + online +
				", onlineTime=" + onlineTime +
				", nobless=" + nobless +
				", vitalityPoints=" + vitalityPoints +
				", createdDate=" + createdDate +
				", currentSp=" + currentSp +
				", currentExp=" + currentExp +
				", expBeforeDeath=" + expBeforeDeath +
				", lastAccess=" + lastAccess +
				", sessionInfo=" + sessionInfo +
				'}';
	}
}
