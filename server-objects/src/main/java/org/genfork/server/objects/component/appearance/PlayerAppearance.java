package org.genfork.server.objects.component.appearance;

import lombok.Getter;
import lombok.Setter;
import org.genfork.server.enums.Sex;
import org.genfork.server.objects.Player;

import java.io.Serializable;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class PlayerAppearance implements Serializable {
	public static final int DEFAULT_TITLE_COLOR = 0xECF9A2;

	@Getter
	@Setter
	private Player player;

	@Getter
	@Setter
	private int face;

	@Getter
	@Setter
	private int hairColor;

	@Getter
	@Setter
	private int hairStyle;

	@Getter
	private int nameColor = 0xFFFFFF;
	@Getter
	private int titleColor = DEFAULT_TITLE_COLOR;

	@Getter
	@Setter
	private int visibleClanId = -1;
	@Getter
	@Setter
	private int visibleClanCrestId = -1;
	@Getter
	@Setter
	private int visibleClanLargeCrestId = -1;
	@Getter
	@Setter
	private int visibleAllyId = -1;
	@Getter
	@Setter
	private int visibleAllyCrestId = -1;

	@Getter
	@Setter
	private boolean sex;

	@Setter
	private String visibleName;
	@Setter
	private String visibleTitle;

	public PlayerAppearance(int face, int hairColor, int hairStyle, boolean sex) {
		this.face = face;
		this.hairColor = hairColor;
		this.hairStyle = hairStyle;
		this.sex = sex;
	}

	public String getVisibleName() {
		if (visibleName == null) {
			return getOwner().getName();
		}

		return visibleName;
	}

	public String getVisibleTitle() {
		if (visibleTitle == null) {
			return getOwner().getTitle();
		}
		return visibleTitle;
	}

	/**
	 * @return Sex of the char
	 */
	public Sex getSexType() {
		return sex ? Sex.FEMALE : Sex.MALE;
	}

	public void setNameColor(int nameColor) {
		if (nameColor < 0) {
			return;
		}

		this.nameColor = nameColor;
	}

	public void setNameColor(int red, int green, int blue) {
		nameColor = (red & 0xFF) + ((green & 0xFF) << 8) + ((blue & 0xFF) << 16);
	}

	public void setTitleColor(int titleColor) {
		if (titleColor < 0) {
			return;
		}

		this.titleColor = titleColor;
	}

	public void setTitleColor(int red, int green, int blue) {
		titleColor = (red & 0xFF) + ((green & 0xFF) << 8) + ((blue & 0xFF) << 16);
	}

	/**
	 * @return Returns the owner.
	 */
	public Player getOwner() {
		return player;
	}

//	TODO
//	public int getVisibleClanId() {
//		return visibleClanId != -1 ? visibleClanId : getOwner().isCursedWeaponEquipped() ? 0 : getOwner().getClanId();
//	}
//
//	public int getVisibleClanCrestId() {
//		return visibleClanCrestId != -1 ? visibleClanCrestId : getOwner().isCursedWeaponEquipped() ? 0 : getOwner().getClanCrestId();
//	}
//
//	public int getVisibleClanLargeCrestId() {
//		return visibleClanLargeCrestId != -1 ? visibleClanLargeCrestId : getOwner().isCursedWeaponEquipped() ? 0 : getOwner().getClanCrestLargeId();
//	}
//
//	public int getVisibleAllyId() {
//		return visibleAllyId != -1 ? visibleAllyId : getOwner().isCursedWeaponEquipped() ? 0 : getOwner().getAllyId();
//	}
//
//	public int getVisibleAllyCrestId() {
//		return visibleAllyCrestId != -1 ? visibleAllyCrestId : getOwner().isCursedWeaponEquipped() ? 0 : getOwner().getAllyCrestId();
//	}

	public void setVisibleClanData(int clanId, int clanCrestId, int clanLargeCrestId, int allyId, int allyCrestId) {
		visibleClanId = clanId;
		visibleClanCrestId = clanCrestId;
		visibleClanLargeCrestId = clanLargeCrestId;
		visibleAllyId = allyId;
		visibleAllyCrestId = allyCrestId;
	}
}
