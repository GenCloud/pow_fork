package org.genfork.server.objects.component.items;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class ItemConstants {
	public static final Map<String, Long> ITEM_SLOTS = new HashMap<>();

	public static final int UNCHANGED = 0;
	public static final int ADDED = 1;
	public static final int REMOVED = 3;
	public static final int MODIFIED = 2;

	public static final int SLOT_NONE = 0x0000;
	public static final int SLOT_UNDERWEAR = 0x0001;
	public static final int SLOT_R_EAR = 0x0002;
	public static final int SLOT_L_EAR = 0x0004;
	public static final int SLOT_LR_EAR = 0x00006;
	public static final int SLOT_NECK = 0x0008;
	public static final int SLOT_R_FINGER = 0x0010;
	public static final int SLOT_L_FINGER = 0x0020;
	public static final int SLOT_LR_FINGER = 0x0030;
	public static final int SLOT_HEAD = 0x0040;
	public static final int SLOT_R_HAND = 0x0080;
	public static final int SLOT_L_HAND = 0x0100;
	public static final int SLOT_GLOVES = 0x0200;
	public static final int SLOT_CHEST = 0x0400;
	public static final int SLOT_LEGS = 0x0800;
	public static final int SLOT_FEET = 0x1000;
	public static final int SLOT_BACK = 0x2000;
	public static final int SLOT_LR_HAND = 0x4000;
	public static final int SLOT_FULL_ARMOR = 0x8000;
	public static final int SLOT_HAIR = 0x010000;
	public static final int SLOT_ALLDRESS = 0x020000;
	public static final int SLOT_HAIR2 = 0x040000;
	public static final int SLOT_HAIRALL = 0x080000;
	public static final int SLOT_R_BRACELET = 0x100000;
	public static final int SLOT_L_BRACELET = 0x200000;
	public static final int SLOT_DECO = 0x400000;
	public static final int SLOT_BELT = 0x10000000;
	public static final int SLOT_BROOCH = 0x20000000;
	public static final int SLOT_BROOCH_JEWEL = 0x40000000;
	public static final long SLOT_AGATHION = 0x3000000000L;
	public static final long SLOT_ARTIFACT_BOOK = 0x20000000000L;
	public static final long SLOT_ARTIFACT = 0x40000000000L;

	public static final int SLOT_WOLF = -100;
	public static final int SLOT_HATCHLING = -101;
	public static final int SLOT_STRIDER = -102;
	public static final int SLOT_BABYPET = -103;
	public static final int SLOT_GREATWOLF = -104;

	public static final int SLOT_MULTI_ALLWEAPON = SLOT_LR_HAND | SLOT_R_HAND;
	public static final int TYPE1_WEAPON_RING_EARRING_NECKLACE = 0;
	public static final int TYPE1_SHIELD_ARMOR = 1;
	public static final int TYPE1_ITEM_QUEST_ITEM_ADENA = 4;
	public static final int TYPE2_WEAPON = 0;
	public static final int TYPE2_SHIELD_ARMOR = 1;
	public static final int TYPE2_ACCESSORY = 2;
	public static final int TYPE2_QUEST = 3;
	public static final int TYPE2_MONEY = 4;
	public static final int TYPE2_OTHER = 5;

	static {
		ITEM_SLOTS.put("shirt", (long) SLOT_UNDERWEAR);
		ITEM_SLOTS.put("lbracelet", (long) SLOT_L_BRACELET);
		ITEM_SLOTS.put("rbracelet", (long) SLOT_R_BRACELET);
		ITEM_SLOTS.put("talisman", (long) SLOT_DECO);
		ITEM_SLOTS.put("chest", (long) SLOT_CHEST);
		ITEM_SLOTS.put("fullarmor", (long) SLOT_FULL_ARMOR);
		ITEM_SLOTS.put("head", (long) SLOT_HEAD);
		ITEM_SLOTS.put("hair", (long) SLOT_HAIR);
		ITEM_SLOTS.put("hairall", (long) SLOT_HAIRALL);
		ITEM_SLOTS.put("underwear", (long) SLOT_UNDERWEAR);
		ITEM_SLOTS.put("back", (long) SLOT_BACK);
		ITEM_SLOTS.put("neck", (long) SLOT_NECK);
		ITEM_SLOTS.put("legs", (long) SLOT_LEGS);
		ITEM_SLOTS.put("feet", (long) SLOT_FEET);
		ITEM_SLOTS.put("gloves", (long) SLOT_GLOVES);
		ITEM_SLOTS.put("chest,legs", (long) SLOT_CHEST | SLOT_LEGS);
		ITEM_SLOTS.put("belt", (long) SLOT_BELT);
		ITEM_SLOTS.put("rhand", (long) SLOT_R_HAND);
		ITEM_SLOTS.put("lhand", (long) SLOT_L_HAND);
		ITEM_SLOTS.put("lrhand", (long) SLOT_LR_HAND);
		ITEM_SLOTS.put("rlear", (long) SLOT_R_EAR | SLOT_L_EAR);
		ITEM_SLOTS.put("rlfinger", (long) SLOT_R_FINGER | SLOT_L_FINGER);
		ITEM_SLOTS.put("wolf", (long) SLOT_WOLF);
		ITEM_SLOTS.put("greatwolf", (long) SLOT_GREATWOLF);
		ITEM_SLOTS.put("hatchling", (long) SLOT_HATCHLING);
		ITEM_SLOTS.put("strider", (long) SLOT_STRIDER);
		ITEM_SLOTS.put("babypet", (long) SLOT_BABYPET);
		ITEM_SLOTS.put("brooch", (long) SLOT_BROOCH);
		ITEM_SLOTS.put("brooch_jewel", (long) SLOT_BROOCH_JEWEL);
		ITEM_SLOTS.put("none", (long) SLOT_NONE);
		ITEM_SLOTS.put("agathion", SLOT_AGATHION);
		ITEM_SLOTS.put("artifactbook", SLOT_ARTIFACT_BOOK);
		ITEM_SLOTS.put("artifact", SLOT_ARTIFACT);

		// retail compatibility
		ITEM_SLOTS.put("onepiece", (long) SLOT_FULL_ARMOR);
		ITEM_SLOTS.put("hair2", (long) SLOT_HAIR2);
		ITEM_SLOTS.put("dhair", (long) SLOT_HAIRALL);
		ITEM_SLOTS.put("alldress", (long) SLOT_ALLDRESS);
		ITEM_SLOTS.put("deco1", (long) SLOT_DECO);
		ITEM_SLOTS.put("waist", (long) SLOT_BELT);
	}
}
