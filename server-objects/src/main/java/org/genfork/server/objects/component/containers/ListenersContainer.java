package org.genfork.server.objects.component.containers;

import lombok.Getter;
import org.genfork.server.dispatcher.interfaces.ListenerOwner;
import org.genfork.server.dispatcher.listeners.AbstractEventListener;
import org.genfork.server.enums.EventType;
import org.genfork.tools.common.EmptyQueue;

import java.io.Serializable;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.function.Predicate;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class ListenersContainer implements Serializable, ListenerOwner {
	@Getter
	private final ConcurrentMap<EventType, Queue<AbstractEventListener>> listeners = new ConcurrentHashMap<>();

	/**
	 * Registers listener for a callback when specified event is executed.
	 */
	public AbstractEventListener addListener(AbstractEventListener listener) {
		if (listener == null) {
			throw new NullPointerException("Listener cannot be null!");
		}

		getListeners().computeIfAbsent(listener.getEventType(), k -> new PriorityBlockingQueue<>()).add(listener);
		return listener;
	}

	/**
	 * Unregisters listener for a callback when specified event is executed.
	 */
	public AbstractEventListener removeListener(AbstractEventListener listener) {
		if (!listeners.containsKey(listener.getEventType())) {
			throw new IllegalAccessError("Listeners container doesn't had " + listener.getEventType() + " event type added!");
		}

		listeners.get(listener.getEventType()).remove(listener);
		return listener;
	}

	/**
	 * @return {@code List} of {@link AbstractEventListener} by the specified type
	 */
	public Queue<AbstractEventListener> getListeners(EventType type) {
		return listeners.containsKey(type) ? listeners.get(type) : EmptyQueue.emptyQueue();
	}

	public void removeListenerIf(EventType type, Predicate<? super AbstractEventListener> filter) {
		getListeners(type).stream().filter(filter).forEach(AbstractEventListener::unregisterMe);
	}

	public void removeListenerIf(Predicate<? super AbstractEventListener> filter) {
		getListeners().values().forEach(queue -> queue.stream().filter(filter).forEach(AbstractEventListener::unregisterMe));
	}

	public boolean hasListener(EventType type) {
		return !getListeners(type).isEmpty();
	}
}
