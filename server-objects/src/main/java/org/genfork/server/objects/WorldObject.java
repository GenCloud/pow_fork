package org.genfork.server.objects;

import com.google.common.util.concurrent.AtomicDouble;
import lombok.Data;
import net.sf.cglib.proxy.NoOp;
import org.genfork.server.model.world.WorldRegion;
import org.genfork.server.objects.component.ILocational;
import org.genfork.server.objects.component.containers.ListenersContainer;

import java.io.Serializable;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Data
public abstract class WorldObject extends ListenersContainer implements ILocational, NoOp, Serializable {
	private static final long serialVersionUID = 1L;

	private String name;
	private final AtomicBoolean spawnedFlag = new AtomicBoolean(false);

	private final AtomicDouble x = new AtomicDouble(0),
			y = new AtomicDouble(0),
			z = new AtomicDouble(0);

	private final AtomicInteger heading = new AtomicInteger(0);
	private final AtomicBoolean invisibleFlag = new AtomicBoolean(false);
	private int objectId;

	private final AtomicReference<WorldRegion> worldRegion = new AtomicReference<>(null);

	@Override
	public int getRealX() {
		return (int) x.get();
	}

	public void setRealX(int x) {
		this.x.getAndSet(x);
	}

	@Override
	public int getRealY() {
		return (int) y.get();
	}

	public void setRealY(int y) {
		this.y.getAndSet(y);
	}

	@Override
	public int getRealZ() {
		return (int) z.get();
	}

	public void setRealZ(int z) {
		this.z.getAndSet(z);
	}

	@Override
	public int getRealHeading() {
		return heading.get();
	}

	public void setRealHeading(int heading) {
		this.heading.getAndSet(heading);
	}

	public WorldRegion getCurrentRegion() {
		return worldRegion.get();
	}

	public void setCurrentRegion(WorldRegion newRegion) {
		worldRegion.compareAndSet(getCurrentRegion(), newRegion);
	}

	public boolean getSpawned() {
		return spawnedFlag.get();
	}

	public void setSpawned(boolean value) {
		final boolean ok = spawnedFlag.compareAndSet(isInvisible(), value);
		if (ok && !value) {
			setCurrentRegion(null);
		}
	}

	public boolean isInvisible() {
		return invisibleFlag.get();
	}

	public boolean isVisibleFor(Player player) {
//		final BooleanReturn term = EventDispatcher.getInstance().notifyEvent(new IsWorldObjectVisibleFor(this, player), this, BooleanReturn.class);
//		if (term != null)
//		{
//			return term.getValue();
//		}

		return !isInvisible() /*|| player.canOverrideCond(PcCondOverride.SEE_ALL_PLAYERS)*/;
	}

	public boolean setInvisible(boolean invisible) {
		return this.invisibleFlag.compareAndSet(isInvisible(), invisible);
	}

	public boolean isPlayable() {
		return false;
	}

	public boolean isPlayer() {
		return false;
	}

	public Player asPlayer() {
		return null;
	}

	public String hashName(int serverId) {
		return name + "_" + serverId;
	}

	@Override
	public boolean equals(Object obj) {
		return obj instanceof WorldObject && ((WorldObject) obj).getObjectId() == getObjectId();
	}

	@Override
	public String toString() {
		return "WorldObject{" +
				"name='" + name + '\'' +
				", objectId=" + objectId +
				", x=" + x +
				", y=" + y +
				", z=" + z +
				", heading=" + heading +
				", spawned=" + spawnedFlag +
				", invisible=" + invisibleFlag +
				", worldRegion=" + worldRegion +
				'}';
	}
}
