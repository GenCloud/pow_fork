package org.genfork.server.objects;

import lombok.Getter;
import org.genfork.server.objects.component.data.IData;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public abstract class Actor extends WorldObject {
	private static final long serialVersionUID = 1L;

	@Getter
	private boolean running = isPlayer();

	private IData data;

	@SuppressWarnings("unchecked")
	public <O extends IData> O getData() {
		return (O) data;
	}

	public <O extends IData> void setData(O data) {
		this.data = data;
	}
}
