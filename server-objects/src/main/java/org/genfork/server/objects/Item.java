package org.genfork.server.objects;

import lombok.Getter;
import lombok.Setter;
import org.genfork.server.enums.LocationData;
import org.genfork.server.objects.component.items.ItemData;

import static org.genfork.server.enums.LocationData.PAPER_DOLL;
import static org.genfork.server.enums.LocationData.PET_EQUIP;
import static org.genfork.server.objects.component.inventory.InventoryConstants.ADENA_ID;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class Item extends WorldObject {
	@Getter
	@Setter
	private int ownerId;

	@Getter
	@Setter
	private int itemId;

	@Getter
	@Setter
	private int paperdollIndex = -1;

	@Getter
	@Setter
	private int locationMask;

	@Getter
	@Setter
	private int enchant;

	@Getter
	@Setter
	private int visualId;

	@Getter
	@Setter
	private int mpLeft;

	@Getter
	@Setter
	private int typeFirst;

	@Getter
	@Setter
	private int typeSecond;

	@Getter
	@Setter
	private int lastChange = 2;

	@Getter
	@Setter
	private long amount = 1;

	@Getter
	@Setter
	private boolean wear;

	@Getter
	@Setter
	private boolean consumingMp;

	@Getter
	private LocationData locationData;

	@Getter
	@Setter
	private ItemData itemData;

	public boolean isEquipped() {
		return locationData == PAPER_DOLL || locationData == PET_EQUIP;
	}

	/**
	 * Sets the location of the item
	 *
	 * @param locationData : LocationData (enumeration)
	 */
	public void setLocationData(LocationData locationData) {
		setLocationData(locationData, 0);
	}

	/**
	 * Sets the location of the item.<BR>
	 * <BR>
	 * <U><I>Remark :</I></U> If locationData and locationMask different from database, say datas not up-to-date
	 *
	 * @param locationData : LocationData (enumeration)
	 * @param locationMask : int designating the slot where the item is stored or the village for freights
	 */
	public void setLocationData(LocationData locationData, int locationMask) {
		if (locationData == this.locationData && locationMask == this.locationMask) {
			return;
		}

		// Remove any inventory skills from the old owner.
//		TODO: to data server
//		removeSkillsFromOwner();

		this.locationData = locationData;
		this.locationMask = locationMask;
//		setStoredInDb(false);

		// Give any inventory skills to the new owner only if the item is in inventory
		// else the skills will be given when location is set to inventory.
//		giveSkillsToOwner();
	}

	public long getTotalWeight() {
		try {
			return Math.multiplyExact(getItemData().getWeight(), getAmount());
		} catch (ArithmeticException ae) {
			return Long.MAX_VALUE;
		}
	}

	/**
	 * Sets the quantity of the item.<BR>
	 * <BR>
	 * <U><I>Remark :</I></U> If loc and loc_data different from database, say datas not up-to-date
	 *
	 * @param count : int
	 */
	public void changeCount(long count) {
		if (count == 0) {
			return;
		}

		//		TODO: config allowed count
		final long max = itemId == ADENA_ID ? Long.MAX_VALUE : Integer.MAX_VALUE;
		if (count > 0 && this.amount > max - count) {
			setAmount(max);
		} else {
			setAmount(this.amount + count);
		}

		if (this.amount < 0) {
			setAmount(0);
		}

//		setStoredInDb(false);
	}

	@Override
	public String hashName(int serverId) {
		return toString() + "_" + serverId;
	}

	@Override
	public String toString() {
		return "Item{" +
				"ownerId=" + ownerId +
				", itemId=" + itemId +
				'}';
	}
}
