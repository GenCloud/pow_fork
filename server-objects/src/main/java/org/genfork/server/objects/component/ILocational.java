package org.genfork.server.objects.component;

import org.genfork.server.location.Location;
import org.genfork.server.utils.Util;
import org.genfork.tools.common.Rnd;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public interface ILocational {
	int getRealX();

	int getRealY();

	int getRealZ();

	int getRealHeading();

	/**
	 * Gets a random position around the current location.
	 *
	 * @param minRange the minimum range from the center to pick a point.
	 * @param maxRange the maximum range from the center to pick a point.
	 * @return a random location between minRange and maxRange of the center location.
	 */
	default Location getRandomPosition(int minRange, int maxRange) {
		final int randomX = Rnd.get(minRange, maxRange);
		final int randomY = Rnd.get(minRange, maxRange);
		final double rndAngle = Math.toRadians(Rnd.get(360));

		final double newX = getRealX() + (randomX * Math.cos(rndAngle));
		final double newY = getRealY() + (randomY * Math.sin(rndAngle));

		return new Location(newX, newY, getRealZ());
	}

	/**
	 * @return degree value of object 2 to the horizontal line with object 1 being the origin
	 */
	default double calculateAngleTo(ILocational target) {
		return Util.calculateAngleFrom(getRealX(), getRealY(), target.getRealX(), target.getRealY());
	}

	/**
	 * @return the heading to the target specified
	 */
	default int calculateHeadingTo(ILocational target) {
		return Util.calculateHeadingFrom(getRealX(), getRealY(), target.getRealX(), target.getRealY());
	}

	/**
	 * Calculates the angle in degrees from this object to the given object.<br>
	 * The return value can be described as how much this object has to turn<br>
	 * to have the given object directly in front of it.
	 *
	 * @param target the object to which to calculate the angle
	 * @return the angle this object has to turn to have the given object in front of it
	 */
	default double calculateDirectionTo(ILocational target) {
		int heading = Util.calculateHeadingFrom(getRealX(), getRealY(), target.getRealX(), target.getRealY()) - getRealHeading();
		if (heading < 0) {
			heading = 65535 + heading;
		}

		return Util.convertHeadingToDegree(heading);
	}

	/**
	 * Computes the 2D Euclidean distance between this locational and (x, y).
	 *
	 * @param x the x coordinate
	 * @param y the y coordinate
	 * @return the 2D Euclidean distance between this locational and (x, y)
	 */
	default double distance2d(double x, double y) {
		return Math.sqrt(Math.pow(getRealX() - x, 2) + Math.pow(getRealY() - y, 2));
	}

	/**
	 * Computes the 2D Euclidean distance between this locational and locational loc.
	 *
	 * @param target the locational
	 * @return the 2D Euclidean distance between this locational and locational loc
	 */
	default double distance2d(ILocational target) {
		return distance2d(target.getRealX(), target.getRealY());
	}

	/**
	 * Computes the 3D Euclidean distance between this locational and (x, y, z).
	 *
	 * @param x the x coordinate
	 * @param y the y coordinate
	 * @param z the z coordinate
	 * @return the 3D Euclidean distance between this locational and (x, y, z)
	 */
	default double distance3d(double x, double y, double z) {
		return Math.sqrt(Math.pow(getRealX() - x, 2) + Math.pow(getRealY() - y, 2) + Math.pow(getRealZ() - z, 2));
	}

	/**
	 * Computes the 3D Euclidean distance between this locational and locational loc.
	 *
	 * @param loc the locational
	 * @return the 3D Euclidean distance between this locational and locational loc
	 */
	default double distance3d(ILocational loc) {
		return distance3d(loc.getRealX(), loc.getRealY(), loc.getRealZ());
	}

	/**
	 * Checks if this locational is in 2D Euclidean radius of (x, y)
	 *
	 * @param x      the x coordinate
	 * @param y      the y coordinate
	 * @param radius the radius
	 * @return {@code true} if this locational is in radius of (x, y), {@code false} otherwise
	 */
	default boolean isInRadius2d(double x, double y, double radius) {
		return distance2d(x, y) <= radius;
	}

	/**
	 * Checks if this locational is in 2D Euclidean radius of locational loc
	 *
	 * @param loc    the locational
	 * @param radius the radius
	 * @return {@code true} if this locational is in radius of locational loc, {@code false} otherwise
	 */
	default boolean isInRadius2d(ILocational loc, double radius) {
		return isInRadius2d(loc.getRealX(), loc.getRealY(), radius);
	}

	/**
	 * Checks if this locational is in 3D Euclidean radius of (x, y, z)
	 *
	 * @param x      the x coordinate
	 * @param y      the y coordinate
	 * @param z      the z coordinate
	 * @param radius the radius
	 * @return {@code true} if this locational is in radius of (x, y, z), {@code false} otherwise
	 */
	default boolean isInRadius3d(double x, double y, double z, double radius) {
		return distance3d(x, y, z) <= radius;
	}

	/**
	 * Checks if this locational is in 3D Euclidean radius of locational loc
	 *
	 * @param loc    the locational
	 * @param radius the radius
	 * @return {@code true} if this locational is in radius of locational loc, {@code false} otherwise
	 */
	default boolean isInRadius3d(ILocational loc, double radius) {
		return isInRadius3d(loc.getRealX(), loc.getRealY(), loc.getRealZ(), radius);
	}
}
