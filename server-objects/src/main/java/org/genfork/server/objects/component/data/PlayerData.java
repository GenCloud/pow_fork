package org.genfork.server.objects.component.data;

import lombok.Data;
import org.genfork.server.enums.ActorStat;
import org.genfork.server.enums.ClassId;
import org.genfork.server.enums.Race;
import org.genfork.server.enums.WeaponType;
import org.genfork.server.location.Location;
import org.genfork.tools.common.Rnd;
import org.genfork.tools.data.StatsSet;

import java.io.Serializable;
import java.util.*;

import static org.genfork.server.enums.ActorStat.*;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Data
public class PlayerData implements IData, Serializable {
	private final Map<Integer, Integer> baseSlotDef = new HashMap<>(12);
	private final Map<ActorStat, Double> baseValues = new EnumMap<>(ActorStat.class);

	private final float[] baseHp = new float[122];
	private final float[] baseMp = new float[122];
	private final float[] baseCp = new float[122];

	private final double[] baseHpReg = new double[122];
	private final double[] baseMpReg = new double[122];
	private final double[] baseCpReg = new double[122];

	private double fCollisionRadius;
	private double fCollisionHeight;
	private double fCollisionHeightMale;
	private double fCollisionRadiusMale;
	private double fCollisionHeightFemale;
	private double fCollisionRadiusFemale;

	private int collisionRadius;
	private int collisionHeight;
	private int baseSafeFallHeight;

	private ClassId classId;
	private Race race;
	private WeaponType baseAttackType;

	private List<Location> creationPoints;

	public PlayerData(StatsSet set) {
		classId = ClassId.getClassId(set.getInt("class_id"));
		race = classId.getRace();

		baseSafeFallHeight = set.getInt("base_safe_fall");

		Arrays.stream(values())
				.filter(stat ->
						set.contains(stat.getValue()))
				.forEach(stat ->
						baseValues.put(stat, set.getDouble(stat.getValue())));

		fCollisionRadiusFemale = set.getDouble("collision_female_radius");
		fCollisionHeightFemale = set.getDouble("collision_female_height");

		fCollisionHeight = set.getDouble("collision_male_height", 0);
		fCollisionRadius = set.getDouble("collision_male_radius", 0);

		collisionRadius = (int) fCollisionRadius;
		collisionHeight = (int) fCollisionHeight;

		baseAttackType = set.getEnum("base_attack_type", WeaponType.class, WeaponType.FIST);

		creationPoints = set.getList("creation_points", Location.class);
	}

	/**
	 * Sets the value of level upgain parameter.
	 *
	 * @param paramName name of parameter
	 * @param level     corresponding character level
	 * @param val       value of parameter
	 */
	public void setUpGainValue(String paramName, int level, double val) {
		switch (paramName) {
			case "hp": {
				baseHp[level] = (float) val;
				break;
			}
			case "mp": {
				baseMp[level] = (float) val;
				break;
			}
			case "cp": {
				baseCp[level] = (float) val;
				break;
			}
			case "hpRegen": {
				baseHpReg[level] = val;
				break;
			}
			case "mpRegen": {
				baseMpReg[level] = val;
				break;
			}
			case "cpRegen": {
				baseCpReg[level] = val;
				break;
			}
		}
	}

	public Location getCreationPoint() {
		return creationPoints.get(Rnd.get(creationPoints.size()));
	}

	/**
	 * @param level character level to return value
	 * @return the baseHpMax for given character level
	 */
	public float getBaseHpMax(int level) {
		return baseHp[level];
	}

	/**
	 * @param level character level to return value
	 * @return the baseMpMax for given character level
	 */
	public float getBaseMpMax(int level) {
		return baseMp[level];
	}

	/**
	 * @param level character level to return value
	 * @return the baseCpMax for given character level
	 */
	public float getBaseCpMax(int level) {
		return baseCp[level];
	}

	/**
	 * @param level character level to return value
	 * @return the base HP Regeneration for given character level
	 */
	public double getBaseHpRegen(int level) {
		return baseHpReg[level];
	}

	/**
	 * @param level character level to return value
	 * @return the base MP Regeneration for given character level
	 */
	public double getBaseMpRegen(int level) {
		return baseMpReg[level];
	}

	/**
	 * @param level character level to return value
	 * @return the base HP Regeneration for given character level
	 */
	public double getBaseCpRegen(int level) {
		return baseCpReg[level];
	}

	/**
	 * @param slotId id of inventory slot to return value
	 * @return defence value of charactert for EMPTY given slot
	 */
	public int getBaseDefBySlot(int slotId) {
		return baseSlotDef.getOrDefault(slotId, 0);
	}

	/**
	 * @return the baseSTR
	 */
	public int getBaseSTR() {
		return baseValues.getOrDefault(STAT_STR, 0d).intValue();
	}

	/**
	 * @return the baseCON
	 */
	public int getBaseCON() {
		return baseValues.getOrDefault(STAT_CON, 0d).intValue();
	}

	/**
	 * @return the baseDEX
	 */
	public int getBaseDEX() {
		return baseValues.getOrDefault(STAT_DEX, 0d).intValue();
	}

	/**
	 * @return the baseINT
	 */
	public int getBaseINT() {
		return baseValues.getOrDefault(STAT_INT, 0d).intValue();
	}

	/**
	 * @return the baseWIT
	 */
	public int getBaseWIT() {
		return baseValues.getOrDefault(STAT_WIT, 0d).intValue();
	}

	/**
	 * @return the baseMEN
	 */
	public int getBaseMEN() {
		return baseValues.getOrDefault(STAT_MEN, 0d).intValue();
	}

	/**
	 * @return the baseLUC
	 */
	public int getBaseLUC() {
		return baseValues.getOrDefault(STAT_LUC, 0d).intValue();
	}

	/**
	 * @return the baseCHA
	 */
	public int getBaseCHA() {
		return baseValues.getOrDefault(STAT_CHA, 0d).intValue();
	}

	/**
	 * @return the baseHpMax
	 */
	public float getBaseHpMax() {
		return baseValues.getOrDefault(MAX_HP, 0d).floatValue();
	}

	/**
	 * @return the baseCpMax
	 */
	public float getBaseCpMax() {
		return baseValues.getOrDefault(MAX_CP, 0d).floatValue();
	}

	/**
	 * @return the baseMpMax
	 */
	public float getBaseMpMax() {
		return baseValues.getOrDefault(MAX_MP, 0d).floatValue();
	}

	/**
	 * @return the baseHpReg
	 */
	public float getBaseHpReg() {
		return baseValues.getOrDefault(REGENERATE_HP_RATE, 0d).floatValue();
	}

	/**
	 * @return the baseMpReg
	 */
	public float getBaseMpReg() {
		return baseValues.getOrDefault(REGENERATE_MP_RATE, 0d).floatValue();
	}

	/**
	 * @return the _baseFire
	 */
	public int getBaseFire() {
		return baseValues.getOrDefault(FIRE_POWER, 0d).intValue();
	}

	/**
	 * @return the _baseWind
	 */
	public int getBaseWind() {
		return baseValues.getOrDefault(WIND_POWER, 0d).intValue();
	}

	/**
	 * @return the _baseWater
	 */
	public int getBaseWater() {
		return baseValues.getOrDefault(WATER_POWER, 0d).intValue();
	}

	/**
	 * @return the _baseEarth
	 */
	public int getBaseEarth() {
		return baseValues.getOrDefault(EARTH_POWER, 0d).intValue();
	}

	/**
	 * @return the _baseHoly
	 */
	public int getBaseHoly() {
		return baseValues.getOrDefault(HOLY_POWER, 0d).intValue();
	}

	/**
	 * @return the _baseDark
	 */
	public int getBaseDark() {
		return baseValues.getOrDefault(DARK_POWER, 0d).intValue();
	}

	/**
	 * @return the _baseFireRes
	 */
	public double getBaseFireRes() {
		return baseValues.getOrDefault(FIRE_RES, 0d);
	}

	/**
	 * @return the _baseWindRes
	 */
	public double getBaseWindRes() {
		return baseValues.getOrDefault(WIND_RES, 0d);
	}

	/**
	 * @return the _baseWaterRes
	 */
	public double getBaseWaterRes() {
		return baseValues.getOrDefault(WATER_RES, 0d);
	}

	/**
	 * @return the _baseEarthRes
	 */
	public double getBaseEarthRes() {
		return baseValues.getOrDefault(EARTH_RES, 0d);
	}

	/**
	 * @return the _baseHolyRes
	 */
	public double getBaseHolyRes() {
		return baseValues.getOrDefault(HOLY_RES, 0d);
	}

	/**
	 * @return the _baseDarkRes
	 */
	public double getBaseDarkRes() {
		return baseValues.getOrDefault(DARK_RES, 0d);
	}

	/**
	 * @return the _baseElementRes
	 */
	public double getBaseElementRes() {
		return baseValues.getOrDefault(BASE_ATTRIBUTE_RES, 0d);
	}

	/**
	 * @return the basePAtk
	 */
	public int getBasePAtk() {
		return baseValues.getOrDefault(PHYSICAL_ATTACK, 0d).intValue();
	}

	/**
	 * @return the baseMAtk
	 */
	public int getBaseMAtk() {
		return baseValues.getOrDefault(MAGIC_ATTACK, 0d).intValue();
	}

	/**
	 * @return the basePDef
	 */
	public int getBasePDef() {
		return baseValues.getOrDefault(PHYSICAL_DEFENCE, 0d).intValue();
	}

	/**
	 * @return the baseMDef
	 */
	public int getBaseMDef() {
		return baseValues.getOrDefault(MAGICAL_DEFENCE, 0d).intValue();
	}

	/**
	 * @return the basePAtkSpd
	 */
	public int getBasePAtkSpd() {
		return baseValues.getOrDefault(PHYSICAL_ATTACK_SPEED, 0d).intValue();
	}

	/**
	 * @return the baseMAtkSpd
	 */
	public int getBaseMAtkSpd() {
		return baseValues.getOrDefault(MAGIC_ATTACK_SPEED, 0d).intValue();
	}

	/**
	 * @return the random damage
	 */
	public int getRandomDamage() {
		return baseValues.getOrDefault(RANDOM_DAMAGE, 0d).intValue();
	}

	/**
	 * @return the baseShldDef
	 */
	public int getBaseShldDef() {
		return baseValues.getOrDefault(SHIELD_DEFENCE, 0d).intValue();
	}

	/**
	 * @return the baseShldRate
	 */
	public int getBaseShldRate() {
		return baseValues.getOrDefault(SHIELD_DEFENCE_RATE, 0d).intValue();
	}

	/**
	 * @return the baseCritRate
	 */
	public int getBaseCritRate() {
		return baseValues.getOrDefault(PHYSICAL_CRITICAL_RATE, 0d).intValue();
	}

	/**
	 * @return the baseMCritRate
	 */
	public int getBaseMCritRate() {
		return baseValues.getOrDefault(MAGIC_CRITICAL_RATE, 0d).intValue();
	}

	/**
	 * @return the baseBreath
	 */
	public int getBaseBreath() {
		return baseValues.getOrDefault(BREATH, 0d).intValue();
	}

	/**
	 * @return base abnormal resist by basic property type.
	 */
	public int getBaseAbnormalResistPhysical() {
		return baseValues.getOrDefault(ABNORMAL_RESIST_PHYSICAL, 0d).intValue();
	}

	/**
	 * @return base abnormal resist by basic property type.
	 */
	public int getBaseAbnormalResistMagical() {
		return baseValues.getOrDefault(ABNORMAL_RESIST_MAGICAL, 0d).intValue();
	}

	@Override
	public double getBaseValue(ActorStat actorStat, double defaultValue) {
		return baseValues.getOrDefault(actorStat, 0d);
	}
}
