package org.genfork.server.objects.component.stat;

import lombok.Data;
import org.genfork.server.enums.*;
import org.genfork.server.model.pojo.StatsHolder;
import org.genfork.server.stats.ActorStatValue;

import java.io.Serializable;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;

/**
 * @author: ngolubenko@context-it.ru
 * @created: 2020/04
 */
@Data
public class ActorStatData implements Serializable {
	private final Set<BooleanStat> booleanStats = EnumSet.noneOf(BooleanStat.class);

	private final Set<Integer> blockActionsAllowedSkills = new HashSet<>();
	private final Set<Integer> blockActionsAllowedItems = new HashSet<>();

	private final Set<TraitType> attackTraits = EnumSet.noneOf(TraitType.class);
	private final Set<TraitType> defenceTraits = EnumSet.noneOf(TraitType.class);
	private final Set<TraitType> invulnerableTraits = EnumSet.noneOf(TraitType.class);

	private final Deque<StatsHolder> additionalAdd = new ConcurrentLinkedDeque<>();
	private final Deque<StatsHolder> additionalMul = new ConcurrentLinkedDeque<>();

	private final Map<ActorStat, ActorStatValue> doubleStats = new EnumMap<>(ActorStat.class);
	private final Map<ActorStat, Map<MoveType, Double>> moveTypeStats = new ConcurrentHashMap<>();

	private final Map<Integer, Double> reuseStat = new ConcurrentHashMap<>();
	private final Map<Integer, Double> castingStat = new HashMap<>();
	private final Map<Integer, Double> mpConsumeStat = new ConcurrentHashMap<>();
	private final Map<Integer, LinkedList<Double>> skillEvasionStat = new ConcurrentHashMap<>();

	private final Map<ActorStat, Map<Position, Double>> positionStats = new ConcurrentHashMap<>();
	private final Map<ActorStat, Double> fixedValue = new ConcurrentHashMap<>();

	private final float[] attackTraitValues = new float[TraitType.values().length];
	private final float[] defenceTraitValues = new float[TraitType.values().length];

	private int maxBuffCount = 20;
	private double vampiricSum = 0;
	private double supportItemBonusRate = 0;
	private double attackSpeedMultiplier = 1;
	private double mAttackSpeedMultiplier = 1;

	public void setAttackTraitValue(TraitType traitValue, float value) {
		attackTraitValues[traitValue.ordinal()] = value;
	}

	public float getAttackTrait(TraitType traitType) {
		return defenceTraitValues[traitType.ordinal()];
	}

	public void setDefenceTraitValue(TraitType traitValue, float value) {
		defenceTraitValues[traitValue.ordinal()] = value;
	}

	public float getDefenceTrait(TraitType traitType) {
		return defenceTraitValues[traitType.ordinal()];
	}
}
