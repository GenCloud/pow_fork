package org.genfork.server.objects.component.data;

import org.genfork.server.enums.ActorStat;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public interface IData {
	default double getBaseValue(ActorStat actorStat) {
		return getBaseValue(actorStat, 0.);
	}

	double getBaseValue(ActorStat actorStat, double defaultValue);
}
