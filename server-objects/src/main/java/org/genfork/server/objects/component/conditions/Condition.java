package org.genfork.server.objects.component.conditions;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.genfork.server.model.skills.Skill;
import org.genfork.server.objects.Actor;
import org.genfork.server.objects.component.items.ItemData;
import org.genfork.tools.data.StatsSet;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Data
@RequiredArgsConstructor
public abstract class Condition {
	private final StatsSet statsSet;
	private String message;
	private int messageId;
	private boolean addName = false;

	public final boolean test(Actor caster, Actor target, Skill skill) {
		return test(caster, target, skill, null);
	}

	public final boolean test(Actor caster, Actor target, Skill skill, ItemData item) {
		return testImpl(caster, target, skill, item);
	}

	/**
	 * Test the condition.
	 *
	 * @param effector the effector
	 * @param effected the effected
	 * @param skill    the skill
	 * @param item     the item
	 * @return {@code true} if successful, {@code false} otherwise
	 */
	public abstract boolean testImpl(Actor effector, Actor effected, Skill skill, ItemData item);
}
