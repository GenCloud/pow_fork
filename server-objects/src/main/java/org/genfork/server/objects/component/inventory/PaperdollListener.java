package org.genfork.server.objects.component.inventory;

import org.genfork.server.objects.Item;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public interface PaperdollListener {
	void notifyEquiped(int slot, Item item, ItemContainerData containerData);

	void notifyUnequiped(int slot, Item item, ItemContainerData containerData);
}
