package org.genfork.server.objects.component.state;

import lombok.RequiredArgsConstructor;
import org.genfork.server.objects.Actor;

import java.io.Serializable;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@RequiredArgsConstructor
public class ActorState implements Serializable {
	private final Actor actor;

	private double currentCp;
	private double currentHp;
	private double currentMp;
}
