package org.genfork.server.location;

import org.genfork.server.objects.component.ILocational;
import org.genfork.tools.data.StatsSet;

import java.io.Serializable;

public class Location implements ILocational, Serializable {
	private static final long serialVersionUID = 9053071440080626121L;

	private double x;
	private double y;
	private double z;
	private int heading;

	public Location(double x, double y, double z) {
		this(x, y, z, 0);
	}

	public Location(double x, double y, double z, int heading) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.heading = heading;
	}

	public Location(Location loc) {
		this(loc.getRealX(), loc.getRealY(), loc.getRealZ(), loc.getRealHeading());
	}

	public Location(StatsSet set) {
		x = set.getInt("x");
		y = set.getInt("y");
		z = set.getInt("z");
		heading = set.getInt("heading", 0);
	}

	@Override
	public int getRealX() {
		return (int) x;
	}

	@Override
	public int getRealY() {
		return (int) y;
	}

	@Override
	public int getRealZ() {
		return (int) z;
	}

	@Override
	public int getRealHeading() {
		return heading;
	}

	/**
	 * Set the x, y, z coordinates.
	 *
	 * @param x the x coordinate
	 * @param y the y coordinate
	 * @param z the z coordinate
	 */
	public void setXYZ(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	/**
	 * Set the x, y, z coordinates.
	 *
	 * @param loc The location.
	 */
	public void setXYZ(Location loc) {
		setXYZ(loc.getRealX(), loc.getRealY(), loc.getRealZ());
	}

	public void setLocation(Location loc) {
		x = loc.getRealX();
		y = loc.getRealY();
		z = loc.getRealZ();
		heading = loc.getRealHeading();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Location) {
			final Location loc = (Location) obj;
			return (int) getRealX() == (int) loc.getRealX()
					&& (int) getRealY() == (int) loc.getRealY()
					&& (int) getRealZ() == (int) loc.getRealZ()
					&& getRealHeading() == loc.getRealHeading();
		}
		return false;
	}

	@Override
	public String toString() {
		return "[" + getClass().getSimpleName() + "] X: " + getRealX() + " Y: " + getRealY() + " Z: " + getRealZ() + " Heading: " + heading;
	}
}
