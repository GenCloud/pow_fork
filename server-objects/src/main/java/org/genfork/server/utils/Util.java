package org.genfork.server.utils;

import lombok.experimental.UtilityClass;
import org.genfork.server.objects.WorldObject;
import org.genfork.server.objects.component.ILocational;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@UtilityClass
public class Util {
	/**
	 * @return degree value of object 2 to the horizontal line with object 1 being the origin
	 */
	public double calculateAngleFrom(double fromX, double fromY, double toX, double toY) {
		double angleTarget = Math.toDegrees(Math.atan2(toY - fromY, toX - fromX));
		if (angleTarget < 0) {
			angleTarget = 360 + angleTarget;
		}
		return angleTarget;
	}

	/**
	 * Calculates distance between one set of x, y, z and another set of x, y, z.
	 *
	 * @param x1           - X coordinate of first point.
	 * @param y1           - Y coordinate of first point.
	 * @param z1           - Z coordinate of first point.
	 * @param x2           - X coordinate of second point.
	 * @param y2           - Y coordinate of second point.
	 * @param z2           - Z coordinate of second point.
	 * @param includeZAxis - If set to true, Z coordinates will be included.
	 * @param squared      - If set to true, distance returned will be squared.
	 * @return {@code double} - Distance between object and given x, y , z.
	 */
	public double calculateDistance(double x1, double y1, double z1, double x2, double y2, double z2, boolean includeZAxis, boolean squared) {
		final double distance = Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2) + (includeZAxis ? Math.pow(z1 - z2, 2) : 0);
		return (squared) ? distance : Math.sqrt(distance);
	}

	/**
	 * Calculates distance between 2 locations.
	 *
	 * @param loc1         - First location.
	 * @param loc2         - Second location.
	 * @param includeZAxis - If set to true, Z coordinates will be included.
	 * @param squared      - If set to true, distance returned will be squared.
	 * @return {@code double} - Distance between object and given location.
	 */
	public double calculateDistance(ILocational loc1, ILocational loc2, boolean includeZAxis, boolean squared) {
		return calculateDistance(loc1.getRealX(), loc1.getRealY(), loc1.getRealZ(), loc2.getRealX(), loc2.getRealY(), loc2.getRealZ(), includeZAxis, squared);
	}

	public double convertHeadingToDegree(int clientHeading) {
		return clientHeading / 182.044444444;
	}

	public double convertHeadingToRadian(int clientHeading) {
		return (clientHeading / 65535.0) * 6.283185307179586;
	}

	/**
	 * Calculates heading from a given angle in degree following the retail-like formula:<br>
	 * <u>(unsigned short)(int)floor(radian * 65535.0 / 6.283185307179586)</u><br>
	 * <font color=red>It is highly suggested to use {@linkplain Util#convertRadianToHeading(double)} if you are working with radians. This method is not as accurate.</font>
	 *
	 * @param degree angle in degrees.
	 * @return heading in value range from 0 to 65535, where:
	 * <ul>
	 * <li>0° = 0 heading.</li>
	 * <li>90° = 16383(x3FFF) heading.</li>
	 * <li>180° = 32767(x7FFF) heading.</li>
	 * <li>-180° = 32769(x8001) heading.</li>
	 * <li>-90° = 49153(xC001) heading.</li>
	 * <li>-0.01° = 65535(xFFFF) heading.</li>
	 * </ul>
	 */
	public int convertDegreeToHeading(double degree) {
		return convertRadianToHeading(Math.toRadians(degree));
	}

	/**
	 * Calculates heading from a given angle in degree following the retail-like formula:<br>
	 * <u>(unsigned short)(int)floor(radian * 65535.0 / 6.283185307179586)</u>
	 *
	 * @param radians angle in radians.
	 * @return heading in value range from 0 to 65535, where:
	 * <ul>
	 * <li>0 = 0 heading.</li>
	 * <li>pi/2 = 16383(x3FFF) heading.</li>
	 * <li>pi = 32767(x7FFF) heading.</li>
	 * <li>-pi = 32768(x8000) heading.</li>
	 * <li>-pi/2 = 49152(xC000) heading.</li>
	 * <li>-0.00001 = 65535(xFFFF) heading.</li>
	 * </ul>
	 */
	public int convertRadianToHeading(double radians) {
		return Short.toUnsignedInt((short) Math.floor((radians * 65535.0) / 6.283185307179586));
	}

	/**
	 * Calculates heading from a point to a point following the retail-like formula:<br>
	 * <u>(unsigned short)(int)floor(atan2(toY - fromY, toX - fromX) * 65535.0 / 6.283185307179586)</u>
	 *
	 * @param fromX from X coord
	 * @param fromY from Y coord
	 * @param toX   to X coord
	 * @param toY   to Y coord
	 * @return heading in value range from 0 to 65535.
	 */
	public int calculateHeadingFrom(double fromX, double fromY, double toX, double toY) {
		return convertRadianToHeading(Math.atan2(toY - fromY, toX - fromX));
	}

	public int calculateHeadingFrom(double dx, double dy) {
		return convertRadianToHeading(Math.atan2(dy, dx));
	}

	/**
	 * Checks if object is within short (sqrt(int.max_value)) radius, not using collisionRadius. Faster calculation than checkIfInRange if distance is short and collisionRadius isn't needed. Not for long distance checks (potential teleports, far away castles etc).
	 *
	 * @param includeZAxis if true, check also Z axis (3-dimensional check), otherwise only 2D
	 * @return {@code true} if objects are within specified range between each other, {@code false} otherwise
	 */
	public static boolean checkIfInShortRange(int range, WorldObject obj1, WorldObject obj2, boolean includeZAxis) {
		if (obj1 == null || obj2 == null) {
			return false;
		}
		if (range == -1) {
			return true; // not limited
		}

		return includeZAxis ? obj1.isInRadius3d(obj2, range) : obj1.isInRadius2d(obj2, range);
	}

	/**
	 * Checks if the cube intersects the sphere.
	 *
	 * @param x1     the cube's first point x
	 * @param y1     the cube's first point y
	 * @param z1     the cube's first point z
	 * @param x2     the cube's second point x
	 * @param y2     the cube's second point y
	 * @param z2     the cube's second point z
	 * @param sX     the sphere's middle x
	 * @param sY     the sphere's middle y
	 * @param sZ     the sphere's middle z
	 * @param radius the sphere's radius
	 * @return {@code true} if cube intersects sphere, {@code false} otherwise
	 */
	public static boolean cubeIntersectsSphere(double x1, double y1, double z1, double x2, double y2, double z2, double sX, double sY, double sZ, int radius) {
		double d = radius * radius;
		if (sX < x1) {
			d -= Math.pow(sX - x1, 2);
		} else if (sX > x2) {
			d -= Math.pow(sX - x2, 2);
		}

		if (sY < y1) {
			d -= Math.pow(sY - y1, 2);
		} else if (sY > y2) {
			d -= Math.pow(sY - y2, 2);
		}

		if (sZ < z1) {
			d -= Math.pow(sZ - z1, 2);
		} else if (sZ > z2) {
			d -= Math.pow(sZ - z2, 2);
		}
		return d > 0;
	}
}
