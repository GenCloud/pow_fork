package org.genfork.server.dispatcher.listeners;

import lombok.extern.slf4j.Slf4j;
import org.genfork.server.dispatcher.interfaces.Event;
import org.genfork.server.dispatcher.interfaces.ListenerOwner;
import org.genfork.server.dispatcher.returns.AbstractEventReturn;
import org.genfork.server.enums.EventType;
import org.genfork.server.objects.component.containers.ListenersContainer;

import java.lang.reflect.Method;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Slf4j
public class AnnotationEventListener extends AbstractEventListener {
	private final Method method;

	public AnnotationEventListener(ListenersContainer container, EventType type, Method method, ListenerOwner owner, int priority) {
		super(container, type, owner);
		this.method = method;
		setPriority(priority);
	}

	@Override
	public <R extends AbstractEventReturn> R executeEvent(Event event, Class<R> returnBackClass) {
		try {
			if (!method.isAccessible()) {
				method.setAccessible(true);
			}

			final Object result = method.invoke(getListenerOwner(), event);
			method.setAccessible(false);
			if (method.getReturnType() == returnBackClass) {
				return returnBackClass.cast(result);
			}
		} catch (Exception e) {
			log.warn("Error while invoking {} on {}", method.getName(), getListenerOwner(), e);
		}
		return null;
	}
}
