package org.genfork.server.dispatcher.listeners;

import lombok.Data;
import lombok.Setter;
import org.genfork.server.dispatcher.interfaces.Event;
import org.genfork.server.dispatcher.interfaces.ListenerOwner;
import org.genfork.server.dispatcher.returns.AbstractEventReturn;
import org.genfork.server.enums.EventType;
import org.genfork.server.objects.component.containers.ListenersContainer;

import java.io.Serializable;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Data
public abstract class AbstractEventListener implements Comparable<AbstractEventListener>, Serializable {
	@Setter
	private ListenersContainer container;
	@Setter
	private EventType eventType;
	@Setter
	private ListenerOwner listenerOwner;

	private int priority = 0;

	public AbstractEventListener(ListenersContainer container, EventType eventType, ListenerOwner listenerOwner) {
		this.container = container;
		this.eventType = eventType;
		this.listenerOwner = listenerOwner;
	}

	/**
	 * Method invoked by EventDispatcher that will use the callback.
	 */
	public abstract <R extends AbstractEventReturn> R executeEvent(Event event, Class<R> returnBackClass);

	/**
	 * Unregisters detaches and unregisters current listener.
	 */
	public void unregisterMe() {
		getContainer().removeListener(this);
	}

	@Override
	public int compareTo(AbstractEventListener o) {
		return Integer.compare(o.getPriority(), getPriority());
	}
}
