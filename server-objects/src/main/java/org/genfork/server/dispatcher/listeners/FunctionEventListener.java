package org.genfork.server.dispatcher.listeners;

import lombok.extern.slf4j.Slf4j;
import org.genfork.server.dispatcher.interfaces.Event;
import org.genfork.server.dispatcher.interfaces.ListenerOwner;
import org.genfork.server.dispatcher.returns.AbstractEventReturn;
import org.genfork.server.enums.EventType;
import org.genfork.server.objects.component.containers.ListenersContainer;

import java.util.function.Function;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Slf4j
public class FunctionEventListener extends AbstractEventListener {
	private final Function<Event, ? extends AbstractEventReturn> function;

	public FunctionEventListener(ListenersContainer container, EventType type, Function<Event, ? extends AbstractEventReturn> function, ListenerOwner owner) {
		super(container, type, owner);
		this.function = function;
	}

	@Override
	public <R extends AbstractEventReturn> R executeEvent(Event event, Class<R> returnBackClass) {
		try {
			return returnBackClass.cast(function.apply(event));
		} catch (Exception e) {
			log.warn("Error while invoking {} on {}", event, getListenerOwner(), e);
		}
		return null;
	}
}
