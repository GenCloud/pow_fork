package org.genfork.server.dispatcher.interfaces;

import org.genfork.server.enums.EventType;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public abstract class Event {
	public abstract EventType getType();
}
