package org.genfork.server.dispatcher.listeners;

import org.genfork.server.dispatcher.interfaces.Event;
import org.genfork.server.dispatcher.interfaces.ListenerOwner;
import org.genfork.server.dispatcher.returns.AbstractEventReturn;
import org.genfork.server.enums.EventType;
import org.genfork.server.objects.component.containers.ListenersContainer;

import java.util.function.Consumer;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class ConsumerEventListener extends AbstractEventListener {
	private final Consumer<Event> consumer;

	@SuppressWarnings("unchecked")
	public ConsumerEventListener(ListenersContainer container, EventType type, Consumer<? extends Event> consumer, ListenerOwner owner) {
		super(container, type, owner);
		this.consumer = (Consumer<Event>) consumer;
	}

	@Override
	public <R extends AbstractEventReturn> R executeEvent(Event event, Class<R> returnBackClass) {
		consumer.accept(event);
		return null;
	}
}
