package org.genfork.server.dispatcher.returns;

import lombok.Data;
import lombok.RequiredArgsConstructor;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@RequiredArgsConstructor
@Data
public abstract class AbstractEventReturn {
	private final boolean override;
	private final boolean abort;
}
