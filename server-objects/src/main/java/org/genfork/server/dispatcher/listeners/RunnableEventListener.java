package org.genfork.server.dispatcher.listeners;

import org.genfork.server.dispatcher.interfaces.Event;
import org.genfork.server.dispatcher.interfaces.ListenerOwner;
import org.genfork.server.dispatcher.returns.AbstractEventReturn;
import org.genfork.server.enums.EventType;
import org.genfork.server.objects.component.containers.ListenersContainer;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class RunnableEventListener extends AbstractEventListener {
	private final Runnable callback;

	public RunnableEventListener(ListenersContainer container, EventType type, Runnable callback, ListenerOwner owner) {
		super(container, type, owner);
		this.callback = callback;
	}

	@Override
	public <R extends AbstractEventReturn> R executeEvent(Event event, Class<R> returnBackClass) {
		callback.run();
		return null;
	}
}
