package org.genfork.server.dispatcher.events;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.genfork.server.dispatcher.interfaces.Event;
import org.genfork.server.enums.EventType;
import org.genfork.tools.network.SessionInfo;

import static org.genfork.server.enums.EventType.ON_PLAYER_ENTER_WORLD;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@RequiredArgsConstructor
@Data
public class OnPlayerEnterWorld extends Event {
	private final SessionInfo sessionInfo;
	private final int[][] trace;

	@Override
	public EventType getType() {
		return ON_PLAYER_ENTER_WORLD;
	}
}
