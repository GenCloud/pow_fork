package org.genfork.server.stats.functions;

import org.genfork.server.enums.ActorStat;
import org.genfork.server.model.skills.Skill;
import org.genfork.server.objects.Actor;
import org.genfork.server.objects.component.conditions.Condition;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class FuncMul extends AbstractFunction {
	public FuncMul(ActorStat actorStat, int order, Object funcOwner, Condition applyCond, double value) {
		super(actorStat, order, funcOwner, applyCond, value);
	}

	@Override
	public double calc(Actor effector, Actor effected, Skill skill, double initVal) {
		final Condition applyCond = getApplyCond();
		if (applyCond == null || applyCond.test(effector, effected, skill)) {
			return initVal * getValue();
		}
		return initVal;
	}
}
