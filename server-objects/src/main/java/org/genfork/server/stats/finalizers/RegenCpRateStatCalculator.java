package org.genfork.server.stats.finalizers;

import org.genfork.server.enums.ActorStat;
import org.genfork.server.objects.Actor;
import org.genfork.server.stats.IStatCalculator;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class RegenCpRateStatCalculator implements IStatCalculator {
	@Override
	public double calc(Actor creature, double base, ActorStat stat) {
		return 1;
	}
}
