package org.genfork.server.stats.functions;

import lombok.Data;
import org.genfork.server.enums.ActorStat;
import org.genfork.server.enums.StatFunction;
import org.genfork.server.model.skills.Skill;
import org.genfork.server.objects.Actor;
import org.genfork.server.objects.component.conditions.Condition;

import java.io.Serializable;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Data
public class FuncTemplate implements Serializable {
	private final Class<?> functionClass;
	private final Condition attachCond;
	private final Condition applyCond;
	private final ActorStat actorStat;
	private final int order;
	private final double value;

	public FuncTemplate(Condition attachCond, Condition applyCond, String functionName, int order, ActorStat actorStat, double value) {
		final StatFunction function = StatFunction.valueOf(functionName.toUpperCase());
		if (order >= 0) {
			this.order = order;
		} else {
			this.order = function.getOrder();
		}

		this.attachCond = attachCond;
		this.applyCond = applyCond;
		this.actorStat = actorStat;
		this.value = value;

		try {
			functionClass = Class.forName(AbstractFunction.class.getPackage().getName() + ".Func" + function.getName());
		} catch (ClassNotFoundException e) {
			throw new RuntimeException(e);
		}
	}

	public boolean meetCondition(Actor effected, Skill skill) {
		if (attachCond != null && !attachCond.test(effected, effected, skill)) {
			return false;
		}

		return applyCond == null || applyCond.test(effected, effected, skill);
	}
}
