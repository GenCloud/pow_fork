/*
 * Copyright (C) 2004-2015 L2J Unity
 *
 * This file is part of L2J Unity.
 *
 * L2J Unity is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * L2J Unity is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package org.genfork.server.stats.functions;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.genfork.server.enums.ActorStat;
import org.genfork.server.model.skills.Skill;
import org.genfork.server.objects.Actor;
import org.genfork.server.objects.component.conditions.Condition;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@RequiredArgsConstructor
@Data
public abstract class AbstractFunction {
	/**
	 * Statistics, that is affected by this function (See Creature.CALCULATOR_XXX constants)
	 */
	private final ActorStat actorStat;
	/**
	 * Order of functions calculation.<br>
	 * Functions with lower order are executed first.<br>
	 * Functions with the same order are executed in unspecified order.<br>
	 * Usually add/subtract functions has lowest order,<br>
	 * then bonus/penalty functions (multiply/divide) are applied, then functions that do more complex<br>
	 * calculations (non-linear functions).
	 */
	private final int order;
	/**
	 * Owner can be an armor, weapon, skill, system event, quest, etc.<br>
	 * Used to remove all functions added by this owner.
	 */
	private final Object funcOwner;
	/**
	 * Function may be disabled by attached condition.
	 */
	private final Condition applyCond;
	/**
	 * The value.
	 */
	private final double value;

	public abstract double calc(Actor effector, Actor effected, Skill skill, double initVal);
}
