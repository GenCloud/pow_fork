package org.genfork.server.stats;

import lombok.Data;
import org.genfork.server.enums.ActorStat;

import java.io.Serializable;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Data
public class ActorStatValue implements Serializable {
	private double add;
	private double mul;
	private double markedAdd;
	private double markedMul;

	public ActorStatValue(ActorStat doubleStat) {
		reset(doubleStat);
	}

	/**
	 * Resets this double stat value.
	 *
	 * @param doubleStat the double stat
	 */
	public void reset(ActorStat doubleStat) {
		add = doubleStat.getResetAddValue();
		mul = doubleStat.getResetMulValue();
	}

	/**
	 * Marks this double stat value so changes can be detected.
	 */
	public void mark() {
		markedAdd = add;
		markedMul = mul;
	}

	/**
	 * Checks if this double stat value has changed since it was marked.
	 *
	 * @return {@code true} if this double stat value has changed since it was marked, {@code false} otherwise
	 */
	public boolean hasChanged() {
		return add != markedAdd || mul != markedMul;
	}
}
