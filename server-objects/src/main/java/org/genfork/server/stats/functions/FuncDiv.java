package org.genfork.server.stats.functions;

import lombok.extern.slf4j.Slf4j;
import org.genfork.server.enums.ActorStat;
import org.genfork.server.model.skills.Skill;
import org.genfork.server.objects.Actor;
import org.genfork.server.objects.component.conditions.Condition;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Slf4j
public class FuncDiv extends AbstractFunction {
	public FuncDiv(ActorStat actorStat, int order, Object funcOwner, Condition applyCond, double value) {
		super(actorStat, order, funcOwner, applyCond, value);
	}

	@Override
	public double calc(Actor effector, Actor effected, Skill skill, double initVal) {
		final Condition condition = getApplyCond();
		if (condition == null || condition.test(effector, effected, skill)) {
			final double value = getValue();
			try {
				return initVal / value;
			} catch (Exception e) {
				log.warn("Division by zero: {}!", value);
			}
		}
		return initVal;
	}
}
