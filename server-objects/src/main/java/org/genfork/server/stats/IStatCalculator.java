package org.genfork.server.stats;

import org.genfork.server.enums.ActorStat;
import org.genfork.server.objects.Actor;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@FunctionalInterface
public interface IStatCalculator {
	double calc(Actor creature, double base, ActorStat stat);
}
