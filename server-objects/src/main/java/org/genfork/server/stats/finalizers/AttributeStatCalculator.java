package org.genfork.server.stats.finalizers;

import org.genfork.server.enums.ActorStat;
import org.genfork.server.enums.AttributeType;
import org.genfork.server.objects.Actor;
import org.genfork.server.stats.IStatCalculator;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class AttributeStatCalculator implements IStatCalculator {
	private final AttributeType attributeType;
	private final boolean weapon;

	public AttributeStatCalculator(AttributeType attributeType, boolean weapon) {
		this.attributeType = attributeType;
		this.weapon = weapon;
	}

	@Override
	public double calc(Actor creature, double base, ActorStat stat) {
		return 1;
	}
}
