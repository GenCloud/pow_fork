package org.genfork.registry;

import lombok.extern.slf4j.Slf4j;
import org.genfork.registry.async.RegistryThreadPoolInitializer;
import org.genfork.registry.config.ConfigMarker;
import org.genfork.tools.common.CommonUtil;
import org.genfork.tools.loader.AppLoader;
import org.genfork.tools.loader.Loader;
import org.genfork.tools.loader.interfaces.*;
import org.genfork.tools.management.ShutdownManager;
import org.genfork.tools.management.TerminationStatus;
import org.genfork.tools.path.BasePathProvider;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.concurrent.CompletableFuture;

import static org.genfork.tools.loader.AppLoader.ApplicationMode.REGISTRY;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Slf4j
@SpringBootApplication
public class AppInit {
	public static void main(String[] args) {
		AppLoader.defaultInit(REGISTRY, ConfigMarker.class.getPackage().getName(), RegistryThreadPoolInitializer.class);

		final Loader loader = new Loader(AppInit.class.getPackage().getName());
		CompletableFuture<Void> sideLoadCompletableFuture;
		try {
			sideLoadCompletableFuture = loader.runAsync(SideLoadGroup.class);
			loader.runAsync(PreLoadGroup.class, ClientAccessLoadGroup.class, LoadGroup.class).join();
		} catch (Exception e) {
			log.warn("", e);
			ShutdownManager.halt(TerminationStatus.RUNTIME_INITIALIZATION_FAILURE);
			return;
		}

		try {
			sideLoadCompletableFuture.join();
		} catch (Exception e) {
			log.warn("", e);
		}

		try {
			loader.writeDependencyTreeToFile(Files.createDirectories(BasePathProvider.resolvePath(Paths.get("log", "loader-stats"))).resolve(LocalDateTime.now().format(CommonUtil.getFilenameDateTimeFormatter()) + ".txt"), SideLoadGroup.class, PreLoadGroup.class, LoadGroup.class, ScriptLoadGroup.class, ClientAccessLoadGroup.class, PostLoadGroup.class);
		} catch (Exception e) {
			log.warn("", e);
		}

		final SpringApplication app = new SpringApplication(AppInit.class);
		app.setBannerMode(Banner.Mode.OFF);
		app.run(args);
	}
}
