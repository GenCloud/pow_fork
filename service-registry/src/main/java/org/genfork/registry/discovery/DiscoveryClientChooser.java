package org.genfork.registry.discovery;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.genfork.registry.model.InstanceInfo;
import org.genfork.registry.network.handler.DiscoveryClientHandler;

import javax.annotation.Nullable;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Slf4j
public class DiscoveryClientChooser {
	@Getter(lazy = true)
	private static final DiscoveryClientChooser instance = new DiscoveryClientChooser();

	private final ConcurrentMap<String, AtomicInteger> clientCounters = new ConcurrentHashMap<>();
	private final ConcurrentMap<String, AtomicInteger> channelCounters = new ConcurrentHashMap<>();

	public void addCounter(String clientName) {
		clientCounters.putIfAbsent(clientName, new AtomicInteger(0));
	}

	public void removeCounter(String clientName) {
		clientCounters.remove(clientName);
	}

	@Nullable
	public DiscoveryClientHandler chooseClient(String clientName) {
		final List<InstanceInfo> clients = DiscoveryManager.getInstance().getClient(clientName);

		final AtomicInteger value = clientCounters.get(clientName);
		if (value == null) {
			log.error("Unable find client by name: {}", clientName);
			return null;
		}
		final int counter = value.incrementAndGet();

		if (!clients.isEmpty()) {
			if (counter >= clients.size()) {
				value.set(0);

				return chooseChannel(clients.get(0));
			} else {
				return chooseChannel(clients.get(counter));
			}
		}

		return null;
	}

	private DiscoveryClientHandler chooseChannel(InstanceInfo instanceInfo) {
		AtomicInteger value;
		if (!channelCounters.containsKey(instanceInfo.getClientName())) {
			value = new AtomicInteger(0);
			channelCounters.put(instanceInfo.getClientName(), value);
		} else {
			value = channelCounters.get(instanceInfo.getClientName());
		}

		final int counter = value.incrementAndGet();

		final List<DiscoveryClientHandler> handlers = instanceInfo.getHandlers();
		if (handlers != null && !handlers.isEmpty()) {
			if (counter >= handlers.size()) {
				value.set(0);
				return handlers.get(0);
			} else {
				return handlers.get(counter);
			}
		}

		return null;
	}
}
