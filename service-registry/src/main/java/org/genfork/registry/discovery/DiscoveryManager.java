package org.genfork.registry.discovery;

import io.netty.channel.Channel;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.genfork.registry.config.ServerConfig;
import org.genfork.registry.model.InstanceInfo;
import org.genfork.registry.network.handler.DiscoveryClientHandler;
import org.genfork.registry.network.packets.send.DiscoveryClientHealthCheckPacket;
import org.genfork.tools.async.ThreadPool;
import org.genfork.tools.common.Rnd;
import org.genfork.tools.network.ChannelInboundHandler;

import java.util.*;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import static java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;
import static java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;
import static org.genfork.registry.network.enums.ConnectionState.REGISTERED;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Slf4j
public class DiscoveryManager {
	@Getter(lazy = true)
	private static final DiscoveryManager instance = new DiscoveryManager();
	private final Map<String, List<InstanceInfo>> clients = new HashMap<>();
	private final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
	private final ReadLock readLock = lock.readLock();
	private final WriteLock writeLock = lock.writeLock();

	private ScheduledFuture<?> healthCheckTask;

	public Map<String, List<InstanceInfo>> getAllClients() {
		return Collections.unmodifiableMap(clients);
	}

	public List<InstanceInfo> getClient(String clientName) {
		List<InstanceInfo> instanceInfos = Collections.emptyList();

		readLock.lock();
		try {
			final boolean existsClients = clients.containsKey(clientName);
			if (existsClients) {
				instanceInfos = clients.get(clientName);
			}
		} finally {
			readLock.unlock();
		}

		return instanceInfos;
	}

	private boolean existsClient(String clientName, String clientIp, int clientPort) {
		final boolean existsClients = clients.containsKey(clientName);
		if (existsClients) {
			final List<InstanceInfo> instanceInfos = clients.get(clientName);
			return instanceInfos.stream().anyMatch(i -> i.getClientIp().equals(clientIp) && i.getClientPort() == clientPort);
		}

		return false;
	}

	public void addClient(DiscoveryClientHandler handler, String clientName, String clientSecretKey, String clientIp, int clientPort, int rootClientPort) {
		writeLock.lock();
		try {
			if (existsClient(clientName, clientIp, clientPort)) {
				final List<InstanceInfo> instanceInfos = clients.get(clientName);
				instanceInfos.stream()
						.filter(i ->
								i.getClientIp().equals(clientIp) && i.getClientPort() == clientPort)
						.findFirst()
						.ifPresent(i -> i.getHandlers().add(handler));

				handler.setConnectionState(REGISTERED);
				return;
			}

			final InstanceInfo info = new InstanceInfo();
			info.setClientName(clientName);
			info.setClientSecretKey(clientSecretKey);
			info.setClientIp(clientIp);
			info.setClientPort(clientPort);
			info.setRootClientPort(rootClientPort);
			info.getHandlers().add(handler);

			if (clients.containsKey(clientName)) {
				final List<InstanceInfo> instanceInfos = clients.get(clientName);
				instanceInfos.add(info);
			} else {
				clients.put(clientName, new ArrayList<>(Collections.singletonList(info)));
			}

			if (healthCheckTask == null) {
				startHealthTask();
			}

			handler.setConnectionState(REGISTERED);

			log.info("Register client in registry [{}] : [{}:{}, {}]", clientName, clientIp, clientPort, rootClientPort);
		} finally {
			writeLock.unlock();
		}
	}

	public void removeClient(String clientId) {
		writeLock.lock();
		try {
			clients.keySet()
					.stream()
					.map(clients::get)
					.forEach(infos ->
							infos.forEach(i ->
									i.getHandlers().removeIf(i0 -> i0.getUuid().toString().equals(clientId))));
		} finally {
			writeLock.unlock();
		}
	}

	public void addHealthStatus(String clientName, long time) {
		writeLock.lock();
		try {
			clients.keySet()
					.stream()
					.map(clients::get)
					.flatMap(Collection::stream)
					.filter(i ->
							i.getClientName().equals(clientName))
					.findFirst()
					.ifPresent(c -> {
						final long startCheckPingTime = c.getStartCheckPingTime();
						c.setStartCheckPingTime(0);
						final int currentPing = (int) (time - startCheckPingTime);
						c.setPing(currentPing);
					});

		} finally {
			writeLock.unlock();
		}
	}

	private void startHealthTask() {
		healthCheckTask = ThreadPool.scheduleAtFixedRate(() -> {
			readLock.lock();
			try {
				final Set<String> keys = clients.keySet();
				keys.forEach(clientName -> {
					final List<InstanceInfo> instances = clients.get(clientName);
					new ArrayList<>(instances).forEach(i -> {
						final List<DiscoveryClientHandler> handlers = i.getHandlers();
						if (!handlers.isEmpty()) {
							final ChannelInboundHandler<?> handler = handlers.get(Rnd.get(handlers.size()));
							final Channel channel = handler.getChannel();
							if (channel != null && channel.isActive()) {
								i.setStartCheckPingTime(System.currentTimeMillis());
								handler.sendPacket(new DiscoveryClientHealthCheckPacket());
							} else {
								log.warn("Removing client by inactive channel - {}", i);
								instances.remove(i);
							}
						}
					});
				});
			} finally {
				readLock.unlock();
			}
		}, 1, ServerConfig.HEALTH_CHECK_TIME, TimeUnit.SECONDS);
	}
}
