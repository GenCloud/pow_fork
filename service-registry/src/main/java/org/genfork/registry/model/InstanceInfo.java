package org.genfork.registry.model;

import lombok.Data;
import org.genfork.registry.network.handler.DiscoveryClientHandler;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Data
public class InstanceInfo implements Serializable {
	private final long startedTime = System.currentTimeMillis();

	private final List<DiscoveryClientHandler> handlers = new ArrayList<>();

	private String clientName;
	private String clientSecretKey;
	private String clientIp;
	private int clientPort;
	private int rootClientPort;

	private long startCheckPingTime;
	private long endCheckPingTime;

	private long bytesReceived;
	private long bytesSent;

	private int ping;
}
