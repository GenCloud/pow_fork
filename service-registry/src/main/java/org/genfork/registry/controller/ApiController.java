package org.genfork.registry.controller;

import org.genfork.registry.discovery.DiscoveryManager;
import org.genfork.registry.model.InstanceInfo;
import org.genfork.tools.network.ChannelInboundHandler;
import org.genfork.tools.network.ConnectionStats;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@RestController
@RequestMapping("/servers")
public class ApiController {
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<InstanceInfo>> listServers() {
		final Map<String, List<InstanceInfo>> allClients = DiscoveryManager.getInstance().getAllClients();
		final List<InstanceInfo> collect = allClients.values().stream().flatMap(Collection::stream).collect(Collectors.toList());
		return new ResponseEntity<>(collect, HttpStatus.OK);
	}

	@GetMapping("/{clientName}/{clientIp}/{clientPort}/stats")
	public ResponseEntity<InstanceInfo> getStats(@PathVariable("clientName") String clientName,
												 @PathVariable("clientIp") String clientIp,
												 @PathVariable("clientPort") int clientPort) {

		final List<InstanceInfo> instanceInfos = DiscoveryManager.getInstance().getClient(clientName);
		if (instanceInfos != null) {
			final InstanceInfo instanceInfo = instanceInfos
					.stream()
					.filter(i ->
							i.getClientIp().equals(clientIp) && i.getClientPort() == clientPort)
					.findFirst()
					.orElse(null);

			if (instanceInfo != null) {
				final List<ConnectionStats> collect = instanceInfo
						.getHandlers()
						.stream().map(ChannelInboundHandler::getConnectionStats)
						.collect(Collectors.toList());
				collect.stream().mapToLong(s -> s.getBytesReceived().get()).max().ifPresent(instanceInfo::setBytesReceived);
				collect.stream().mapToLong(s -> s.getBytesSent().get()).max().ifPresent(instanceInfo::setBytesSent);
			}

			return new ResponseEntity<>(instanceInfo, HttpStatus.OK);
		}

		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
}
