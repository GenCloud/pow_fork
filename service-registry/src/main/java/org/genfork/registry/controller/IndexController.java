package org.genfork.registry.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Controller
@RequestMapping("/")
public class IndexController {
	@GetMapping
	public String main() {
		return "index.html";
	}
}
