package org.genfork.registry.config;

import org.genfork.tools.config.abstraction.ConfigPropertiesLoader;
import org.genfork.tools.config.annotation.ConfigClass;
import org.genfork.tools.config.annotation.ConfigField;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@ConfigClass(fileName = "Server")
public class ServerConfig extends ConfigPropertiesLoader {
	@ConfigField(name = "ServerHost", value = "127.0.0.1")
	public static String SERVER_HOST;

	@ConfigField(name = "ServerPort", value = "5000")
	public static int SERVER_PORT;

	@ConfigField(name = "ClientHealthCheckTime", value = "10")
	public static int HEALTH_CHECK_TIME;

	@ConfigField(name = "ServerKey", value = "{bcrypt}$2a$10$QLekRpQ7ib/LoQzaok7wBuNbLAklR5mQWMop8oH8k4PBmB8yCXHIO")
	public static String SERVER_PRIVATE_KEY;
}
