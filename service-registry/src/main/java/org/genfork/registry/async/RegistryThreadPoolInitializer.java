package org.genfork.registry.async;

import org.genfork.tools.async.IThreadPoolInitializer;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class RegistryThreadPoolInitializer implements IThreadPoolInitializer {
	@Override
	public int getScheduledThreadPoolSize() {
		return Runtime.getRuntime().availableProcessors() * 4;
	}

	@Override
	public int getThreadPoolSize() {
		return Runtime.getRuntime().availableProcessors() * 2;
	}
}
