package org.genfork.registry.network.handler;

import io.netty.channel.ChannelHandlerContext;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.genfork.registry.discovery.DiscoveryManager;
import org.genfork.tools.network.ChannelInboundHandler;
import org.genfork.tools.network.IncomingPacket;
import org.genfork.tools.network.OutgoingPacket;

import java.net.InetAddress;
import java.net.InetSocketAddress;

import static org.genfork.registry.network.enums.ConnectionState.CONNECTED;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Slf4j
public class DiscoveryClientHandler extends ChannelInboundHandler<DiscoveryClientHandler> {
	@Getter
	private InetAddress inetAddress;

	@Override
	public void channelActive(ChannelHandlerContext ctx) {
		super.channelActive(ctx);

		setConnectionState(CONNECTED);

		final InetSocketAddress address = (InetSocketAddress) ctx.channel().remoteAddress();
		inetAddress = address.getAddress();
	}

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, IncomingPacket<DiscoveryClientHandler> packet) throws Exception {
		try {
			connectionStats.appendBytesReceived(packet.getByteBuf().capacity());

			packet.run(this);
		} catch (Exception ex) {
			log.warn("Exception for: {} on packet.run: {}", toString(), packet.getClass().getSimpleName(), ex);
		}
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
		log.warn("Network exception caught for: {} : {}", toString(), cause.getLocalizedMessage());

		DiscoveryManager.getInstance().removeClient(getUuid().toString());
	}

	public void closeNow() {
		if (channel != null) {
			channel.close();
		}
	}

	public void sendPacket(OutgoingPacket packet) {
		if (packet == null) {
			return;
		}

		channel.writeAndFlush(packet);
	}
}
