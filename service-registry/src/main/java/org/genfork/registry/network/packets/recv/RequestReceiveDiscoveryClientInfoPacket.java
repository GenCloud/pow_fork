package org.genfork.registry.network.packets.recv;

import lombok.extern.slf4j.Slf4j;
import org.genfork.registry.discovery.DiscoveryClientChooser;
import org.genfork.registry.discovery.DiscoveryManager;
import org.genfork.registry.network.handler.DiscoveryClientHandler;
import org.genfork.tools.network.IncomingPacket;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Slf4j
public class RequestReceiveDiscoveryClientInfoPacket extends IncomingPacket<DiscoveryClientHandler> {
	private String clientName;
	private String clientSecretKey;
	private String clientIp;
	private int clientPort;
	private int rootClientPort;

	@Override
	public boolean read(DiscoveryClientHandler client) {
		clientName = readS();
		clientSecretKey = readS();
		clientIp = readS();
		clientPort = readD();
		rootClientPort = readD();

		return true;
	}

	@Override
	public void run(DiscoveryClientHandler client) throws Exception {
		DiscoveryManager.getInstance().addClient(client, clientName, clientSecretKey, clientIp, clientPort, rootClientPort);
		DiscoveryClientChooser.getInstance().addCounter(clientName);
	}
}
