package org.genfork.registry.network.packets.recv;

import org.genfork.registry.discovery.DiscoveryManager;
import org.genfork.registry.network.handler.DiscoveryClientHandler;
import org.genfork.tools.network.IncomingPacket;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class RequestReceiveDiscoveryHealthCheckPacket extends IncomingPacket<DiscoveryClientHandler> {
	private String clientName;

	@Override
	public boolean read(DiscoveryClientHandler client) {
		clientName = readS();
		return true;
	}

	@Override
	public void run(DiscoveryClientHandler client) throws Exception {
		final long endCheckPingTime = System.currentTimeMillis();
		DiscoveryManager.getInstance().addHealthStatus(clientName, endCheckPingTime);
	}
}
