package org.genfork.registry.network;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.traffic.GlobalTrafficShapingHandler;
import org.genfork.registry.async.EventLoopGroupData;
import org.genfork.registry.network.enums.DiscoveryClientIncomingPackets;
import org.genfork.registry.network.handler.DiscoveryClientHandler;
import org.genfork.tools.network.codecs.LengthFieldBasedFrameEncoder;
import org.genfork.tools.network.codecs.PacketDecoder;
import org.genfork.tools.network.codecs.PacketEncoder;

import java.nio.ByteOrder;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class DiscoveryClientInitializer extends ChannelInitializer<SocketChannel> {
	private static final LengthFieldBasedFrameEncoder LENGTH_ENCODER = new LengthFieldBasedFrameEncoder();

	@Override
	protected void initChannel(SocketChannel ch) {
		final DiscoveryClientHandler clientHandler = new DiscoveryClientHandler();
		ch.pipeline().addLast("length-decoder", new LengthFieldBasedFrameDecoder(ByteOrder.LITTLE_ENDIAN, 1024000 - 4, 0, 4, -4, 4, false));
		ch.pipeline().addLast("length-encoder", LENGTH_ENCODER);
//		ch.pipeline().addLast(new LoggingHandler(LogLevel.INFO));
		ch.pipeline().addLast("packet-decoder", new PacketDecoder<>(DiscoveryClientIncomingPackets.PACKET_ARRAY, clientHandler));
		ch.pipeline().addLast("packet-encoder", new PacketEncoder(clientHandler, 1024000 - 4));
		ch.pipeline().addLast(new GlobalTrafficShapingHandler(EventLoopGroupData.getInstance().getWorkerGroup().next()));
		ch.pipeline().addLast(clientHandler);
	}
}
