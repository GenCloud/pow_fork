package org.genfork.registry.network.enums;

import org.genfork.registry.network.handler.DiscoveryClientHandler;
import org.genfork.registry.network.packets.recv.RequestClientDataPacket;
import org.genfork.registry.network.packets.recv.RequestReceiveDiscoveryClientInfoPacket;
import org.genfork.registry.network.packets.recv.RequestReceiveDiscoveryHealthCheckPacket;
import org.genfork.tools.network.IConnectionState;
import org.genfork.tools.network.IIncomingPackets;
import org.genfork.tools.network.IncomingPacket;
import org.genfork.tools.network.opcodes.ClientsBranchOpcodes;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Supplier;

import static org.genfork.registry.network.enums.ConnectionState.CONNECTED;
import static org.genfork.registry.network.enums.ConnectionState.REGISTERED;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public enum DiscoveryClientIncomingPackets implements IIncomingPackets<DiscoveryClientHandler> {
	REQUEST_REGISTER_DISCOVERY_PACKET(ClientsBranchOpcodes.DISCOVERY_CLIENT_PACKET.getId1(), RequestReceiveDiscoveryClientInfoPacket::new, CONNECTED),
	REQUEST_DISCOVERY_HEALTH_CHECK(ClientsBranchOpcodes.CLIENT_SEND_HEALTH_CHECK.getId1(), RequestReceiveDiscoveryHealthCheckPacket::new, REGISTERED),

	REQUEST_CLIENT_DATA_PACKET(ClientsBranchOpcodes.DISCOVERY_RETRANSMIT_DATA.getId1(), RequestClientDataPacket::new, REGISTERED);
	public static final DiscoveryClientIncomingPackets[] PACKET_ARRAY;

	static {
		final short maxPacketId = (short) Arrays.stream(values()).mapToInt(IIncomingPackets::getPacketId).max().orElse(0);
		PACKET_ARRAY = new DiscoveryClientIncomingPackets[maxPacketId + 1];
		for (DiscoveryClientIncomingPackets incomingPacket : values()) {
			PACKET_ARRAY[incomingPacket.getPacketId()] = incomingPacket;
		}
	}

	private short packetId;
	private Supplier<IncomingPacket<DiscoveryClientHandler>> incomingPacketFactory;
	private Set<IConnectionState> connectionStates;

	DiscoveryClientIncomingPackets(int packetId, Supplier<IncomingPacket<DiscoveryClientHandler>> incomingPacketFactory, IConnectionState... connectionStates) {
		// packetId is an unsigned byte
		if (packetId > 0xFF) {
			throw new IllegalArgumentException("packetId must not be bigger than 0xFF");
		}

		this.packetId = (short) packetId;
		this.incomingPacketFactory = incomingPacketFactory != null ? incomingPacketFactory : () -> null;
		this.connectionStates = new HashSet<>(Arrays.asList(connectionStates));
	}

	@Override
	public int getPacketId() {
		return packetId;
	}

	@Override
	public IncomingPacket<DiscoveryClientHandler> newIncomingPacket() {
		return incomingPacketFactory.get();
	}

	@Override
	public Set<IConnectionState> getConnectionStates() {
		return connectionStates;
	}

	@Override
	public IIncomingPackets<DiscoveryClientHandler>[] getArray() {
		return PACKET_ARRAY;
	}
}
