package org.genfork.registry.network.packets.recv;

import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;
import org.genfork.registry.discovery.DiscoveryClientChooser;
import org.genfork.registry.network.handler.DiscoveryClientHandler;
import org.genfork.tools.network.IncomingPacket;
import org.genfork.tools.registry.network.send.SendClientDataPacket;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Slf4j
public class RequestClientDataPacket extends IncomingPacket<DiscoveryClientHandler> {
	private String clientName;
	private String originClientName;

	private boolean fromPlayer;

	private int authKeyFirst;
	private int authKeySecond;
	private int playKeyFirst;
	private int playKeySecond;

	private ByteBuf byteBuf;

	@Override
	public boolean read(DiscoveryClientHandler client) {
		clientName = readS();
		originClientName = readS();

		fromPlayer = readC() == 1;

		if (fromPlayer) {
			authKeyFirst = readD();
			authKeySecond = readD();
			playKeyFirst = readD();
			playKeySecond = readD();
		}

		byteBuf = readB();
		return true;
	}

	@Override
	public void run(DiscoveryClientHandler client) throws Exception {
		final DiscoveryClientHandler handler = DiscoveryClientChooser.getInstance().chooseClient(clientName);
		if (handler != null) {

			if (fromPlayer) {
				handler.sendPacket(new SendClientDataPacket(clientName, originClientName, authKeyFirst, authKeySecond, playKeyFirst, playKeySecond, byteBuf));
			} else {
				handler.sendPacket(new SendClientDataPacket(clientName, originClientName, byteBuf));
			}

			if (log.isDebugEnabled()) {
				log.debug("Retransmit data to server: to - {}, from - {}, id -> {}, ip -> {}", clientName, originClientName, handler.getChannel().id(), handler.getInetAddress().toString());
			}
		} else {
			log.error("Handler not found for client: {}", clientName);
		}

		byteBuf.retain();
	}
}
