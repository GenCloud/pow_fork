package org.genfork.registry.network;

import lombok.Getter;
import org.genfork.registry.async.EventLoopGroupData;
import org.genfork.registry.config.ServerConfig;
import org.genfork.registry.network.handler.DiscoveryClientHandler;
import org.genfork.tools.loader.annotations.InstanceGetter;
import org.genfork.tools.loader.annotations.Load;
import org.genfork.tools.loader.interfaces.ClientAccessLoadGroup;
import org.genfork.tools.network.ServerNetworkController;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class DiscoveryServerNetworkController extends ServerNetworkController<DiscoveryClientHandler> {
	@Getter(lazy = true, onMethod = @__(@InstanceGetter))
	private static final DiscoveryServerNetworkController instance = new DiscoveryServerNetworkController();

	public DiscoveryServerNetworkController() {
		super(EventLoopGroupData.getInstance().getBossGroup(),
				EventLoopGroupData.getInstance().getWorkerGroup(),
				new DiscoveryClientInitializer(),
				ServerConfig.SERVER_HOST,
				ServerConfig.SERVER_PORT);
	}

	@Load(group = ClientAccessLoadGroup.class)
	private void load() throws InterruptedException {
		start();
	}
}
