package org.genfork.registry.network.packets.send;

import org.genfork.tools.network.OutgoingPacket;

import static org.genfork.tools.network.opcodes.ClientsBranchOpcodes.DISCOVERY_CLIENT_HEALTH_CHECK;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class DiscoveryClientHealthCheckPacket extends OutgoingPacket {
	@Override
	public boolean write() {
		DISCOVERY_CLIENT_HEALTH_CHECK.writeId(this);
		return true;
	}
}
