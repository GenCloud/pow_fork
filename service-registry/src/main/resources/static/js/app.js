var proxy = angular.module('proxy', ['ngRoute', 'ui.materialize', 'pageslide-directive', 'chart.js'])

proxy.config(function ($routeProvider, $httpProvider) {
    $routeProvider.when('/', {
        templateUrl: 'views/home.html',
        controller: 'home'
    }).when('/proxy/:clientName/:clientIp/:clientPort', {
        templateUrl: 'views/proxy.html',
        controller: 'proxy'
    }).when('/proxy/delete/:id', {
        templateUrl: 'views/home.html',
        controller: 'home'
    });

    $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
});