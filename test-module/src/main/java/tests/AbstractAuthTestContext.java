package tests;

import lombok.extern.slf4j.Slf4j;
import org.genfork.async.ClientThreadPoolInitializer;
import org.genfork.config.ConfigMarker;
import org.genfork.config.TestsConfig;
import org.genfork.model.TestClassData;
import org.genfork.tools.console.ConsoleUtil;
import org.genfork.tools.data.Pair;
import org.genfork.tools.loader.AppLoader;
import org.genfork.tools.loader.Loader;
import org.genfork.tools.loader.annotations.After;
import org.genfork.tools.loader.annotations.Before;
import org.genfork.tools.loader.annotations.Test;
import org.genfork.tools.loader.interfaces.*;
import org.genfork.tools.management.ShutdownManager;
import org.genfork.tools.management.TerminationStatus;
import org.reflections.Reflections;
import org.springframework.util.Assert;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import static org.genfork.tools.loader.AppLoader.ApplicationMode.LOGIN;

/**
 * @author: ngolubenko@context-it.ru
 * @created: 2020/04
 */
@Slf4j
public abstract class AbstractAuthTestContext extends Assert {
	private static final String CLASS_DELIMITER = "#";
	private static final String METHOD_DELIMITER = "\\|";

	private List<TestClassData> tests = Collections.emptyList();

	public AbstractAuthTestContext() {
		initialize();
	}

	private void initialize() {
		AppLoader.defaultInit(LOGIN, ConfigMarker.class.getPackage().getName(), ClientThreadPoolInitializer.class);

		final Loader loader = new Loader(getClass().getPackage().getName());
		CompletableFuture<Void> sideLoadCompletableFuture;
		try {
			sideLoadCompletableFuture = loader.runAsync(SideLoadGroup.class);
			loader.runAsync(PreLoadGroup.class, LoadGroup.class, ScriptLoadGroup.class, ClientAccessLoadGroup.class, PostLoadGroup.class).join();
		} catch (Exception e) {
			log.warn("", e);
			ShutdownManager.halt(TerminationStatus.RUNTIME_INITIALIZATION_FAILURE);
			return;
		}

		try {
			sideLoadCompletableFuture.join();
		} catch (Exception e) {
			log.warn("", e);
		}

		final List<Pair<String, List<String>>> exclusions = parseExclusions();

		final Reflections reflection = new Reflections("tests");
		tests = reflection.getTypesAnnotatedWith(Test.class)
				.stream()
				.sorted((Comparator.comparing(Class::getSimpleName)))
				.map(t -> {
					try {
						final String typeName = t.getName();
						final Pair<String, List<String>> exclusion = exclusions
								.stream()
								.filter(p -> p.getKey().equals(typeName))
								.findFirst()
								.orElse(null);

						final TestClassData data = new TestClassData();

						if (exclusion != null) {
							final List<String> exclusionMethods = exclusion.getValue();
							if (exclusionMethods.isEmpty()) {
								return null;
							}

							Arrays
									.stream(t.getDeclaredMethods())
									.filter(method ->
											(method.isAnnotationPresent(Test.class) || method.isAnnotationPresent(Before.class) || method.isAnnotationPresent(After.class))
													&& exclusionMethods.stream().noneMatch(s -> method.getName().equals(s)))
									.sorted((Comparator.comparing(Method::getName)))
									.forEach(method ->
											data.getTestMethods().add(method));

						} else {
							Arrays
									.stream(t.getDeclaredMethods())
									.filter(method ->
											method.isAnnotationPresent(Test.class) || method.isAnnotationPresent(Before.class) || method.isAnnotationPresent(After.class))
									.sorted((Comparator.comparing(Method::getName)))
									.forEach(method ->
											data.getTestMethods().add(method));
						}

						if (data.getTestMethods().isEmpty()) {
							log.warn("Test class [{}] does not have test methods. Ignore.", t.getSimpleName());
							return null;
						}

						final Object instance = t.getConstructors()[0].newInstance();
						data.setTestInstance(instance);
						return data;
					} catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
						log.error("Error initialize test object: {}, reason: {}", t.getSimpleName(), e.getLocalizedMessage());
					}

					return null;
				})
				.filter(Objects::nonNull)
				.collect(Collectors.toList());

		log.info("Tests initialized count {}", tests.size());

		if (tests.isEmpty()) {
			log.warn("Tests not found in context. Shutdown");
			ShutdownManager.exit(TerminationStatus.RUNTIME_UNCAUGHT_ERROR);
		}

		startTests();

		ShutdownManager.exit(TerminationStatus.ENVIRONMENT_SUPERUSER);
	}

	private void startTests() {
		tests.forEach(t -> {
			final Object testInstance = t.getTestInstance();

			ConsoleUtil.printSection("Start Test " + testInstance.getClass().getSimpleName());

			final List<Method> testMethods = t.getTestMethods();
			final List<Method> beforeMethods = testMethods.stream().filter(m -> m.isAnnotationPresent(Before.class)).collect(Collectors.toList());
			final List<Method> mainMethods = testMethods.stream().filter(m -> m.isAnnotationPresent(Test.class)).collect(Collectors.toList());
			final List<Method> afterMethods = testMethods.stream().filter(m -> m.isAnnotationPresent(After.class)).collect(Collectors.toList());

			execMethods(testInstance, beforeMethods);
			execMethods(testInstance, mainMethods);
			execMethods(testInstance, afterMethods);

			ConsoleUtil.printSection("End Test " + testInstance.getClass().getSimpleName());
		});
	}

	private void execMethods(Object instance, List<Method> methods) {
		methods.forEach(method -> {
			try {
				log.info("Exec method: {}", method.getName());
				final long startExec = System.nanoTime();
				method.invoke(instance);
				printExecTime(startExec);
			} catch (Exception ex) {
				log.error("Could not evaluate test! Method: [{}]", method.getName(), ex);
				ShutdownManager.exit(TerminationStatus.RUNTIME_UNCAUGHT_ERROR);
			}
		});
	}

	private void printExecTime(long startExec) {
		final long elapsedTime = System.nanoTime() - startExec;
		final StringBuilder sb = new StringBuilder();

		long seconds = elapsedTime / 1000000000;
		final long days = seconds / (3600 * 24);
		append(sb, days, "d");

		seconds -= (days * 3600 * 24);

		final long hours = seconds / 3600;
		append(sb, hours, "h");

		seconds -= (hours * 3600);

		final long minutes = seconds / 60;
		append(sb, minutes, "m");

		seconds -= (minutes * 60);
		append(sb, seconds, "s");

		final long nanos = elapsedTime % 1000000000;
		append(sb, nanos, "ns");
		log.info("Execution time: {}", sb.toString());
	}

	private List<Pair<String, List<String>>> parseExclusions() {
		final String[] excludedClasses = TestsConfig.EXCLUDED_CLASSES;

		return Arrays
				.stream(excludedClasses)
				.map(s -> {
					final String[] args = s.split(CLASS_DELIMITER);
					final String className = args[0];

					List<String> methods = Collections.emptyList();
					if (args.length > 1) {
						methods = Arrays.stream(args[1].split(METHOD_DELIMITER)).collect(Collectors.toList());
					}

					return Pair.of(className, methods);
				})
				.collect(Collectors.toList());
	}

	private void append(StringBuilder sb, long value, String text) {
		if (value > 0) {
			if (sb.length() > 0) {
				sb.append(" ");
			}

			sb.append(value).append(text);
		}
	}
}
