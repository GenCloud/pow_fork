package tests.auth;

import lombok.extern.slf4j.Slf4j;
import org.genfork.manager.QueuePacketManager;
import org.genfork.network.AuthClientHandler;
import org.genfork.network.AuthClientNetworkManager;
import org.genfork.network.recv.ReceiveAuthFailPacket;
import org.genfork.network.recv.ReceiveAuthInitPacket;
import org.genfork.network.recv.ReceiveAuthSuccessPacket;
import org.genfork.network.recv.ReceiveGameGuardPacket;
import org.genfork.network.send.AuthGameGuardPacket;
import org.genfork.network.send.AuthLoginPacket;
import org.genfork.tools.loader.annotations.Before;
import org.genfork.tools.loader.annotations.Test;
import org.genfork.tools.network.IncomingPacket;
import org.genfork.tools.network.opcodes.AuthPlayerBranchOpcodes;
import org.junit.jupiter.api.Assertions;

import java.security.PublicKey;
import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * @author: ngolubenko@context-it.ru
 * @created: 2020/04
 */
@Test
@Slf4j
public class CredentialsTest {
	@Before
	public void init() throws InterruptedException {
		AuthClientNetworkManager.getInstance().load();
	}

	@Test
	public void t01_testAccountExists() throws Exception {
		final CompletableFuture<ReceiveAuthInitPacket> initPacketFuture = CompletableFuture
				.supplyAsync(() ->
						QueuePacketManager.getInstance().getPacketData((short) AuthPlayerBranchOpcodes.INIT_PACKET.getId1()));

		final ReceiveAuthInitPacket initPacket = initPacketFuture.get();

		final byte[] scrambledModulus = initPacket.getScrambledModulus();
		final byte[] blowfish = initPacket.getBlowfish();
		final int connectionId = initPacket.getConnectionId();
		final int guardDataFirst = initPacket.getGuardDataFirst();
		final int guardDataSecond = initPacket.getGuardDataSecond();
		final int guardDataThird = initPacket.getGuardDataThird();
		final int guardDataFourth = initPacket.getGuardDataFourth();

		Assertions.assertTrue(scrambledModulus.length > 0);
		Assertions.assertTrue(blowfish.length > 0);

		final AuthClientHandler client = initPacket.getClient();

		final PublicKey rsaPubKey = AuthTestController.parseScrambledModulus(scrambledModulus);
		client.setPublicKey(rsaPubKey);
		client.getAuthCrypt().enableCrypt(blowfish);

		client.sendPacket(new AuthGameGuardPacket(connectionId, guardDataFirst, guardDataSecond, guardDataThird, guardDataFourth));

		final CompletableFuture<ReceiveGameGuardPacket> ggPacketFuture = CompletableFuture
				.supplyAsync(() ->
						QueuePacketManager.getInstance().getPacketData((short) AuthPlayerBranchOpcodes.AUTH_GAME_GUARD_PACKET.getId1()));

		final ReceiveGameGuardPacket guardPacket = ggPacketFuture.get();
		Assertions.assertEquals(guardPacket.getConnectionId(), connectionId);
		Assertions.assertEquals(guardPacket.getGuardDataFirst(), guardDataFirst);
		Assertions.assertEquals(guardPacket.getGuardDataSecond(), guardDataSecond);
		Assertions.assertEquals(guardPacket.getGuardDataThird(), guardDataThird);
		Assertions.assertEquals(guardPacket.getGuardDataFourth(), guardDataFourth);

		final byte[] userData = AuthTestController.createUserData(client);
		client.sendPacket(new AuthLoginPacket(connectionId, userData));

		final CompletableFuture<List<IncomingPacket<?>>> packetList = CompletableFuture
				.supplyAsync(() ->
						QueuePacketManager.getInstance().getPacketsData(p ->
								p.getKey() == AuthPlayerBranchOpcodes.AUTH_SUCCESS_PACKET.getId1()
										|| p.getKey() == AuthPlayerBranchOpcodes.AUTH_FAIL_PACKET.getId1()));

		final List<IncomingPacket<?>> incomingPackets = packetList.get();
		Assertions.assertFalse(incomingPackets.stream().anyMatch(p -> p instanceof ReceiveAuthFailPacket));
		Assertions.assertTrue(incomingPackets.stream().anyMatch(p -> p instanceof ReceiveAuthSuccessPacket));
	}
}
