package tests.auth;

import lombok.experimental.UtilityClass;
import org.genfork.config.ClientConfig;
import org.genfork.network.AuthClientHandler;
import org.genfork.network.utils.CryptoUtils;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.ShortBufferException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAKeyGenParameterSpec;
import java.security.spec.RSAPublicKeySpec;

/**
 * @author: ngolubenko@context-it.ru
 * @created: 2020/04
 */
@UtilityClass
public class AuthTestController {
	public PublicKey parseScrambledModulus(byte[] scrambledModulus) {
		scrambledModulus = CryptoUtils.encryptXorModulus(scrambledModulus);

		final KeyFactory keyFactory = CryptoUtils.getRsaKeyFactory();

		final BigInteger modulus = new BigInteger(1, scrambledModulus);
		final RSAPublicKeySpec rsaPublicKeySpec = new RSAPublicKeySpec(modulus, RSAKeyGenParameterSpec.F4);
		try {
			return keyFactory.generatePublic(rsaPublicKeySpec);
		} catch (InvalidKeySpecException e) {
			throw new RuntimeException(e);
		}
	}

	public byte[] createUserData(AuthClientHandler client) throws InvalidKeyException, BadPaddingException, ShortBufferException, IllegalBlockSizeException {
		final Cipher cipher = CryptoUtils.getRsaCipher();
		cipher.init(Cipher.ENCRYPT_MODE, client.getPublicKey());

		final byte[] username = String.format("%-14s", ClientConfig.USERNAME).getBytes(StandardCharsets.US_ASCII);
		final byte[] password = String.format("%-16s", ClientConfig.PASSWORD).getBytes(StandardCharsets.US_ASCII);

		final byte[] raw = new byte[128];
		System.arraycopy(username, 0, raw, 94, 14);
		System.arraycopy(password, 0, raw, 108, 16);

		final byte[] encrypted = new byte[128];
		cipher.doFinal(raw, 0, 128, encrypted, 0);

		return encrypted;
	}
}
