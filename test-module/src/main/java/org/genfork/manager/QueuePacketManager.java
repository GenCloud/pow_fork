package org.genfork.manager;

import lombok.Getter;
import org.genfork.tools.data.Pair;
import org.genfork.tools.network.IncomingPacket;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * @author: ngolubenko@context-it.ru
 * @created: 2020/04
 */
public class QueuePacketManager {
	@Getter(lazy = true)
	private static final QueuePacketManager instance = new QueuePacketManager();

	private final ConcurrentMap<Pair<Short, Short>, IncomingPacket<?>> queue = new ConcurrentHashMap<>();

	public void addPacket(short firstOpcode, IncomingPacket<?> packet) {
		queue.put(Pair.of(firstOpcode, (short) 0x00), packet);
	}

	public void addPacket(short firstOpcode, short secondOpcode, IncomingPacket<?> packet) {
		queue.put(Pair.of(firstOpcode, secondOpcode), packet);
	}

	public boolean hasPacket(short firstOpcode) {
		return queue.containsKey(Pair.of(firstOpcode, (short) 0x00));
	}

	public boolean hasPacket(short firstOpcode, short secondOpcode) {
		return queue.containsKey(Pair.of(firstOpcode, secondOpcode));
	}

	@SuppressWarnings("unchecked")
	public <O extends IncomingPacket<?>> List<O> getPacketsData(Predicate<Pair<Short, Short>> predicate) {
		List<Pair<Short, Short>> exists;
		do {
			exists = queue.keySet().stream().filter(predicate).collect(Collectors.toList());
			if (exists.isEmpty()) {
				try {
					TimeUnit.MILLISECONDS.sleep(5);
				} catch (InterruptedException e) {
					throw new RuntimeException(e);
				}
			}
		} while (exists.isEmpty());

		return exists
				.stream()
				.map(p -> {
					final O packet = (O) queue.get(p);
					queue.remove(p);
					return packet;
				})
				.collect(Collectors.toList());
	}

	@SuppressWarnings("unchecked")
	public <O extends IncomingPacket<?>> O getPacketData(short firstOpcode) {
		boolean exists;
		do {
			exists = hasPacket(firstOpcode);
			if (!exists) {
				try {
					TimeUnit.MILLISECONDS.sleep(5);
				} catch (InterruptedException e) {
					throw new RuntimeException(e);
				}
			}
		} while (!exists);

		final O packet = (O) queue.get(Pair.of(firstOpcode, (short) 0x00));
		queue.remove(Pair.of(firstOpcode, (short) 0x00));
		return packet;
	}

	@SuppressWarnings("unchecked")
	public <O extends IncomingPacket<?>> O getPacketData(short firstOpcode, short secondOpcode) {
		boolean exists;
		do {
			exists = hasPacket(firstOpcode, secondOpcode);
			if (!exists) {
				try {
					TimeUnit.MILLISECONDS.sleep(5);
				} catch (InterruptedException e) {
					throw new RuntimeException(e);
				}
			}
		} while (!exists);

		final O packet = (O) queue.get(Pair.of(firstOpcode, secondOpcode));
		queue.remove(Pair.of(firstOpcode, secondOpcode));
		return packet;
	}
}
