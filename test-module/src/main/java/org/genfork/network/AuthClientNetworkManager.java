package org.genfork.network;

import lombok.Getter;
import org.genfork.async.EventLoopGroupData;
import org.genfork.config.ClientConfig;
import org.genfork.tools.network.ClientNetworkController;

/**
 * @author: ngolubenko@context-it.ru
 * @created: 2020/04
 */
public class AuthClientNetworkManager extends ClientNetworkController<AuthClientHandler> {
	@Getter(lazy = true)
	private static final AuthClientNetworkManager instance = new AuthClientNetworkManager();

	public AuthClientNetworkManager() {
		super(EventLoopGroupData.getInstance().getBossGroup(),
				new AuthClientInitializer(),
				ClientConfig.AUTH_SERVER_HOST,
				ClientConfig.AUTH_SERVER_PORT);
	}

	public void load() throws InterruptedException {
		start(false);
	}
}
