package org.genfork.network;

import org.genfork.network.recv.ReceiveAuthFailPacket;
import org.genfork.network.recv.ReceiveAuthInitPacket;
import org.genfork.network.recv.ReceiveAuthSuccessPacket;
import org.genfork.network.recv.ReceiveGameGuardPacket;
import org.genfork.tools.network.IConnectionState;
import org.genfork.tools.network.IIncomingPackets;
import org.genfork.tools.network.IncomingPacket;
import org.genfork.tools.network.opcodes.AuthPlayerBranchOpcodes;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Supplier;

import static org.genfork.enums.ConnectionState.CONNECTED;

/**
 * @author: ngolubenko@context-it.ru
 * @created: 2020/04
 */
public enum AuthIncomingPackets implements IIncomingPackets<AuthClientHandler> {
	RECEIVE_INIT(AuthPlayerBranchOpcodes.INIT_PACKET.getId1(), ReceiveAuthInitPacket::new, CONNECTED),
	RECEIVE_GG(AuthPlayerBranchOpcodes.AUTH_GAME_GUARD_PACKET.getId1(), ReceiveGameGuardPacket::new, CONNECTED),
	RECEIVE_AUTH_SUCCESS(AuthPlayerBranchOpcodes.AUTH_SUCCESS_PACKET.getId1(), ReceiveAuthSuccessPacket::new, CONNECTED),
	RECEIVE_AUTH_FAIL(AuthPlayerBranchOpcodes.AUTH_FAIL_PACKET.getId1(), ReceiveAuthFailPacket::new, CONNECTED),

	;

	public static final AuthIncomingPackets[] PACKET_ARRAY;

	static {
		final short maxPacketId = (short) Arrays.stream(values()).mapToInt(IIncomingPackets::getPacketId).max().orElse(0);
		PACKET_ARRAY = new AuthIncomingPackets[maxPacketId + 1];
		Arrays.stream(values()).forEach(incomingPacket -> PACKET_ARRAY[incomingPacket.getPacketId()] = incomingPacket);
	}

	private final short packetId;
	private final Supplier<IncomingPacket<AuthClientHandler>> incomingPacketFactory;
	private final Set<IConnectionState> connectionStates;

	AuthIncomingPackets(int packetId, Supplier<IncomingPacket<AuthClientHandler>> incomingPacketFactory, IConnectionState... connectionStates) {
		// packetId is an unsigned byte
		if (packetId > 0xFF) {
			throw new IllegalArgumentException("packetId must not be bigger than 0xFF");
		}

		this.packetId = (short) packetId;
		this.incomingPacketFactory = incomingPacketFactory != null ? incomingPacketFactory : () -> null;
		this.connectionStates = new HashSet<>(Arrays.asList(connectionStates));
	}

	@Override
	public int getPacketId() {
		return packetId;
	}

	public Supplier<IncomingPacket<AuthClientHandler>> getIncomingPacketFactory() {
		return incomingPacketFactory;
	}

	@Override
	public IncomingPacket<AuthClientHandler> newIncomingPacket() {
		return incomingPacketFactory.get();
	}

	@Override
	public Set<IConnectionState> getConnectionStates() {
		return connectionStates;
	}

	@Override
	public IIncomingPackets<AuthClientHandler>[] getArray() {
		return PACKET_ARRAY;
	}
}
