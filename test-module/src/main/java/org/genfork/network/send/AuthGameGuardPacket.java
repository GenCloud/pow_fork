package org.genfork.network.send;

import lombok.RequiredArgsConstructor;
import org.genfork.tools.network.OutgoingPacket;

/**
 * @author: ngolubenko@context-it.ru
 * @created: 2020/04
 */
@RequiredArgsConstructor
public class AuthGameGuardPacket extends OutgoingPacket {
	private final int connectionId;
	private final int guardDataFirst, guardDataSecond, guardDataThird, guardDataFourth;

	@Override
	public boolean write() {
		writeC(0x07); //opcode
		writeD(connectionId);
		writeD(guardDataFirst);
		writeD(guardDataSecond);
		writeD(guardDataThird);
		writeD(guardDataFourth);
		return true;
	}
}
