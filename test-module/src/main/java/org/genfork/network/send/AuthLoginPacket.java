package org.genfork.network.send;

import lombok.RequiredArgsConstructor;
import org.genfork.tools.network.OutgoingPacket;

/**
 * @author: ngolubenko@context-it.ru
 * @created: 2020/04
 */
@RequiredArgsConstructor
public class AuthLoginPacket extends OutgoingPacket {
	private final int connectionId;
	private final byte[] raw;

	@Override
	public boolean write() {
		writeC(0x00); //opcode

		writeB(raw);
		writeD(connectionId);

		writeD(0x29DD954E); // UNKNOWN GAMEGUARD
		writeD(0x77C39CFC); // UNKNOWN GAMEGUARD
		writeD(0x97ADB620); // UNKNOWN GAMEGUARD
		writeD(0x07BDE0F7); // UNKNOWN GAMEGUARD
		return true;
	}
}
