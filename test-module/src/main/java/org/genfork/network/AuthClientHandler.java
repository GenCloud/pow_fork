package org.genfork.network;

import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.genfork.manager.QueuePacketManager;
import org.genfork.network.crypt.AuthCrypt;
import org.genfork.tools.network.ChannelInboundHandler;
import org.genfork.tools.network.IncomingPacket;

import java.security.PublicKey;

import static org.genfork.enums.ConnectionState.CONNECTED;

/**
 * @author: ngolubenko@context-it.ru
 * @created: 2020/04
 */
@Slf4j
@ChannelHandler.Sharable
public class AuthClientHandler extends ChannelInboundHandler<AuthClientHandler> {
	@Getter
	private final AuthCrypt authCrypt = new AuthCrypt();

	@Getter
	@Setter
	private PublicKey publicKey;

	@Override
	public void channelActive(ChannelHandlerContext ctx) {
		super.channelActive(ctx);

		setConnectionState(CONNECTED);
	}

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, IncomingPacket<AuthClientHandler> packet) throws Exception {
		try {
			packet.run(this);

			QueuePacketManager.getInstance().addPacket(packet.getFirstOpcode(), packet);
		} catch (Exception ex) {
			log.warn("Exception for: {} on packet.run: {}", toString(), packet.getClass().getSimpleName(), ex);
		}
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
		log.warn("Network exception caught for: {} : {}", toString(), cause.getLocalizedMessage());
	}
}
