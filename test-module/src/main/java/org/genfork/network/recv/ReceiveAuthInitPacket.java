package org.genfork.network.recv;

import lombok.Getter;
import org.genfork.network.AuthClientHandler;
import org.genfork.tools.network.IncomingPacket;

/**
 * @author: ngolubenko@context-it.ru
 * @created: 2020/04
 */
public class ReceiveAuthInitPacket extends IncomingPacket<AuthClientHandler> {
	@Getter
	private int connectionId;
	@Getter
	private byte[] blowfish;
	@Getter
	private byte[] scrambledModulus;
	@Getter
	private int protocolRevision;
	@Getter
	private int guardDataFirst, guardDataSecond, guardDataThird, guardDataFourth;

	@Override
	public boolean read(AuthClientHandler client) {
		connectionId = readD(); // connection id
		protocolRevision = readD();// protocol revision
		scrambledModulus = readB(128); // RSA Public Key
		guardDataFirst = readD(); // UNKNOWN GAMEGUARD
		guardDataSecond = readD(); // UNKNOWN GAMEGUARD
		guardDataThird = readD(); // UNKNOWN GAMEGUARD
		guardDataFourth = readD(); // UNKNOWN GAMEGUARD
		blowfish = readB(16);
		readC();
		return true;
	}

	@Override
	public void run(AuthClientHandler client) throws Exception {
	}
}
