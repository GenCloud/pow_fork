package org.genfork.network.recv;

import lombok.Getter;
import org.genfork.network.AuthClientHandler;
import org.genfork.tools.network.IncomingPacket;

/**
 * @author: ngolubenko@context-it.ru
 * @created: 2020/04
 */
public class ReceiveAuthSuccessPacket extends IncomingPacket<AuthClientHandler> {
	@Getter
	private int authKeyFirst;
	@Getter
	private int authKeySecond;
	@Getter
	private int playKeyFirst;
	@Getter
	private int playKeySecond;

	@Override
	public boolean read(AuthClientHandler client) {
		authKeyFirst = readD();
		authKeySecond = readD();
		playKeyFirst = readD();
		playKeySecond = readD();
		return true;
	}

	@Override
	public void run(AuthClientHandler client) throws Exception {

	}
}
