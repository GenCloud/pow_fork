package org.genfork.network.recv;

import org.genfork.network.AuthClientHandler;
import org.genfork.tools.network.IncomingPacket;

/**
 * @author: ngolubenko@context-it.ru
 * @created: 2020/04
 */
public class ReceiveAuthFailPacket extends IncomingPacket<AuthClientHandler> {
	private short code;

	@Override
	public boolean read(AuthClientHandler client) {
		code = readC();
		return true;
	}

	@Override
	public void run(AuthClientHandler client) throws Exception {

	}
}
