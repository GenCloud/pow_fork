package org.genfork.network.recv;

import lombok.Getter;
import org.genfork.network.AuthClientHandler;
import org.genfork.tools.network.IncomingPacket;

/**
 * @author: ngolubenko@context-it.ru
 * @created: 2020/04
 */
public class ReceiveGameGuardPacket extends IncomingPacket<AuthClientHandler> {
	@Getter
	private int connectionId;
	@Getter
	private int guardDataFirst, guardDataSecond, guardDataThird, guardDataFourth;

	@Override
	public boolean read(AuthClientHandler client) {
		connectionId = readD();
		guardDataFirst = readD(); // UNKNOWN GAMEGUARD
		guardDataSecond = readD(); // UNKNOWN GAMEGUARD
		guardDataThird = readD(); // UNKNOWN GAMEGUARD
		guardDataFourth = readD(); // UNKNOWN GAMEGUARD
		return true;
	}

	@Override
	public void run(AuthClientHandler client) throws Exception {
	}
}
