package org.genfork.network.crypt;

import io.netty.buffer.ByteBuf;
import org.genfork.network.utils.CryptoUtils;
import org.genfork.tools.crypto.controller.BlowfishController;
import org.genfork.tools.network.ICrypt;

import java.nio.ByteOrder;

/**
 * @author: ngolubenko@context-it.ru
 * @created: 2020/04
 */
public class AuthCrypt implements ICrypt {
	private static final byte[] STATIC_BLOWFISH_KEY =
			{
					(byte) 0x6b,
					(byte) 0x60,
					(byte) 0xcb,
					(byte) 0x5b,
					(byte) 0x82,
					(byte) 0xce,
					(byte) 0x90,
					(byte) 0xb1,
					(byte) 0xcc,
					(byte) 0x2b,
					(byte) 0x6c,
					(byte) 0x55,
					(byte) 0x6c,
					(byte) 0x6c,
					(byte) 0x6c,
					(byte) 0x6c
			};

	private static final BlowfishController STATIC_BLOWFISH_ENGINE = new BlowfishController();

	static {
		STATIC_BLOWFISH_ENGINE.init(STATIC_BLOWFISH_KEY);
	}

	private final BlowfishController blowfishController = new BlowfishController();
	private boolean staticKey = true;

	public void enableCrypt(byte[] key) {
		blowfishController.init(key);
	}

	@Override
	@SuppressWarnings("deprecation")
	public void encrypt(ByteBuf buf) {
		if (buf.order() != ByteOrder.LITTLE_ENDIAN) {
			buf = buf.order(ByteOrder.LITTLE_ENDIAN);
		}

		// Checksum & XOR Key or Checksum only
		buf.writeZero(4);

		// Padding
		buf.writeZero(8 - (buf.readableBytes() % 8));

		int checksum = 0;
		while (buf.isReadable(8)) {
			checksum ^= buf.readInt();
		}

		buf.setInt(buf.readerIndex(), checksum);

		buf.resetReaderIndex();

		final byte[] block = new byte[8];
		while (buf.isReadable(8)) {
			buf.readBytes(block);
			blowfishController.encryptBlock(block, 0);
			buf.setBytes(buf.readerIndex() - block.length, block);
		}
	}

	@Override
	public void decrypt(ByteBuf buf) {
		if ((buf.readableBytes() % 8) != 0) {
			buf.clear();
			return;
		}

		if (buf.order() != ByteOrder.LITTLE_ENDIAN) {
			buf = buf.order(ByteOrder.LITTLE_ENDIAN);
		}

		final byte[] block = new byte[8];
		if (staticKey) {
			staticKey = false;

			while (buf.isReadable(8)) {
				buf.readBytes(block);
				STATIC_BLOWFISH_ENGINE.decryptBlock(block, 0);
				buf.setBytes(buf.readerIndex() - block.length, block);
			}

			buf.resetReaderIndex();

			CryptoUtils.decryptInitialPadding(buf);
		} else {
			while (buf.isReadable(8)) {
				buf.readBytes(block);
				blowfishController.decryptBlock(block, 0);
				buf.setBytes(buf.readerIndex() - block.length, block);
			}
		}
	}
}
