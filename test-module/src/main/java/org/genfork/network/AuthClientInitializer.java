package org.genfork.network;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.MessageToMessageEncoder;
import org.genfork.tools.network.codecs.CryptCodec;
import org.genfork.tools.network.codecs.PacketDecoder;
import org.genfork.tools.network.codecs.PacketEncoder;

import java.nio.ByteOrder;
import java.util.List;

/**
 * @author: ngolubenko@context-it.ru
 * @created: 2020/04
 */
public class AuthClientInitializer extends ChannelInitializer<NioSocketChannel> {
	private static final PacketEncoder PACKET_ENCODER = new PacketEncoder(32768 - 2);

	@Override
	protected void initChannel(NioSocketChannel ch) {
		final AuthClientHandler authClientHandler = new AuthClientHandler();
		ch.pipeline().addLast("length-decoder", new LengthFieldBasedFrameDecoder(ByteOrder.LITTLE_ENDIAN, 32768 - 2, 0, 2, -2, 2, false));
		ch.pipeline().addLast("length-encoder", new MessageToMessageEncoder<ByteBuf>() {
			@Override
			protected void encode(ChannelHandlerContext ctx, ByteBuf msg, List<Object> out) throws Exception {
				final ByteBuf buf = ctx.alloc().buffer(2);
				final short length = (short) (msg.readableBytes() + 2);
				buf.writeShortLE(length);
				out.add(buf);
				out.add(msg.retain());
			}
		});

		ch.pipeline().addLast("crypt-codec", new CryptCodec(authClientHandler.getAuthCrypt()));
//		ch.pipeline().addLast(new LoggingHandler(LogLevel.INFO));
		ch.pipeline().addLast("packet-decoder", new PacketDecoder<>(AuthIncomingPackets.PACKET_ARRAY, authClientHandler));
		ch.pipeline().addLast("packet-encoder", PACKET_ENCODER);
		ch.pipeline().addLast(authClientHandler);
	}
}
