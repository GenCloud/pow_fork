package org.genfork.network.utils;

import io.netty.buffer.ByteBuf;
import lombok.Getter;
import lombok.experimental.UtilityClass;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

/**
 * @author: ngolubenko@context-it.ru
 * @created: 2020/04
 */
@UtilityClass
public class CryptoUtils {
	@Getter
	public KeyFactory rsaKeyFactory;

	@Getter
	public Cipher rsaCipher;

	static {
		try {
			rsaKeyFactory = KeyFactory.getInstance("RSA");
			rsaCipher = Cipher.getInstance("RSA/ECB/NoPadding");
		} catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
			e.printStackTrace();
		}
	}

	public byte[] encryptXorModulus(byte[] xor) {
		// step 4 xor last 0x40 bytes with first 0x40 bytes
		for (int i = 0; i < 0x40; i++) {
			xor[0x40 + i] = (byte) (xor[0x40 + i] ^ xor[i]);
		}

		// step 3 xor bytes 0x0d-0x10 with bytes 0x34-0x38
		for (int i = 0; i < 4; i++) {
			xor[0x0d + i] = (byte) (xor[0x0d + i] ^ xor[0x34 + i]);
		}

		// step 2 xor first 0x40 bytes with last 0x40 bytes
		for (int i = 0; i < 0x40; i++) {
			xor[i] = (byte) (xor[i] ^ xor[0x40 + i]);
		}

		// step 1 : 0x00-0x04 <-> 0x4d-0x50
		for (int i = 0; i < 4; i++) {
			final byte temp = xor[i];
			xor[i] = xor[0x4d + i];
			xor[0x4d + i] = temp;
		}

		if (xor.length == 0x81 && xor[0] == 0) {
			xor = Arrays.copyOfRange(xor, 1, 0x81);
		}

		return xor;
	}

	public void decryptInitialPadding(ByteBuf buf) {
		final int offset = buf.readerIndex();
		int readIdx = buf.readableBytes() - 8;
		int key = buf.getInt(readIdx);

		while (true) {
			readIdx -= 4;
			if (readIdx <= offset) {
				return;
			}

			int i = buf.getInt(readIdx);
			buf.setInt(readIdx, i ^= key);
			key -= i;
		}
	}
}
