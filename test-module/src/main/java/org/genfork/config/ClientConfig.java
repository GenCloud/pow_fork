package org.genfork.config;

import org.genfork.tools.config.abstraction.ConfigPropertiesLoader;
import org.genfork.tools.config.annotation.ConfigClass;
import org.genfork.tools.config.annotation.ConfigField;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@ConfigClass(fileName = "Client")
public class ClientConfig extends ConfigPropertiesLoader {
	@ConfigField(name = "AuthServerHost", value = "127.0.0.1")
	public static String AUTH_SERVER_HOST;

	@ConfigField(name = "AuthServerPort", value = "2106")
	public static int AUTH_SERVER_PORT;

	@ConfigField(name = "GameServerHost", value = "127.0.0.1")
	public static String GAME_SERVER_HOST;

	@ConfigField(name = "GameServerPort", value = "7777")
	public static int GAME_SERVER_PORT;

	@ConfigField(name = "ServerId", value = "3")
	public static short SERVER_ID;

	@ConfigField(name = "AccountUsername", value = "tests")
	public static String USERNAME;

	@ConfigField(name = "AccountPassword", value = "tests")
	public static String PASSWORD;
}
