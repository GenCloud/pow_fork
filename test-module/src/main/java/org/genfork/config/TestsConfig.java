package org.genfork.config;

import org.genfork.tools.config.abstraction.ConfigPropertiesLoader;
import org.genfork.tools.config.annotation.ConfigClass;
import org.genfork.tools.config.annotation.ConfigField;

/**
 * @author: ngolubenko@context-it.ru
 * @created: 2020/04
 */
@ConfigClass(fileName = "Tests")
public class TestsConfig extends ConfigPropertiesLoader {
	@ConfigField(name = "ExcludedTestClasses", value = "")
	public static String[] EXCLUDED_CLASSES = {};
}
