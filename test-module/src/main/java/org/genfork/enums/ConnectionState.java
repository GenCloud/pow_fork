package org.genfork.enums;

import org.genfork.tools.network.IConnectionState;

/**
 * @author: ngolubenko@context-it.ru
 * @created: 2020/04
 */
public enum ConnectionState implements IConnectionState {
	CONNECTED
}
