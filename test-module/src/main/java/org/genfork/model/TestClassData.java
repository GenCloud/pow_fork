package org.genfork.model;

import lombok.Data;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * @author: ngolubenko@context-it.ru
 * @created: 2020/04
 */
@Data
public class TestClassData {
	private Object testInstance;

	private List<Method> testMethods = new ArrayList<>();
}
