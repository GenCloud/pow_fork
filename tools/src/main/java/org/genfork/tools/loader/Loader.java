package org.genfork.tools.loader;

import org.genfork.tools.async.ThreadPool;
import org.genfork.tools.common.CommonUtil;
import org.genfork.tools.loader.annotations.InstanceGetter;
import org.genfork.tools.loader.annotations.Load;
import org.genfork.tools.loader.annotations.Reload;
import org.genfork.tools.loader.interfaces.ILoadGroup;
import org.genfork.tools.loader.model.TreeNode;
import org.genfork.tools.reflection.ClassPathUtil;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.TRUNCATE_EXISTING;
import static java.util.concurrent.TimeUnit.NANOSECONDS;
import static org.genfork.tools.loader.AppLoader.ApplicationMode.ALL;

public final class Loader {
	private final Map<Class<? extends ILoadGroup>, List<TreeNode<LoadHolder>>> loadTreeGroups = new HashMap<>();
	private final Map<String, LoadHolder> reloadGroups = new HashMap<>();
	private final Map<LoadHolder, Long> executionTimes = new ConcurrentHashMap<>();

	public Loader(String prefix) {
		try {
			ClassPathUtil.getAllMethodsAnnotatedWith(prefix, Load.class)
					.forEach(loadMethod -> {
						final TreeNode<LoadHolder> loadTreeNode = new TreeNode<>(new LoadHolder(findInstanceGetterMethod(loadMethod.getDeclaringClass()), loadMethod));
						final Load loadAnnotation = loadMethod.getAnnotation(Load.class);
						if (loadAnnotation.module() == ALL || loadAnnotation.module() == AppLoader.MODE) {
							initDepndencies(loadTreeNode, loadAnnotation);
							loadTreeGroups.computeIfAbsent(loadAnnotation.group(), k -> new LinkedList<>()).add(loadTreeNode);
						}
					});
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		loadTreeGroups.values()
				.forEach(loadTrees ->
						loadTrees.removeIf(loadTreeNode1 -> {
							boolean wasAdopted = false;
							for (TreeNode<LoadHolder> loadTreeNode2 : loadTrees) {
								if (loadTreeNode1 != loadTreeNode2) {
									final List<TreeNode<LoadHolder>> foundLoadTreeNodes = loadTreeNode2.findAll(loadTreeNode1.getValue());
									foundLoadTreeNodes
											.forEach(foundLoadTreeNode ->
													foundLoadTreeNode.addChildren(loadTreeNode1.getChildren()));

									if (!foundLoadTreeNodes.isEmpty()) {
										wasAdopted = true;
									}
								}
							}
							return wasAdopted;
						}));

		loadTreeGroups.values()
				.stream()
				.flatMap(Collection::stream)
				.forEach(loadTree -> {
					final Queue<Deque<TreeNode<LoadHolder>>> frontier = new LinkedList<>();
					{
						final Deque<TreeNode<LoadHolder>> deque = new LinkedList<>();
						deque.add(loadTree);
						frontier.offer(deque);
					}

					while (!frontier.isEmpty()) {
						final Deque<TreeNode<LoadHolder>> front = frontier.poll();
						front.getLast().getChildren()
								.forEach(loadTreeNode -> {
									if (front.contains(loadTreeNode)) {
										front.add(loadTreeNode);
										while (!front.getFirst().getValue().equals(front.getLast().getValue())) {
											front.removeFirst();
										}

										throw new RuntimeException("Cyclic Dependency found [" + front.stream().map(t -> t.getValue().toString()).collect(Collectors.joining(" -> ")) + "]");
									}

									final Deque<TreeNode<LoadHolder>> deque = new LinkedList<>(front);
									deque.add(loadTreeNode);
									frontier.offer(deque);
								});
					}
				});

		try {
			for (Method reloadMethod : ClassPathUtil.getAllMethodsAnnotatedWith(prefix, Reload.class)) {
				final String reloadName = reloadMethod.getAnnotation(Reload.class).value();
				final LoadHolder loadHolder = new LoadHolder(findInstanceGetterMethod(reloadMethod.getDeclaringClass()), reloadMethod);
				final LoadHolder previousLoadHolder = reloadGroups.putIfAbsent(reloadName, loadHolder);
				if (previousLoadHolder != null) {
					throw new RuntimeException("More than one Reload with name " + reloadName + " found [" + previousLoadHolder + ", " + loadHolder + "]");
				}
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private void initDepndencies(TreeNode<LoadHolder> loadTreeNode, Load loadAnnotation) {
		Arrays
				.stream(loadAnnotation.dependencies())
				.forEach(dependency ->
						Arrays
								.stream(dependency.method())
								.forEach(dependencyMethod -> {
									try {
										final LoadHolder dependencyLoadHolder = new LoadHolder(findInstanceGetterMethod(dependency.clazz()), dependency.clazz().getDeclaredMethod(dependencyMethod));
										final Load dependencyLoadAnnotation = dependencyLoadHolder.getLoadMethod().getAnnotation(Load.class);
										if (dependencyLoadAnnotation == null) {
											throw new RuntimeException("Dependency " + dependencyLoadHolder + " of " + loadTreeNode.getValue() + " is not annotated with Load annotation");
										}

										if (loadAnnotation.group() != dependencyLoadAnnotation.group()) {
											throw new RuntimeException("Dependency " + dependencyLoadHolder + " of " + loadTreeNode.getValue() + " can not be in different Load group");
										}

										loadTreeNode.addChild(dependencyLoadHolder);

										if (loadTreeNode.getChildren().stream().distinct().count() != loadTreeNode.getChildren().size()) {
											throw new RuntimeException("Duplicated Dependency " + dependencyLoadHolder + " on " + loadTreeNode.getValue());
										}
									} catch (NoSuchMethodException e) {
										throw new RuntimeException("Dependency method for " + dependency.clazz().getName() + "." + dependencyMethod + "()" + " on " + loadTreeNode.getValue() + " was not found.", e);
									}
								}));
	}

	private static Method findInstanceGetterMethod(Class<?> clazz) {
		final Method[] instanceGetterMethods = Arrays.stream(clazz.getDeclaredMethods()).filter(m -> m.isAnnotationPresent(InstanceGetter.class)).toArray(Method[]::new);
		if (instanceGetterMethods.length == 0) {
			throw new UnsupportedOperationException(clazz + " contains Load annotated method(s) but it does not have an InstanceGetter annotated method.");
		}

		if (instanceGetterMethods.length != 1) {
			throw new UnsupportedOperationException("There should be only one InstanceGetter annotated method " + Arrays.toString(instanceGetterMethods));
		}

		if (!Modifier.isPublic(instanceGetterMethods[0].getModifiers())) {
			throw new UnsupportedOperationException("non public InstanceGetter method " + instanceGetterMethods[0]);
		}

		if (!Modifier.isStatic(instanceGetterMethods[0].getModifiers())) {
			throw new UnsupportedOperationException("non static InstanceGetter method " + instanceGetterMethods[0]);
		}
		return instanceGetterMethods[0];
	}

	@SafeVarargs
	public final CompletableFuture<Void> runAsync(Class<? extends ILoadGroup> loadGroup, Class<? extends ILoadGroup>... loadGroups) {
		final Deque<CompletableFuture<Void>> loadGroupCompletableFutures = new LinkedList<>();
		CommonUtil.forEachArgAndVarArgs(loadGroup2 -> {
			final CompletableFuture<Void> previousLoadGroupCompletableFuture = loadGroupCompletableFutures.peekLast();
			final Map<LoadHolder, CompletableFuture<Void>> completableFutures = new HashMap<>();
			final List<CompletableFuture<Void>> rootCompletableFutures = new LinkedList<>();
			for (TreeNode<LoadHolder> loadTreeNode : loadTreeGroups.getOrDefault(loadGroup2, Collections.emptyList())) {
				CompletableFuture<Void> lastCompletableFuture = null;
				for (TreeNode<LoadHolder> treeNode : loadTreeNode.postOrderTraversal()) {
					lastCompletableFuture = completableFutures.computeIfAbsent(treeNode.getValue(), k ->
					{
						Stream<CompletableFuture<Void>> dependencyCompletableFutureStream = treeNode.getChildren()
								.stream()
								.map(TreeNode::getValue)
								.map(completableFutures::get);
						if (previousLoadGroupCompletableFuture != null) {
							dependencyCompletableFutureStream = Stream.concat(Stream.of(previousLoadGroupCompletableFuture), dependencyCompletableFutureStream);
						}

						return CompletableFuture.allOf(dependencyCompletableFutureStream.toArray(CompletableFuture[]::new))
								.thenRunAsync(() -> runTreeNode(treeNode), ThreadPool.getThreadPoolExecutor());
					});
				}

				rootCompletableFutures.add(lastCompletableFuture);
			}
			loadGroupCompletableFutures.add(CompletableFuture.allOf(rootCompletableFutures.toArray(new CompletableFuture[0])));
		}, loadGroup, loadGroups);
		return CompletableFuture.allOf(loadGroupCompletableFutures.toArray(new CompletableFuture[0]));
	}

	@SafeVarargs
	public final void run(Class<? extends ILoadGroup> loadGroup, Class<? extends ILoadGroup>... loadGroups) {
		CommonUtil.forEachArgAndVarArgs(loadGroup2 -> {
			final Set<LoadHolder> runNodes = new HashSet<>();
			loadTreeGroups.getOrDefault(loadGroup2, Collections.emptyList()).forEach(loadTreeNode -> {
				for (TreeNode<LoadHolder> treeNode : loadTreeNode.postOrderTraversal()) {
					if (runNodes.add(treeNode.getValue())) {
						runTreeNode(treeNode);
					}
				}
			});
		}, loadGroup, loadGroups);
	}

	private void runTreeNode(TreeNode<LoadHolder> treeNode) throws RuntimeException {
		try {
			final long startTime = System.nanoTime();
			treeNode.getValue().call();
			executionTimes.put(treeNode.getValue(), treeNode.getChildren().stream().map(TreeNode::getValue).mapToLong(executionTimes::get).max().orElse(0) + (System.nanoTime() - startTime));
		} catch (IllegalAccessException | InvocationTargetException e) {
			throw new RuntimeException("Calling " + treeNode.getValue() + " failed", e);
		}
	}

	@SafeVarargs
	public final String getDependencyTreeString(Class<? extends ILoadGroup> loadGroup, Class<? extends ILoadGroup>... loadGroups) {
		final StringJoiner sj = new StringJoiner(System.lineSeparator());
		CommonUtil.forEachArgAndVarArgs(loadGroups2 -> {
			sj.add("=========================" + loadGroups2.getName() + "=========================");
			for (Iterator<TreeNode<LoadHolder>> loadTreesIterator = loadTreeGroups.getOrDefault(loadGroups2, Collections.emptyList()).stream().sorted(Comparator.comparingLong(tn -> executionTimes.getOrDefault(tn.getValue(), 0L))).iterator(); loadTreesIterator.hasNext(); ) {
				getDependencyTreeString(sj, loadTreesIterator.next(), "", !loadTreesIterator.hasNext());
			}
		}, loadGroup, loadGroups);
		return sj.toString();
	}

	private void getDependencyTreeString(StringJoiner sj, TreeNode<LoadHolder> treeNode, String indent, boolean lastChild) {
		sj.add(indent + (lastChild ? "\\" : "+") + "--- " + treeNode.getValue() + " " + NANOSECONDS.toMillis(executionTimes.getOrDefault(treeNode.getValue(), -1L)) + " ms");
		for (Iterator<TreeNode<LoadHolder>> childrenIterator = treeNode.getChildren().stream().sorted(Comparator.<TreeNode<LoadHolder>>comparingLong(tn -> executionTimes.getOrDefault(tn.getValue(), 0L)).reversed()).iterator(); childrenIterator.hasNext(); ) {
			getDependencyTreeString(sj, childrenIterator.next(), indent + (lastChild ? " " : "|") + "    ", !childrenIterator.hasNext());
		}
	}

	@SafeVarargs
	public final void writeDependencyTreeToFile(Path path, Class<? extends ILoadGroup> loadGroup, Class<? extends ILoadGroup>... loadGroups) throws IOException {
		Files.write(path, getDependencyTreeString(loadGroup, loadGroups).getBytes(), CREATE, TRUNCATE_EXISTING);
	}

	public Map<String, LoadHolder> getReloads() {
		return reloadGroups;
	}
}
