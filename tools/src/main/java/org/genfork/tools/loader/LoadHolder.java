package org.genfork.tools.loader;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Objects;

public class LoadHolder {
	private final Method instanceGetterMethod;
	private final Method loadMethod;

	public LoadHolder(Method instanceGetterMethod, Method loadMethod) {
		this.instanceGetterMethod = Objects.requireNonNull(instanceGetterMethod);
		this.loadMethod = Objects.requireNonNull(loadMethod);
	}

	public Method getInstanceGetterMethod() {
		return instanceGetterMethod;
	}

	public Method getLoadMethod() {
		return loadMethod;
	}

	public void call() throws InvocationTargetException, IllegalAccessException {
		final boolean loadMethodAccessible = loadMethod.isAccessible();
		loadMethod.setAccessible(true);
		loadMethod.invoke(instanceGetterMethod.invoke(null));
		loadMethod.setAccessible(loadMethodAccessible);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		final LoadHolder that = (LoadHolder) o;
		if (!instanceGetterMethod.equals(that.instanceGetterMethod)) {
			return false;
		}
		return loadMethod.equals(that.loadMethod);

	}

	@Override
	public int hashCode() {
		int result = instanceGetterMethod.hashCode();
		result = (31 * result) + loadMethod.hashCode();
		return result;
	}

	@Override
	public String toString() {
		return instanceGetterMethod.getDeclaringClass().getName() + "." + instanceGetterMethod.getName() + "()." + loadMethod.getName() + "()";
	}
}
