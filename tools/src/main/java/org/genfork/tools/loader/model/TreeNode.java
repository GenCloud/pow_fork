package org.genfork.tools.loader.model;

import com.google.common.graph.Traverser;
import org.checkerframework.checker.nullness.qual.NonNull;

import java.util.*;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class TreeNode<T> implements Iterable<TreeNode<T>> {
	private final List<TreeNode<T>> childNodes = new LinkedList<>();
	private TreeNode<T> parentNode;
	private T value;

	public TreeNode(T value) {
		this.value = value;
	}

	public TreeNode<T> getParent() {
		return parentNode;
	}

	public List<TreeNode<T>> getChildren() {
		return childNodes;
	}

	public TreeNode<T> addChild(T value) {
		final TreeNode<T> childNode = new TreeNode<>(value);
		childNode.parentNode = this;
		childNodes.add(childNode);
		return childNode;
	}

	public void addChildren(Collection<TreeNode<T>> children) {
		children.forEach(child -> addChild(child.getValue()).addChildren(child.getChildren()));
	}

	public T getValue() {
		return value;
	}

	public void setValue(T value) {
		this.value = value;
	}

	@Override
	@NonNull
	public Iterator<TreeNode<T>> iterator() {
		return breadthFirstTraversal().iterator();
	}

	public Iterable<TreeNode<T>> breadthFirstTraversal() {
		return Traverser.<TreeNode<T>>forTree(TreeNode::getChildren).breadthFirst(this);
	}

	public Iterable<TreeNode<T>> preOrderTraversal() {
		return Traverser.<TreeNode<T>>forTree(TreeNode::getChildren).depthFirstPreOrder(this);
	}

	public Iterable<TreeNode<T>> postOrderTraversal() {
		return Traverser.<TreeNode<T>>forTree(TreeNode::getChildren).depthFirstPostOrder(this);
	}

	public List<TreeNode<T>> findAll(T value) {
		final List<TreeNode<T>> treeNodes = new LinkedList<>();
		for (TreeNode<T> treeNode : this) {
			if (treeNode.getValue().equals(value)) {
				treeNodes.add(treeNode);
			}
		}
		return treeNodes;
	}

	public boolean contains(T value) {
		for (TreeNode<T> treeNode : this) {
			if (treeNode.getValue().equals(value)) {
				return true;
			}
		}
		return false;
	}

	public Queue<TreeNode<T>> getLeafNodes() {
		final Queue<TreeNode<T>> leafNodes = new LinkedList<>();
		for (TreeNode<T> treeNode : this) {
			if (treeNode.getChildren().isEmpty()) {
				leafNodes.add(treeNode);
			}
		}
		return leafNodes;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		final TreeNode<?> treeNode = (TreeNode<?>) o;
		return value != null ? value.equals(treeNode.value) : treeNode.value == null;
	}

	@Override
	public int hashCode() {
		return value != null ? value.hashCode() : 0;
	}
}
