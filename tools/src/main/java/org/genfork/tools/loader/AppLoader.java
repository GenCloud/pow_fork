package org.genfork.tools.loader;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.genfork.tools.async.IThreadPoolInitializer;
import org.genfork.tools.async.ThreadPool;
import org.genfork.tools.config.ConfigManager;
import org.genfork.tools.console.ConsoleUtil;
import org.genfork.tools.management.ShutdownManager;
import org.genfork.tools.management.TerminationStatus;
import org.genfork.tools.path.BasePathProvider;
import org.genfork.tools.time.TimeAmountInterpreter;

import java.lang.management.ManagementFactory;
import java.util.Arrays;
import java.util.List;

@Slf4j
@UtilityClass
public class AppLoader {
	/**
	 * A way to determine whether the running application is LS, GS or other.
	 */
	public static ApplicationMode MODE = null;

	/**
	 * JUnit scripts run at once. JVM is not terminated, therefore this boolean is necessary.
	 */
	public static boolean INITIALIZED = false;

	/**
	 * A dedicated method for all unity based applications initialization process. Prints useful system information and loads the commonly used functionalities.
	 *
	 * @param mode              the application mode, used for {@link BasePathProvider}
	 * @param configPackageName the package that holds config classes
	 * @param classes           be careful: order is important, and some of the modules cannot be used without each-other!
	 */
	public void defaultInit(ApplicationMode mode, String configPackageName, Class<?>... classes) {
		if (INITIALIZED) {
			return;
		}

		MODE = mode;

		Thread.setDefaultUncaughtExceptionHandler((t, e) -> {
			log.warn("Exception occurred on thread: {}", t, e);
			if (e instanceof Error && !(e instanceof StackOverflowError)) {
				ShutdownManager.halt(TerminationStatus.RUNTIME_UNCAUGHT_ERROR);
			}
		});

		if (StringUtils.isNotEmpty(configPackageName)) {
			ConsoleUtil.printSection("Configuration");
			ConfigManager.getInstance().load(configPackageName);
		}

		ConsoleUtil.printSection("Shutdown Hook");
		ShutdownManager.initShutdownHook();

		Arrays.stream(classes).filter(IThreadPoolInitializer.class::isAssignableFrom).forEach(clazz -> {
			ConsoleUtil.printSection("Thread Pool(s)");
			try {
				ThreadPool.initThreadPools((IThreadPoolInitializer) clazz.getConstructors()[0].newInstance());
			} catch (final Exception e) {
				log.error("Could not setup thread pools!", e);
				ShutdownManager.exit(TerminationStatus.RUNTIME_INITIALIZATION_FAILURE);
			}
		});

		ConsoleUtil.printSection("Application");

		INITIALIZED = true;
	}

	/**
	 * The default post-init for all unity based applications. This method finishes the initialization.
	 *
	 * @param dependencyInfo version info mostly, but can be anything else
	 */
	public static void defaultPostInit(List<String> dependencyInfo) {
		if (dependencyInfo != null && !dependencyInfo.isEmpty()) {
			ConsoleUtil.printSection("Execution Environment");
			dependencyInfo.forEach(log::info);
		}

		ConsoleUtil.printSection("Memory (Heap)");

		System.gc();
		System.runFinalization();

		ConsoleUtil.printSection("Summary");
		log.info("Initialized in {}.", TimeAmountInterpreter.consolidateMillis(ManagementFactory.getRuntimeMXBean().getUptime()));

		// To split everything that comes after boot.
		ConsoleUtil.printSection("ApplicationLog");
	}

	public enum ApplicationMode {
		ALL,
		DATA,
		LOGIN,
		GAME_MASTER,
		GAME_SLAVE,
		NPC,
		REGISTRY,
		OTHER;

		public String getModeName() {
			return name().toLowerCase();
		}
	}
}
