package org.genfork.tools.loader.annotations;

import org.genfork.tools.loader.AppLoader.ApplicationMode;
import org.genfork.tools.loader.interfaces.ILoadGroup;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Load {
	Class<? extends ILoadGroup> group();

	Dependency[] dependencies() default {};

	ApplicationMode module() default ApplicationMode.ALL;
}
