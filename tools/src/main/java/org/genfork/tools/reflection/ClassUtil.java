package org.genfork.tools.reflection;

import lombok.experimental.UtilityClass;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.stream.IntStream;

@UtilityClass
public class ClassUtil {
	public Type[] getGenericTypes(final Field field) {
		final Type genType = field.getGenericType();
		if (!(genType instanceof ParameterizedType)) {
			return null;
		}

		final ParameterizedType pType = (ParameterizedType) genType;
		return pType.getActualTypeArguments();
	}

	public Class<?> getFirstGenericTypeOfGenerizedField(final Field field) {
		final Type[] allGenTypes = getGenericTypes(field);
		if (allGenTypes == null) {
			return Object.class;
		}

		return (Class<?>) allGenTypes[0];
	}

	@SuppressWarnings({"unchecked", "rawtypes"})
	public Object get(Class<?> type, String value) {
		if (type == Boolean.class || type == Boolean.TYPE) {
			return getBoolean(value);
		} else if (type == Long.class || type == Long.TYPE) {
			return getLong(value);
		} else if (type == Integer.class || type == Integer.TYPE) {
			return getInteger(value);
		} else if (type == Short.class || type == Short.TYPE) {
			return getShort(value);
		} else if (type == Byte.class || type == Byte.TYPE) {
			return getByte(value);
		} else if (type == Double.class || type == Double.TYPE) {
			return getDouble(value);
		} else if (type == Float.class || type == Float.TYPE) {
			return getFloat(value);
		} else if (type == String.class) {
			return getString(value);
		} else if (type.isEnum()) {
			return getEnum((Class<? extends Enum>) type, value);
		} else {
			throw new FieldParserException("Not covered type: " + type + "!");
		}
	}

	public boolean getBoolean(String value) {
		if (value == null) {
			throw new FieldParserException(Boolean.class);
		}

		try {
			return Boolean.parseBoolean(value);
		} catch (final RuntimeException e) {
			throw new FieldParserException(Boolean.class, value, e);
		}
	}

	public byte getByte(String value) {
		if (value == null) {
			throw new FieldParserException(Byte.class);
		}

		try {
			return Byte.decode(value);
		} catch (final RuntimeException e) {
			throw new FieldParserException(Byte.class, value, e);
		}
	}

	public short getShort(String value) {
		if (value == null) {
			throw new FieldParserException(Short.class);
		}

		try {
			return Short.decode(value);
		} catch (final RuntimeException e) {
			throw new FieldParserException(Short.class, value, e);
		}
	}

	public int getInteger(String value) {
		if (value == null) {
			throw new FieldParserException(Integer.class);
		}

		try {
			return Integer.decode(value);
		} catch (final RuntimeException e) {
			throw new FieldParserException(Integer.class, value, e);
		}
	}

	public long getLong(String value) {
		if (value == null) {
			throw new FieldParserException(Long.class);
		}

		try {
			return Long.decode(value);
		} catch (final RuntimeException e) {
			throw new FieldParserException(Long.class, value, e);
		}
	}

	public float getFloat(String value) {
		if (value == null) {
			throw new FieldParserException(Float.class);
		}

		try {
			return Float.parseFloat(value);
		} catch (final RuntimeException e) {
			throw new FieldParserException(Float.class, value, e);
		}
	}

	public double getDouble(String value) {
		if (value == null) {
			throw new FieldParserException(Double.class);
		}

		try {
			return Double.parseDouble(value);
		} catch (final RuntimeException e) {
			throw new FieldParserException(Double.class, value, e);
		}
	}

	public String getString(String value) {
		if (value == null) {
			throw new FieldParserException(String.class);
		}

		return value;
	}

	public <T extends Enum<T>> T getEnum(Class<T> enumClass, String value) {
		if (value == null) {
			throw new FieldParserException(enumClass);
		}

		try {
			return Enum.valueOf(enumClass, value);
		} catch (final RuntimeException e) {
			throw new FieldParserException(enumClass, value, e);
		}
	}

	public Object getArray(Class<?> componentClass, String value, String regex) {
		final String[] values = value.split(regex);

		final Object array = Array.newInstance(componentClass, values.length);

		IntStream.range(0, values.length).forEach(i -> Array.set(array, i, get(componentClass, values[i])));

		return array;
	}

	public final class FieldParserException extends RuntimeException {
		public FieldParserException(String message) {
			super(message);
		}

		public FieldParserException(Class<?> requiredType) {
			super(requiredType + " value required, but not specified!");
		}

		public FieldParserException(Class<?> requiredType, String value, RuntimeException cause) {
			super(requiredType + " value required, but found: '" + value + "'!", cause);
		}
	}
}
