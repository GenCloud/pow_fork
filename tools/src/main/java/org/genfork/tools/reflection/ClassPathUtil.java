package org.genfork.tools.reflection;

import com.google.common.reflect.ClassPath;
import com.google.common.reflect.ClassPath.ClassInfo;
import lombok.experimental.UtilityClass;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@SuppressWarnings("unchecked")
@UtilityClass
public class ClassPathUtil {
	public <T> List<Class<T>> getAllClassesExtending(Class<T> targetClass) throws IOException {
		final ClassPath classPath = ClassPath.from(ClassLoader.getSystemClassLoader());
		return classPath.getTopLevelClasses()
				.stream()
				.map(ClassInfo::load)
				.filter(targetClass::isAssignableFrom)
				.filter(clazz -> !Modifier.isAbstract(clazz.getModifiers()))
				.filter(clazz -> !Modifier.isInterface(clazz.getModifiers()))
				.map(clazz -> (Class<T>) clazz)
				.collect(Collectors.toList());
	}

	public <T> List<Class<T>> getAllClassesExtending(String packageName, Class<T> targetClass) throws IOException {
		final ClassPath classPath = ClassPath.from(ClassLoader.getSystemClassLoader());
		return classPath.getTopLevelClassesRecursive(packageName)
				.stream()
				.map(ClassInfo::load)
				.filter(targetClass::isAssignableFrom)
				.filter(clazz -> !Modifier.isAbstract(clazz.getModifiers()))
				.filter(clazz -> !Modifier.isInterface(clazz.getModifiers()))
				.map(clazz -> (Class<T>) clazz)
				.collect(Collectors.toList());
	}

	public List<Method> getAllMethodsAnnotatedWith(Class<? extends Annotation> annotationClass) throws IOException {
		final ClassPath classPath = ClassPath.from(ClassLoader.getSystemClassLoader());
		return classPath.getTopLevelClasses()
				.stream()
				.map(ClassInfo::load)
				.flatMap(clazz -> Arrays.stream(clazz.getDeclaredMethods()))
				.filter(method -> method.isAnnotationPresent(annotationClass))
				.collect(Collectors.toList());
	}

	public List<Method> getAllMethodsAnnotatedWith(String packageName, Class<? extends Annotation> annotationClass) throws IOException {
		final ClassPath classPath = ClassPath.from(ClassLoader.getSystemClassLoader());
		return classPath.getTopLevelClassesRecursive(packageName)
				.stream()
				.map(ClassInfo::load)
				.flatMap(clazz -> Arrays.stream(clazz.getDeclaredMethods()))
				.filter(method -> method.isAnnotationPresent(annotationClass))
				.collect(Collectors.toList());
	}
}
