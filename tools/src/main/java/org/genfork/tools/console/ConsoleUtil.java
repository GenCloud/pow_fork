package org.genfork.tools.console;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Slf4j
@UtilityClass
public class ConsoleUtil {
	public void printSection(String sectionName) {
		final String sb = IntStream
				.range(0, 115 - sectionName.length())
				.mapToObj(i -> "-").
						collect(Collectors.joining("", "", "=[ " + sectionName + " ]"));
		log.info(sb);
	}
}
