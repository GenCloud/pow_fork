package org.genfork.tools.async;

public interface IThreadPoolInitializer {
	int getScheduledThreadPoolSize();

	int getThreadPoolSize();
}
