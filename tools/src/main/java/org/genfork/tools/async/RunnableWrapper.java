package org.genfork.tools.async;

import lombok.RequiredArgsConstructor;

import java.lang.Thread.UncaughtExceptionHandler;

@RequiredArgsConstructor
public class RunnableWrapper implements Runnable {
	private final Runnable runnable;

	@Override
	public void run() {
		try {
			runnable.run();
		} catch (final Throwable e) {
			final Thread t = Thread.currentThread();
			final UncaughtExceptionHandler h = t.getUncaughtExceptionHandler();
			if (h != null) {
				h.uncaughtException(t, e);
			}
		}
	}
}
