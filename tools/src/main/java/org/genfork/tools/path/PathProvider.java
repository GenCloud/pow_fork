package org.genfork.tools.path;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Predicate;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Slf4j
public class PathProvider {
	private final Path serverName = Paths.get("servers", "server");

	private Predicate<Path> acceptFilter = path -> true;
	private boolean overrideLogging;

	public PathProvider setAcceptFilter(Predicate<Path> filter) {
		acceptFilter = Objects.requireNonNull(filter);
		return this;
	}

	public PathProvider setOverrideLogging(boolean value) {
		overrideLogging = value;
		return this;
	}

	public Path resolvePath(Path path, Path dataPack) {
		final Path datapackOverridePath = dataPack.resolve(serverName).resolve(path).normalize();
		final Path baseOverridePath = BasePathProvider.resolvePath(serverName).resolve(path).normalize();
		final Path datapackPath = dataPack.resolve(path).normalize();
		final Path basePath = BasePathProvider.resolvePath(path).normalize();

		if (Files.exists(datapackOverridePath)) {
			return datapackOverridePath;
		} else if (Files.exists(baseOverridePath)) {
			return baseOverridePath;
		} else if (Files.exists(datapackPath)) {
			return datapackPath;
		}
		return basePath;
	}

	public Path relativePath(Path path, Path dataPack) {
		final Path datapackOverridePath = dataPack.resolve(serverName).resolve(path).normalize();
		final Path baseOverridePath = BasePathProvider.resolvePath(serverName).resolve(path).normalize();
		final Path datapackPath = dataPack.resolve(path).normalize();
		final Path basePath = BasePathProvider.resolvePath(path).normalize();

		if (Files.exists(datapackOverridePath)) {
			return dataPack.resolve(serverName).relativize(datapackOverridePath);
		} else if (Files.exists(baseOverridePath)) {
			BasePathProvider.resolvePath(serverName).relativize(baseOverridePath);
		} else if (Files.exists(datapackPath)) {
			return dataPack.relativize(datapackPath);
		} else if (Files.exists(basePath)) {
			return BasePathProvider.getBasePath().relativize(basePath);
		}
		return path;
	}

	public Set<Path> resolveOverriddenFile(Path path, Path dataPack, OverrideMode overrideMode) {
		final Set<Path> files = new LinkedHashSet<>();
		switch (overrideMode) {
			case INCREMENTAL: {
				checkAndAddPath(files, dataPack.resolve(serverName).resolve(path).normalize(), overrideMode, true);
				checkAndAddPath(files, BasePathProvider.resolvePath(serverName).resolve(path).normalize(), overrideMode, true);
				checkAndAddPath(files, dataPack.resolve(path).normalize(), overrideMode, false);
				checkAndAddPath(files, BasePathProvider.resolvePath(path).normalize(), overrideMode, false);
				break;
			}
			case OVERLAYING: {
				final Path resolvedPath = resolvePath(path, dataPack);
				if (Files.exists(resolvedPath)) {
					files.add(resolvedPath);
				}
				break;
			}
			default: {
				throw new InternalError(); // never happens
			}
		}
		return files;
	}

	public Set<Path> resolveOverriddenDirectory(Path dir, Path dataPack, boolean recursive, OverrideMode overrideMode) throws IOException {
		final Set<Path> files = new LinkedHashSet<>();
		final Path datapackOverridePath = dataPack.resolve(serverName).resolve(dir).normalize();
		final Path baseOverridePath = BasePathProvider.resolvePath(serverName).resolve(dir).normalize();
		final Path datapackPath = dataPack.resolve(dir).normalize();
		final Path basePath = BasePathProvider.resolvePath(dir).normalize();

		switch (overrideMode) {
			case INCREMENTAL: {
				checkAndAddDir(files, basePath, recursive, overrideMode, Files.exists(basePath) && Files.exists(datapackPath) && !Files.isSameFile(basePath, datapackPath), path -> true);
				checkAndAddDir(files, datapackPath, recursive, overrideMode, false, path -> true);
				checkAndAddDir(files, baseOverridePath, recursive, overrideMode, true, path -> true);
				checkAndAddDir(files, datapackOverridePath, recursive, overrideMode, true, path -> true);
				break;
			}
			case OVERLAYING: {
				final Map<String, Path> paths = new LinkedHashMap<>();
				checkAndAddDir(files, basePath, recursive, overrideMode, Files.exists(basePath) && Files.exists(datapackPath) && !Files.isSameFile(basePath, datapackPath), path -> paths.putIfAbsent(basePath.relativize(path).toString(), path) == null);
				checkAndAddDir(files, datapackPath, recursive, overrideMode, false, path -> paths.putIfAbsent(datapackPath.relativize(path).toString(), path) == null);
				checkAndAddDir(files, baseOverridePath, recursive, overrideMode, true, path -> paths.putIfAbsent(baseOverridePath.relativize(path).toString(), path) == null);
				checkAndAddDir(files, datapackOverridePath, recursive, overrideMode, true, path -> paths.putIfAbsent(datapackOverridePath.relativize(path).toString(), path) == null);
				break;
			}
			default: {
				throw new InternalError(); // never happens
			}
		}

		return files;
	}

	private void checkAndAddPath(Set<Path> paths, Path path, OverrideMode overrideMode, boolean override) {
		if (Files.exists(path) && acceptFilter.test(path)) {
			if (overrideLogging && override) {
				log.info("override system [{}] - using: '{}'", overrideMode, path);
			}
			paths.add(path);
		}
	}

	private void checkAndAddDir(Set<Path> paths, Path dir, boolean recursive, OverrideMode overrideMode, boolean override, Predicate<Path> filter) throws IOException {
		if (Files.exists(dir) && acceptFilter.test(dir)) {
			Files.walk(dir, recursive ? Integer.MAX_VALUE : 1)
					.forEach(path -> {
						if (filter.test(path) && acceptFilter.test(path)) {
							if (overrideLogging && override) {
								log.info("override system [{}] - using: '{}'", overrideMode, path);
							}
							paths.add(path);
						}
					});
		}
	}
}
