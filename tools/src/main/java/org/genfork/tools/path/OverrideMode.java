package org.genfork.tools.path;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public enum OverrideMode {
	INCREMENTAL,
	OVERLAYING
}
