package org.genfork.tools.path;

import lombok.experimental.UtilityClass;
import org.genfork.tools.loader.AppLoader;
import org.genfork.tools.loader.AppLoader.ApplicationMode;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.Map;

@UtilityClass
public class BasePathProvider {
	private final Path BASE_PATH;

	static {
		final ApplicationMode appMode = AppLoader.MODE;
		if (appMode == null) {
			throw new NullPointerException("Application Mode is missing! Your application is not initialized properly!");
		}

		final String appModeName = appMode.getModeName();
		final Path baseRepoPath = Paths.get("gameserver", "dist", appModeName);
		final Path projectPath = Paths.get("dist", appModeName);
		final Path gamePath = Paths.get(".");
		BASE_PATH = Files.exists(baseRepoPath) ? baseRepoPath : Files.exists(projectPath) ? projectPath : gamePath;
	}

	public Path getBasePath() {
		return BASE_PATH;
	}

	public Path resolvePath(String path) {
		return BASE_PATH.resolve(path).normalize();
	}

	public Path resolvePath(Path path) {
		return BASE_PATH.resolve(path).normalize();
	}

	public Path resolveDatapackPath(Path datapackRoot, String path) {
		final Path resolvedPath = datapackRoot.resolve(path);
		final Path basePath = BASE_PATH.resolve(path);
		return Files.exists(basePath) ? basePath : Files.exists(resolvedPath) ? resolvedPath : Paths.get(path);
	}

	public Path resolveDatapackPath(Path datapackRoot, Path path) {
		final Path resolvedPath = datapackRoot.resolve(path);
		final Path basePath = BASE_PATH.resolve(path);
		return Files.exists(basePath) ? basePath : Files.exists(resolvedPath) ? resolvedPath : path;
	}

	public Map<String, Path> resolveFiles(Path dir, boolean recursive) throws IOException {
		final Map<String, Path> files = new LinkedHashMap<>();
		final Path path = resolvePath(dir);
		if (Files.exists(path)) {
			Files.walk(path, recursive ? Integer.MAX_VALUE : 1).forEach(file -> files.put(file.toAbsolutePath().normalize().toString(), file));
		}
		return files;
	}

	public Map<String, Path> resolveDatapackFiles(Path datapackRoot, Path dir, boolean recursive) throws IOException {
		final Map<String, Path> files = resolveFiles(dir, recursive);

		final Path datapackPath = datapackRoot.resolve(dir);
		if (Files.exists(datapackPath) && !Files.isSameFile(resolvePath(dir.normalize()).toAbsolutePath(), datapackPath.toAbsolutePath())) {
			Files.walk(datapackPath, recursive ? Integer.MAX_VALUE : 1).forEach(file -> files.put(file.toAbsolutePath().normalize().toString(), file));
		}
		return files;
	}
}
