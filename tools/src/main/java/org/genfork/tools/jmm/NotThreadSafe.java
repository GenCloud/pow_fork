package org.genfork.tools.jmm;

import java.lang.annotation.*;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface NotThreadSafe {
	boolean ignoreWarn() default false;
}
