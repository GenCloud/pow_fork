package org.genfork.tools.servers.enums;

import lombok.Getter;

import java.util.Arrays;

public enum AgeLimit {
	NONE(0),
	FIFTEEN(15),
	EIGHTEEN(18);

	@Getter
	private final int age;

	AgeLimit(int age) {
		this.age = age;
	}

	public static AgeLimit byAge(int age) {
		return Arrays
				.stream(AgeLimit.values())
				.filter(type ->
						type.getAge() == age)
				.findFirst()
				.orElse(NONE);
	}
}
