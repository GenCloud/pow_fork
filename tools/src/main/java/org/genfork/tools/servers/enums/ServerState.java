package org.genfork.tools.servers.enums;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public enum ServerState {
	OFFLINE,
	ONLINE
}
