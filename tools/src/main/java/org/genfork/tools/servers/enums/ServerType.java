package org.genfork.tools.servers.enums;

import lombok.Getter;

import java.util.Arrays;

public enum ServerType {
	RELAX(0x02),
	TEST(0x04),
	BROAD(0x08),
	CREATE_RESTRICT(0x10),
	EVENT(0x20),
	FREE(0x40),
	WORLD_RAID(0x100),
	NEW(0x200),
	CLASSIC(0x400);

	@Getter
	private final int mask;

	ServerType(int mask) {
		this.mask = mask;
	}

	public static ServerType byMask(int mask) {
		return Arrays
				.stream(ServerType.values())
				.filter(type ->
						type.getMask() == mask)
				.findFirst()
				.orElse(FREE);
	}
}
