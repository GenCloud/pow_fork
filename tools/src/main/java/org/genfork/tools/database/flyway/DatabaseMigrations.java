package org.genfork.tools.database.flyway;

import com.zaxxer.hikari.HikariDataSource;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.Location;
import org.genfork.tools.database.factory.AbstractDatabaseFactory;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@UtilityClass
@Slf4j
public class DatabaseMigrations {
	private static final String CLASSPATH_PREFIX = "classpath:";

	public void startMigration(AbstractDatabaseFactory factory, String source) {
		log.info("Installing tables in database.");

		final HikariDataSource connectionPool = factory.getConnectionPool();
		final Flyway flyway = Flyway.configure()
				.dataSource(connectionPool.getJdbcUrl(), connectionPool.getUsername(), connectionPool.getPassword())
				.validateMigrationNaming(true)
				.validateOnMigrate(true)
				.outOfOrder(false)
				.locations(new Location(CLASSPATH_PREFIX + source))
				.load();

		flyway.migrate();
	}
}
