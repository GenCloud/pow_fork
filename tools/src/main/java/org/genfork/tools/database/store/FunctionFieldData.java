package org.genfork.tools.database.store;

import lombok.Data;

import static org.genfork.tools.database.factory.AbstractDatabaseFactory.*;

/**
 * @author: ngolubenko@turbosolution.ru
 * @date: 02/2020
 */
@Data
public class FunctionFieldData {
	private int idx;
	private int paramType;
	private Class<?> type;
	private boolean array;
	private String typeString;

	public boolean isInOutParameter() {
		return paramType == INOUT;
	}

	@SuppressWarnings("unused")
	public boolean isOutputParameter() {
		return paramType == OUT;
	}

	public boolean isInputParameter() {
		return paramType == IN;
	}

	public boolean isFunctionReturnParameter() {
		return paramType == FUNCTION_RETURN;
	}
}
