package org.genfork.tools.database;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * A ResultSet handler may be implemented as a business logic that iterates over a
 * Jdbc ResultSet object.
 */
@FunctionalInterface
public interface ResultSetHandler {
	/**
	 * Should do whatever is needed for the row.
	 * The next() method of the resultset is called prior to calling this method
	 * so you should never call the next() method within the processRow method
	 * unless you know what you are doing.
	 *
	 * @param rs Fetched resultSet
	 * @throws SQLException Thrown in case of a db error
	 */
	void processRow(ResultSet rs) throws SQLException;
}
