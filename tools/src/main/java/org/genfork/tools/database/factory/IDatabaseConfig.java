package org.genfork.tools.database.factory;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public interface IDatabaseConfig {
	String getUrl();

	String getUserName();

	String getPassword();

	int getMinIdle();

	int getMaxPoolSize();

	int getConnectionTimeout();

	int getIdleTimeout();
}
