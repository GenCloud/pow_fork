package org.genfork.tools.database.factory;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.flywaydb.core.internal.jdbc.JdbcUtils;
import org.genfork.tools.database.store.FunctionFieldData;
import org.genfork.tools.database.store.FunctionMetadata;
import org.postgresql.PGProperty;

import java.math.BigDecimal;
import java.sql.*;
import java.util.LinkedHashMap;
import java.util.Objects;
import java.util.Properties;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Slf4j
public abstract class AbstractDatabaseFactory {
	@Getter
	private HikariDataSource connectionPool;

	public static final int IN = 1;
	public static final int INOUT = 2;
	public static final int OUT = 4;
	public static final int FUNCTION_RETURN = 5;

	@Getter
	private final LinkedHashMap<String, FunctionMetadata> functions = new LinkedHashMap<>();

	public AbstractDatabaseFactory() {
		preInit();
		postInit();
	}

	public abstract void preInit();

	public abstract void postInit();

	public void start(String url, String username, String password, int maxPoolSize, int minIdleConnections, int connectionTimeout, int idleConnectionTimout) {
		checkAndInitDatabase(url, username, password);

		try {
			final HikariConfig config = new HikariConfig();
			config.setJdbcUrl(url);
			config.setUsername(username);
			config.setPassword(password);

			config.setMaximumPoolSize(maxPoolSize);
			config.setMinimumIdle(minIdleConnections);

			config.setConnectionTimeout(connectionTimeout);
			config.setIdleTimeout(idleConnectionTimout);

			connectionPool = new HikariDataSource(config);
		} catch (RuntimeException e) {
			log.warn("Could not init database connection.", e);
		}
	}

	private void checkAndInitDatabase(String url, String username, String password) {
		final Driver driver = new org.postgresql.Driver();
		final Properties properties = new Properties();
		properties.put(PGProperty.USER.getName(), Objects.requireNonNull(username));
		properties.put(PGProperty.PASSWORD.getName(), Objects.requireNonNull(password));

		final String database = Objects.requireNonNull(url).substring(url.lastIndexOf("/") + 1);

		try (final Connection connection = driver.connect(url.substring(0, url.lastIndexOf("/")) + "/", properties)) {
			final PreparedStatement statement = connection.prepareStatement("CREATE DATABASE " + database
					+ " OWNER = " + username
					+ " TABLESPACE = pg_default"
					+ " ENCODING = UTF8");
			statement.execute();
		} catch (SQLException e) {
			log.warn(e.getLocalizedMessage());
		}
	}

	public void retrieveFunctionsMetaData() {
		try {
			final Connection connection = getConnection();
			final ResultSet functionData = JdbcUtils.getDatabaseMetaData(connection)
					.getProcedureColumns(connection.getCatalog(), null, null, null);

			String oldProcedureName = null;
			int idx = 0;
			while (functionData.next()) {
				final String functionName = functionData.getString("PROCEDURE_NAME");
				if (oldProcedureName != null && !oldProcedureName.equals(functionName)) {
					idx = 0;
				}

				oldProcedureName = functionName;

				final String columnName = functionData.getString("COLUMN_NAME");
				final int paramType = functionData.getInt("COLUMN_TYPE");
				final int columnDataType = functionData.getInt("DATA_TYPE");
				final String typeName = functionData.getString("TYPE_NAME");
				final Class<?> type = toJavaType(columnDataType, typeName);

				if (paramType != IN && paramType != INOUT) {
					if (!functions.containsKey(functionName)) {
						this.functions.put(functionName, new FunctionMetadata());
					}

					continue;
				}

				final FunctionFieldData functionFieldData = new FunctionFieldData();
				functionFieldData.setIdx(idx);
				functionFieldData.setParamType(paramType);
				functionFieldData.setType(type);
				functionFieldData.setTypeString(typeName);

				final FunctionMetadata found = this.functions.get(functionName);
				if (found == null) {
					final FunctionMetadata functionMetadata = new FunctionMetadata();
					setColumnInfo(columnName, functionFieldData, functionMetadata);
					this.functions.put(functionName, functionMetadata);
				} else {
					setColumnInfo(columnName, functionFieldData, found);
					found.getParams().put(columnName, functionFieldData);
				}

				idx++;
			}

			functionData.close();
			connection.close();
		} catch (SQLException e) {
			log.error("Failed read database metadata structures info.");
		}
	}

	private void setColumnInfo(String columnName, FunctionFieldData functionFieldData, FunctionMetadata found) {
		if (functionFieldData.isInputParameter() || functionFieldData.isInOutParameter()) {
			found.getParams().put(columnName, functionFieldData);
		} else if (functionFieldData.isFunctionReturnParameter()) {
			found.setReturnType(functionFieldData.getType());
		}
	}

	private Class<?> toJavaType(int type, String typeName) {
		Class<?> result = Object.class;

		if (type == Types.CHAR || type == Types.VARCHAR || type == Types.LONGVARCHAR) {
			result = String.class;
		} else if (type == Types.NUMERIC || type == Types.DECIMAL) {
			result = BigDecimal.class;
		} else if (type == Types.BIT) {
			result = Boolean.class;
		} else if (type == Types.TINYINT) {
			result = Byte.class;
		} else if (type == Types.SMALLINT) {
			result = Short.class;
		} else if (type == Types.INTEGER) {
			result = Integer.class;
		} else if (type == Types.BIGINT) {
			result = Long.class;
		} else if (type == Types.REAL || type == Types.FLOAT) {
			result = Float.class;
		} else if (type == Types.DOUBLE) {
			result = Double.class;
		} else if (type == Types.BINARY || type == Types.VARBINARY || type == Types.LONGVARBINARY) {
			result = Byte[].class;
		} else if (type == Types.DATE) {
			result = Date.class;
		} else if (type == Types.TIME) {
			result = Time.class;
		} else if (type == Types.TIMESTAMP) {
			result = Timestamp.class;
		} else if (type == Types.ARRAY || typeName.startsWith("_")) {
			result = Array.class;
		}

		return result;
	}

	public void stop() {
		connectionPool.close();
	}

	public Connection getConnection() {
		try {
			return connectionPool.getConnection();
		} catch (SQLException e) {
			log.warn("Can't get connection from database", e);
		}

		return null;
	}
}
