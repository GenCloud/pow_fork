package org.genfork.tools.database.store;

import lombok.Data;

import java.util.LinkedHashMap;

/**
 * @author: ngolubenko@turbosolution.ru
 * @date: 02/2020
 */
@Data
public class FunctionMetadata {
	private final LinkedHashMap<String, FunctionFieldData> params = new LinkedHashMap<>();

	private Class<?> returnType;
}
