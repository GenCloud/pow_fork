package org.genfork.tools.common;

import lombok.experimental.UtilityClass;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@UtilityClass
public class CommonUtil {
	private final DateTimeFormatter FILENAME_DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH-mm-ss");

	@SafeVarargs
	public <T> List<T> argAndVarArgsToList(T value, T... values) {
		final List<T> valueList = new ArrayList<>(1 + (values != null ? values.length : 0));
		forEachArgAndVarArgs(valueList::add, value, values);
		return Collections.unmodifiableList(valueList);
	}

	@SafeVarargs
	public <T> void forEachArgAndVarArgs(Consumer<T> consumer, T value, T... values) {
		consumer.accept(value);
		if (values != null) {
			Arrays.stream(values).forEach(consumer);
		}
	}

	public DateTimeFormatter getFilenameDateTimeFormatter() {
		return FILENAME_DATE_TIME_FORMATTER;
	}
}
