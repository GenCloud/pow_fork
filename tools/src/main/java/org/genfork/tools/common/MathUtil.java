package org.genfork.tools.common;

import lombok.experimental.UtilityClass;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@UtilityClass
public class MathUtil {
	public byte add(byte oldValue, byte value) {
		return (byte) (oldValue + value);
	}

	public short add(short oldValue, short value) {
		return (short) (oldValue + value);
	}

	public int add(int oldValue, int value) {
		return oldValue + value;
	}

	public double add(double oldValue, double value) {
		return oldValue + value;
	}

	public byte mul(byte oldValue, byte value) {
		return (byte) (oldValue * value);
	}

	public short mul(short oldValue, short value) {
		return (short) (oldValue * value);
	}

	public int mul(int oldValue, int value) {
		return oldValue * value;
	}

	public double mul(double oldValue, double value) {
		return oldValue * value;
	}

	public byte div(byte oldValue, byte value) {
		return (byte) (oldValue / value);
	}

	public short div(short oldValue, short value) {
		return (short) (oldValue / value);
	}

	public int div(int oldValue, int value) {
		return oldValue / value;
	}

	public double div(double oldValue, double value) {
		return oldValue / value;
	}

	public byte sub(byte oldValue, byte value) {
		return (byte) (oldValue - value);
	}

	public short sub(short oldValue, short value) {
		return (short) (oldValue - value);
	}

	public int sub(int oldValue, int value) {
		return oldValue - value;
	}

	public double sub(double oldValue, double value) {
		return oldValue - value;
	}
}
