package org.genfork.tools.xml;

import org.genfork.tools.path.PathProvider;
import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static org.genfork.tools.path.OverrideMode.INCREMENTAL;

public interface XmlReader {
	/**
	 * Whether logging is allowed or not.
	 *
	 * @return logging
	 */
	default boolean overrideSystemLogging() {
		return true;
	}

	/**
	 * A simple accepter filter for override system.
	 *
	 * @return whether file is accepted by the filter or not
	 */
	default boolean acceptFilter(Path path) {
		return true;
	}

	/**
	 * Wrapper for {@link #loadFile(Element)} method.
	 *
	 * @param path the relative path to the datapack root of the XML file to parse.
	 */
	default void parseFile(String path, Path dataPack) {
		parseFile(Paths.get(path), dataPack);
	}

	default void parseFile(Path path, Path dataPack) {
		final PathProvider pathProvider = new PathProvider().setAcceptFilter(this::acceptFilter).setOverrideLogging(overrideSystemLogging());
		for (Path file : pathProvider.resolveOverriddenFile(path, dataPack, INCREMENTAL)) {
			if (path.toString().endsWith(".xml")) {
				final SAXBuilder builder = new SAXBuilder();
				try {
					final Document document = builder.build(file.toFile());
					final Element rootElement = document.getRootElement();
					loadFile(rootElement);
				} catch (JDOMException | IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * Wrapper for {@link #parseDirectory(Path, Path, boolean)}.
	 *
	 * @param path      the path to the directory where the XML files are
	 * @param recursive parses all sub folders if there is
	 * @return {@code false} if it fails to find the directory, {@code true} otherwise
	 */
	default boolean parseDirectory(String path, Path dataPack, boolean recursive) throws IOException {
		return parseDirectory(Paths.get(path), dataPack, recursive);
	}

	/**
	 * Loads all XML files from {@code path} and for each one of them.
	 *
	 * @param dirPath   the directory object to scan.
	 * @param recursive parses all sub folders if there is.
	 * @return {@code false} if it fails to find the directory, {@code true} otherwise.
	 */
	default boolean parseDirectory(Path dirPath, Path dataPack, boolean recursive) throws IOException {
		final PathProvider pathProvider = new PathProvider().setAcceptFilter(this::acceptFilter).setOverrideLogging(overrideSystemLogging());
		pathProvider.resolveOverriddenDirectory(dirPath, dataPack, recursive, INCREMENTAL)
				.forEach(path -> {
					if (path.toString().endsWith(".xml")) {
						try {
							final SAXBuilder builder = new SAXBuilder();
							final Document document = builder.build(path.toFile());
							final Element rootElement = document.getRootElement();
							loadFile(rootElement);
						} catch (IOException | JDOMException e) {
							e.printStackTrace();
						}
					}
				});

		return true;
	}

	void loadFile(Element rootElement);

	default Attribute getAttribute(Element element, String name) {
		return element.getAttribute(name);
	}

	default String getValue(Element element, String attrName) {
		return element.getAttributeValue(attrName).trim();
	}

	default <O extends Enum<O>> O parseEnum(Element element, Class<O> enumClass) {
		if (element != null && element.getValue() != null) {
			return Enum.valueOf(enumClass, element.getText().trim());
		}

		return null;
	}

	default <O extends Enum<O>> O parseEnum(Element element, Class<O> enumClass, O defaultValue) {
		if (element != null && element.getValue() != null) {
			try {
				return Enum.valueOf(enumClass, element.getText().trim());
			} catch (Exception ex) {
				return defaultValue;
			}
		}

		return null;
	}

	default <O extends Enum<O>> O parseEnum(Attribute attribute, Class<O> enumClass) {
		if (attribute != null && attribute.getValue() != null) {
			return Enum.valueOf(enumClass, attribute.getValue().trim());
		}

		return null;
	}

	default <O extends Enum<O>> O parseEnum(Element element, String attrName, Class<O> enumClass) {
		final Attribute attribute = getAttribute(element, attrName);
		return parseEnum(attribute, enumClass);
	}

	default boolean parseBoolean(Element element) {
		if (element != null && element.getValue() != null) {
			return Boolean.parseBoolean(element.getText().trim());
		}

		return false;
	}

	default boolean parseBoolean(Attribute attribute) {
		if (attribute != null && attribute.getValue() != null) {
			return Boolean.parseBoolean(attribute.getValue().trim());
		}

		return false;
	}

	default boolean parseBoolean(Element element, String attrName) {
		final Attribute attribute = getAttribute(element, attrName);
		return parseBoolean(attribute);
	}

	default boolean parseBoolean(Element element, String attrName, Boolean defaultValue) {
		final Attribute attribute = getAttribute(element, attrName);
		if (attribute == null) {
			return defaultValue;
		}

		return parseBoolean(attribute);
	}

	default byte parseByte(Element element) {
		if (element != null && element.getValue() != null) {
			return Byte.parseByte(element.getText().trim());
		}

		return 0;
	}

	default byte parseByte(Attribute attribute) {
		if (attribute != null && attribute.getValue() != null) {
			return Byte.parseByte(attribute.getValue().trim());
		}

		return 0;
	}

	default byte parseByte(Element element, String attrName) {
		final Attribute attribute = getAttribute(element, attrName);
		return parseByte(attribute);
	}

	default double parseDouble(Element element) {
		if (element != null && element.getValue() != null) {
			return Double.parseDouble(element.getText().trim());
		}

		return 0.;
	}

	default double parseDouble(Attribute attribute) {
		if (attribute != null && attribute.getValue() != null) {
			return Double.parseDouble(attribute.getValue().trim());
		}

		return 0.;
	}

	default double parseDouble(Element element, String attrName) {
		final Attribute attribute = getAttribute(element, attrName);
		return parseDouble(attribute);
	}

	default short parseShort(Element element) {
		if (element != null && element.getValue() != null) {
			return Short.parseShort(element.getText().trim());
		}

		return 0;
	}

	default short parseShort(Attribute attribute) {
		if (attribute != null && attribute.getValue() != null) {
			return Short.parseShort(attribute.getValue().trim());
		}

		return 0;
	}

	default short parseShort(Element element, String attrName) {
		final Attribute attribute = getAttribute(element, attrName);
		return parseShort(attribute);
	}

	default int parseInt(Element element, int defaultValue) {
		if (element != null && element.getValue() != null) {
			return Integer.parseInt(element.getText().trim());
		}

		return defaultValue;
	}

	default Integer parseInt(Element element) {
		if (element != null && element.getText() != null) {
			return Integer.parseInt(element.getText().trim());
		}

		return null;
	}

	default Integer parseInt(Attribute attribute, Integer defaultValue) {
		if (attribute != null && attribute.getValue() != null) {
			return Integer.parseInt(attribute.getValue().trim());
		}

		return defaultValue;
	}

	default Integer parseInt(Attribute attribute) {
		if (attribute != null && attribute.getValue() != null) {
			return Integer.parseInt(attribute.getValue().trim());
		}

		return null;
	}

	default Integer parseInt(Element element, String attrName, Integer defaultValue) {
		final Attribute attribute = getAttribute(element, attrName);
		return parseInt(attribute, defaultValue);
	}

	default Integer parseInt(Element element, String attrName) {
		final Attribute attribute = getAttribute(element, attrName);
		return parseInt(attribute);
	}

	default long parseLong(Attribute attribute) {
		if (attribute != null && attribute.getValue() != null) {
			return Long.parseLong(attribute.getValue().trim());
		}

		return 0;
	}

	default long parseLong(Element element) {
		if (element != null && element.getValue() != null) {
			return Long.parseLong(element.getText().trim());
		}

		return 0;
	}

	default long parseLong(Element element, String attrName) {
		final Attribute attribute = getAttribute(element, attrName);
		return parseLong(attribute);
	}

	/**
	 * @return parses all attributes to a Map
	 */
	default Map<String, Object> parseAttributes(Element element) {
		final List<Attribute> attrs = element.getAttributes();
		return attrs
				.stream()
				.collect(Collectors.toMap(Attribute::getName, Attribute::getValue, (a, b) -> b, LinkedHashMap::new));
	}

	/**
	 * Executes action for each child that matches nodeName
	 *
	 * @param elementName - multiple nodes can be specified by separating them with |
	 */
	default void forEach(Element element, String elementName, Consumer<Element> action) {
		forEach(element, innerNode -> {
			if (elementName.contains("|")) {
				final String[] nodeNames = elementName.split("\\|");
				return Arrays.stream(nodeNames).anyMatch(name -> !name.isEmpty() && name.equalsIgnoreCase(innerNode.getName()));
			}
			return elementName.equalsIgnoreCase(innerNode.getName());
		}, action);
	}

	/**
	 * Executes action for each child of node if matches the filter specified
	 */
	default void forEach(Element node, Predicate<Element> filter, Consumer<Element> action) {
		final List<Element> children = node.getChildren();
		children.stream().filter(filter).forEach(action);
	}

	/**
	 * Executes action for each child of node if matches the filter specified
	 */
	default void forEach(Element node, Consumer<Element> action) {
		final List<Element> children = node.getChildren();
		children.forEach(action);
	}
}
