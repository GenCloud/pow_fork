package org.genfork.tools.data;

import lombok.extern.slf4j.Slf4j;
import org.genfork.tools.network.OutgoingPacket;
import org.genfork.tools.time.TimeUtil;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.sql.Timestamp;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * This class is meant to hold a set of (key,value) pairs.<br>
 * They are stored as object but can be retrieved in any type wanted. As long as cast is available.<br>
 */
@Slf4j
public class StatsSet implements IParserAdvUtils, Serializable {
	/**
	 * Static empty immutable map, used to avoid multiple null checks over the source.
	 */
	public static final StatsSet EMPTY_STATSET = new StatsSet(Collections.emptyMap());

	private final Map<String, Object> objectMap;

	public StatsSet() {
		this(ConcurrentHashMap::new);
	}

	public StatsSet(Supplier<Map<String, Object>> mapFactory) {
		this(mapFactory.get());
	}

	public StatsSet(Map<String, Object> objectMap) {
		this.objectMap = objectMap;
	}

	public static StatsSet valueOf(String key, Object value) {
		final StatsSet set = new StatsSet();
		set.set(key, value);
		return set;
	}

	/**
	 * Returns the set of values
	 *
	 * @return HashMap
	 */
	public final Map<String, Object> getSet() {
		return objectMap;
	}

	/**
	 * Add a set of couple values in the current set
	 *
	 * @param newSet : StatsSet pointing out the list of couples to add in the current set
	 */
	public void merge(StatsSet newSet) {
		objectMap.putAll(newSet.getSet());
	}

	/**
	 * Verifies if the stat set is empty.
	 *
	 * @return {@code true} if the stat set is empty, {@code false} otherwise
	 */
	public boolean isEmpty() {
		return objectMap.isEmpty();
	}

	/**
	 * Return the boolean value associated with key.
	 *
	 * @param key : String designating the key in the set
	 * @return boolean : value associated to the key
	 * @throws IllegalArgumentException : If value is not set or value is not boolean
	 */
	@Override
	public boolean getBoolean(String key) {
		Objects.requireNonNull(key);
		final Object val = objectMap.get(key);
		if (val == null) {
			throw new IllegalArgumentException("Boolean value required, but not specified");
		}

		if (val instanceof Boolean) {
			return (Boolean) val;
		}

		try {
			return Boolean.parseBoolean((String) val);
		} catch (Exception e) {
			throw new IllegalArgumentException("Boolean value required, but found: " + val);
		}
	}

	/**
	 * Return the boolean value associated with key.<br>
	 * If no value is associated with key, or type of value is wrong, returns defaultValue.
	 *
	 * @param key : String designating the key in the entry set
	 * @return boolean : value associated to the key
	 */
	@Override
	public boolean getBoolean(String key, boolean defaultValue) {
		Objects.requireNonNull(key);
		final Object val = objectMap.get(key);
		if (val == null) {
			return defaultValue;
		}
		if (val instanceof Boolean) {
			return (Boolean) val;
		}
		try {
			return Boolean.parseBoolean((String) val);
		} catch (Exception e) {
			return defaultValue;
		}
	}

	@Override
	public byte getByte(String key) {
		Objects.requireNonNull(key);
		final Object val = objectMap.get(key);
		if (val == null) {
			throw new IllegalArgumentException("Byte value required, but not specified");
		}

		if (val instanceof Number) {
			return ((Number) val).byteValue();
		}

		try {
			return Byte.parseByte((String) val);
		} catch (Exception e) {
			throw new IllegalArgumentException("Byte value required, but found: " + val);
		}
	}

	@Override
	public byte getByte(String key, byte defaultValue) {
		Objects.requireNonNull(key);
		final Object val = objectMap.get(key);
		if (val == null) {
			return defaultValue;
		}
		if (val instanceof Number) {
			return ((Number) val).byteValue();
		}
		try {
			return Byte.parseByte((String) val);
		} catch (Exception e) {
			throw new IllegalArgumentException("Byte value required, but found: " + val);
		}
	}

	@Override
	public short getShort(String key) {
		Objects.requireNonNull(key);
		final Object val = objectMap.get(key);
		if (val == null) {
			throw new IllegalArgumentException("Short value required, but not specified");
		}
		if (val instanceof Number) {
			return ((Number) val).shortValue();
		}
		try {
			return Short.parseShort((String) val);
		} catch (Exception e) {
			throw new IllegalArgumentException("Short value required, but found: " + val);
		}
	}

	@Override
	public short getShort(String key, short defaultValue) {
		Objects.requireNonNull(key);
		final Object val = objectMap.get(key);
		if (val == null) {
			return defaultValue;
		}
		if (val instanceof Number) {
			return ((Number) val).shortValue();
		}
		try {
			return Short.parseShort((String) val);
		} catch (Exception e) {
			throw new IllegalArgumentException("Short value required, but found: " + val);
		}
	}

	@Override
	public int getInt(String key) {
		Objects.requireNonNull(key);
		final Object val = objectMap.get(key);
		if (val == null) {
			throw new IllegalArgumentException("Integer value required, but not specified: " + key + "!");
		}

		if (val instanceof Number) {
			return ((Number) val).intValue();
		}

		try {
			return Integer.parseInt((String) val);
		} catch (Exception e) {
			throw new IllegalArgumentException("Integer value required, but found: " + val + "!");
		}
	}

	@Override
	public int getInt(String key, int defaultValue) {
		Objects.requireNonNull(key);
		final Object val = objectMap.get(key);
		if (val == null) {
			return defaultValue;
		}
		if (val instanceof Number) {
			return ((Number) val).intValue();
		}
		try {
			return Integer.parseInt((String) val);
		} catch (Exception e) {
			throw new IllegalArgumentException("Integer value required, but found: " + val);
		}
	}

	@Override
	public long getLong(String key) {
		Objects.requireNonNull(key);
		final Object val = objectMap.get(key);
		if (val == null) {
			throw new IllegalArgumentException("Long value required, but not specified");
		}
		if (val instanceof Number) {
			return ((Number) val).longValue();
		}
		try {
			return Long.parseLong((String) val);
		} catch (Exception e) {
			throw new IllegalArgumentException("Long value required, but found: " + val);
		}
	}

	@Override
	public long getLong(String key, long defaultValue) {
		Objects.requireNonNull(key);
		final Object val = objectMap.get(key);
		if (val == null) {
			return defaultValue;
		}
		if (val instanceof Number) {
			return ((Number) val).longValue();
		}
		try {
			return Long.parseLong((String) val);
		} catch (Exception e) {
			throw new IllegalArgumentException("Long value required, but found: " + val);
		}
	}

	@Override
	public float getFloat(String key) {
		Objects.requireNonNull(key);
		final Object val = objectMap.get(key);
		if (val == null) {
			throw new IllegalArgumentException("Float value required, but not specified");
		}
		if (val instanceof Number) {
			return ((Number) val).floatValue();
		}
		try {
			return Float.parseFloat((String) val);
		} catch (Exception e) {
			throw new IllegalArgumentException("Float value required, but found: " + val);
		}
	}

	@Override
	public float getFloat(String key, float defaultValue) {
		Objects.requireNonNull(key);
		final Object val = objectMap.get(key);
		if (val == null) {
			return defaultValue;
		}
		if (val instanceof Number) {
			return ((Number) val).floatValue();
		}
		try {
			return Float.parseFloat((String) val);
		} catch (Exception e) {
			throw new IllegalArgumentException("Float value required, but found: " + val);
		}
	}

	@Override
	public double getDouble(String key) {
		Objects.requireNonNull(key);
		final Object val = objectMap.get(key);
		if (val == null) {
			throw new IllegalArgumentException("Double value required, but not specified");
		}
		if (val instanceof Number) {
			return ((Number) val).doubleValue();
		}
		try {
			return Double.parseDouble((String) val);
		} catch (Exception e) {
			throw new IllegalArgumentException("Double value required, but found: " + val);
		}
	}

	@Override
	public double getDouble(String key, double defaultValue) {
		Objects.requireNonNull(key);
		final Object val = objectMap.get(key);
		if (val == null) {
			return defaultValue;
		}
		if (val instanceof Number) {
			return ((Number) val).doubleValue();
		}
		try {
			return Double.parseDouble((String) val);
		} catch (Exception e) {
			throw new IllegalArgumentException("Double value required, but found: " + val);
		}
	}

	@Override
	public String getString(String key) {
		Objects.requireNonNull(key);
		final Object val = objectMap.get(key);
		if (val == null) {
			throw new IllegalArgumentException("String value required, but not specified");
		}
		return String.valueOf(val);
	}

	@Override
	public String getString(String key, String defaultValue) {
		Objects.requireNonNull(key);
		final Object val = objectMap.get(key);
		if (val == null) {
			return defaultValue;
		}
		return String.valueOf(val);
	}

	@Override
	public Duration getDuration(String key) {
		Objects.requireNonNull(key);
		final Object val = objectMap.get(key);
		if (val == null) {
			throw new IllegalArgumentException("String value required, but not specified");
		}
		return TimeUtil.parseDuration(String.valueOf(val));
	}

	@Override
	public Duration getDuration(String key, Duration defaultValue) {
		Objects.requireNonNull(key);
		final Object val = objectMap.get(key);
		if (val == null) {
			return defaultValue;
		}
		return TimeUtil.parseDuration(String.valueOf(val));
	}

	@Override
	@SuppressWarnings("unchecked")
	public <T extends Enum<T>> T getEnum(String key, Class<T> enumClass) {
		Objects.requireNonNull(key);
		Objects.requireNonNull(enumClass);
		final Object val = objectMap.get(key);
		if (val == null) {
			throw new IllegalArgumentException("Enum value of type " + enumClass.getName() + " required, but not specified");
		}
		if (enumClass.isInstance(val)) {
			return (T) val;
		}
		try {
			return Enum.valueOf(enumClass, String.valueOf(val));
		} catch (Exception e) {
			throw new IllegalArgumentException("Enum value of type " + enumClass.getName() + " required, but found: " + val);
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public <T extends Enum<T>> T getEnum(String key, Class<T> enumClass, T defaultValue) {
		Objects.requireNonNull(key);
		Objects.requireNonNull(enumClass);
		final Object val = objectMap.get(key);
		if (val == null) {
			return defaultValue;
		}
		if (enumClass.isInstance(val)) {
			return (T) val;
		}
		try {
			return Enum.valueOf(enumClass, key.equals("default_action") ? "action_" + String.valueOf(val).toLowerCase() : String.valueOf(val));
		} catch (Exception e) {
			throw new IllegalArgumentException("Enum value of type " + enumClass.getName() + " required, but found: " + val);
		}
	}

	@SuppressWarnings("unused")
	public short increaseByte(String key, byte increaseWith) {
		byte newValue = (byte) (getByte(key) + increaseWith);
		set(key, newValue);
		return newValue;
	}

	@SuppressWarnings("unused")
	public short increaseByte(String key, byte defaultValue, byte increaseWith) {
		byte newValue = (byte) (getByte(key, defaultValue) + increaseWith);
		set(key, newValue);
		return newValue;
	}

	public byte[] getByteArray(String key, String splitOn) {
		Objects.requireNonNull(key);
		Objects.requireNonNull(splitOn);
		final Object val = objectMap.get(key);
		if (val == null) {
			throw new IllegalArgumentException("Byte value required, but not specified");
		}
		if (val instanceof Number) {
			return new byte[]
					{
							((Number) val).byteValue()
					};
		}
		int c = 0;
		String[] vals = ((String) val).split(splitOn);
		byte[] result = new byte[vals.length];
		for (String v : vals) {
			try {
				result[c++] = Byte.parseByte(v);
			} catch (Exception e) {
				throw new IllegalArgumentException("Byte value required, but found: " + val);
			}
		}
		return result;
	}

	@SuppressWarnings("unused")
	public List<Byte> getByteList(String key, String splitOn) {
		Objects.requireNonNull(key);
		Objects.requireNonNull(splitOn);
		final List<Byte> result = new ArrayList<>();
		for (Byte i : getByteArray(key, splitOn)) {
			result.add(i);
		}
		return result;
	}

	@SuppressWarnings("unused")
	public short increaseShort(String key, short increaseWith) {
		short newValue = (short) (getShort(key) + increaseWith);
		set(key, newValue);
		return newValue;
	}

	@SuppressWarnings("unused")
	public short increaseShort(String key, short defaultValue, short increaseWith) {
		short newValue = (short) (getShort(key, defaultValue) + increaseWith);
		set(key, newValue);
		return newValue;
	}

	public int increaseInt(String key, int increaseWith) {
		int newValue = getInt(key) + increaseWith;
		set(key, newValue);
		return newValue;
	}

	public int increaseInt(String key, int defaultValue, int increaseWith) {
		int newValue = getInt(key, defaultValue) + increaseWith;
		set(key, newValue);
		return newValue;
	}

	public int[] getIntArray(String key, String splitOn) {
		Objects.requireNonNull(key);
		Objects.requireNonNull(splitOn);
		final Object val = objectMap.get(key);
		if (val == null) {
			throw new IllegalArgumentException("Integer value required, but not specified");
		}
		if (val instanceof Number) {
			return new int[]
					{
							((Number) val).intValue()
					};
		}
		int c = 0;
		String[] vals = ((String) val).split(splitOn);
		int[] result = new int[vals.length];
		for (String v : vals) {
			try {
				result[c++] = Integer.parseInt(v);
			} catch (Exception e) {
				throw new IllegalArgumentException("Integer value required, but found: " + val);
			}
		}
		return result;
	}

	public List<Integer> getIntegerList(String key, String splitOn) {
		Objects.requireNonNull(key);
		Objects.requireNonNull(splitOn);
		final List<Integer> result = new ArrayList<>();
		for (int i : getIntArray(key, splitOn)) {
			result.add(i);
		}
		return result;
	}

	@SuppressWarnings("unused")
	public long increaseLong(String key, long increaseWith) {
		long newValue = getLong(key) + increaseWith;
		set(key, newValue);
		return newValue;
	}

	@SuppressWarnings("unused")
	public long increaseLong(String key, long defaultValue, long increaseWith) {
		long newValue = getLong(key, defaultValue) + increaseWith;
		set(key, newValue);
		return newValue;
	}

	@SuppressWarnings("unused")
	public float increaseFloat(String key, float increaseWith) {
		float newValue = getFloat(key) + increaseWith;
		set(key, newValue);
		return newValue;
	}

	@SuppressWarnings("unused")
	public float increaseFloat(String key, float defaultValue, float increaseWith) {
		float newValue = getFloat(key, defaultValue) + increaseWith;
		set(key, newValue);
		return newValue;
	}

	@SuppressWarnings("unused")
	public double increaseDouble(String key, double increaseWith) {
		double newValue = getDouble(key) + increaseWith;
		set(key, newValue);
		return newValue;
	}

	@SuppressWarnings("unused")
	public double increaseDouble(String key, double defaultValue, double increaseWith) {
		double newValue = getDouble(key, defaultValue) + increaseWith;
		set(key, newValue);
		return newValue;
	}

	@SuppressWarnings("unchecked")
	public final <A> A getObject(String name, Class<A> type) {
		Objects.requireNonNull(name);
		Objects.requireNonNull(type);
		final Object obj = objectMap.get(name);
		if ((obj == null) || !type.isAssignableFrom(obj.getClass())) {
			return null;
		}

		return (A) obj;
	}

	@SuppressWarnings("unchecked")
	public final <A> A getObject(String name, Class<A> type, A defaultValue) {
		Objects.requireNonNull(name);
		Objects.requireNonNull(type);
		final Object obj = objectMap.get(name);
		if ((obj == null) || !type.isAssignableFrom(obj.getClass())) {
			return defaultValue;
		}

		return (A) obj;
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> getList(String key, Class<T> clazz) {
		Objects.requireNonNull(key);
		Objects.requireNonNull(clazz);
		final Object obj = objectMap.get(key);
		if (!(obj instanceof List<?>)) {
			return null;
		}

		final List<Object> originalList = (List<Object>) obj;
		if (!originalList.isEmpty() && !originalList.stream().allMatch(clazz::isInstance)) {
			if (clazz.getSuperclass() == Enum.class) {
				throw new IllegalAccessError("Please use getEnumList if you want to get list of Enums!");
			}

			// Attempt to convert the list
			final List<T> convertedList = convertList(originalList, clazz);
			if (convertedList == null) {
				log.warn("getList(\"{}\", {}) requested with wrong generic type: {}!", key, clazz.getSimpleName(), obj.getClass().getGenericInterfaces()[0], new ClassNotFoundException());
				return null;
			}

			// Overwrite the existing list with proper generic type
			objectMap.put(key, convertedList);
			return convertedList;
		}
		return (List<T>) obj;
	}

	public <T> List<T> getList(String key, Class<T> clazz, List<T> defaultValue) {
		final List<T> list = getList(key, clazz);
		return list == null ? defaultValue : list;
	}

	@SuppressWarnings("unchecked")
	public <T extends Enum<T>> List<T> getEnumList(String key, Class<T> clazz) {
		final Object obj = objectMap.get(key);
		if (!(obj instanceof List<?>)) {
			return null;
		}

		final List<Object> originalList = (List<Object>) obj;
		if (!originalList.isEmpty() && (obj.getClass().getGenericInterfaces()[0] != clazz) && originalList.stream().allMatch(name -> isEnum(name.toString(), clazz))) {
			final List<T> convertedList = originalList.stream().map(Object::toString).map(name -> Enum.valueOf(clazz, name)).map(clazz::cast).collect(Collectors.toList());

			// Overwrite the existing list with proper generic type
			objectMap.put(key, convertedList);
			return convertedList;
		}
		return (List<T>) obj;
	}

	private <T> List<T> convertList(List<Object> originalList, Class<T> clazz) {
		if (clazz == Integer.class) {
			if (originalList.stream().map(Object::toString).allMatch(this::isInteger)) {
				return originalList.stream().map(Object::toString).map(Integer::valueOf).map(clazz::cast).collect(Collectors.toList());
			}
		} else if (clazz == Float.class) {
			if (originalList.stream().map(Object::toString).allMatch(this::isFloat)) {
				return originalList.stream().map(Object::toString).map(Float::valueOf).map(clazz::cast).collect(Collectors.toList());
			}
		} else if (clazz == Double.class) {
			if (originalList.stream().map(Object::toString).allMatch(this::isDouble)) {
				return originalList.stream().map(Object::toString).map(Double::valueOf).map(clazz::cast).collect(Collectors.toList());
			}
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public <K, V> Map<K, V> getMap(String key, Class<K> keyClass, Class<V> valueClass) {
		final Object obj = objectMap.get(key);
		if (!(obj instanceof Map<?, ?>)) {
			return null;
		}

		final Map<?, ?> originalList = (Map<?, ?>) obj;
		if (!originalList.isEmpty()) {
			if ((!originalList.keySet().stream().allMatch(keyClass::isInstance)) || (!originalList.values().stream().allMatch(valueClass::isInstance))) {
				log.warn("getMap(\"{}\", {}, {}) requested with wrong generic type: {}!", key, keyClass.getSimpleName(), valueClass.getSimpleName(), obj.getClass().getGenericInterfaces()[0], new ClassNotFoundException());
			}
		}
		return (Map<K, V>) obj;
	}

	@SuppressWarnings("unchecked")
	public <K, V> Map<K, List<V>> getMapOfList(String key, Class<K> keyClass, Class<V> valueClass) {
		final Object obj = objectMap.get(key);
		if (!(obj instanceof Map<?, ?>)) {
			return null;
		}

		final Map<?, ?> originalList = (Map<?, ?>) obj;
		if (!originalList.isEmpty()) {
			if ((!originalList.keySet().stream().allMatch(keyClass::isInstance)) || (!originalList.values().stream().allMatch(List.class::isInstance))) {
				log.warn("getMap(\"{}\", {}, {}) requested with wrong generic type: {}!", key, keyClass.getSimpleName(), valueClass.getSimpleName(), obj.getClass().getGenericInterfaces()[0], new ClassNotFoundException());
			}
		}
		return (Map<K, List<V>>) obj;
	}

	public StatsSet set(String name, Object value) {
		if (value == null) {
			return this;
		}
		objectMap.put(name, value);
		return this;
	}

	public StatsSet set(String key, boolean value) {
		objectMap.put(key, value);
		return this;
	}

	public StatsSet set(String key, byte value) {
		objectMap.put(key, value);
		return this;
	}

	public StatsSet set(String key, short value) {
		objectMap.put(key, value);
		return this;
	}

	public StatsSet set(String key, int value) {
		objectMap.put(key, value);
		return this;
	}

	public StatsSet set(String key, long value) {
		objectMap.put(key, value);
		return this;
	}

	public StatsSet set(String key, float value) {
		objectMap.put(key, value);
		return this;
	}

	public StatsSet set(String key, double value) {
		objectMap.put(key, value);
		return this;
	}

	public StatsSet set(String key, String value) {
		if (value == null) {
			return this;
		}
		objectMap.put(key, value);
		return this;
	}

	public StatsSet set(String key, Enum<?> value) {
		if (value == null) {
			return this;
		}
		objectMap.put(key, value);
		return this;
	}

	public void remove(String key) {
		objectMap.remove(key);
	}

	public boolean contains(String name) {
		return objectMap.containsKey(name);
	}

	public <T extends Enum<T>> boolean isEnum(String name, Class<T> enumType) {
		if (name == null || name.isEmpty()) {
			return false;
		}

		try {
			Enum.valueOf(enumType, name);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * @param text - the text to check
	 * @return {@code true} if {@code text} contains only numbers, {@code false} otherwise
	 */
	public boolean isDigit(String text) {
		if (text == null || text.isEmpty()) {
			return false;
		}

		for (char c : text.toCharArray()) {
			if (!Character.isDigit(c)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * @param text - the text to check
	 * @return {@code true} if {@code text} is integer, {@code false} otherwise
	 */
	public boolean isInteger(String text) {
		if (text == null || text.isEmpty()) {
			return false;
		}

		try {
			Integer.parseInt(text);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * @param text - the text to check
	 * @return {@code true} if {@code text} is float, {@code false} otherwise
	 */
	public boolean isFloat(String text) {
		if (text == null || text.isEmpty()) {
			return false;
		}

		try {
			Float.parseFloat(text);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * @param text - the text to check
	 * @return {@code true} if {@code text} is double, {@code false} otherwise
	 */
	public boolean isDouble(String text) {
		if (text == null || text.isEmpty()) {
			return false;
		}

		try {
			Double.parseDouble(text);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public void writeToPacket(OutgoingPacket packet) {
		getSet().values().forEach(val -> {
			final Class<?> type = val.getClass();
			write(type, val, packet);
		});
	}

	public void writeToPacket(StatsSet statsSet, OutgoingPacket packet) {
		statsSet.getSet().values().forEach(val -> {
			final Class<?> type = val.getClass();
			write(type, val, packet);
		});
	}

	@SuppressWarnings("unchecked")
	private void write(Class<?> type, Object value, OutgoingPacket packet) {
		if (type == Integer.class || type == int.class) {
			packet.writeD((Integer) value);
		} else if (type == Short.class || type == short.class
				|| type == Byte.class || type == byte.class) {
			packet.writeC((Integer) value);
		} else if (type == Float.class || type == float.class) {
			packet.writeE((Float) value);
		} else if (type == Double.class || type == double.class) {
			packet.writeF((Double) value);
		} else if (type == Long.class || type == long.class) {
			packet.writeQ((Long) value);
		} else if (type == Boolean.class || type == boolean.class) {
			packet.writeC(((Boolean) value) ? 0x01 : 0x00);
		} else if (type == String.class) {
			packet.writeS((String) value);
		} else if (type == Timestamp.class) {
			packet.writeQ(((Timestamp) value).getTime());
		} else if (type.isArray()) {
			Object[] array;
			if (type == long[].class
					|| type == int[].class
					|| type == float[].class
					|| type == boolean[].class) {
				array = IntStream.range(0, Array.getLength(value))
						.mapToObj(i -> Array.get(value, i))
						.toArray();
			} else {
				array = (Object[]) value;
			}

			final int size = array.length;
			packet.writeD(size);
			Arrays.stream(array, 0, size)
					.forEach(arrayValue -> {
						final Class<?> arrayType = arrayValue.getClass();
						write(arrayType, arrayValue, packet);
					});
		} else if (List.class.isAssignableFrom(type) || Set.class.isAssignableFrom(type)) {
			final Collection<Object> collection = (Collection<Object>) value;
			final int size = collection.size();
			packet.writeD(size);
			collection.forEach(colValue -> {
				final Class<?> colValueType = colValue.getClass();
				write(colValueType, colValue, packet);
			});
		} else if (Map.class.isAssignableFrom(type)) {
			final Map<?, ?> map = (Map<?, ?>) value;
			final int size = map.size();
			packet.writeD(size);
			map.forEach((key, mVal) -> {
				final Class<?> keyType = key.getClass();
				final Class<?> mValType = mVal.getClass();
				write(keyType, key, packet);
				write(mValType, mVal, packet);
			});
		} else if (type == StatsSet.class) {
			writeToPacket((StatsSet) value, packet);
		}
	}

	@Override
	public String toString() {
		return "StatsSet{" + "_set=" + objectMap + '}';
	}
}
