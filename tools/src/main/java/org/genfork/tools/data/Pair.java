package org.genfork.tools.data;

import lombok.Data;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Data
public class Pair<K, V> implements Serializable {
	private K key;
	private V value;

	private Pair(K key, V value) {
		this.key = key;
		this.value = value;
	}

	public static <K, V> Pair<K, V> of(K key, V value) {
		return new Pair<>(key, value);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Pair<?, ?> pair = (Pair<?, ?>) o;
		return Objects.equals(key, pair.key) &&
				Objects.equals(value, pair.value);
	}

	@Override
	public int hashCode() {
		return Objects.hash(key, value);
	}
}
