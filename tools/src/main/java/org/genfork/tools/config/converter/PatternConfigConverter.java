package org.genfork.tools.config.converter;

import lombok.Getter;

import java.lang.reflect.Field;
import java.util.regex.Pattern;

public class PatternConfigConverter implements IConfigConverter {
	@Getter(lazy = true)
	private static final PatternConfigConverter instance = new PatternConfigConverter();

	@Override
	public Object convertFromString(Field field, Class<?> type, String value) {
		return Pattern.compile(value);
	}

	@Override
	public String convertToString(Field field, Class<?> type, Object obj) {
		return ((Pattern) obj).pattern();
	}
}
