package org.genfork.tools.config;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.genfork.tools.config.parser.PropertiesParser;
import org.genfork.tools.path.BasePathProvider;
import org.genfork.tools.reflection.ClassPathUtil;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
public class ConfigManager {
	@Getter(lazy = true)
	private static final ConfigManager instance = new ConfigManager();

	private final AtomicBoolean reloading = new AtomicBoolean(false);
	private final Map<String, Map<Path, Set<String>>> propertiesRegistry = new TreeMap<>();
	private final Path overridePath;
	private PropertiesParser overridenProperties;

	protected ConfigManager() {
		final String profile = System.getProperty("active.profile");
		if (profile != null) {
			overridePath = BasePathProvider.resolvePath(Paths.get("config/" + profile, "override.properties"));
		} else {
			overridePath = null;
			log.error("Error loading configurations - use correctly property value of [active.profile]");
			System.exit(0);
			return;
		}

		if (Files.notExists(overridePath)) {
			try {
				Files.createDirectories(overridePath.getParent());
				Files.createFile(overridePath);
			} catch (final IOException e) {
				throw new Error("Failed to create override config and/or its directory!", e);
			}
		}

		overridenProperties = new PropertiesParser(overridePath);
	}

	/**
	 * Registers a configuration property into this manager.
	 */
	public void registerProperty(String packageName, Path configFile, String propertyKey) {
		if (!propertiesRegistry.containsKey(packageName)) {
			propertiesRegistry.put(packageName, new HashMap<>());
		}

		if (!propertiesRegistry.get(packageName).containsKey(configFile)) {
			propertiesRegistry.get(packageName).put(configFile, new TreeSet<>());
		}

		propertiesRegistry.get(packageName)
				.forEach((entryConfigFile, entryProperties) -> {
					if (entryProperties.contains(propertyKey)) {
						log.warn("Property key '{}' is already defined in config file '{}', so now '{}' overwrites that! Please fix this!", propertyKey, entryConfigFile, configFile);
					}
				});

		propertiesRegistry.get(packageName).get(configFile).add(propertyKey);
	}

	/**
	 * Loads all config classes from the specified package and overwrites their configs according to override config if necessary.
	 *
	 * @param packageName the name of the package which contains the config classes
	 */
	public void load(String packageName) {
		if (overridenProperties == null) {
			throw new NullPointerException("Override properties is missing!");
		}

		final AtomicInteger configCount = new AtomicInteger();
		try {
			ClassPathUtil.getAllClassesExtending(packageName, IConfigLoader.class)
					.forEach(clazz -> {
						try {
							final IConfigLoader configLoader = clazz.newInstance();
							configLoader.load(overridenProperties);
							configCount.incrementAndGet();
						} catch (InstantiationException | IllegalAccessException e) {
							log.warn("Failed to load config.", e);
						}
					});
		} catch (IOException e) {
			log.warn("Failed to load class path.", e);
		}

		log.info("Loaded {} config file(s).", configCount.get());
	}

	/**
	 * Reloads configurations by package name.
	 */
	public void reload(String packageName) {
		overridenProperties = new PropertiesParser(overridePath);

		if (propertiesRegistry.containsKey(packageName)) {
			propertiesRegistry.get(packageName).clear();
		}

		reloading.set(true);
		try {
			load(packageName);
		} finally {
			reloading.set(false);
		}
	}

	/**
	 * Checks whether reload is in progress or not.
	 *
	 * @return {@code true} if reload is in progress, otherwise {@code false}
	 */
	public boolean isReloading() {
		return reloading.get();
	}
}
