package org.genfork.tools.config.converter;

import lombok.Getter;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

public class ArrayConfigConverter implements IConfigConverter {
	@Getter(lazy = true)
	private static final ArrayConfigConverter instance = new ArrayConfigConverter();

	@Override
	public Object convertFromString(Field field, Class<?> type, String value) {
		final Class<?> componentType = type.getComponentType();

		if (value.isEmpty()) {
			return Array.newInstance(componentType, 0);
		}

		final String[] splitted = Pattern.compile(getElementDelimiter(), Pattern.LITERAL).split(value);
		final Object array = Array.newInstance(componentType, splitted.length);

		IntStream.range(0, splitted.length)
				.forEach(i ->
						Array.set(array, i, getElementConverter().convertFromString(field, componentType, splitted[i])));

		if (Comparable.class.isAssignableFrom(componentType)) {
			Arrays.sort((Comparable<?>[]) array);
		}

		return array;
	}

	@Override
	public String convertToString(Field field, Class<?> type, Object obj) {
		final Class<?> componentType = type.getComponentType();

		if (obj == null) {
			return "";
		}

		final int length = Array.getLength(obj);
		if (length < 1) {
			return "";
		}

		if (Comparable.class.isAssignableFrom(componentType)) {
			Arrays.sort((Comparable<?>[]) obj);
		}

		final StringBuilder sb = new StringBuilder();

		IntStream.range(0, length)
				.forEach(i -> {
					if (i > 0) {
						sb.append(getElementDelimiter());
					}
					sb.append(getElementConverter().convertToString(field, componentType, Array.get(obj, i)));
				});

		return sb.toString();
	}

	protected String getElementDelimiter() {
		return ",";
	}

	protected IConfigConverter getElementConverter() {
		return DefaultConfigConverter.getInstance();
	}
}
