package org.genfork.tools.config.converter;

import java.lang.reflect.Field;

public interface IConfigConverter {
	Object convertFromString(Field field, Class<?> type, String value);

	String convertToString(Field field, Class<?> type, Object obj);
}
