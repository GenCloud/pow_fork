package org.genfork.tools.config.annotation;

import org.genfork.tools.config.converter.DefaultConfigConverter;
import org.genfork.tools.config.converter.IConfigConverter;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ConfigField {
	String name();

	String value();

	String[] comment() default {};

	boolean required() default true;

	boolean onlyComment() default false;

	boolean reloadable() default true;

	Class<? extends IConfigConverter> converter() default DefaultConfigConverter.class;
}
