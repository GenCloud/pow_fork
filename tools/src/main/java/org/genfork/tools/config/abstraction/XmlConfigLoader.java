package org.genfork.tools.config.abstraction;

import lombok.extern.slf4j.Slf4j;
import org.genfork.tools.config.IConfigLoader;
import org.genfork.tools.config.annotation.ConfigClass;
import org.genfork.tools.config.parser.PropertiesParser;
import org.genfork.tools.path.BasePathProvider;
import org.genfork.tools.xml.XmlReader;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Slf4j
public abstract class XmlConfigLoader implements IConfigLoader, XmlReader {
	public XmlConfigLoader() {
		load(null); // Override cannot be used with XMLs.
	}

	@Override
	public void load(PropertiesParser override) {
		final Class<?> clazz = getClass();
		final ConfigClass configClass = clazz.getAnnotation(ConfigClass.class);

		if (configClass == null) {
			log.warn("Your class {} doesn't have @ConfigClass annotation!", clazz);
			return;
		}
		final Path configPath = BasePathProvider.resolvePath(Paths.get("", configClass.pathNames()).resolve(configClass.fileName() + ".xml"));

		if (Files.notExists(configPath)) {
			pathCannotBeFound(configPath);
			return;
		}

		parseFile(configPath, Paths.get("."));
		loadImpl();
	}

	protected void loadImpl() {
	}

	protected void pathCannotBeFound(Path path) {
		log.warn("Config File {} doesn't exist!", path);
	}
}
