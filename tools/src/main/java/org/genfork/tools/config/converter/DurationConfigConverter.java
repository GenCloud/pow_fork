package org.genfork.tools.config.converter;

import lombok.Getter;
import org.genfork.tools.time.TimeUtil;

import java.lang.reflect.Field;

public class DurationConfigConverter implements IConfigConverter {
	@Getter(lazy = true)
	private static final DurationConfigConverter instance = new DurationConfigConverter();

	@Override
	public Object convertFromString(Field field, Class<?> type, String value) {
		return TimeUtil.parseDuration(value);
	}

	@Override
	public String convertToString(Field field, Class<?> type, Object obj) {
		return obj.toString();
	}
}
