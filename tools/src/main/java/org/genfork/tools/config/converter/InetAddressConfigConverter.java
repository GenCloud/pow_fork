package org.genfork.tools.config.converter;

import java.net.InetAddress;
import java.net.UnknownHostException;

public final class InetAddressConfigConverter extends TypedConfigConverter<InetAddress> {
	@Override
	protected InetAddress convertFromString(String value) {
		try {
			return InetAddress.getByName(value);
		} catch (final UnknownHostException e) {
			throw new IllegalArgumentException(e);
		}
	}

	@Override
	protected String convertToString(InetAddress obj) {
		return obj.getHostAddress();
	}

	@Override
	protected Class<InetAddress> getRequiredType() {
		return InetAddress.class;
	}
}
