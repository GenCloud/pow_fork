package org.genfork.tools.config.converter;

import lombok.Getter;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class SetConfigConverter extends CollectionConfigConverter {
	@Getter(lazy = true)
	private static final SetConfigConverter instance = new SetConfigConverter();

	@Override
	protected Set<Object> createCollection(int size) {
		return new HashSet<>(size);
	}

	@Override
	protected <E> Set<E> emptyCollection() {
		return Collections.emptySet();
	}
}
