package org.genfork.tools.config.converter;

import lombok.Getter;
import org.genfork.tools.reflection.ClassUtil;

import java.io.File;
import java.lang.reflect.Field;
import java.nio.file.Path;
import java.time.Duration;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

public class DefaultConfigConverter implements IConfigConverter {
	@Getter(lazy = true)
	private static final DefaultConfigConverter instance = new DefaultConfigConverter();

	@Override
	public Object convertFromString(Field field, Class<?> type, String value) {
		if (type.isArray()) {
			return getArrayConverter().convertFromString(field, type, value);
		}

		if (type == List.class) {
			return getListConverter().convertFromString(field, type, value);
		}

		if (type == Set.class) {
			return getSetConverter().convertFromString(field, type, value);
		}

		if (type == Path.class) {
			return getPathConverter().convertFromString(field, type, value);
		}

		if (type == File.class) {
			return getFileConverter().convertFromString(field, type, value);
		}

		if (type == Pattern.class) {
			return getPatternConverter().convertFromString(field, type, value);
		}

		if (type == Duration.class) {
			return getDurationConverter().convertFromString(field, type, value);
		}

		return ClassUtil.get(type, value);
	}

	@Override
	public String convertToString(Field field, Class<?> type, Object obj) {
		if (type.isArray()) {
			return getArrayConverter().convertToString(field, type, obj);
		}

		if (type == List.class) {
			return getListConverter().convertToString(field, type, obj);
		}

		if (type == Set.class) {
			return getSetConverter().convertToString(field, type, obj);
		}

		if (type == Path.class) {
			return getPathConverter().convertToString(field, type, obj);
		}

		if (type == File.class) {
			return getFileConverter().convertToString(field, type, obj);
		}

		if (type == Pattern.class) {
			return getPatternConverter().convertToString(field, type, obj);
		}

		if (type == Duration.class) {
			return getDurationConverter().convertToString(field, type, obj);
		}

		if (obj == null) {
			return "";
		}

		return obj.toString();
	}

	protected IConfigConverter getArrayConverter() {
		return ArrayConfigConverter.getInstance();
	}

	protected IConfigConverter getListConverter() {
		return ListConfigConverter.getInstance();
	}

	protected IConfigConverter getSetConverter() {
		return SetConfigConverter.getInstance();
	}

	protected IConfigConverter getPathConverter() {
		return PathConfigConverter.getInstance();
	}

	protected IConfigConverter getFileConverter() {
		return FileConfigConverter.getInstance();
	}

	protected IConfigConverter getPatternConverter() {
		return PatternConfigConverter.getInstance();
	}

	protected IConfigConverter getDurationConverter() {
		return DurationConfigConverter.getInstance();
	}
}
