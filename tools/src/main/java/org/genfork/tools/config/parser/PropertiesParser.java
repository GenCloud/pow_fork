package org.genfork.tools.config.parser;

import lombok.extern.slf4j.Slf4j;
import org.genfork.tools.path.BasePathProvider;
import org.genfork.tools.time.TimeUtil;

import java.io.File;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Slf4j
public class PropertiesParser {
	private final Properties properties = new Properties();
	private final Path path;

	public PropertiesParser(String name) {
		this(BasePathProvider.resolvePath(name));
	}

	public PropertiesParser(Path path) {
		this.path = path;

		try (InputStream is = Files.newInputStream(path)) {
			properties.load(is);
		} catch (Exception e) {
			log.warn("[{}] There was an error loading config", path, e);
		}
	}

	public PropertiesParser(File file) {
		this(file.toPath());
	}

	public boolean containsKey(String key) {
		return properties.containsKey(key);
	}

	public Set<Entry<Object, Object>> entrySet() {
		return properties.entrySet();
	}

	public String getValue(String key) {
		final String value = properties.getProperty(key);
		return value != null ? value.trim() : null;
	}

	public boolean getBoolean(String key, boolean defaultValue) {
		final String value = getValue(key);
		if (value == null) {
			log.warn("[{}] missing property for key: {} using default value: {}", path, key, defaultValue);
			return defaultValue;
		}

		if (value.equalsIgnoreCase("true")) {
			return true;
		} else if (value.equalsIgnoreCase("false")) {
			return false;
		} else {
			log.warn("[{}] Invalid value specified for key: {} specified value: {} should be \"boolean\" using default value: {}", path, key, value, defaultValue);
			return defaultValue;
		}
	}

	public byte getByte(String key, byte defaultValue) {
		final String value = getValue(key);
		if (value == null) {
			log.warn("[{}] missing property for key: {} using default value: {}", path, key, defaultValue);
			return defaultValue;
		}

		try {
			return Byte.parseByte(value);
		} catch (NumberFormatException e) {
			log.warn("[{}] Invalid value specified for key: {} specified value: {} should be \"byte\" using default value: {}", path, key, value, defaultValue);
			return defaultValue;
		}
	}

	public short getShort(String key, short defaultValue) {
		final String value = getValue(key);
		if (value == null) {
			log.warn("[{}] missing property for key: {} using default value: {}", path, key, defaultValue);
			return defaultValue;
		}

		try {
			return Short.parseShort(value);
		} catch (NumberFormatException e) {
			log.warn("[{}] Invalid value specified for key: {} specified value: {} should be \"short\" using default value: {}", path, key, value, defaultValue);
			return defaultValue;
		}
	}

	public int getInt(String key, int defaultValue) {
		final String value = getValue(key);
		if (value == null) {
			log.warn("[{}] missing property for key: {} using default value: {}", path, key, defaultValue);
			return defaultValue;
		}

		try {
			return Integer.parseInt(value);
		} catch (NumberFormatException e) {
			log.warn("[{}] Invalid value specified for key: {} specified value: {} should be \"int\" using default value: {}", path, key, value, defaultValue);
			return defaultValue;
		}
	}

	public long getLong(String key, long defaultValue) {
		final String value = getValue(key);
		if (value == null) {
			log.warn("[{}] missing property for key: {} using default value: {}", path, key, defaultValue);
			return defaultValue;
		}

		try {
			return Long.parseLong(value);
		} catch (NumberFormatException e) {
			log.warn("[{}] Invalid value specified for key: {} specified value: {} should be \"long\" using default value: {}", path, key, value, defaultValue);
			return defaultValue;
		}
	}

	public float getFloat(String key, float defaultValue) {
		final String value = getValue(key);
		if (value == null) {
			log.warn("[{}] missing property for key: {} using default value: {}", path, key, defaultValue);
			return defaultValue;
		}

		try {
			return Float.parseFloat(value);
		} catch (NumberFormatException e) {
			log.warn("[{}] Invalid value specified for key: {} specified value: {} should be \"float\" using default value: {}", path, key, value, defaultValue);
			return defaultValue;
		}
	}

	public double getDouble(String key, double defaultValue) {
		final String value = getValue(key);
		if (value == null) {
			log.warn("[{}] missing property for key: {} using default value: {}", path, key, defaultValue);
			return defaultValue;
		}

		try {
			return Double.parseDouble(value);
		} catch (NumberFormatException e) {
			log.warn("[{}] Invalid value specified for key: {} specified value: {} should be \"double\" using default value: {}", path, key, value, defaultValue);
			return defaultValue;
		}
	}

	public String getString(String key, String defaultValue) {
		final String value = getValue(key);
		if (value == null) {
			log.warn("[{}] missing property for key: {} using default value: {}", path, key, defaultValue);
			return defaultValue;
		}
		return value;
	}

	public <T extends Enum<T>> T getEnum(String key, Class<T> clazz, T defaultValue) {
		final String value = getValue(key);
		if (value == null) {
			log.warn("[{}] missing property for key: {} using default value: {}", path, key, defaultValue);
			return defaultValue;
		}

		try {
			return Enum.valueOf(clazz, value);
		} catch (IllegalArgumentException e) {
			log.warn("[{}] Invalid value specified for key: {} specified value: {} should be enum value of \"{}\" using default value: {}", path, key, value, clazz.getSimpleName(), defaultValue);
			return defaultValue;
		}
	}

	/**
	 * @return {@link Duration} object by the durationPattern specified, {@code null} in case of malformed pattern.
	 */
	public Duration getDuration(String durationPattern, String defaultValue) {
		return getDuration(durationPattern, defaultValue, null);
	}

	/**
	 * @return {@link Duration} object by the durationPattern specified, the defaultDuration in case of malformed pattern.
	 */
	public Duration getDuration(String durationPattern, String defaultValue, Duration defaultDuration) {
		final String value = getString(durationPattern, defaultValue);
		try {
			return TimeUtil.parseDuration(value);
		} catch (IllegalStateException e) {
			log.warn("[{}] Invalid value specified for key: {} specified value: {}] should be time patttern using default value: {}", path, durationPattern, value, defaultValue);
		}
		return defaultDuration;
	}

	/**
	 * @return int array
	 */
	public int[] getIntArray(String key, String separator, int... defaultValues) {
		final String value = getValue(key);
		if (value == null) {
			log.warn("[{}] missing property for key: {} using default value: {}", path, key, defaultValues);
			return defaultValues;
		}

		try {
			final String[] data = value.trim().split(separator);
			return Arrays.stream(data).mapToInt(datum -> Integer.decode(datum.trim())).toArray();
		} catch (Exception e) {
			log.warn("[{}] Invalid value specified for key: {} specified value: {} should be array using default value: {}", path, key, value, defaultValues);
			return defaultValues;
		}
	}

	/**
	 * @return enum array
	 */
	@SafeVarargs
	public final <T extends Enum<T>> T[] getEnumArray(String key, String separator, Class<T> clazz, T... defaultValues) {
		final String value = getValue(key);
		if (value == null) {
			log.warn("[{}] missing property for key: {} using default value: {}", path, key, defaultValues);
			return defaultValues;
		}

		try {
			final String[] data = value.trim().split(separator);
			@SuppressWarnings("unchecked") final T[] result = (T[]) Array.newInstance(clazz, data.length);
			IntStream.range(0, data.length).forEach(i -> result[i] = Enum.valueOf(clazz, data[i]));
			return result;
		} catch (Exception e) {
			log.warn("[{}] Invalid value specified for key: {} specified value: {} should be array using default value: {}", path, key, value, defaultValues);
			return defaultValues;
		}
	}

	/**
	 * @return list
	 */
	@SafeVarargs
	public final <T extends Enum<T>> List<T> getEnumList(String key, String separator, Class<T> clazz, T... defaultValues) {
		final String value = getValue(key);
		if (value == null) {
			log.warn("[{}] missing property for key: {} using default value: {}", path, key, defaultValues);
			return Arrays.asList(defaultValues);
		}

		try {
			final String[] data = value.trim().split(separator);
			return Arrays
					.stream(data)
					.map(element -> Enum.valueOf(clazz, element))
					.collect(Collectors.toCollection(() -> new ArrayList<>(data.length)));
		} catch (Exception e) {
			log.warn("[{}] Invalid value specified for key: {} specified value: {} should be array using default value: {}", path, key, value, defaultValues);
			return Arrays.asList(defaultValues);
		}
	}
}
