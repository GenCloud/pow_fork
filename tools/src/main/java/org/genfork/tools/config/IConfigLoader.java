package org.genfork.tools.config;

import org.genfork.tools.config.parser.PropertiesParser;

public interface IConfigLoader {
	void load(PropertiesParser override);
}
