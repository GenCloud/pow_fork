package org.genfork.tools.config.converter;

import lombok.Getter;

import java.io.File;
import java.lang.reflect.Field;

public class FileConfigConverter implements IConfigConverter {
	@Getter(lazy = true)
	private static final FileConfigConverter instance = new FileConfigConverter();

	@Override
	public Object convertFromString(Field field, Class<?> type, String value) {
		return new File(value);
	}

	@Override
	public String convertToString(Field field, Class<?> type, Object obj) {
		return obj.toString();
	}
}
