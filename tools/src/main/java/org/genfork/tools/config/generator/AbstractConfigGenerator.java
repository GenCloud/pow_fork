package org.genfork.tools.config.generator;

import lombok.extern.slf4j.Slf4j;
import org.genfork.tools.config.IConfigLoader;
import org.genfork.tools.config.annotation.ConfigClass;
import org.genfork.tools.config.annotation.ConfigField;
import org.genfork.tools.config.annotation.ConfigGroupBeginning;
import org.genfork.tools.config.annotation.ConfigGroupEnding;
import org.genfork.tools.path.BasePathProvider;
import org.genfork.tools.reflection.ClassPathUtil;

import java.io.BufferedWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.stream.Collectors;

@Slf4j
public abstract class AbstractConfigGenerator {
	public AbstractConfigGenerator() {
		try {
			ClassPathUtil.getAllClassesExtending(getPackageName(), IConfigLoader.class)
					.forEach(clazz -> {
						try {
							printConfigClass(clazz);
						} catch (IOException e) {
							log.warn("Failed to generate config.", e);
						}
					});
		} catch (IOException e) {
			log.warn("Failed to scan for configs", e);
		}
	}

	public static void printConfigClass(final Class<?> clazz) throws IOException {
		final StringBuilder out = new StringBuilder();

		final ConfigClass configClass = clazz.getAnnotation(ConfigClass.class);
		if (configClass == null) {
			return;
		}

		final String settings = configClass.fileName().replace("_", " ") + " Settings";

		out.append("################################################################################\r\n");
		out.append("## Copyright (C) 2020 GenCloud - ").append(settings).append(System.lineSeparator());
		out.append("################################################################################\r\n");

		out.append(System.lineSeparator());

		if (configClass.comment().length > 0) {
			Arrays.stream(configClass.comment()).forEach(line -> out.append("# ").append(line).append(System.lineSeparator()));
			out.append(System.lineSeparator());
		}

		Arrays.stream(clazz.getFields()).forEach(field -> printConfigField(out, field));

		final String environment = System.getProperty("active.profile");
		final Path configPath = BasePathProvider.resolvePath("config/" + environment + "/" + Paths.get("", configClass.pathNames()).resolve(configClass.fileName() + configClass.fileExtension()));
		Files.createDirectories(configPath.getParent());

		try (BufferedWriter bw = Files.newBufferedWriter(configPath)) {
			bw.append(out.toString());
		}

		log.info("Generated: '{}'", configPath);
	}

	private static void printConfigField(final StringBuilder out, final Field field) {
		final ConfigField configField = field.getAnnotation(ConfigField.class);

		if (configField == null) {
			return;
		}

		final ConfigGroupBeginning beginningGroup = field.getAnnotation(ConfigGroupBeginning.class);
		if (beginningGroup != null) {
			out.append("########################################").append(System.lineSeparator());

			out.append(Arrays
					.stream(beginningGroup.comment())
					.map(line -> "# " + line + System.lineSeparator())
					.collect(Collectors.joining("", "## Section BEGIN: " + beginningGroup.name() + System.lineSeparator(), System.lineSeparator())));
		}

		out.append(Arrays
				.stream(configField.comment())
				.map(line -> "# " + line + System.lineSeparator())
				.collect(Collectors.joining()));

		if (!configField.onlyComment()) {
			out.append("# Default: ").append(configField.value()).append(System.lineSeparator());
			if (field.getType().isEnum()) {
				out.append("# Available: ").append(Arrays.stream(field.getType().getEnumConstants()).map(String::valueOf).collect(Collectors.joining("|"))).append(System.lineSeparator());
			} else if (field.getType().isArray()) {
				final Class<?> fieldComponentType = field.getType().getComponentType();
				if (fieldComponentType.isEnum()) {
					out.append("# Available: ").append(Arrays.stream(field.getType().getEnumConstants()).map(String::valueOf).collect(Collectors.joining(","))).append(System.lineSeparator());
				}
			}

			out.append(configField.name()).append(" = ").append(configField.value()).append(System.lineSeparator());
			out.append(System.lineSeparator());
		}

		final ConfigGroupEnding endingGroup = field.getAnnotation(ConfigGroupEnding.class);
		if (endingGroup != null) {
			out.append(Arrays
					.stream(endingGroup.comment())
					.map(line -> "# " + line + System.lineSeparator())
					.collect(Collectors.joining("", "", "## Section END: " + endingGroup.name() + System.lineSeparator())));

			out.append("########################################").append(System.lineSeparator());

			out.append(System.lineSeparator());
		}
	}

	protected abstract String getPackageName();
}
