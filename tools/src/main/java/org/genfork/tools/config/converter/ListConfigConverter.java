package org.genfork.tools.config.converter;

import lombok.Getter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ListConfigConverter extends CollectionConfigConverter {
	@Getter(lazy = true)
	private static final ListConfigConverter instance = new ListConfigConverter();

	@Override
	protected List<Object> createCollection(int size) {
		return new ArrayList<>(size);
	}

	@Override
	protected <E> List<E> emptyCollection() {
		return Collections.emptyList();
	}
}
