package org.genfork.tools.config.abstraction;

import lombok.extern.slf4j.Slf4j;
import org.genfork.tools.config.ConfigManager;
import org.genfork.tools.config.IConfigLoader;
import org.genfork.tools.config.annotation.ConfigClass;
import org.genfork.tools.config.annotation.ConfigField;
import org.genfork.tools.config.converter.IConfigConverter;
import org.genfork.tools.config.generator.AbstractConfigGenerator;
import org.genfork.tools.config.parser.PropertiesParser;
import org.genfork.tools.path.BasePathProvider;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Objects;
import java.util.Properties;
import java.util.stream.Stream;

@Slf4j
public abstract class ConfigPropertiesLoader implements IConfigLoader {
	/**
	 * Gets the result of two properties parser, where second is the override which overwrites the content of the first if necessary.
	 *
	 * @return properties of the two properties parser
	 */
	public static Properties propertiesOf(PropertiesParser properties, PropertiesParser override) {
		final Properties result = new Properties();
		Stream.concat(properties.entrySet().stream(), override.entrySet().stream())
				.forEach(e -> result.setProperty(e.getKey().toString(), e.getValue().toString()));
		return result;
	}

	/**
	 * Loads the config file and overrides its properties from override config if necessary.
	 *
	 * @param override the properties from override config
	 */
	@Override
	public final void load(PropertiesParser override) {
		final Class<?> clazz = getClass();
		final ConfigClass configClass = clazz.getAnnotation(ConfigClass.class);
		if (configClass == null) {
			log.warn("Class {} doesn't have @ConfigClass annotation!", clazz);
			return;
		}

		Path configDirectory;
		final String environment = System.getProperty("active.profile");
		if (environment != null) {
			configDirectory = Paths.get("config/" + environment, configClass.pathNames());
		} else {
			log.error("Error loading configurations - use correctly property value of [active.profile]");
			System.exit(0);
			return;
		}

		final Path configPath = BasePathProvider.resolvePath(configDirectory.resolve(configClass.fileName() + configClass.fileExtension()));
		if (Files.notExists(configPath)) {
			log.warn("Config File {} doesn't exist! Generating ...", configPath);

			try {
				AbstractConfigGenerator.printConfigClass(clazz);
			} catch (IOException e) {
				log.warn("Failed to generate config!", e);
			}
		}

		final PropertiesParser properties = new PropertiesParser(configPath);
		// Skip inappropriate fields.
		Arrays.stream(clazz.getFields())
				.filter(Objects::nonNull)
				.filter(field ->
						Modifier.isStatic(field.getModifiers()) && !Modifier.isFinal(field.getModifiers()))
				.forEach(field -> {
					final ConfigField configField = field.getAnnotation(ConfigField.class);
					if (configField != null) {
						// If field is just a comment holder, then do not try to load it.
						if (configField.onlyComment()) {
							return;
						}

						try {
							final String propertyKey = configField.name();
							final String propertyValue = configField.value();

							ConfigManager.getInstance().registerProperty(clazz.getPackage().getName(), configPath, propertyKey);

							if (!configField.reloadable() && ConfigManager.getInstance().isReloading()) {
								log.debug("Property '{}' retained with its previous value!", propertyKey);
								return;
							}

							final boolean required = configField.required();
							final String configProperty = getProperty(required, properties, override, propertyKey, propertyValue);
							if (!required && !StringUtils.hasText(configProperty)) {
								return;
							}

							final IConfigConverter converter = configField.converter().newInstance();
							field.set(null, converter.convertFromString(field, field.getType(), configProperty));
						} catch (Exception e) {
							log.warn("Failed to set field!", e);
						}
					}
				});

		loadImpl(properties, override);

		log.debug("Loaded '{}'", configPath);
	}

	/**
	 * Gets the right property. Its usage is mandatory for non-annotation based configs, without it the override function won't work.<br>
	 * For non-annotation based configs please use {@link #loadImpl(PropertiesParser, PropertiesParser)}.
	 *
	 * @param properties   the original properties
	 * @param override     the properties from override config
	 * @param name         the name of the property
	 * @param defaultValue a default value which will be used when the property is missing
	 * @return the property
	 */
	private String getProperty(boolean required, PropertiesParser properties, PropertiesParser override, String name, String defaultValue) {
		String property = override.getValue(name);
		if (property == null) {
			property = properties.getValue(name);
			if (!required && property == null) {
				return null;
			}

			if (property == null) {
				log.warn("Property key '{}' is missing, using default value!", name);
				return defaultValue;
			}
		}
		return property;
	}

	/**
	 * A properties loader method for alternative configs. Can be used for post hook events or for non-annotation based configs.
	 *
	 * @param properties the original properties
	 * @param override   the properties from override config
	 */
	protected void loadImpl(PropertiesParser properties, PropertiesParser override) {
	}
}
