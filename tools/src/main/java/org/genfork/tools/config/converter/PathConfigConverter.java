package org.genfork.tools.config.converter;

import lombok.Getter;

import java.lang.reflect.Field;
import java.nio.file.Paths;

public class PathConfigConverter implements IConfigConverter {
	@Getter(lazy = true)
	private static final PathConfigConverter instance = new PathConfigConverter();

	@Override
	public Object convertFromString(Field field, Class<?> type, String value) {
		return Paths.get(value);
	}

	@Override
	public String convertToString(Field field, Class<?> type, Object obj) {
		return obj.toString();
	}
}
