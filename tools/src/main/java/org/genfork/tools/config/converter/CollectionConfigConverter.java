package org.genfork.tools.config.converter;

import org.genfork.tools.reflection.ClassUtil;

import java.lang.reflect.Field;
import java.util.*;

@SuppressWarnings(
		{
				"unchecked",
				"rawtypes"
		})
public abstract class CollectionConfigConverter implements IConfigConverter {
	@Override
	public Object convertFromString(Field field, Class<?> type, String value) {
		if (value.isEmpty()) {
			return emptyCollection();
		}

		Collection<Object> result = null;
		final String[] splitted = value.split(",");

		final Class<?> elementType = ClassUtil.getFirstGenericTypeOfGenerizedField(field);
		if (type == Set.class) {
			// for enums, impose enum order
			if (elementType.isEnum()) {
				final Class<? extends Enum> c = elementType.asSubclass(Enum.class);
				if ("*".equals(value)) {
					return EnumSet.allOf(c);
				}

				result = (Collection<Object>) EnumSet.noneOf(c);
			}
			// otherwise, impose natural order (if applicable)
			else if (Comparable.class.isAssignableFrom(elementType)) {
				result = new TreeSet<>();
			}
		}

		if (result == null) {
			result = createCollection(splitted.length);
		}

		final Collection<Object> copy = result;
		Arrays.stream(splitted)
				.forEach(e ->
						copy.add(getElementConverter().convertFromString(null, elementType, e)));
		return result;
	}

	@Override
	public String convertToString(Field field, Class<?> type, Object obj) {
		if (obj == null) {
			return "";
		}

		final Collection<?> col = (Collection<?>) obj;
		if (col.isEmpty()) {
			return "";
		}

		final Class<?> elementType = ClassUtil.getFirstGenericTypeOfGenerizedField(field);
		if (elementType.isEnum() && type == Set.class) {
			final Class<? extends Enum> c = elementType.asSubclass(Enum.class);
			if (col.equals(EnumSet.allOf(c))) {
				return "*";
			}
		}

		final Iterator<?> it = col.iterator();
		final StringBuilder sb = new StringBuilder().append(getElementConverter().convertToString(null, elementType, it.next()));
		while (it.hasNext()) {
			sb.append(',').append(getElementConverter().convertToString(null, elementType, it.next()));
		}
		return sb.toString();
	}

	protected abstract Collection<Object> createCollection(int size);

	protected abstract <E> Collection<E> emptyCollection();

	protected IConfigConverter getElementConverter() {
		return DefaultConfigConverter.getInstance();
	}
}
