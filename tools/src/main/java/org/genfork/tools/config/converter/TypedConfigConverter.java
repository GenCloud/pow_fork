package org.genfork.tools.config.converter;

import java.lang.reflect.Field;

public abstract class TypedConfigConverter<T> implements IConfigConverter {
	@Override
	public final Object convertFromString(Field field, Class<?> type, String value) {
		if (getRequiredType() != type) {
			throw new ClassCastException(getRequiredType() + " type required, but found: " + type + "!");
		}

		value = (String) DefaultConfigConverter.getInstance().convertFromString(field, String.class, value);

		return convertFromString(value);
	}

	@Override
	public final String convertToString(Field field, Class<?> type, Object obj) {
		if (getRequiredType() != type) {
			throw new ClassCastException(getRequiredType() + " type required, but found: " + type + "!");
		}

		if (obj == null) {
			return "";
		}

		if (!getRequiredType().isInstance(obj)) {
			throw new ClassCastException(getRequiredType() + " value required, but found: '" + obj + "'!");
		}

		final String value = convertToString(getRequiredType().cast(obj));

		return DefaultConfigConverter.getInstance().convertToString(field, String.class, value);
	}

	protected abstract T convertFromString(String value);

	protected abstract String convertToString(T obj);

	protected abstract Class<T> getRequiredType();
}
