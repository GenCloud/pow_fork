package org.genfork.tools.versioning;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.jar.Attributes;
import java.util.jar.JarFile;
import java.util.stream.Collectors;

public class VersionInfo {
	/**
	 * A default string for the cases when version info cannot be retrieved. (IDE Mode)
	 */
	private static final String IDE_MODE = "Version Info - IDE Mode.";
	private final Map<String, String> manifestAttributes;
	private String filename = null;

	/**
	 * Gather version information from the class.
	 */
	public VersionInfo(Class<?> clazz) {
		manifestAttributes = new HashMap<>();

		final File file = Locator.getClassSource(clazz);
		if (!file.isFile()) {
			return;
		}

		final String filename = file.getName();
		this.filename = filename.substring(0, filename.lastIndexOf("."));

		try (JarFile jarFile = new JarFile(file);) {
			final Attributes attributes = jarFile.getManifest().getMainAttributes();
			attributes.forEach((key, value) -> manifestAttributes.put(String.valueOf(key), String.valueOf(value)));
		} catch (IOException e) {
			// ignore, IDE mode, etc...
		}
	}

	/**
	 * Gather version info of multiply classes. You can use it to gather information of Commons/Network/MMOCore/GeoDriver/ETC from LS/GS or any other application.
	 *
	 * @return string array of version info
	 */
	public static List<String> of(Class<?>... classes) {
		return Arrays.stream(classes).map(VersionInfo::new).map(VersionInfo::getFormattedClassInfo).collect(Collectors.toList());
	}

	/**
	 * Gets a manifest from the manifest attribute map, shows {@link #IDE_MODE} if null.
	 *
	 * @return manifest info
	 */
	public String getManifest(final String name) {
		return manifestAttributes.getOrDefault(name, IDE_MODE);
	}

	/**
	 * Gets a pretty formatted class info.
	 *
	 * @return formatted class info
	 */
	public String getFormattedClassInfo() {
		if (filename == null || filename.isEmpty()) {
			return IDE_MODE;
		}

		return filename + ": " +
				getManifest(VersionInfoManifest.GIT_HASH_SHORT) + ", " +
				getManifest(VersionInfoManifest.GIT_COMMIT_COUNT) + ", " +
				getManifest(VersionInfoManifest.IMPLEMENTATION_TIME) +
				" [ " + getManifest(VersionInfoManifest.GIT_BRANCH) + " ]";
	}

	public interface VersionInfoManifest {
		String CREATED_BY = "Created-By";
		String BUILT_BY = "Built-By";
		String IMPLEMENTATION_URL = "Implementation-URL";
		String IMPLEMENTATION_TIME = "Implementation-Time";
		String GIT_BRANCH = "Git-Branch";
		String GIT_HASH_FULL = "Git-Hash-Full";
		String GIT_HASH_SHORT = "Git-Hash-Short";
		String GIT_COMMIT_COUNT = "Git-Commit-Count";
	}
}
