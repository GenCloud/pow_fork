package org.genfork.tools.registry.annotation;

import org.genfork.tools.registry.config.IRegistryConfig;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface DiscoveryClient {
	Class<? extends IRegistryConfig> config();
}
