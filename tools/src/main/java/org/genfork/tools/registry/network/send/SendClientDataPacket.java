package org.genfork.tools.registry.network.send;

import io.netty.buffer.ByteBuf;
import org.genfork.tools.network.OutgoingPacket;

import static org.genfork.tools.network.opcodes.ClientsBranchOpcodes.DISCOVERY_RETRANSMIT_DATA;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class SendClientDataPacket extends OutgoingPacket {
	private final String toClient;
	private final String fromClient;

	private final boolean fromPlayer;

	private int authKeyFirst;
	private int authKeySecond;
	private int playKeyFirst;
	private int playKeySecond;

	private final ByteBuf byteBuf;

	public SendClientDataPacket(String toClient,
								String fromClient,
								int authKeyFirst,
								int authKeySecond,
								int playKeyFirst,
								int playKeySecond,
								ByteBuf byteBuf) {
		this.toClient = toClient;
		this.fromClient = fromClient;
		this.authKeyFirst = authKeyFirst;
		this.authKeySecond = authKeySecond;
		this.playKeyFirst = playKeyFirst;
		this.playKeySecond = playKeySecond;

		this.byteBuf = byteBuf;

		fromPlayer = true;
	}

	public SendClientDataPacket(String toClient, String fromClient, ByteBuf byteBuf) {
		this.toClient = toClient;
		this.fromClient = fromClient;
		this.byteBuf = byteBuf;

		fromPlayer = false;
	}

	@Override
	public boolean write() {
		DISCOVERY_RETRANSMIT_DATA.writeId(this);

		writeS(toClient);
		writeS(fromClient);

		writeC(fromPlayer ? 0x01 : 0x00);

		if (fromPlayer) {
			writeD(authKeyFirst);
			writeD(authKeySecond);
			writeD(playKeyFirst);
			writeD(playKeySecond);
		}

		getByteBuf().writeBytes(byteBuf);

		byteBuf.clear();
		return true;
	}
}
