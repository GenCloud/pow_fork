package org.genfork.tools.registry.network.recv;

import io.netty.buffer.ByteBuf;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.genfork.tools.network.ChannelInboundHandler;
import org.genfork.tools.network.IIncomingPackets;
import org.genfork.tools.network.IncomingPacket;
import org.genfork.tools.network.SessionInfo;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Slf4j
@RequiredArgsConstructor
public class ReceiveClientDataPacket extends IncomingPacket<ChannelInboundHandler<?>> {
	private final IIncomingPackets<? extends ChannelInboundHandler<?>>[] packets;

	@Override
	@SuppressWarnings("unchecked")
	public boolean read(ChannelInboundHandler<?> client) {
		final String toClient = readS();
		final String fromClient = readS();

		final boolean fromPlayer = readC() == 1;

		int authKeyFirst = -1, authKeySecond = -1, playKeyFirst = -1, playKeySecond = -1;
		if (fromPlayer) {
			authKeyFirst = readD();
			authKeySecond = readD();
			playKeyFirst = readD();
			playKeySecond = readD();
		}

		final ByteBuf byteBuf = getByteBuf();
		final short packetId = byteBuf.readUnsignedByte();
		if (packetId >= packets.length) {
			log.warn("Unknown packet: {}", Integer.toHexString(packetId));
			return false;
		}

		final IIncomingPackets<? extends ChannelInboundHandler<?>> incomingPacket = packets[packetId];
		if (incomingPacket == null) {
			log.warn("Unknown packet: {}", Integer.toHexString(packetId));
			return false;
		}

		final IncomingPacket<ChannelInboundHandler<?>> packet = (IncomingPacket<ChannelInboundHandler<?>>) incomingPacket.newIncomingPacket();

		if (packet != null) {
			packet.setByteBuf(getByteBuf());

			boolean read;
			if (fromPlayer) {
				final SessionInfo sessionInfo = SessionInfo.of(playKeyFirst, playKeySecond, authKeyFirst, authKeySecond);

				packet.setFromClientName(fromClient);
				packet.setToClientName(toClient);
				packet.setSessionInfo(sessionInfo);

				read = packet.read(client);
			} else {

				packet.setFromClientName(fromClient);
				packet.setToClientName(toClient);

				read = packet.read(client);
			}

			if (!read) {
				log.error("Error read data packet [{}]", packet);
			} else {
				try {
					packet.run(client);
				} catch (Exception e) {
					log.error("Error run data packet [{}]: {}", packet, e.getLocalizedMessage());
				}
			}
		}

		return true;
	}

	@Override
	public void run(ChannelInboundHandler<?> client) throws Exception {
		//unused
	}
}
