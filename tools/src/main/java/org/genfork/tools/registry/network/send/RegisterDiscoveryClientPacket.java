package org.genfork.tools.registry.network.send;

import lombok.RequiredArgsConstructor;
import org.genfork.tools.network.OutgoingPacket;

import static org.genfork.tools.network.opcodes.ClientsBranchOpcodes.DISCOVERY_CLIENT_PACKET;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@RequiredArgsConstructor
public class RegisterDiscoveryClientPacket extends OutgoingPacket {
	private final String clientName;
	private final String clientSecretKey;
	private final String clientIp;

	private final int clientPort;
	private final int rootClientPort;

	@Override
	public boolean write() {
		DISCOVERY_CLIENT_PACKET.writeId(this);

		writeS(clientName);
		writeS(clientSecretKey);
		writeS(clientIp);
		writeD(clientPort);
		writeD(rootClientPort);
		return true;
	}
}
