package org.genfork.tools.registry;

/**
 * @author: ngolubenko@context-it.ru
 * @created: 2020/04
 */
public class NetworkConstants {
	public static final String PACKET_PARTS_PARAM = "parts: ";
	public static final String PACKET_PARAMS_DELIMITER = ";";

	public static final String PACKET_PARTS_LENGTH_PARAM = "partsLength: ";
	public static final String PACKET_PARTS_DELIMITER = "-";
}
