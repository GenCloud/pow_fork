package org.genfork.tools.registry.config;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public interface IRegistryConfig {
	String getClientName();

	String getClientSecretKey();

	String getClientIp();

	int getClientPort();

	int getRootClientPort();
}
