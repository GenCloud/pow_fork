package org.genfork.tools.registry.network.send;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import lombok.RequiredArgsConstructor;
import org.genfork.tools.network.OutgoingPacket;

import java.util.Arrays;
import java.util.Collection;

/**
 * @author: ngolubenko@context-it.ru
 * @created: 2020/04
 */
@RequiredArgsConstructor
public class PartClientDataPacket extends OutgoingPacket {
	private final String partString;
	private final Collection<ByteBuf> byteBuffs;

	@Override
	public boolean write() {
		writeS(partString);

		final ByteBuf[] byteBufs = byteBuffs.toArray(new ByteBuf[0]);
		final ByteBuf copied = Unpooled.copiedBuffer(byteBufs);
		getByteBuf().writeBytes(copied);

		copied.clear();
		Arrays.stream(byteBufs).forEach(ByteBuf::clear);
		return false;
	}
}
