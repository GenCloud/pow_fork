package org.genfork.tools.registry.network.recv;

import lombok.RequiredArgsConstructor;
import org.genfork.tools.network.ChannelInboundHandler;
import org.genfork.tools.network.IncomingPacket;
import org.genfork.tools.registry.network.send.ClientSendHealthCheckPacket;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@RequiredArgsConstructor
public class RequestDiscoveryServerHealthCheckPacket<C extends ChannelInboundHandler<C>> extends IncomingPacket<C> {
	private final String clientName;

	@Override
	public boolean read(C client) {
		return true;
	}

	@Override
	public void run(C client) throws Exception {
		client.sendPacket(new ClientSendHealthCheckPacket(clientName));
	}
}
