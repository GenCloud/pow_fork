package org.genfork.tools.registry.network.send;

import lombok.RequiredArgsConstructor;
import org.genfork.tools.network.OutgoingPacket;

import static org.genfork.tools.network.opcodes.ClientsBranchOpcodes.CLIENT_SEND_HEALTH_CHECK;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@RequiredArgsConstructor
public class ClientSendHealthCheckPacket extends OutgoingPacket {
	private final String clientId;

	@Override
	public boolean write() {
		CLIENT_SEND_HEALTH_CHECK.writeId(this);

		writeS(clientId);
		return true;
	}
}
