package org.genfork.tools.registry.network;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import lombok.experimental.UtilityClass;
import org.genfork.tools.network.OutgoingPacket;
import org.genfork.tools.network.SessionInfo;
import org.genfork.tools.registry.network.send.SendClientDataPacket;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@UtilityClass
public class NetworkUtil {
	public SendClientDataPacket buildSendData(String clientName, String originClientName, OutgoingPacket packet) {
		ByteBuf byteBuf = packet.getByteBuf();
		if (byteBuf == null) {
			packet.setByteBuf(byteBuf = Unpooled.directBuffer());
		}

		packet.write();

		return new SendClientDataPacket(clientName, originClientName, byteBuf);
	}

	public SendClientDataPacket buildPlayerSendData(String clientName, String originClientName,
													SessionInfo sessionInfo,
													OutgoingPacket packet) {
		ByteBuf byteBuf = packet.getByteBuf();
		if (byteBuf == null) {
			packet.setByteBuf(byteBuf = Unpooled.directBuffer());
		}

		packet.write();

		return new SendClientDataPacket(clientName,
				originClientName,
				sessionInfo.getAuthKeyFirst(),
				sessionInfo.getAuthKeySecond(),
				sessionInfo.getPlayKeyFirst(),
				sessionInfo.getPlayKeySecond(),
				byteBuf);
	}
}
