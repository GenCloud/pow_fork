package org.genfork.tools.crypto;

import lombok.experimental.UtilityClass;
import org.genfork.tools.common.Rnd;

@UtilityClass
public class BlowFishKeygen {
	private static final int CRYPT_KEYS_SIZE = 20;
	private static final byte[][] CRYPT_KEYS = new byte[CRYPT_KEYS_SIZE][16];

	static {
		for (int i = 0; i < CRYPT_KEYS_SIZE; i++) {
			for (int j = 0; j < CRYPT_KEYS[i].length; j++) {
				CRYPT_KEYS[i][j] = (byte) Rnd.get(255);
			}

			CRYPT_KEYS[i][8] = (byte) 0xc8;
			CRYPT_KEYS[i][9] = (byte) 0x27;
			CRYPT_KEYS[i][10] = (byte) 0x93;
			CRYPT_KEYS[i][11] = (byte) 0x01;
			CRYPT_KEYS[i][12] = (byte) 0xa1;
			CRYPT_KEYS[i][13] = (byte) 0x6c;
			CRYPT_KEYS[i][14] = (byte) 0x31;
			CRYPT_KEYS[i][15] = (byte) 0x97;
		}
	}

	/**
	 * Returns a key from this keygen pool, the logical ownership is retained by this keygen.<BR>
	 * Thus when getting a key with interests other then read-only a copy must be performed.<BR>
	 *
	 * @return A key from this keygen pool.
	 */
	public byte[] getRandomKey() {
		return CRYPT_KEYS[Rnd.get(CRYPT_KEYS_SIZE)];
	}
}
