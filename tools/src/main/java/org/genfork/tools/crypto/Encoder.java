package org.genfork.tools.crypto;

import lombok.experimental.UtilityClass;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@UtilityClass
public class Encoder {
	private final PasswordEncoder passwordEncoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();

	public PasswordEncoder getPasswordEncoder() {
		return passwordEncoder;
	}
}
