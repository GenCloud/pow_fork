package org.genfork.tools.crypto;

import io.netty.buffer.ByteBuf;
import org.genfork.tools.network.ICrypt;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class ClientCrypt implements ICrypt {
	private final byte[] inKey = new byte[16];
	private final byte[] outKey = new byte[16];
	private boolean enabled;

	public void setKey(byte[] key) {
		System.arraycopy(key, 0, inKey, 0, 16);
		System.arraycopy(key, 0, outKey, 0, 16);
	}

	@Override
	public void encrypt(ByteBuf buf) {
		if (!enabled) {
			enabled = true;
			return;
		}

		int read = 0;
		while (buf.isReadable()) {
			final int sub = buf.readByte() & 0xFF;
			read = sub ^ outKey[(buf.readerIndex() - 1) & 15] ^ read;
			buf.setByte(buf.readerIndex() - 1, read);
		}

		shiftKey(outKey, buf.writerIndex());
	}

	@Override
	public void decrypt(ByteBuf buf) {
		if (!enabled) {
			return;
		}

		int read = 0;
		while (buf.isReadable()) {
			final int sub = buf.readByte() & 0xFF;
			buf.setByte(buf.readerIndex() - 1, sub ^ inKey[(buf.readerIndex() - 1) & 15] ^ read);
			read = sub;
		}

		shiftKey(inKey, buf.writerIndex());
	}

	private void shiftKey(byte[] key, int size) {
		int old = key[8] & 0xff;
		old |= (key[9] << 8) & 0xff00;
		old |= (key[10] << 0x10) & 0xff0000;
		old |= (key[11] << 0x18) & 0xff000000;

		old += size;

		key[8] = (byte) (old & 0xff);
		key[9] = (byte) ((old >> 0x08) & 0xff);
		key[10] = (byte) ((old >> 0x10) & 0xff);
		key[11] = (byte) ((old >> 0x18) & 0xff);
	}
}
