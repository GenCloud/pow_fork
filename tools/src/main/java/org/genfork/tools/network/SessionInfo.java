package org.genfork.tools.network;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@RequiredArgsConstructor
@Data
public class SessionInfo implements Serializable {
	private String accountName;

	private String enteredPlayerName;
	private int enteredPlayerId;

	private final int playKeyFirst;
	private final int playKeySecond;

	private final int authKeyFirst;
	private final int authKeySecond;

	public static SessionInfo of(int playKeyFirst, int playKeySecond, int authKeyFirst, int authKeySecond) {
		return new SessionInfo(playKeyFirst, playKeySecond, authKeyFirst, authKeySecond);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		SessionInfo that = (SessionInfo) o;
		return playKeyFirst == that.playKeyFirst &&
				playKeySecond == that.playKeySecond &&
				authKeyFirst == that.authKeyFirst &&
				authKeySecond == that.authKeySecond;
	}

	@Override
	public int hashCode() {
		return Objects.hash(playKeyFirst, playKeySecond, authKeyFirst, authKeySecond);
	}

	@Override
	public String toString() {
		return "SessionInfo{" +
				"playKeyFirst=" + playKeyFirst +
				", playKeySecond=" + playKeySecond +
				", authKeyFirst=" + authKeyFirst +
				", authKeySecond=" + authKeySecond +
				'}';
	}
}
