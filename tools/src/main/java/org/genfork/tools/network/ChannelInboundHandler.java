package org.genfork.tools.network;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import lombok.Getter;
import lombok.Setter;
import org.genfork.tools.registry.annotation.DiscoveryClient;
import org.genfork.tools.registry.config.IRegistryConfig;
import org.genfork.tools.registry.network.NetworkUtil;
import org.genfork.tools.registry.network.send.RegisterDiscoveryClientPacket;
import org.genfork.tools.registry.network.send.SendClientDataPacket;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.UUID;

public abstract class ChannelInboundHandler<T extends ChannelInboundHandler<?>> extends SimpleChannelInboundHandler<IncomingPacket<T>> implements Serializable {
	@Getter
	private final transient UUID uuid = UUID.randomUUID();

	@Getter
	protected transient Channel channel;
	@Getter
	@Setter
	protected transient ConnectionStats connectionStats = new ConnectionStats();
	@Getter
	@Setter
	private transient SessionInfo sessionInfo;

	@Override
	public void channelActive(ChannelHandlerContext ctx) {
		channel = ctx.channel();

		final DiscoveryClient discoveryClient = getClass().getDeclaredAnnotation(DiscoveryClient.class);
		if (discoveryClient != null) {
			final Class<? extends IRegistryConfig> type = discoveryClient.config();
			try {
				final IRegistryConfig config = (IRegistryConfig) type.getConstructors()[0].newInstance();

				final String clientName = config.getClientName();
				final String clientSecretKey = config.getClientSecretKey();
				final String clientIp = config.getClientIp();
				final int clientPort = config.getClientPort();
				final int rootClientPort = config.getRootClientPort();

				sendPacket(new RegisterDiscoveryClientPacket(clientName, clientSecretKey, clientIp, clientPort, rootClientPort));
			} catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
				e.printStackTrace();
			}
		}
	}

	public void closeNow() {
		if (channel != null) {
			channel.close();
		}
	}

	public void sendPacket(OutgoingPacket packet) {
		if (packet == null) {
			return;
		}

		channel.writeAndFlush(packet, channel.voidPromise());
	}

	public void sendPacket(IncomingPacket<?> incomingPacket, OutgoingPacket packet) {
		final SendClientDataPacket sendClientDataPacket = NetworkUtil.buildPlayerSendData(incomingPacket.getToClientName(),
				incomingPacket.getFromClientName(),
				incomingPacket.getSessionInfo(),
				packet);

		sendPacket(sendClientDataPacket);
	}

	public void sendPacket(String toClientName, String fromClientName, OutgoingPacket packet) {
		final SendClientDataPacket sendClientDataPacket = NetworkUtil.buildSendData(toClientName,
				fromClientName,
				packet);

		sendPacket(sendClientDataPacket);
	}

	public void sendPacket(String toClientName, String fromClientName, SessionInfo sessionInfo, OutgoingPacket packet) {
		final SendClientDataPacket sendClientDataPacket = NetworkUtil.buildPlayerSendData(toClientName,
				fromClientName,
				sessionInfo,
				packet);

		sendPacket(sendClientDataPacket);
	}

	public IConnectionState getConnectionState() {
		return channel != null ? channel.attr(IConnectionState.ATTRIBUTE_KEY).get() : null;
	}

	public void setConnectionState(IConnectionState connectionState) {
		channel.attr(IConnectionState.ATTRIBUTE_KEY).set(connectionState);
	}
}
