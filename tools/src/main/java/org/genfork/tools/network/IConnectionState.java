package org.genfork.tools.network;

import io.netty.util.AttributeKey;

public interface IConnectionState {
	AttributeKey<IConnectionState> ATTRIBUTE_KEY = AttributeKey.valueOf(IConnectionState.class, "");
}
