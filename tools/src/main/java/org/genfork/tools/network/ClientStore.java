package org.genfork.tools.network;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public abstract class ClientStore<C> implements Serializable {
	private final Map<String, C> clientData = new ConcurrentHashMap<>();

	public void addClient(UUID uuid, C client) {
		clientData.put(uuid.toString(), client);
	}

	public void removeClient(UUID uuid) {
		clientData.remove(uuid.toString());
	}

	public C getClient(UUID uuid) {
		return clientData.get(uuid.toString());
	}

	public Collection<C> getAllClients() {
		return Collections.unmodifiableCollection(clientData.values());
	}

	public C findClientBy(Predicate<C> p) {
		return getAllClients().stream().filter(p).findFirst().orElse(null);
	}
}
