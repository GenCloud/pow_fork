package org.genfork.tools.network;

import io.netty.buffer.ByteBuf;

public interface ICrypt {
	void encrypt(ByteBuf buf);

	void decrypt(ByteBuf buf);
}
