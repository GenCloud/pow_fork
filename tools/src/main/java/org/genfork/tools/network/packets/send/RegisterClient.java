package org.genfork.tools.network.packets.send;

import org.genfork.tools.network.OutgoingPacket;

import static org.genfork.tools.network.opcodes.ClientsBranchOpcodes.REGISTER_CLIENT_PACKET;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class RegisterClient extends OutgoingPacket {
	private final String secretKey;

	public RegisterClient(String secretKey) {
		this.secretKey = secretKey;
	}

	@Override
	public boolean write() {
		REGISTER_CLIENT_PACKET.writeId(this);
		writeString(secretKey);
		return true;
	}
}
