package org.genfork.tools.network;

import lombok.Getter;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author GenCloud
 * @date: 06/2018
 */
public class ConnectionStats {
	@Getter
	private final AtomicInteger connectionCount = new AtomicInteger(0);
	@Getter
	private final AtomicLong bytesSent = new AtomicLong(0L);
	@Getter
	private final AtomicLong bytesReceived = new AtomicLong(0L);

	public ConnectionStats() {
	}

	public ConnectionStats(long bytesSent, long bytesReceived) {
		this.bytesSent.set(bytesSent);
		this.bytesReceived.set(bytesReceived);
	}

	public int increaseConnectionCount() {
		return connectionCount.incrementAndGet();
	}

	public int decreaseConnectionCount() {
		return connectionCount.decrementAndGet();
	}

	public long appendBytesSent(long delta) {
		return bytesSent.getAndAdd(delta);
	}

	public long appendBytesReceived(long delta) {
		return bytesReceived.getAndAdd(delta);
	}
}


