package org.genfork.tools.network.opcodes;

import org.genfork.tools.network.OutgoingPacket;

import java.util.Arrays;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public enum AuthPlayerBranchOpcodes {
	INIT_PACKET(0x00),
	AUTH_FAIL_PACKET(0x01),
	BLOCKED_ACCOUNT_PACKET(0x02),
	AUTH_SUCCESS_PACKET(0x03),
	GAME_SERVER_LIST_PACKET(0x04),
	GAME_SERVER_PLAYER_FAIL_PACKET(0x06),
	GAME_SERVER_PLAYER_SUCCESS_PACKET(0x07),
	AUTH_GAME_GUARD_PACKET(0x0B);

	private final int id1;
	private final int id2;

	AuthPlayerBranchOpcodes(int id1) {
		this(id1, -1);
	}

	AuthPlayerBranchOpcodes(int id1, int id2) {
		this.id1 = id1;
		this.id2 = id2;
	}

	public static AuthPlayerBranchOpcodes getPacket(int id1, int id2) {
		return Arrays.stream(values()).filter(packet -> packet.getId1() == id1 && packet.getId2() == id2).findFirst().orElse(null);
	}

	public int getId1() {
		return id1;
	}

	public int getId2() {
		return id2;
	}

	public void writeId(OutgoingPacket packet) {
		packet.writeC(id1);
		if (id2 > 0) {
			packet.writeH(id2);
		}
	}
}
