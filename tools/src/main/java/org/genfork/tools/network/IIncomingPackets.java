package org.genfork.tools.network;

import java.util.Set;

public interface IIncomingPackets<T> extends IConnectionState {
	int getPacketId();

	IncomingPacket<T> newIncomingPacket();

	Set<IConnectionState> getConnectionStates();

	IIncomingPackets<? extends ChannelInboundHandler<?>>[] getArray();
}
