package org.genfork.tools.network;

import io.netty.buffer.ByteBuf;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

public abstract class OutgoingPacket implements Serializable {
	@Getter
	@Setter
	private ByteBuf byteBuf;

	public abstract boolean write();

	/**
	 * Gets the writable bytes.
	 *
	 * @return the writable bytes
	 */
	public int getWritableBytes() {
		return byteBuf.writableBytes();
	}

	/**
	 * Writes a byte.
	 *
	 * @param value the byte (The 24 high-order bits are ignored)
	 */
	public void writeC(int value) {
		byteBuf.writeByte(value);
	}

	/**
	 * Writes a short.
	 *
	 * @param value the short (The 16 high-order bits are ignored)
	 */
	public void writeH(int value) {
		byteBuf.writeShortLE(value);
	}

	/**
	 * Writes an integer.
	 *
	 * @param value the integer
	 */
	public void writeD(int value) {
		byteBuf.writeIntLE(value);
	}

	/**
	 * Writes a long.
	 *
	 * @param value the long
	 */
	public void writeQ(long value) {
		byteBuf.writeLongLE(value);
	}

	/**
	 * Writes a float.
	 *
	 * @param value the float
	 */
	public void writeE(float value) {
		byteBuf.writeIntLE(Float.floatToIntBits(value));
	}

	/**
	 * Writes a double.
	 *
	 * @param value the double
	 */
	public void writeF(double value) {
		byteBuf.writeLongLE(Double.doubleToLongBits(value));
	}

	/**
	 * Writes a string.
	 *
	 * @param value the string
	 */
	public void writeS(String value) {
		if (value != null) {
			for (int i = 0; i < value.length(); i++) {
				byteBuf.writeChar(Character.reverseBytes(value.charAt(i)));
			}
		}

		byteBuf.writeChar(0);
	}

	/**
	 * Writes a string with fixed length specified as [short length, char[length] data].
	 *
	 * @param value the string
	 */
	public void writeString(String value) {
		if (value != null) {
			byteBuf.writeShortLE(value.length());
			for (int i = 0; i < value.length(); i++) {
				byteBuf.writeChar(Character.reverseBytes(value.charAt(i)));
			}
		} else {
			byteBuf.writeShort(0);
		}
	}

	/**
	 * Writes a byte array.
	 *
	 * @param bytes the byte array
	 */
	public void writeB(byte[] bytes) {
		byteBuf.writeBytes(bytes);
	}
}
