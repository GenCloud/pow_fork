package org.genfork.tools.network.packets.recv;

import lombok.extern.slf4j.Slf4j;
import org.genfork.tools.network.ChannelInboundHandler;
import org.genfork.tools.network.IConnectionState;
import org.genfork.tools.network.IncomingPacket;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Slf4j
public abstract class RequestRegisterClient<C extends ChannelInboundHandler<C>> extends IncomingPacket<C> {
	private String secretKey;

	@Override
	public boolean read(C client) {
		secretKey = readString();
		return true;
	}

	@Override
	public void run(C client) throws Exception {
		if (secretKey.equalsIgnoreCase(getServerSecretKey())) {
			client.setConnectionState(getConnectionState());
		} else {
			client.closeNow();

			log.warn("Client not registered - invalid key");
		}
	}

	public abstract IConnectionState getConnectionState();

	public abstract String getServerSecretKey();
}
