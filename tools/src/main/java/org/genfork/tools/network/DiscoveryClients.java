package org.genfork.tools.network;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public class DiscoveryClients {
	public static final String DATA_STORE_NODE = "data-client";
	public static final String AUTH_NODE = "auth-client";
	public static final String GAME_MASTER_NODE = "game-master";
	public static final String GAME_SLAVE_NODE = "game-slave";
}
