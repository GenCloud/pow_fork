package org.genfork.tools.network;

import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.*;
import io.netty.channel.socket.nio.NioSocketChannel;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.Timer;
import java.util.TimerTask;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Slf4j
public class ClientNetworkController<C> extends ClientStore<C> {
	public static final int BASIC_NETWORK_POOL = 100;
	private final String host;
	private final int port;

	private final Timer timer = new Timer();
	@Getter
	private final Bootstrap bootstrap;

	@Getter
	private ChannelFuture channelFuture;

	public ClientNetworkController(EventLoopGroup workerGroup, ChannelInitializer<NioSocketChannel> clientInitializer, String host, int port) {
		bootstrap = new Bootstrap()
				.group(workerGroup)
				.channel(NioSocketChannel.class)
				.remoteAddress(host, port)
				.handler(clientInitializer)
				.option(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT);

		this.host = host;
		this.port = port;
	}

	public void start(boolean enableReconnection) throws InterruptedException {
		if (channelFuture != null && !channelFuture.isDone()) {
			return;
		}

		try {
			channelFuture = bootstrap.connect().sync();
			if (enableReconnection) {
				channelFuture.addListener(new ChannelFutureListener() {
					@Override
					public void operationComplete(ChannelFuture future) {
						try {
							if (!future.isSuccess()) {
								future.channel().close();
								bootstrap.connect().addListener(this);
							} else {
								addCloseDetectListener(future.channel());
							}
						} catch (Exception e) {
							addCloseDetectListener(future.channel());
						}
					}

					private void addCloseDetectListener(Channel channel) {
						channel.closeFuture().addListener((ChannelFutureListener) future -> scheduleConnect(true));
					}
				});
			}

			log.info("Connect to {}:{}", host, port);
		} catch (Exception e) {
			scheduleConnect(enableReconnection);
		}
	}

	private void scheduleConnect(boolean enableReconnection) {
		log.warn("Connection established. Try reconnect...");
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				try {
					start(enableReconnection);
				} catch (InterruptedException ignored) {
				}
			}
		}, 3000);
	}

	public void stop() throws InterruptedException {
		channelFuture.channel().close().sync();
	}
}
