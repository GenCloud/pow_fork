package org.genfork.tools.network.codecs;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import lombok.extern.slf4j.Slf4j;
import org.genfork.tools.network.ChannelInboundHandler;
import org.genfork.tools.network.OutgoingPacket;

@Sharable
@Slf4j
public class PacketEncoder extends MessageToByteEncoder<OutgoingPacket> {
	private final int maxPacketSize;

	private ChannelInboundHandler<?> handler;

	public PacketEncoder(ChannelInboundHandler<?> handler, int maxPacketSize) {
		super();
		this.handler = handler;
		this.maxPacketSize = maxPacketSize;
	}

	public PacketEncoder(int maxPacketSize) {
		super();
		this.maxPacketSize = maxPacketSize;
	}

	@Override
	protected void encode(ChannelHandlerContext ctx, OutgoingPacket packet, ByteBuf byteBuf) {
		try {
			if (packet.getByteBuf() == null) {
				packet.setByteBuf(byteBuf);
			}

			if (packet.write()) {

				if (handler != null) {
					handler.getConnectionStats().appendBytesSent(packet.getByteBuf().capacity());
				}

				if (byteBuf.writerIndex() > maxPacketSize) {
					throw new IllegalStateException("Packet (" + packet + ") size (" + byteBuf.writerIndex() + ") is bigger than the limit (" + maxPacketSize + ")");
				}
			} else {
				byteBuf.clear();
			}
		} catch (Throwable e) {
			log.warn("Failed sending Packet({})", packet, e);
			byteBuf.clear();
		}
	}
}