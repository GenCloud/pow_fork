package org.genfork.tools.network;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.PooledByteBufAllocator;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

@Slf4j
public class ServerNetworkController<C> extends ClientStore<C> {
	private final ServerBootstrap serverBootstrap;
	private final String host;
	private final int port;

	@Getter
	private ChannelFuture channelFuture;

	public ServerNetworkController(EventLoopGroup bossGroup, EventLoopGroup workerGroup, ChannelInitializer<SocketChannel> clientInitializer, String host, int port) {
		serverBootstrap = new ServerBootstrap()
				.group(bossGroup, workerGroup)
				.channel(NioServerSocketChannel.class)
				.childHandler(clientInitializer)
				.childOption(ChannelOption.ALLOCATOR, PooledByteBufAllocator.DEFAULT);

		Map<ChannelOption<?>, Object> options = serverBootstrap.config().options();

		this.host = host;
		this.port = port;
	}

	public void start() throws InterruptedException {
		if (channelFuture != null && !channelFuture.isDone()) {
			return;
		}

		channelFuture = serverBootstrap.bind(host, port).sync();
		log.info("Listening on {}:{}", host, port);
	}

	public void stop() throws InterruptedException {
		channelFuture.channel().close().sync();
	}
}
