package org.genfork.tools.network.codecs;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;
import lombok.extern.slf4j.Slf4j;
import org.genfork.tools.network.IConnectionState;
import org.genfork.tools.network.IIncomingPackets;
import org.genfork.tools.network.IncomingPacket;

import java.util.List;

@Slf4j
public class PacketDecoder<T> extends ByteToMessageDecoder {
	private final IIncomingPackets<T>[] incomingPackets;
	private final T client;

	public PacketDecoder(IIncomingPackets<T>[] incomingPackets, T client) {
		this.incomingPackets = incomingPackets;
		this.client = client;
	}

	@Override
	protected void decode(ChannelHandlerContext ctx, ByteBuf byteBuf, List<Object> out) {
		if (byteBuf == null || !byteBuf.isReadable()) {
			return;
		}

		try {
			final short packetId = byteBuf.readUnsignedByte();
			if (packetId >= incomingPackets.length) {
				log.warn("Unknown packet: {}", Integer.toHexString(packetId));
				return;
			}

			final IIncomingPackets<T> incomingPacket = incomingPackets[packetId];
			if (incomingPacket == null) {
				log.warn("Unknown packet: {}", Integer.toHexString(packetId));
				return;
			}

			final IConnectionState connectionState = ctx.channel().attr(IConnectionState.ATTRIBUTE_KEY).get();
			if (connectionState == null || !incomingPacket.getConnectionStates().contains(connectionState)) {
				log.warn("{}: Connection at invalid state: {} Required States: {}", incomingPacket, connectionState, incomingPacket.getConnectionStates());
				return;
			}

			final IncomingPacket<T> packet = incomingPacket.newIncomingPacket();
			if (packet != null) {
				packet.setByteBuf(byteBuf);
				packet.setFirstOpcode(packetId);
				packet.setClient(client);

				if (packet.read(client)) {
					out.add(packet);
				}
			}
		} finally {
			byteBuf.readerIndex(byteBuf.writerIndex());
		}
	}
}
