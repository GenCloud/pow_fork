package org.genfork.tools.network;

import io.netty.buffer.ByteBuf;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public abstract class IncomingPacket<T> implements Serializable {
	@Getter
	@Setter
	private ByteBuf byteBuf;

	@Getter
	@Setter
	private String fromClientName;

	@Getter
	@Setter
	private String toClientName;

	@Getter
	@Setter
	private SessionInfo sessionInfo;

	@Getter
	@Setter
	private short firstOpcode;

	@Getter
	@Setter
	private short secondOpcode;

	@Getter
	@Setter
	private T client;

	/**
	 * Reads a packet.
	 *
	 * @param client the client
	 * @return {@code true} if packet was read successfully, {@code false} otherwise.
	 */
	public abstract boolean read(T client);

	public abstract void run(T client) throws Exception;

	/**
	 * Gets the readable bytes.
	 *
	 * @return the readable bytes
	 */
	public int getReadableBytes() {
		return byteBuf.readableBytes();
	}

	/**
	 * Reads an unsigned byte.
	 *
	 * @return the unsigned byte
	 * @throws IndexOutOfBoundsException if {@code readableBytes} is less than {@code 1}
	 */
	public short readC() {
		return byteBuf.readUnsignedByte();
	}

	/**
	 * Reads an unsigned short.
	 *
	 * @return the unsigned short
	 * @throws IndexOutOfBoundsException if {@code readableBytes} is less than {@code 2}
	 */
	public int readH() {
		return byteBuf.readUnsignedShortLE();
	}

	/**
	 * Reads an integer.
	 *
	 * @return the integer
	 * @throws IndexOutOfBoundsException if {@code readableBytes} is less than {@code 4}
	 */
	public int readD() {
		return byteBuf.readIntLE();
	}

	/**
	 * Reads a long.
	 *
	 * @return the long
	 * @throws IndexOutOfBoundsException if {@code readableBytes} is less than {@code 8}
	 */
	public long readQ() {
		return byteBuf.readLongLE();
	}

	/**
	 * Reads a float.
	 *
	 * @return the float
	 * @throws IndexOutOfBoundsException if {@code readableBytes} is less than {@code 4}
	 */
	public float readE() {
		return Float.intBitsToFloat(byteBuf.readIntLE());
	}

	/**
	 * Reads a double.
	 *
	 * @return the double
	 * @throws IndexOutOfBoundsException if {@code readableBytes} is less than {@code 8}
	 */
	public double readF() {
		return Double.longBitsToDouble(byteBuf.readLongLE());
	}

	/**
	 * Reads a string.
	 *
	 * @return the string
	 * @throws IndexOutOfBoundsException if string {@code null} terminator is not found within {@code readableBytes}
	 */
	public String readS() {
		final StringBuilder sb = new StringBuilder();
		char chr;
		while ((chr = Character.reverseBytes(byteBuf.readChar())) != 0) {
			sb.append(chr);
		}
		return sb.toString();
	}

	/**
	 * Reads a string.
	 *
	 * @return the string
	 * @throws IndexOutOfBoundsException if string {@code null} terminator is not found within {@code readableBytes}
	 */
	public String readS(ByteBuf byteBuf) {
		final StringBuilder sb = new StringBuilder();
		char chr;
		while ((chr = Character.reverseBytes(byteBuf.readChar())) != 0) {
			sb.append(chr);
		}
		return sb.toString();
	}

	/**
	 * Reads a fixed length string.
	 *
	 * @return the string
	 * @throws IndexOutOfBoundsException if {@code readableBytes} is less than {@code 2 + String.length * 2}
	 */
	public String readString() {
		return getStringFromByteBuf(byteBuf);
	}

	/**
	 * Reads a fixed length string.
	 *
	 * @return the string
	 * @throws IndexOutOfBoundsException if {@code readableBytes} is less than {@code 2 + String.length * 2}
	 */
	public String readString(ByteBuf byteBuf) {
		return getStringFromByteBuf(byteBuf);
	}

	private String getStringFromByteBuf(ByteBuf byteBuf) {
		final String sb;
		final int stringLength = byteBuf.readShortLE();
		if (stringLength * 2 > getReadableBytes()) {
			throw new IndexOutOfBoundsException("readerIndex(" + byteBuf.readerIndex() + ") + length(" + (stringLength * 2) + ") exceeds writerIndex(" + byteBuf.writerIndex() + "): " + byteBuf);
		}

		sb = IntStream.range(0, stringLength)
				.mapToObj(i -> String.valueOf(Character.reverseBytes(byteBuf.readChar())))
				.collect(Collectors.joining());
		return sb;
	}

	/**
	 * Reads a byte array.
	 *
	 * @param length the length
	 * @return the byte array
	 * @throws IndexOutOfBoundsException if {@code readableBytes} is less than {@code length}
	 */
	public byte[] readB(int length) {
		final byte[] result = new byte[length];
		byteBuf.readBytes(result);
		return result;
	}

	/**
	 * Reads a byte array.
	 *
	 * @param dst      the destination
	 * @param dstIndex the destination index to start writing the bytes from
	 * @param length   the length
	 * @throws IndexOutOfBoundsException if {@code readableBytes} is less than {@code length}, if the specified dstIndex is less than 0 or if {@code dstIndex + length} is greater than {@code dst.length}
	 */
	public void readB(byte[] dst, int dstIndex, int length) {
		byteBuf.readBytes(dst, dstIndex, length);
	}

	public ByteBuf readB() {
		return byteBuf.readBytes(getReadableBytes());
//		final ByteBuf buf = byteBuf.readSlice(getReadableBytes());
//		buf.retain();
//		return buf;
	}
}
