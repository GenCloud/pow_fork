package org.genfork.tools.network.opcodes;

import org.genfork.tools.network.OutgoingPacket;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
public enum ClientsBranchOpcodes {
	// ALL CLIENTS PACKET
	REGISTER_CLIENT_PACKET(0x00),

	// SERVICE DISCOVERY - ALL GROUPS
	DISCOVERY_CLIENT_PACKET(0x0A),
	DISCOVERY_CLIENT_HEALTH_CHECK(0x0B),
	CLIENT_SEND_HEALTH_CHECK(0x0C),
	DISCOVERY_RETRANSMIT_DATA(0x0D),

	// AUTH CLIENT <-> DATA SERVER
	ACCOUNT_AUTH_PACKET(0x10),
	ACCOUNT_UPDATE_INFO_PACKET(0x11),
	ACCOUNT_SELECT_RESULT_PACKET(0x12),

	// GAME CLIENT <-> AUTH SERVER
	REGISTER_GAME_SERVER_PACKET(0x13),
	PLAYER_TRY_PLAYER_GAME_PACKET(0x0E, 0x13),

	REPLY_PLAY_GAME_PACKET(0x0E, 0x14),
	TRY_AUTH_PACKET(0x0E, 0x14),

	// GAME CLIENT <-> DATA SERVER
	ACCOUNT_PLAYERS_INFO_PACKET(0x15),
	RECEIVE_ACCOUNT_PLAYERS_RESULT_PACKET(0x0E, 0x15),

	PLAYER_CREATION_PACKET(0x16),
	PLAYER_CREATION_RESULT_PACKET(0x0E, 0x16),

	PLAYER_CHECK_NAME_PACKET(0x17),
	PLAYER_CHECK_NAME_RESULT_PACKET(0x0E, 0x17),

	PLAYER_GET_INVENTORY_DATA_PACKET(0x18),
	PLAYER_RECEIVE_INVENTORY_DATA_PACKET(0x0E, 0x18),

	PLAYER_TRY_ENTER_WORLD_PACKET(0x19),
	PLAYER_TRY_ENTER_WORLD_RESULT_PACKET(0x0E, 0x19),

	PLAYER_TRY_RECEIVE_INVENTORY_DATA_PACKET(0x1A),
	PLAYER_TRY_RECEIVE_INVENTORY_DATA_RESULT_PACKET(0x0E, 0x1A);

	private final int id1;
	private final int id2;
	private final int id3;

	ClientsBranchOpcodes(int id1) {
		this(id1, -1, -1);
	}

	ClientsBranchOpcodes(int id1, int id2) {
		this.id1 = id1;
		this.id2 = id2;
		this.id3 = -1;
	}

	ClientsBranchOpcodes(int id1, int id2, int id3) {
		this.id1 = id1;
		this.id2 = id2;
		this.id3 = id3;
	}

	public int getId1() {
		return id1;
	}

	public int getId2() {
		return id2;
	}

	public int getId3() {
		return id2;
	}

	public void writeId(OutgoingPacket packet) {
		packet.writeC(id1);
		if (id2 > 0) {
			packet.writeH(id2);
		}

		if (id3 > 0) {
			packet.writeH(id3);
		}
	}
}
