package org.genfork.tools.network;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import static java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;
import static java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Slf4j
public class NetworkConnectionPool {
	@Getter(lazy = true)
	private static final NetworkConnectionPool instance = new NetworkConnectionPool();

	private final Map<String, List<ChannelInboundHandler<?>>> channels = new HashMap<>();

	private final Map<String, AtomicInteger> counters = new HashMap<>();

	private final Timer timer = new Timer();

	private final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
	private final ReadLock readLock = lock.readLock();
	private final WriteLock writeLock = lock.writeLock();

	public void initialize(String clientName, Bootstrap bootstrap) {
		final Bootstrap clone = bootstrap.clone();
		try {
			final ChannelFuture sync = clone.connect().sync();
			sync.addListener(new ChannelFutureListener() {
				@Override
				public void operationComplete(ChannelFuture future) {
					if (!future.isSuccess()) {
						future.channel().close();
						bootstrap.connect().addListener(this);
					} else {
						addCloseDetectListener(future.channel());
					}
				}

				private void addCloseDetectListener(Channel channel) {
					channel.closeFuture().addListener((ChannelFutureListener) future -> scheduleConnect(clientName, bootstrap));
				}
			});
		} catch (Exception ex) {
			scheduleConnect(clientName, bootstrap);
		}
	}

	public void addChannel(String clientName, ChannelInboundHandler<?> channelInboundHandler) {
		writeLock.lock();
		try {
			if (channels.containsKey(clientName)) {
				channels.get(clientName).add(channelInboundHandler);
			} else {
				channels.put(clientName, new ArrayList<>(Collections.singletonList(channelInboundHandler)));
			}

			if (!counters.containsKey(clientName)) {
				counters.put(clientName, new AtomicInteger(0));
			}
		} finally {
			writeLock.unlock();
		}
	}

	private void scheduleConnect(String clientName, Bootstrap bootstrap) {
		log.warn("Connection established. Try reconnect...");
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				initialize(clientName, bootstrap);
			}
		}, 3000);
	}

	public ChannelInboundHandler<?> getNextChannel(String clientName) {
		ChannelInboundHandler<?> channelFuture = null;

		readLock.lock();
		try {
			final AtomicInteger value = counters.get(clientName);
			final List<ChannelInboundHandler<?>> channelFutures = channels.get(clientName);
			if (value != null && channelFutures != null) {
				final int counter = value.incrementAndGet();

				if (!channelFutures.isEmpty()) {
					if (counter >= channelFutures.size()) {
						value.set(0);
						channelFuture = channelFutures.get(0);
					} else {
						channelFuture = channelFutures.get(counter);
					}
				}
			}
		} finally {
			readLock.unlock();
		}

		return channelFuture;
	}

	public void closeAndClearAllChannels() {
		writeLock.lock();
		try {
			channels.forEach((clientName, futures) -> futures.forEach(ChannelInboundHandler::closeNow));
			channels.clear();
		} finally {
			writeLock.unlock();
		}
	}
}
