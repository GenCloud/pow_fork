package org.genfork.tools.network.codecs;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageCodec;
import org.genfork.tools.network.ICrypt;

import java.util.List;

public class CryptCodec extends ByteToMessageCodec<ByteBuf> {
	private final ICrypt crypt;

	public CryptCodec(ICrypt crypt) {
		super();
		this.crypt = crypt;
	}

	@Override
	protected void encode(ChannelHandlerContext ctx, ByteBuf msg, ByteBuf out) {
		if (!msg.isReadable()) {
			return;
		}

		msg.resetReaderIndex();
		crypt.encrypt(msg);
		msg.resetReaderIndex();
		out.writeBytes(msg);
	}

	@Override
	protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) {
		in.resetReaderIndex();
		crypt.decrypt(in);
		in.readerIndex(in.writerIndex());
		out.add(in.copy(0, in.writerIndex()));
	}
}
