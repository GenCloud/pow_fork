package org.genfork.tools.network.codecs;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageEncoder;

import java.util.List;

@Sharable
public class LengthFieldBasedFrameEncoder extends MessageToMessageEncoder<ByteBuf> {
	@Override
	protected void encode(ChannelHandlerContext ctx, ByteBuf msg, List<Object> out) {
		final ByteBuf buf = ctx.alloc().buffer(4);
		final int length = msg.readableBytes() + 4;
		buf.writeIntLE(length);
		out.add(buf);
		out.add(msg.retain());
	}
}
