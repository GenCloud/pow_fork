package org.genfork.tools.network;

import io.netty.channel.Channel;
import lombok.Data;

import java.io.Serializable;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Data
public class NetworkClient implements Serializable {
	protected Channel channel;

	private SessionInfo sessionInfo;

	public void closeNow() {
		if (channel != null) {
			channel.close();
		}
	}

	public void sendPacket(OutgoingPacket packet) {
		if (packet == null) {
			return;
		}

		channel.writeAndFlush(packet, channel.voidPromise());
	}
}
