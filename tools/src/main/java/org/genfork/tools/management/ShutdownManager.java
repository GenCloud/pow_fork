package org.genfork.tools.management;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.genfork.tools.async.ThreadPool;
import org.genfork.tools.console.ConsoleUtil;

import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

@Slf4j
@UtilityClass
public class ShutdownManager {
	protected final Set<ShutdownCounterListener> SHUTDOWN_COUNTER_LISTENERS = ConcurrentHashMap.newKeySet();
	// indicates shutdown in progress (hooks being run)
	private final AtomicBoolean IN_PROGRESS = new AtomicBoolean(false);
	private final Set<ShutdownAbortListener> SHUTDOWN_ABORT_LISTENERS = ConcurrentHashMap.newKeySet();
	private final PriorityQueue<ShutdownHook> SHUTDOWN_HOOKS = new PriorityQueue<>(), SPECIAL_HOOKS = new PriorityQueue<>();
	protected TerminationStatus MODE = TerminationStatus.INVALID;
	// indicates that there is a shutdown scheduled in the future
	protected ShutdownCounter SHUTDOWN_COUNTER;

	/**
	 * Add listener to shutdown counting.
	 *
	 * @param listener listening to actions during shutdown
	 */
	public void addShutdownCounterListener(ShutdownCounterListener listener) {
		SHUTDOWN_COUNTER_LISTENERS.add(listener);
	}

	/**
	 * Add listener to shutdown aborting.
	 *
	 * @param listener listening to actions when shutdown is aborted
	 */
	public void addShutdownAbortListener(ShutdownAbortListener listener) {
		SHUTDOWN_ABORT_LISTENERS.add(listener);
	}

	/**
	 * Returns to the shutdown counter.
	 *
	 * @return the shutdown counter
	 */
	public ShutdownCounter getCounter() {
		return SHUTDOWN_COUNTER;
	}

	/**
	 * Returns whether this application has been requested to end execution.
	 *
	 * @return whether a shutdown was ordered
	 */
	public boolean isInProgress() {
		return SHUTDOWN_COUNTER != null;
	}

	/**
	 * Requests this application to end execution after the specified number of seconds, on behalf of <TT>initiator</TT>. <BR>
	 * <BR>
	 * If there is a pending request, it will be overridden.
	 *
	 * @param mode      request type
	 * @param duration  shutdown delay
	 * @param interval  shutdown listener interval
	 * @param initiator requestor (may be <TT>null</TT>)
	 */
	public synchronized void start(TerminationStatus mode, int duration, int interval, String initiator) {
		if (isInProgress()) {
			abort(initiator);
		}

		if (initiator != null) {
			log.info("{} issued a halt command: {}!", initiator, mode.getDescription());
		} else {
			log.info("A halt command was issued: {} !", mode.getDescription());
		}

		MODE = mode;

		SHUTDOWN_COUNTER = new ShutdownCounter(duration, interval, MODE);
		SHUTDOWN_COUNTER.start();
	}

	/**
	 * Cancels a requested shutdown on behalf of <TT>initiator</TT>.
	 *
	 * @param initiator canceler (may be <TT>null</TT>)
	 */
	public synchronized void abort(String initiator) {
		if (!isInProgress()) {
			return;
		}

		if (initiator != null) {
			log.info("{} issued a halt command: {}!", initiator, MODE.getDescription());
		} else {
			log.info("A halt command was issued: {} !", MODE.getDescription());
		}

		SHUTDOWN_ABORT_LISTENERS.forEach(listener -> listener.onShutdownAbort(MODE, initiator));

		MODE = TerminationStatus.INVALID;

		SHUTDOWN_COUNTER = null;
	}

	/**
	 * Issues an orderly shutdown.
	 *
	 * @param initiator initiator's ID/name
	 */
	public void shutdown(String initiator) {
		exit(TerminationStatus.MANUAL_SHUTDOWN, initiator);
	}

	/**
	 * Issues an orderly shutdown and requests a restart.
	 *
	 * @param initiator initiator's ID/name
	 */
	public void restart(String initiator) {
		exit(TerminationStatus.MANUAL_RESTART, initiator);
	}

	/**
	 * Exits the server.
	 *
	 * @param mode indicates the cause and/or effect of the action
	 */
	public void exit(TerminationStatus mode) {
		exit(mode, null);
	}

	/**
	 * Exits the server.
	 *
	 * @param mode      indicates the cause and/or effect of the action
	 * @param initiator initiator's ID/name
	 */
	public void exit(TerminationStatus mode, String initiator) {
		if (IN_PROGRESS.getAndSet(true)) {
			return;
		}

		try {
			MODE = mode;

			if (initiator != null) {
				log.info("{} issued a halt command: {}!", initiator, mode.getDescription());
			} else {
				log.info("A halt command was issued: {} !", mode.getDescription());
			}
		} finally {
			runSpecialHooks();
			Runtime.getRuntime().exit(mode.getStatusCode());
		}
	}

	/**
	 * Issues a forced shutdown and requests a restart.
	 *
	 * @param initiator initiator's ID/name
	 */
	public void halt(String initiator) {
		halt(TerminationStatus.MANUAL_RESTART, initiator);
	}

	/**
	 * Halts the server.
	 *
	 * @param mode indicates the cause and/or effect of the action
	 */
	public void halt(TerminationStatus mode) {
		halt(mode, null);
	}

	/**
	 * Halts the server.
	 *
	 * @param mode      indicates the cause and/or effect of the action
	 * @param initiator initiator's ID/name
	 */
	public void halt(TerminationStatus mode, String initiator) {
		try {
			MODE = mode;

			if (initiator != null) {
				log.info("{} issued a halt command: {}!", initiator, mode.getDescription());
			} else {
				log.info("A halt command was issued: {} !", mode.getDescription());
			}
		} finally {
			Runtime.getRuntime().halt(mode.getStatusCode());
		}
	}

	/**
	 * Adds a global shutdown hook to run all registered shutdown hooks sequentially in order they were registered.
	 */
	public void initShutdownHook() {
		log.info("Initialized.");
		Runtime.getRuntime().addShutdownHook(new Thread(() ->
		{
			ConsoleUtil.printSection("Shutdown Hook(s)");
			try {
				runShutdownHooks();
			} finally {
				final int exitCode = MODE.getStatusCode();
				if (System.getProperty("org.genfork/write_exit_code") != null) {
					try (Writer w = Files.newBufferedWriter(Paths.get("exit_code"))) {
						w.write(String.valueOf(exitCode));
					} catch (IOException ignored) {
					}
				}
			}
		}, "CumulativeShutdownHook"));
	}

	/**
	 * Adds a managed shutdown hook to be run before the application terminates unless an uncaught termination signal is used.
	 *
	 * @param hook shutdown hook
	 */
	public synchronized void addShutdownHook(Runnable hook) {
		addShutdownHook(0, hook);
	}

	public synchronized void addShutdownHook(int priority, Runnable hook) {
		SHUTDOWN_HOOKS.add(new ShutdownHook(priority, hook));
	}

	public synchronized void addSpecialHook(int priority, Runnable hook) {
		SPECIAL_HOOKS.add(new ShutdownHook(priority, hook));
	}

	public boolean isRunningHooks() {
		return IN_PROGRESS.get();
	}

	protected synchronized void runShutdownHooks() {
		while (!SHUTDOWN_HOOKS.isEmpty()) {
			try {
				SHUTDOWN_HOOKS.remove().run();
			} catch (final Throwable t) {
				log.warn("", t);
			}
		}

		ThreadPool.shutdown();
	}

	protected synchronized void runSpecialHooks() {
		while (!SPECIAL_HOOKS.isEmpty()) {
			try {
				SPECIAL_HOOKS.remove().run();
			} catch (final Throwable t) {
				log.warn("", t);
			}
		}
	}

	/**
	 * A simple interface to extend shutdown counting with new actions.
	 *
	 * @author lord_rex
	 */
	public interface ShutdownCounterListener {
		void onShutdownCount(ShutdownCounter shutdownCounter, int i);
	}

	/**
	 * A simple interface to extend shutdown aborting with new actions.
	 *
	 * @author lord_rex
	 */
	public interface ShutdownAbortListener {
		void onShutdownAbort(TerminationStatus mode, String initiator);
	}

	public final class ShutdownCounter extends Thread {
		private final int duration;
		private final int interval;
		private final TerminationStatus mode;
		private boolean fastMode;

		protected ShutdownCounter(int duration, int interval, TerminationStatus mode) {
			this.duration = duration;
			this.interval = interval;
			this.mode = mode;
		}

		@Override
		public void run() {
			for (int i = duration; i >= interval; i -= interval) {
				// shutdown aborted
				if (this != SHUTDOWN_COUNTER) {
					return;
				}

				try {
					log.info("Server is {} ing in {} seconds(s).", mode.getShortDescription(), i);

					for (ShutdownCounterListener listener : ShutdownManager.SHUTDOWN_COUNTER_LISTENERS) {
						listener.onShutdownCount(this, i);
					}

					if (fastMode) {
						log.info("Counting was skipped, using fast mode.");
						break; // fast mode.
					}

					if (i > interval) {
						Thread.sleep(interval * 1000);
					} else {
						Thread.sleep(i * 1000);
					}
				} catch (InterruptedException e) {
					return;
				}
			}

			// shutdown aborted
			if (this != SHUTDOWN_COUNTER) {
				return;
			}

			// last point where logging is operational :(
			log.info("Shutdown countdown is over: {} NOW!", mode.getDescription());

			Runtime.getRuntime().exit(mode.getStatusCode());
		}

		public int getDuration() {
			return duration;
		}

		public int getInterval() {
			return interval;
		}

		public TerminationStatus getMode() {
			return mode;
		}

		public boolean isFastMode() {
			return fastMode;
		}

		public void setFastMode(boolean fastMode) {
			this.fastMode = fastMode;
		}
	}

	private final class ShutdownHook implements Comparable<ShutdownHook>, Runnable {
		private final int priority;
		private final Runnable hook;

		protected ShutdownHook(int priority, Runnable hook) {
			this.priority = priority;
			this.hook = hook;
		}

		@Override
		public void run() {
			hook.run();
		}

		@Override
		public int compareTo(ShutdownHook o) {
			return priority - o.priority;
		}

		@Override
		public String toString() {
			return "[" + priority + "] " + hook;
		}
	}
}
