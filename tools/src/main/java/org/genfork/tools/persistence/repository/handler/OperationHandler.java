package org.genfork.tools.persistence.repository.handler;

import java.io.Serializable;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@FunctionalInterface
public interface OperationHandler<O extends Serializable> {
	O invoke(O value);
}
