package org.genfork.tools.persistence.manager;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import net.sf.cglib.proxy.Enhancer;
import org.genfork.tools.data.Pair;
import org.genfork.tools.persistence.annotations.DataMethod;
import org.genfork.tools.persistence.annotations.DataMethod.Type;
import org.genfork.tools.persistence.annotations.DataRepository;
import org.genfork.tools.persistence.annotations.ExpParam;
import org.genfork.tools.persistence.repository.CrudDataRepository;
import org.genfork.tools.persistence.repository.RepositoryInfo;
import org.genfork.tools.persistence.repository.RepositoryMethod;
import org.genfork.tools.persistence.repository.interceptor.RepositoryMethodInterceptor;
import org.redisson.api.RFuture;
import org.redisson.api.RLock;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import org.reflections.Reflections;
import org.springframework.expression.Expression;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import static org.genfork.tools.common.ExpressionUtil.parser;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Data
@Slf4j
public abstract class PersistenceManager {
	private static final String CONTROLLABLE_PACKAGE = "org.genfork";
	protected RedissonClient client;
	private Map<String, ? extends CrudDataRepository> repos = new ConcurrentHashMap<>();

	public PersistenceManager() {
		final Reflections reflections = new Reflections(CONTROLLABLE_PACKAGE);

		final Set<Class<? extends CrudDataRepository>> repositories = reflections.getSubTypesOf(CrudDataRepository.class);
		if (!repositories.isEmpty()) {
			repos = repositories.stream().map(this::createProxy)
					.collect(Collectors.toMap(k -> k.getClass().getSimpleName().split("\\$\\$")[0], v -> v));

			log.info("Initialized data control repositories {} count.", repos.size());
		}
	}

	public CrudDataRepository createProxy(Class<? extends CrudDataRepository> type) {
		if (type.isAnnotationPresent(DataRepository.class)) {
			final DataRepository dataRepository = type.getAnnotation(DataRepository.class);
			final Method[] declaredMethods = type.getDeclaredMethods();
			final RepositoryInfo info = new RepositoryInfo(dataRepository.bucketName(), new ArrayList<>());

			for (Method method : declaredMethods) {
				if (method.isAnnotationPresent(DataMethod.class)) {
					final DataMethod dataMethod = method.getDeclaredAnnotation(DataMethod.class);
					final boolean enableLock = dataMethod.enableLock();
					final String keyExpression = dataMethod.keyExpression();
					final Type opType = dataMethod.type();

					Expression expression = null;
					try {
						if (!keyExpression.isEmpty()) {
							expression = parser.parseExpression(keyExpression);
						}
					} catch (Exception e) {
						log.error("Can't parse expression variable [{}] in type {}. Reason: {}", keyExpression, type.getSimpleName(), e.getLocalizedMessage());
						return null;
					}

					final List<Pair<String, ? extends Class<?>>> collect = Arrays
							.stream(method.getParameters())
							.filter(t ->
									t.isAnnotationPresent(ExpParam.class))
							.map(t -> Pair.of(t.getAnnotation(ExpParam.class).value(), t.getType()))
							.collect(Collectors.toList());

					final RepositoryMethod repositoryMethod = new RepositoryMethod(method.getName(), opType, enableLock, collect);
					repositoryMethod.setExpression(expression);
					info.getMethods().add(repositoryMethod);
				}
			}

			final Enhancer enhancer = new Enhancer();
			enhancer.setInterfaces(new Class[]{type});
			enhancer.setCallback(new RepositoryMethodInterceptor(this, info));
			enhancer.setCallbackFilter(m -> m.isBridge() ? 1 : 0);
			return (CrudDataRepository) enhancer.create();
		}

		return null;
	}

	@SuppressWarnings("unchecked")
	public <R extends CrudDataRepository> R getRepository(Class<? extends CrudDataRepository> type) {
		return (R) repos.get(type.getSimpleName());
	}

	public <O extends Serializable> RMap<String, O> getMap(String bucketName) {
		return client.getMap(bucketName);
	}

	public Set<String> getKeys(boolean enableLock, RMap<String, Serializable> initMap, String bucketName) {
		if (client != null) {
			final RMap<String, Serializable> map = initMap != null ? initMap : client.getMap(bucketName);
			Set<String> result;

			if (enableLock) {
				final RLock lock = map.getFairLock(bucketName);
				lock.lockAsync();
				try {
					final RFuture<Set<String>> future = map.readAllKeySetAsync();
					result = tryAllReceiveKeys(bucketName, future);
				} finally {
					lock.unlockAsync();
				}
			} else {
				final RFuture<Set<String>> future = map.readAllKeySetAsync();
				result = tryAllReceiveKeys(bucketName, future);
			}

			return result;
		}

		return Collections.emptySet();
	}

	public <O extends Serializable> void addData(boolean enableLock, RMap<String, O> initMap, String bucketName, String key, O object) {
		if (client != null) {
			final RMap<String, O> map = initMap != null ? initMap : client.getMap(bucketName);

			if (enableLock) {
				final RLock lock = map.getFairLock(bucketName);
				lock.lockAsync();
				try {
					final RFuture<Boolean> future = map.fastPutAsync(key, object);
					sendDataAndCheck(bucketName, key, future);
				} finally {
					lock.unlockAsync();
				}
			} else {
				final RFuture<Boolean> future = map.fastPutAsync(key, object);
				sendDataAndCheck(bucketName, key, future);
			}
		}
	}

	public <O extends Serializable> Map<String, O> getAllData(boolean enableLock, RMap<String, O> initMap, String bucketName) {
		if (client != null) {
			final RMap<String, O> map = initMap != null ? initMap : client.getMap(bucketName);
			Map<String, O> result;
			if (enableLock) {
				final RLock lock = map.getFairLock(bucketName);
				lock.lockAsync();
				try {
					final Set<String> keys = map.readAllKeySet();
					final RFuture<Map<String, O>> hgetall = map.getAllAsync(keys);
					result = tryAllReceiveData(bucketName, hgetall);
				} finally {
					lock.unlockAsync();
				}
			} else {
				final Set<String> keys = map.readAllKeySet();
				final RFuture<Map<String, O>> hgetall = map.getAllAsync(keys);
				result = tryAllReceiveData(bucketName, hgetall);
			}

			return result;
		}

		return Collections.emptyMap();
	}

	public <O extends Serializable> O getData(boolean enableLock, RMap<String, O> initMap, String bucketName, String key) {
		if (client != null) {
			final RMap<String, O> map = initMap != null ? initMap : client.getMap(bucketName);
			O result;
			if (enableLock) {
				final RLock lock = map.getFairLock(key);
				lock.lockAsync();
				try {
					final RFuture<O> async = map.getAsync(key);
					result = tryReceiveData(bucketName, key, async);
				} finally {
					lock.unlockAsync();
				}
			} else {
				final RFuture<O> async = map.getAsync(key);
				result = tryReceiveData(bucketName, key, async);
			}

			return result;
		}

		return null;
	}

	public void clearAllData(boolean enableLock, RMap<String, Serializable> initMap, String bucketName) {
		if (client != null) {
			final RMap<String, Serializable> map = initMap != null ? initMap : client.getMap(bucketName);

			if (enableLock) {
				final RLock lock = map.getFairLock(bucketName);
				lock.lockAsync();
				try {
					map.clear();
				} finally {
					lock.unlockAsync();
				}
			} else {
				map.clear();
			}
		}
	}

	public void clearData(boolean enableLock, RMap<String, Serializable> initMap, String bucketName, String key) {
		if (client != null) {
			final RMap<String, Serializable> map = initMap != null ? initMap : client.getMap(bucketName);
			if (enableLock) {
				final RLock lock = map.getFairLock(bucketName);
				lock.lockAsync();
				try {
					map.fastRemoveAsync(key);
				} finally {
					lock.unlockAsync();
				}
			} else {
				map.fastRemoveAsync(key);
			}
		}
	}

	private <O extends Serializable> void sendDataAndCheck(String bucketName, String key, RFuture<O> future) {
		try {
			final O result = future.get();
			if (log.isDebugEnabled()) {
				log.debug("Data {} - bucket : {}, key : {}, object : {}", result != null ? "sent" : "not sent", bucketName, key, result);
			}
		} catch (InterruptedException | ExecutionException e) {
			log.error("Cant write data to redis node, client throws exception: {}", e.getLocalizedMessage());
			e.printStackTrace();
		}
	}

	private Set<String> tryAllReceiveKeys(String bucketName, RFuture<Set<String>> hget) {
		try {
			final Set<String> result = hget.get();
			if (log.isDebugEnabled()) {
				log.debug("Received All Keys - bucket : {}, object : {}", bucketName, result);
			}

			return result;
		} catch (InterruptedException | ExecutionException e) {
			log.error("Cant receiving data from redis node, client throws exception: {}", e.getLocalizedMessage());
			e.printStackTrace();
		}

		return null;
	}

	private <O extends Serializable> Map<String, O> tryAllReceiveData(String bucketName, RFuture<Map<String, O>> hget) {
		try {
			final Map<String, O> result = hget.get();
			if (log.isDebugEnabled()) {
				log.debug("Received All Data - bucket : {}, object : {}", bucketName, result);
			}

			return result;
		} catch (InterruptedException | ExecutionException e) {
			log.error("Cant receiving data from redis node, client throws exception: {}", e.getLocalizedMessage());
			e.printStackTrace();
		}

		return null;
	}

	private <O extends Serializable> O tryReceiveData(String bucketName, String key, RFuture<O> hget) {
		try {
			final O result = hget.get();
			if (log.isDebugEnabled()) {
				log.debug("Received Data - bucket : {}, key : {}, object : {}", bucketName, key, result);
			}

			return result;
		} catch (InterruptedException | ExecutionException e) {
			log.error("Cant receiving data from redis node, client throws exception: {}", e.getLocalizedMessage());
			e.printStackTrace();
		}

		return null;
	}
}
