package org.genfork.tools.persistence.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface DataMethod {
	/**
	 * Active lock record by specific key in redis bucket
	 *
	 * @return enable lock
	 */
	boolean enableLock() default true;

	/**
	 * Redis data bucket key object
	 *
	 * @return key of object
	 */
	String keyExpression() default "";

	Type type();

	enum Type {
		GET_KEYS,
		READ,
		READ_ALL,
		WRITE,
		REMOVE,
		REMOVE_ALL
	}
}
