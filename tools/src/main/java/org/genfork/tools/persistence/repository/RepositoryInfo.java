package org.genfork.tools.persistence.repository;

import lombok.Data;

import java.util.List;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Data
public class RepositoryInfo {
	private final String bucketName;
	private final List<RepositoryMethod> methods;
}
