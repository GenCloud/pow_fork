package org.genfork.tools.persistence.repository;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.genfork.tools.data.Pair;
import org.genfork.tools.persistence.annotations.DataMethod.Type;
import org.springframework.expression.Expression;

import java.util.List;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@Data
@RequiredArgsConstructor
public class RepositoryMethod {
	private final String methodName;
	private final Type operationType;
	private final boolean enableLock;
	private final List<Pair<String, ? extends Class<?>>> annotatedVars;

	private Expression expression;
}
