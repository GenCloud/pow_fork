package org.genfork.tools.persistence.repository.interceptor;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;
import org.genfork.tools.data.Pair;
import org.genfork.tools.jmm.NotThreadSafe;
import org.genfork.tools.persistence.annotations.DataMethod.Type;
import org.genfork.tools.persistence.annotations.DataRepository;
import org.genfork.tools.persistence.annotations.DataValue;
import org.genfork.tools.persistence.manager.PersistenceManager;
import org.genfork.tools.persistence.repository.RepositoryInfo;
import org.genfork.tools.persistence.repository.RepositoryMethod;
import org.genfork.tools.persistence.repository.handler.OperationHandler;
import org.redisson.api.RLock;
import org.redisson.api.RMap;
import org.springframework.expression.Expression;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.lang.Nullable;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.genfork.tools.persistence.annotations.DataMethod.Type.*;

/**
 * @author: ngolubenko@context-it.ru
 * @date: 02/2020
 */
@RequiredArgsConstructor
@Slf4j
public class RepositoryMethodInterceptor implements MethodInterceptor {
	private final PersistenceManager persistenceManager;
	private final RepositoryInfo repositoryInfo;

	@Override
	@SuppressWarnings("unchecked")
	public Object intercept(Object object, Method method, Object[] args, MethodProxy proxy) throws Throwable {
		final Class<?> type = method.getDeclaringClass();

		if (type.isAnnotationPresent(DataRepository.class)) {
			final RepositoryMethod repositoryMethod = repositoryInfo.getMethods()
					.stream()
					.filter(m -> m.getMethodName().equals(method.getName()))
					.findFirst()
					.orElse(null);
			final String bucketName = repositoryInfo.getBucketName();
			if (repositoryMethod != null) {
				if (method.isAnnotationPresent(NotThreadSafe.class)) {
					final NotThreadSafe threadSafe = method.getAnnotation(NotThreadSafe.class);
					if (!threadSafe.ignoreWarn()) {
						log.warn("Use not thread safe implementation of persistable object. Method {}:{}. Use OperationHandler for lock operation.", type.getSimpleName(), method.getName());
					}
				}

				final Map<String, Object> variables =
						repositoryMethod.getAnnotatedVars()
								.stream()
								.collect(Collectors
										.toMap(Pair::getKey,
												v ->
														Arrays
																.stream(args)
																.filter(arg ->
																		v.getValue().isAssignableFrom(arg.getClass()))
																.findFirst()
																.orElse(Optional.empty())));

				final StandardEvaluationContext context = new StandardEvaluationContext();
				context.setVariables(variables);

				final Expression expression = repositoryMethod.getExpression();
				final String key = expression != null ? String.valueOf(expression.getValue(context)) : "";

				Object value = null;
				final Parameter annotatedType = Arrays
						.stream(method.getParameters())
						.filter(t ->
								t.isAnnotationPresent(DataValue.class))
						.findFirst()
						.orElse(null);
				if (annotatedType != null) {
					value = Arrays
							.stream(args)
							.filter(Objects::nonNull)
							.filter(arg ->
									annotatedType.getType().isAssignableFrom(arg.getClass()))
							.findFirst()
							.orElse(null);
				}

				final Type operationType = repositoryMethod.getOperationType();
				final boolean enableLock = repositoryMethod.isEnableLock();

				Object result;
				final int length = args.length;
				if (length > 0 && args[length - 1] instanceof OperationHandler) {
					final OperationHandler<Serializable> operationHandler = (OperationHandler<Serializable>) args[length - 1];
					final RMap<String, Serializable> map = persistenceManager.getMap(bucketName);
					final RLock fairLock = map.getFairLock(key);
					fairLock.lockAsync();
					try {
						if (operationType == READ || operationType == READ_ALL) {
							result = callOperation(operationType, map, false, bucketName, key, (Serializable) value);
							operationHandler.invoke((Serializable) result);
						} else {
							if (value == null) {
								value = operationHandler.invoke(null);
							} else {
								operationHandler.invoke((Serializable) value);
							}

							if (operationType == WRITE && value == null) {
								log.error("Error store object, repository {} - type does marked with DataValue annotation or not implement Serializable interface", type.getSimpleName());
								return null;
							}

							result = callOperation(operationType, map, false, bucketName, key, (Serializable) value);
						}
					} finally {
						fairLock.unlockAsync();
					}
				} else {
					if (operationType == WRITE && (value == null || !Serializable.class.isAssignableFrom(value.getClass()))) {
						log.error("Error store object, repository {} - type does marked with DataValue annotation or not implement Serializable interface", type.getSimpleName());
						return null;
					}

					result = callOperation(operationType, null, enableLock, bucketName, key, (Serializable) value);
					if (log.isDebugEnabled()) {
						log.debug("Called operation by type {}, bucket {}, key {}, value {}", operationType, bucketName, key, value);
					}
				}
				return result;
			}
		}

		return proxy.invokeSuper(object, args);
	}

	private Object callOperation(Type opType, @Nullable RMap<String, Serializable> map, boolean enableLock, String bucketName, String key, Serializable value) {
		switch (opType) {
			case GET_KEYS:
				return persistenceManager.getKeys(enableLock, map, bucketName);
			case READ:
				return persistenceManager.getData(enableLock, map, bucketName, key);
			case READ_ALL:
				return persistenceManager.getAllData(enableLock, map, bucketName);
			case WRITE:
				persistenceManager.addData(enableLock, map, bucketName, key, value);
				return value;
			case REMOVE:
				persistenceManager.clearData(enableLock, map, bucketName, key);
				return value;
			case REMOVE_ALL:
				persistenceManager.clearAllData(enableLock, map, bucketName);
			default:
				return value;
		}
	}
}
